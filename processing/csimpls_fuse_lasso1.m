function [result,xcentr,ycentr]=csimpls_lasso(x,y,A_fuse,lasso_p,fuse_p,varargin)

addpath('simpls');
%CSIMPLS performs Partial Least Squares regression using the SIMPLS 
% algorithm of de Jong (1993).
%
% Reference: 
%    de Jong, S. (1993),
%    "SIMPLS: an alternative approach to Partial Least Squares Regression", 
%    Chemometrics and Intelligent Laboratory Systems, 18, 251-263.
%
% Required input arguments:
%            x : Data matrix of the explanatory variables
%                (n observations in rows, p variables in columns)
%            y : Data matrix of the response variables
%                (n observations in rows, q variables in columns)
%     
% Optional input arguments:
%            k : Number of components to be used.  
%                (default = min([9,rank([x,y]),floor(n/2),p])). 
%        plots : If equal to one, a menu is shown which allows to draw several plots,
%                such as a score outlier map and a regression outlier map. (default)
%                If 'plots' is equal to zero, all plots are suppressed.
%                See also makeplot.m
%
% I/O: result=csimpls(x,y,'k',10);
%
% The output of CSIMPLS is a structure containing:
%
%   result.slope   : Classical slope estimate
%   result.int     : Classical intercept estimate
%   result.fitted  : Classical prediction vector
%   result.res     : Classical residuals
%   result.cov     : Estimated variance-covariance matrix of the residuals 
%   result.T            : Classical scores
%   result.weights.r : Classical simpls weights
%   result.weights.p : Classical simpls weights
%   result.k       : Number of components used in the regression
%   result.sd    : Classical score distances
%   result.od    : Classical orthogonal distances
%   result.rd    : Residual distances (when there are several response variables).
%                    If univariate regression is performed, it contains the standardized residuals.
%   result.cutoff  : Cutoff values for the score, orthogonal and residual distances.
%   result.flag    : The observations whose orthogonal distance is larger than result.cutoff.od
%                    and/or whose residual distance is larger than result.cutoff.rd
%                    receive a flag equal to zero. The other observations receive a flag 1.
%   result.class   : 'SIMPLS'
%
% This function is part of LIBRA: the Matlab library for Robust Analysis,
% available at: 
%              http://wis.kuleuven.be/stat/robust.html
%
% Written by Karlien Vanden Branden
% Last Update: 05/04/2003 

A_fuse=sparse(A_fuse);


[n,p1]=size(x);
[n2,q1]=size(y);
if n~=n2
    error('The response variables and the predictor variables have a different number of observations.')
end
kmin=min([9,rank(x),floor(n/2),p1]);
default=struct('plots',1,'k',kmin);
list=fieldnames(default);
options=default;
IN=length(list);
i=1;   
counter=1;
if nargin>2
    %
    % placing inputfields in array of strings
    %
%    for j=1:nargin-2
    for j=1:nargin-6
        if rem(j,2)~=0
            chklist{i}=varargin{j};
            i=i+1;
        end
    end 
    % Checking which default parameters have to be changed
    % and keep them in the structure 'options'.
    while counter<=IN 
        index=strmatch(list(counter,:),chklist,'exact');
        if ~isempty(index) % in case of similarity
 %           for j=1:nargin-2 % searching the index of the accompanying field
            for j=1:nargin-6 % searching the index of the accompanying field
                if rem(j,2)~=0 % fieldnames are placed on odd index
                    if strcmp(chklist{index},varargin{j})
                        I=j;
                    end
                end
            end
            options=setfield(options,chklist{index},varargin{I+1});
            index=[];
        end
        counter=counter+1;
    end
end

[xcentr,cx]=mcenter(x);
[ycentr,cy]=mcenter(y);

%centered and scale
xcentr=zscore(x);
ycentr=zscore(y);


sigmaxy=xcentr'*ycentr;




for i=1:options.k
    sigmayx=sigmaxy';

%%%%%%%compute fuse and lasso from eigenvectors%%%%%%%%%%%%
[RR,LL]=eig(sigmaxy*sigmayx);
[LL,I]=greatsort(diag(LL));
rr=RR(:,I(1));
lasso=lasso_p*sum(abs(rr));
fuse=fuse_p*sum(abs(A_fuse*rr));

F     = -2*sigmaxy*sigmayx*1e-6;

%%%% x0 to (x0 x1 x2) x1 is Fn0 by 1, x2 is An by 1%%%%%%
Fn0=size(F,1);
An0=size(A_fuse,1);
Fn = 2*Fn0+An0;
F1=zeros(Fn,Fn);
F1(1:Fn0,1:Fn0)=F;
F=F1;

%%%%%%%%% real max %%%%%%%
realm=1e6;

%x_0   = zeros(Fn,1);
%x_0(1:Fn0)=1/sqrt(Fn0);
x_0=ones(Fn,1)/sqrt(Fn);

%%%%% (x0 x1 x2)>(-realm 0 0)%%%%%%%%
x_L=zeros(Fn,1);
x_L(1:Fn0)=-realm;

%%%%%%% -realm<=x0-x1<=0   0<=x0+x1<=realm  0<=sum(x1)<=lasso  %%%%%%%%%%%
Fn0_m=sparse(eye(Fn0,Fn0));
An0_m=sparse(Fn0,An0);
A1=sparse([Fn0_m -Fn0_m An0_m;Fn0_m Fn0_m An0_m;zeros(1,Fn0) ones(1,Fn0) zeros(1,An0)]);
b_L1=sparse([-realm*ones(Fn0,1);zeros(Fn0,1);0]);
b_U1=sparse([zeros(Fn0,1);realm*ones(Fn0,1);lasso]);

%%%%%%% -realm<=A*x0-x2<=0   0<=A*x0+x2<=realm  0<=sum(x2)<=lasso  %%%%%%%%%%%
An0_m1=sparse(eye(An0,An0));
A2=sparse([A_fuse An0_m' -An0_m1;A_fuse An0_m' An0_m1;zeros(1,Fn0) zeros(1,Fn0) ones(1,An0)]); 
b_L2=sparse([-realm*ones(An0,1);zeros(An0,1);0]);
b_U2=sparse([zeros(An0,1);realm*ones(An0,1);fuse]);
%%%%%%%%%%%%%%%
A=[A1;A2];
b_L=[b_L1;b_L2];
b_U=[b_U1;b_U2];

%%%%%%%%%%%  x0'x=1; ==>   (Kx)'(Kx)=1%%%%%%%%
%K=sparse(Fn,Fn);
%K(1:Fn0,1:Fn0)=eye(Fn0);
%K_Q=K'*K;
%K_Q=sparse(K_Q);

%qc(1).Q=K_Q;
%qc(1).a=zeros(Fn,1);
%qc(1).r_U=1;

%qc(2).Q=-K_Q;
%qc(2).a=zeros(Fn,1);
%qc(2).r_U=-1;


%x_0   = ww(:,i)/sqrt(sum(ww(:,i).^2));

%Prob = qpconAssign(F,[], [], [], Name, x_0, [], [], [],...
%Prob = miqqAssign(F,[], A,b_L, b_U, x_L,[],x_0,qc);
Prob=miqqAssign(F,[],A,b_L,b_U,x_L,[],x_0);
Result = tomRun('snopt', Prob, 1);
%        rr=Result.x_k;
        rr=Result.x_k;
        rr=rr(1:Fn0);


        F=F(1:Fn0,1:Fn0); 
        rr'*F*rr
        rr'*rr      
        sum(abs(rr))
        sum(abs(A_fuse*rr))
        lasso 
        fuse 
   
        rr1=rr/sqrt(rr'*rr);
        rr1'*rr1
        rr1'*F*rr1
        sum(abs(rr1))
        sum(abs(A_fuse*rr1))
         
 

        qq=sigmayx*rr; 
        qq=qq/norm(qq);


%    if q1>p1        
%        [RR,LL]=eig(sigmaxy*sigmayx);  
%        [LL,I]=greatsort(diag(LL));
%        rr=RR(:,I(1));
%        qq=sigmayx*rr; 
%        qq=qq/norm(qq);
%    else
%        if q1==1
%            qq = 1;
%            rr = sigmaxy;
%        else
%            [QQ,LL]=eig(sigmayx*sigmaxy);
%            [LL,I]=greatsort(diag(LL));
%            qq=QQ(:,I(1));
%            rr=sigmaxy*qq;
%        end
%    end
    tt=xcentr*rr;
    nttc=norm(tt);
    rr=rr/nttc;
    tt=tt/nttc;
    qq=ycentr'*tt;
    uu=ycentr*qq;
    pp=xcentr'*tt;
    vv=pp;
    if i>1 												
        vv = vv -v*(v'*pp);
    end
    vv = vv/norm(vv);
    sigmaxy = sigmaxy - vv*(vv'*sigmaxy);
    v(:,i)=vv;
    q(:,i)=qq;
    t(:,i)=tt;
    u(:,i)=uu;
    p(:,i)=pp;
    r(:,i)=rr;
end

%Second Stage : Classical Regression and transformation

b=r*q';
int=cy-cx*b;

%classical output
out.T=t;
out.weights.p=p;
out.weights.q=q;
out.weights.u=u;
out.weights.v=v;
out.weights.r=r;
out.coef=[b; int];
out.b=b;
out.int=int;
out.yhat=x*b+repmat(int,n,1);
out.res=y-out.yhat;
out.covar=cov(out.res);
if q1==1
    out.stdres=out.res./sqrt(out.covar);
end
out.k=options.k;
STTm=sum((y-repmat(mean(y),length(y),1)).^2);
SSE=sum(out.res.^2);
out.rsquare=1-SSE/STTm;
out.class='CSIMPLS';

%calculating classical distances in x-space
out.sd=sqrt(mahalanobis(t,zeros(1,out.k),'cov',cov(t)))';
out.cutoff.sd=sqrt(chi2inv(0.975,out.k));

%calculating classical orthogonal distances
xt=t*p';
tempo=xcentr-xt;
for i=1:n
    out.od(i,1)=norm(tempo(i,:));
end
r=rank(x);
if out.k~=r
    m=mean(out.od.^(2/3));
    s=sqrt(var(out.od.^(2/3)));
    out.cutoff.od = sqrt(norminv(0.975,m,s)^3); 
else
    out.cutoff.od=0;
end

%calculating residual distances
if q1>1
    if (-log(det(out.covar)/(p1+q1-1)))>50
        out.rd='singularity';
    else
       cen=zeros(q1,1);
       out.rd=sqrt(mahalanobis(out.res,cen','cov',out.covar))';
   end
else    %here q==1
    out.rd=out.stdres; %standardized residuals
end
out.cutoff.resd=sqrt(chi2inv(0.975,q1));

%Computing flags
out.flag=((out.od<=out.cutoff.od)&(abs(out.rd)<=out.cutoff.resd));

result=struct('slope',{out.b}, 'int',{out.int},'fitted',{out.yhat},'res',{out.res}, 'cov',{out.covar},...
    'T',{out.T} ,'weights', {out.weights},'k',{out.k},'sd', {out.sd},'od',{out.od},'resd',{out.rd},...
    'cutoff',{out.cutoff},'flag',{out.flag},'class',{out.class});

%try
%    if options.plots
%        makeplot(result)
%    end
%catch %output must be given even if plots are interrupted 
end






