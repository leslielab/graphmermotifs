%regular PLS with filtering motifs,6 and 7 mers

clear all;
N=5;
addpath('simpls');
dir='../../data/hill/output1';

output_dir='../../experiments/';
output_file=['PLS_fuse2_hill_mot=qt75_gene=qt25_kmer67_l4f2_1_one_random_model.mat'];

input_file='/cexp_mot=qt75gene=qt25_one_random_model.mat';
load([dir,input_file]);


fold=floor(size(cexp_real,1)/10);    

for j=1:10

j
load([dir,input_file]);

holdout_gene=fold*(j-1)+1:fold*j;
tr_gene=setdiff(1:size(cexp_real,1),holdout_gene);

mot_tr=mot(tr_gene,:);
mot_tst=mot(holdout_gene,:);
cexp_tr=cexp_real(tr_gene,:);
cexp_tst=cexp_real(holdout_gene,:);

idx=find(sum(mot_tr,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

idx=find(sum(mot_tst,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

idx=find(sum(mot_tst,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

motifs=motifs1;

m=mean(mot_tr);
s=std(mot_tr);
un=ones(size(mot_tst,1),1);
X_tst=(mot_tst-(un*m))./(un*s);

m=mean(cexp_tr);
s=std(cexp_tr);
un=ones(size(cexp_tst,1),1);
Y_tst=(cexp_tst-(un*m))./(un*s);
clear m s un;

mot_ham=ones(length(motifs));
for i=1:length(motifs)
    for k=i+1:length(motifs)
       if(min(length(motifs{i}),length(motifs{k}))==4|max(length(motifs{i}),length(motifs{k}))==5)
             mot_ham(i,k)=1;
       else
             mot_ham(i,k)=hamming(motifs{i},motifs{k});
       end
    end
end

[i,j1]=find(mot_ham<=0.3);
mot_ham_inv=1./(mot_ham(find(mot_ham<=0.3))).^2;


A=sparse(length(i),size(mot_ham,1));
k=1:length(i);

ind1=sub2ind(size(A),k',i);
ind2=sub2ind(size(A),k',j1);
A(ind1)=1;
A(ind2)=-1;


%%%%%%%%%%%%%%%%%%PLS_simpls%%%%%%%%%%%%%%%%%%%%%%%%%%
[result,xcentr,ycentr]=csimpls(full(mot_tr),cexp_tr,'k',N);
Y=ycentr;
X=xcentr;
tr_s(j,1)=sum(sum(Y.^2))/size(Y,1);
tst_s(j,1)=sum(sum(Y_tst.^2))/size(Y_tst,1);
tst_s(j,1)=sum(sum(Y_tst.^2))/size(Y_tst,1);


for i=1:N
    B_pls=result.weights.r(:,1:i)*result.weights.q(:,1:i)'; 
    Y1=X*B_pls;
    tr_s(j,i+1)=sum(sum((Y1-Y).^2))/size(X,1);
    Y_tst1=X_tst*B_pls;
    tst_s(j,i+1)=sum(sum((Y_tst1-Y_tst).^2))/size(X_tst,1);
end


%%%%%%%%%%%%%%%%%%%%PLS_fuse_lasso%%%%

%lasso=4;
%fuse=3;
N1=10;

%
%for lasso=8:-2:2
% for fuse=8:-2:2

lasso=4;
fuse=2;
tr_l(lasso,fuse,j,1)=sum(sum(Y.^2))/size(Y,1);
tst_l(lasso,fuse,j,1)=sum(sum(Y_tst.^2))/size(Y_tst,1);

  [result,xcentr,ycentr]=csimpls_fuse_lasso2(full(mot_tr),cexp_tr,A, lasso*0.1,fuse*0.1,'k',N1);

   Y=ycentr;
   X=xcentr;

  for i=1:N1
    B_pls=result.weights.r(:,1:i)*result.weights.q(:,1:i)';

    Y1=X*B_pls;
    Y_tst1=X_tst*B_pls;
    tr_l(lasso,fuse,j,i+1)=sum(sum((Y-Y1).^2))/size(Y,1);
    tst_l(lasso,fuse,j,i+1)=sum(sum((Y_tst-Y_tst1).^2))/size(Y_tst,1);

  end

save([output_dir,output_file]);

end

%end
%end










