file='matfiles/fmin.mat';
N=300;
f=zeros(N,5);

%fuse 0.4;  beta distribution parameters
f_a=20;
f_b=30;

%lasso 0.3; beta distribution parameters
l_a=10;
l_b=23;

%mot_qt 0.25; beta distribution parameters
q_a=10;
q_b=30;

%ham_cut 0.3; beta distribution parameters
h_a=10;
h_b=23;


for i=1:N
   if(i~=1)
%generate r=[fuse lasso mot_qt ham_cut] from the 4 beta distributions   
    r=[betarnd(f_a,f_b) betarnd(l_a,l_b) betarnd(q_a,q_b) betarnd(h_a,h_b)]; 
   else
    r=[0.4 0.3 0.25 0.3]; 
   end
   min_chi=run_PLS_fuse2_lasso_filter_mot_bkseq_count_fun(r);
  
%save chi-square with r    
   f(i,:)=[min_chi r];
    
   save(file,'f');  
end 
