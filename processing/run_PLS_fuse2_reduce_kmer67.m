%regular PLS with filtering motifs,6 and 7 mers
%shuffle the sequence and estimate a empirical distribution of count; estimate a zscore for each motif 

clear all;
N=10;
addpath('simpls');
dir='../../data';

%motif counts in real sequence data
rfile='mot_Nhit_7mer.mat';
%motif counts in shuffled sequence data
sfile='rand_count_bkseq_7mer.mat';
qt=0.5;

output_dir='../../experiments/';


output_file=['PLS_filter_mot_count_bkseq_zscore_qt',num2str(100*qt),'_kmer67_reduce_1st_factor.mat'];

load([dir,'/targets.mat'],'holdout_genes');
load([dir,'/cexp.mat']);

idx=[];
for i=1:length(motifs_names)
   if(length(motifs_names{i})>=5&length(motifs_names{i})<=7)
    idx(end+1)=i;
   end
end


motifs=motifs_names(idx);


%filtering
remove_idx=[];
for i=1:length(motifs)
      if(length(strfind(motifs{i},'AAA'))>0|length(strfind(motifs{i},'CCC'))>0| length(strfind(motifs{i},'GGG'))>0|length(strfind(motifs{i},'TTT'))>0|sum(mot(:,i))<200|sum(mot(:,i))>5000)
      remove_idx(end+1)=i;
      end
end

idx=setdiff(1:length(motifs),remove_idx);
motifs=motifs(idx);
motifs=unique(motifs);
%%% replace binary matrix by count matrix
load([dir,'/',rfile]);
mot=mot_Nhit;

%%%%%%%% estimate a zscore for each motif %%%%%%%%%%%%%%%

mot_count=sum(mot_Nhit,1);


load([dir,'/',sfile]);
rand_count=rand_count(:,1:5);
zscore=(mot_count'-mean(rand_count,2))./sqrt(mean(rand_count,2));
%filter kmers
mot_idx=find(zscore>quantile(zscore,qt));
motifs=motifs(mot_idx);
mot_processed=mot(:,mot_idx);

%remove reverse complements
remove_idx=rm_rever_com(motifs);
motifs(remove_idx)=[];
mot_processed(:,remove_idx)=[];

%consider only 6-7 mers

remove_idx=[];
for i=1:length(motifs)
   if(length(motifs{i})==5)
    remove_idx(end+1)=i;
   end
end

motifs(remove_idx)=[];
mot_processed(:,remove_idx)=[];


for j=1:10

j
load([dir,'/cexp.mat']);

holdout_gene=holdout_genes{j};
tr_gene=setdiff(1:size(cexp_real,1),holdout_gene);

[m,n]=ismember(holdout_gene,sperm_idx);
sperm_tstidx=find(m);

[m,n]=ismember(tr_gene,sperm_idx);
sperm_tridx=find(m);

[m,n]=ismember(holdout_gene,oocyte_idx);
oocyte_tstidx=find(m);

[m,n]=ismember(tr_gene,oocyte_idx);
oocyte_tridx=find(m);

mot_tr=mot_processed(tr_gene,:);
mot_tst=mot_processed(holdout_gene,:);
cexp_tr=cexp_real(tr_gene,:);
cexp_tst=cexp_real(holdout_gene,:);

idx=find(sum(mot_tr,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

idx=find(sum(mot_tst,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

idx=find(sum(mot_tst,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

motifs=motifs1;

m=mean(mot_tr);
s=std(mot_tr);
un=ones(size(mot_tst,1),1);
X_tst=(mot_tst-(un*m))./(un*s);

m=mean(cexp_tr);
s=std(cexp_tr);
un=ones(size(cexp_tst,1),1);
Y_tst=(cexp_tst-(un*m))./(un*s);
clear m s un;

mot_ham=ones(length(motifs));
for i=1:length(motifs)
    for k=i+1:length(motifs)
       if(min(length(motifs{i}),length(motifs{k}))==4|max(length(motifs{i}),length(motifs{k}))==5)
             mot_ham(i,k)=1;
       else
             mot_ham(i,k)=hamming(motifs{i},motifs{k});
       end
    end
end

[i,j1]=find(mot_ham<=0.3);
mot_ham_inv=1./(mot_ham(find(mot_ham<=0.3))).^2;


A=sparse(length(i),size(mot_ham,1));
k=1:length(i);

ind1=sub2ind(size(A),k',i);
ind2=sub2ind(size(A),k',j1);
A(ind1)=1;
A(ind2)=-1;


%%%%%%%%%%%% PLS individual experiment %%%


N1=5;
%i factors
%e experiment

fuse=2;
lasso=4;

r_ind=zeros(length(motifs),size(cexp_tr,2));

  for i=1:N1  
 for e=1:size(cexp_tr,2) 
   fprintf('e=%d\n',e);

     [result,xcentr,ycentr]=csimpls_fuse_lasso2(full(mot_tr),cexp_tr(:,e),A,lasso*0.1,fuse*0.1,'k',i);
 
     Y(:,e)=ycentr;
     X=xcentr;

     i
     r_ind(:,e)=result.weights.r;
     B_pls=result.weights.r*result.weights.q';
    
     Y1_reduce(:,e)=X*B_pls;
     Y1_tst_reduce(:,e)=X_tst*B_pls;

%time-specific chi-square. (e is time point and i is factor index) 
    tr_l_reduce_ind(j,e,i)=sum(sum((Y1_reduce(:,e)-Y(:,e)).^2))/sum(sum(Y(:,e).^2));
    tst_l_reduce_ind(j,e,i)=sum(sum((Y1_tst_reduce(:,e)-Y_tst(:,e)).^2))/sum(Y_tst(:,e).^2);
  
    tr_l_reduce_ind_sperm(j,e,i)=sum((Y1_reduce(sperm_tridx,e)-Y(sperm_tridx,e)).^2)/sum(Y(sperm_tridx,e).^2);
    tst_l_reduce_ind_sperm(j,e,i)=sum((Y1_tst_reduce(sperm_tstidx,e)-Y_tst(sperm_tstidx,e)).^2)/sum(Y_tst(sperm_tstidx,e).^2);
    tr_l_reduce_ind_oocyte(j,e,i)=sum((Y1_reduce(oocyte_tridx,e)-Y(oocyte_tridx,e)).^2)/sum(Y(oocyte_tridx,e).^2);
    tst_l_reduce_ind_oocyte(j,e,i)=sum((Y1_tst_reduce(oocyte_tstidx,e)-Y_tst(oocyte_tstidx,e)).^2)/sum(Y_tst(oocyte_tstidx,e).^2);

    non_tstidx=setdiff(1:size(Y_tst,1),union(oocyte_tstidx,sperm_tstidx));
    tst_l_reduce_ind_non(j,e,i)=sum((Y1_tst_reduce(non_tstidx,e)-Y_tst(non_tstidx,e)).^2)/sum(Y_tst(non_tstidx,e).^2); 

  end

    tr_l_reduce(j,i)=sum(sum((Y1_reduce-Y).^2))/sum(sum(Y.^2));
    tst_l_reduce(j,i)=sum(sum((Y1_tst_reduce-Y_tst).^2))/sum(sum(Y_tst.^2));
     
    
end

  save([output_dir,output_file]);

end




















