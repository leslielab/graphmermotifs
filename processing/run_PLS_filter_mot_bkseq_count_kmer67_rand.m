%regular PLS with filtering motifs,6 and 7 mers
%shuffle the sequence and estimate a empirical distribution of count; estimate a zscore for each motif 

clear all;
N=10;
addpath('simpls');
dir='../../data';

%motif counts in real sequence data
rfile='mot_Nhit_7mer.mat';
%motif counts in shuffled sequence data
sfile='rand_count_bkseq_7mer.mat';
qt=0.5;

output_dir='../../experiments/';


output_file=['PLS_filter_mot_count_bkseq_zscore_qt',num2str(100*qt),'_kmer67.mat'];

load([dir,'/targets.mat'],'holdout_genes');
load([dir,'/cexp.mat']);

idx=[];
for i=1:length(motifs_names)
   if(length(motifs_names{i})>=5&length(motifs_names{i})<=7)
    idx(end+1)=i;
   end
end


motifs=motifs_names(idx);


%filtering
remove_idx=[];
for i=1:length(motifs)
      if(length(strfind(motifs{i},'AAA'))>0|length(strfind(motifs{i},'CCC'))>0| length(strfind(motifs{i},'GGG'))>0|length(strfind(motifs{i},'TTT'))>0|sum(mot(:,i))<200|sum(mot(:,i))>5000)
      remove_idx(end+1)=i;
      end
end

idx=setdiff(1:length(motifs),remove_idx);
motifs=motifs(idx);
motifs=unique(motifs);
%%% replace binary matrix by count matrix
load([dir,'/',rfile]);
mot=mot_Nhit;

%%%%%%%% estimate a zscore for each motif %%%%%%%%%%%%%%%

mot_count=sum(mot_Nhit,1);


load([dir,'/',sfile]);
rand_count=rand_count(:,1:5);
zscore=(mot_count'-mean(rand_count,2))./sqrt(mean(rand_count,2));
%filter kmers
mot_idx=find(zscore>quantile(zscore,qt));
motifs=motifs(mot_idx);
mot_processed=mot(:,mot_idx);

%remove reverse complements
remove_idx=rm_rever_com(motifs);
motifs(remove_idx)=[];
mot_processed(:,remove_idx)=[];

%consider only 6-7 mers

remove_idx=[];
for i=1:length(motifs)
   if(length(motifs{i})==5)
    remove_idx(end+1)=i;
   end
end

motifs(remove_idx)=[];
mot_processed(:,remove_idx)=[];

tst_s_rand=zeros(10,N+1);
tr_s_rand=zeros(10,N+1);

%randomize cexp_real
ridx=randperm(size(cexp_real,1));
cexp_real_rand=cexp_real(ridx,:);


for j=1:10

j
load([dir,'/cexp.mat']);

holdout_gene=holdout_genes{j};
tr_gene=setdiff(1:size(cexp_real,1),holdout_gene);

mot_tr=mot_processed(tr_gene,:);
mot_tst=mot_processed(holdout_gene,:);
cexp_tr=cexp_real_rand(tr_gene,:);
cexp_tst=cexp_real_rand(holdout_gene,:);

idx=find(sum(mot_tr,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

idx=find(sum(mot_tst,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

idx=find(sum(mot_tst,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

motifs=motifs1;

m=mean(mot_tr);
s=std(mot_tr);
un=ones(size(mot_tst,1),1);
X_tst=(mot_tst-(un*m))./(un*s);

m=mean(cexp_tr);
s=std(cexp_tr);
un=ones(size(cexp_tst,1),1);
Y_tst=(cexp_tst-(un*m))./(un*s);
clear m s un;

%%%%%%%%%%%%%%%%%%PLS_simpls%%%%%%%%%%%%%%%%%%%%%%%%%%
[result,xcentr,ycentr]=csimpls(full(mot_tr),cexp_tr,'k',N);
Y=ycentr;
X=xcentr;
tr_s_rand(j,1)=sum(sum(Y.^2))/size(Y,1);
tst_s_rand(j,1)=sum(sum(Y_tst.^2))/size(Y_tst,1);

for i=1:N
    B_pls=result.weights.r(:,1:i)*result.weights.q(:,1:i)'; 
 
    Y1=X*B_pls;
    tr_s_rand(j,i+1)=sum(sum((Y1-Y).^2))/size(X,1);
    Y_tst1=X_tst*B_pls;
    tst_s_rand(j,i+1)=sum(sum((Y_tst1-Y_tst).^2))/size(X_tst,1);

end

     save([output_dir,output_file],'-append','tr_s_rand','tst_s_rand');
end













