function [result,xcentr,ycentr]=csimpls_fuse_lasso2(x,y,A_fuse,lasso_p,fuse_p,varargin)

addpath('simpls');
%CSIMPLS_fuse_lasso2 performs Partial Least Squares regression with lasso and smoothness regularization based on the SIMPLS 
% algorithm of de Jong (1993).
%
% Reference: 
%    de Jong, S. (1993),
%    "SIMPLS: an alternative approach to Partial Least Squares Regression", 
%    Chemometrics and Intelligent Laboratory Systems, 18, 251-263.
%
% Required input arguments:
%            x : Data matrix of the explanatory variables
%                (n observations in rows, p variables in columns)
%            y : Data matrix of the response variables
%                (n observations in rows, q variables in columns)
%            lasso_p: lasso coefficient from 0 to 1
%            A_fuse:  matrix containing 1s and -1s to represent connected kmers
%            fuse_p:  fuse smoothness coefficient from 0 to 1
% Optional input arguments:
%            k : Number of components to be used.  
%                (default = min([9,rank([x,y]),floor(n/2),p])). 
%        plots : If equal to one, a menu is shown which allows to draw several plots,
%                such as a score outlier map and a regression outlier map. (default)
%                If 'plots' is equal to zero, all plots are suppressed.
%                See also makeplot.m
%
% I/O: result=csimpls(x,y, A, 0.3,0.4, 'k',10);
%
% The output of CSIMPLS is a structure containing:
%
%   result.slope   : Classical slope estimate
%   result.int     : Classical intercept estimate
%   result.fitted  : Classical prediction vector
%   result.res     : Classical residuals
%   result.cov     : Estimated variance-covariance matrix of the residuals 
%   result.T            : Classical scores
%   result.weights.r : Classical simpls weights
%   result.weights.p : Classical simpls weights
%   result.k       : Number of components used in the regression
%   result.sd    : Classical score distances
%   result.od    : Classical orthogonal distances
%   result.rd    : Residual distances (when there are several response variables).
%                    If univariate regression is performed, it contains the standardized residuals.
%   result.cutoff  : Cutoff values for the score, orthogonal and residual distances.
%   result.flag    : The observations whose orthogonal distance is larger than result.cutoff.od
%                    and/or whose residual distance is larger than result.cutoff.rd
%                    receive a flag equal to zero. The other observations receive a flag 1.
%   result.class   : 'SIMPLS'
%
% This function is part of LIBRA: the Matlab library for Robust Analysis,
% available at: 
%              http://wis.kuleuven.be/stat/robust.html
%
% Written by Karlien Vanden Branden
% Last Update: 05/04/2003 

% p1 is number of predictor variables; q1 number of response variables
[n,p1]=size(x);
[n2,q1]=size(y);
if n~=n2
    error('The response variables and the predictor variables have a different number of observations.')
end
kmin=min([9,rank(x),floor(n/2),p1]);
default=struct('plots',1,'k',kmin);
list=fieldnames(default);
options=default;
IN=length(list);
i=1;   
counter=1;
%  read in additional arguments 
if nargin>4
    %
    % placing inputfields in array of strings
    %
%    for j=1:nargin-2
    for j=1:nargin-6
        if rem(j,2)~=0
            chklist{i}=varargin{j};
            i=i+1;
        end
    end 
    % Checking which default parameters have to be changed
    % and keep them in the structure 'options'.
    while counter<=IN 
        index=strmatch(list(counter,:),chklist,'exact');
        if ~isempty(index) % in case of similarity
 %           for j=1:nargin-2 % searching the index of the accompanying field
            for j=1:nargin-6 % searching the index of the accompanying field
                if rem(j,2)~=0 % fieldnames are placed on odd index
                    if strcmp(chklist{index},varargin{j})
                        I=j;
                    end
                end
            end
            options=setfield(options,chklist{index},varargin{I+1});
            index=[];
        end
        counter=counter+1;
    end
end


%column center X and Y (from the original csimpls.m)
[xcentr,cx]=mcenter(x);
[ycentr,cy]=mcenter(y);

%column center and scale X and Y (code added)
xcentr=zscore(x);
ycentr=zscore(y);


sigmaxy=xcentr'*ycentr;
for i=1:options.k
    sigmayx=sigmaxy';


% estimate fuse and lasso value
        [RR,LL]=eig(sigmaxy*sigmayx);  
        [LL,I]=greatsort(diag(LL));
        rr0=RR(:,I(1));

        lasso=lasso_p*sum(abs(rr0));
        fuse=fuse_p*(A_fuse*rr0)'*(A_fuse*rr0);


%Original Optimization Problem: max(rr'*sigmaxy*sigmaxy'*rr)
%subject to constraints: sum(|rr|) <= lasso, (A_fuse*rr)'(A_fuse*rr)<=fuse, t'*(x*rr)=0 (orthogonality) and rr'*rr=1

% To avoid  abs in the lasso constraint, convert rr into two other variables:  rr=z1-z2  |rr|=z1+z2  z1>=0  z2>=0

% The new optimization problem for rr_z=[z1;z2]  
%  max(rr'*sigmaxy*sigmaxy'*rr) -->
%  max(z1'*sigmaxy*sigmaxy'*z1+z2'*sigmaxy*sigmaxy'*z2-2*z1'*sigmaxy*sigmaxy'*z2)-> 
%%%%%%%%%%%%%%% min(0.5*rr_z'*[F -F;-F F]*rr_z)  where F=-2*sigmaxy*sigmaxy'   
  
% New constraints:
%%%%%%%% (1)rr_yz>=0  (in the optimization x_L=0)
%%%%%%%% (2)sum(|rr|)=sum(rr_z)<=lasso -> ones(1,2*p1)*rr_z<=lasso ( in the optimization Al=ones(1,2*p1) b_Ul=lasso b_Ll=0)
%%%%%%%% (3) t'*(x*rr)=t'*x*[I;-I]*rr_z=0  (in the optimization At=t'*x*[I;-I] b_Ut=0 b_Lt=0)
%%%%%%%% combine (2) and (3): A=[At;Al]  b_U=[b_Ut;b_Ul] b_L=[b_Lt;b_Ll]      b_L<=A*rr_z<=b_U 
%%%%%%%% (4) A1=A_fuse'*A_fuse; rr'*A1*rr<=fuse => rr_z'*[A1 -A1;-A1 A1]*rr_z<=fuse (in the optimization qc(3).Q=[A1 -A1;-A1 A1] and qc(3).rU=fuse )
%%%%%%%% (5) rr'*rr=(z1-z2)'(z1-z2)=rr_z'*[I -I;-I I]*rr_z=1  equivalent to rr_z'*[I -I;-I I]*rr_z<=1 and -rr_z'*[I -I;-I I]*rr_z<=-1  (in the optimization qc(1).Q=[I -I;-I I] and qc(1).rU=1,  qc(2).Q=-[I -I;-I I] and qc(2).rU=-1)  


% min(0.5*rr_z'*F*rr_z); 0.5 factor since miqqAssign minimzes 1/2*x'Fx
F     = -2*sigmaxy*sigmayx*1e-6;
F =[F -F;-F F];
% Initialize rr_z
x_0   = ones(2*p1,1)/sqrt(2*p1);

% rr_z>=0
x_L=zeros(2*p1,1);


% 0<=sum(rr_z) <= lasso ; 0<=t'*x*[I;-I]*rr_z<=0 orthogonality
Al=ones(1,2*p1);


%no orthogonality constraint
%if(i>1)
% At=t'*xcentr*[eye(p1,p1) -eye(p1,p1)];
% A=[At;Al];
% b_U=[zeros(i-1,1);lasso];
% b_L=zeros(i,1);
% if the first factor, no orthogonality constraint
%else
 A=Al;
 b_U=lasso;
 b_L=[];
%end

% rr_z'*[I -I;-I I]*rr_z<=1 
qc(1).Q=[spdiags(ones(p1,1),0,p1,p1) spdiags(-ones(p1,1),0,p1,p1); spdiags(-ones(p1,1),0,p1,p1) spdiags(ones(p1,1),0,p1,p1)];
qc(1).a=zeros(2*p1,1);
qc(1).r_U=1;

% -rr_z'*[I -I;-I I]*rr_z<=-1 same as rr_z'*[I -I;-I I]*rr_z>=1  
qc(2).Q=-[spdiags(ones(p1,1),0,p1,p1) spdiags(-ones(p1,1),0,p1,p1); spdiags(-ones(p1,1),0,p1,p1) spdiags(ones(p1,1),0,p1,p1)];
qc(2).a=zeros(2*p1,1);
qc(2).r_U=1;

% rr_z'*[A1 -A1;-A1 A1]*rr_z<=fuse
A1=A_fuse'*A_fuse;
qc(3).Q=[A1 -A1; -A1 A1];
qc(3).a=zeros(2*p1,1);
qc(3).r_U=fuse;




Prob=miqqAssign(F,[],A,b_L,b_U,x_L,[],x_0,qc);
 
%clear global variables whose size is dependent on p1
%clear global QP_Qx, QP_Qxx;
Result = tomRun('snopt', Prob, 1);
        rr_z=Result.x_k;
        rr=rr_z(1:length(rr_z)/2)-rr_z(length(rr_z)/2+1:end);
        
%    if q1>p1        
%        [RR,LL]=eig(sigmaxy*sigmayx);  
%        [LL,I]=greatsort(diag(LL));
%        rr=RR(:,I(1));
%        qq=sigmayx*rr; 
%        qq=qq/norm(qq);
%    else
%        if q1==1
%            qq = 1;
%            rr = sigmaxy;
%        else
%            [QQ,LL]=eig(sigmayx*sigmaxy);
%            [LL,I]=greatsort(diag(LL));
%            qq=QQ(:,I(1));
%            rr=sigmaxy*qq;
%        end
%    end
  
% latent factor tt for X; tt normalized;  
    tt=xcentr*rr;
    nttc=norm(tt);
    rr=rr/nttc;
    tt=tt/nttc;
% weight vector qq for Y; latent factor uu for Y    
    qq=ycentr'*tt;
    uu=ycentr*qq;
    pp=xcentr'*tt;
    vv=pp;
    if i>1 									
        vv = vv -v*(v'*pp);
   end
    vv = vv/norm(vv);
% deflate sigmaxy
    sigmaxy = sigmaxy - vv*(vv'*sigmaxy);
% store latent factors and weight vectors in matrices
    v(:,i)=vv;
    q(:,i)=qq;
    t(:,i)=tt;
    u(:,i)=uu;
    p(:,i)=pp;
    r(:,i)=rr;
end

%Second Stage : Classical Regression and transformation

% regression matrix b 
b=r*q';
% intercept int
int=cy-cx*b;

%classical output
out.T=t;
out.weights.p=p;
out.weights.q=q;
out.weights.u=u;
out.weights.v=v;
out.weights.r=r;
out.coef=[b; int];
out.b=b;
out.int=int;
% map x to y
out.yhat=x*b+repmat(int,n,1);
out.res=y-out.yhat;
out.covar=cov(out.res);
if q1==1
    out.stdres=out.res./sqrt(out.covar);
end
out.k=options.k;
STTm=sum((y-repmat(mean(y),length(y),1)).^2);
SSE=sum(out.res.^2);
out.rsquare=1-SSE/STTm;
out.class='CSIMPLS';

%calculating classical distances in x-space
out.sd=sqrt(mahalanobis(t,zeros(1,out.k),'cov',cov(t)))';
out.cutoff.sd=sqrt(chi2inv(0.975,out.k));

%calculating classical orthogonal distances
xt=t*p';
tempo=xcentr-xt;
for i=1:n
    out.od(i,1)=norm(tempo(i,:));
end
r=rank(x);
if out.k~=r
    m=mean(out.od.^(2/3));
    s=sqrt(var(out.od.^(2/3)));
    out.cutoff.od = sqrt(norminv(0.975,m,s)^3); 
else
    out.cutoff.od=0;
end

%calculating residual distances
if q1>1
    if (-log(det(out.covar)/(p1+q1-1)))>50
        out.rd='singularity';
    else
       cen=zeros(q1,1);
       out.rd=sqrt(mahalanobis(out.res,cen','cov',out.covar))';
   end
else    %here q==1
    out.rd=out.stdres; %standardized residuals
end
out.cutoff.resd=sqrt(chi2inv(0.975,q1));

%Computing flags
out.flag=((out.od<=out.cutoff.od)&(abs(out.rd)<=out.cutoff.resd));

result=struct('slope',{out.b}, 'int',{out.int},'fitted',{out.yhat},'res',{out.res}, 'cov',{out.covar},...
    'T',{out.T} ,'weights', {out.weights},'k',{out.k},'sd', {out.sd},'od',{out.od},'resd',{out.rd},...
    'cutoff',{out.cutoff},'flag',{out.flag},'class',{out.class});

%try
%    if options.plots
%        makeplot(result)
%    end
%catch %output must be given even if plots are interrupted 
end






