%regular PLS with filtering motifs
%shuffle the sequence and estimate a empirical distribution of count; estimate a zscore for each motif 

clear all;
N=10;
addpath('../simpls');
addpath('scan_motif');
dir='../medusa';

load([dir,'/cexp.mat']);

idx=[];
for i=1:length(motifs_names)
   if(length(motifs_names{i})>=5&length(motifs_names{i})<=6)
    idx(end+1)=i;
   end
end

mot=mot(:,idx);
motifs=motifs_names(idx);

%only first 2592 motifs are effective among the total 6552
mot=mot(:,1:2592);
length(unique(motifs))
motifs=motifs(1:2592);
length(unique(motifs))

remove_idx=[];
for i=1:length(motifs)
      if(length(strfind(motifs{i},'AAA'))>0|length(strfind(motifs{i},'CCC'))>0| length(strfind(motifs{i},'GGG'))>0|length(strfind(motifs{i},'TTT'))>0|sum(mot(:,i))<50|sum(mot(:,i))>5000)
      remove_idx(end+1)=i;
      end
end

idx=setdiff(1:length(motifs),remove_idx);
mot=mot(:,idx);
motifs=motifs(idx);

%%% replace binary matrix by count matrix
load([dir,'/mot_Nhit.mat']);
mot=mot_Nhit;
length(unique(motifs))

Nc=size(cexp_real,1);
Nidx=randperm(Nc);
Nf=floor(Nc/10);


%%%%%%%% estimate a zscore for each motif %%%%%%%%%%%%%%%

load([dir,'/seqs.mat']);
%seq=a; %concatenate all promoter sequences 
a=a';
seq=a(find(a<5)); %concatenate all promoter sequences 
N_sh=100; %times of shuffling the sequence
mot_count=sum(mot_Nhit,1);
load matfiles/rand_count.mat;
rand_count=mean(rand_count(:,1:7),2);
zscore=(mot_count'-rand_count)./sqrt(rand_count);


mot_idx=find(abs(zscore)>10);
motifs=motifs(mot_idx);
mot=mot(:,mot_idx);
%rand_count=zscore_mot(motifs,seq,N_sh);
%count=zscore_mot_test(motifs,seq,N_sh);

tst_s=zeros(10,N+1);
tr_s=zeros(10,N+1);


for j=1:10

tstidx=Nidx((j-1)*Nf+1:j*Nf);tridx=setdiff(Nidx,tstidx);

mot_tr=mot(tridx,:);
mot_tst=mot(tstidx,:);
cexp_tr=cexp_real(tridx,:);
cexp_tst=cexp_real(tstidx,:);

idx=find(sum(mot_tr,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

idx=find(sum(mot_tst,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs=motifs(idx);


m=mean(mot_tr);
s=std(mot_tr);
un=ones(size(mot_tst,1),1);
X_tst=(mot_tst-(un*m))./(un*s);

m=mean(cexp_tr);
s=std(cexp_tr);
un=ones(size(cexp_tst,1),1);
Y_tst=(cexp_tst-(un*m))./(un*s);


%%%%%%%%%%%%%%%%%%PLS_simpls%%%%%%%%%%%%%%%%%%%%%%%%%%

[result,xcentr,ycentr]=csimpls(full(mot_tr),cexp_tr,'k',N);
Y=ycentr;
X=xcentr;
tr_s(j,1)=sum(sum(Y.^2))/size(Y,1);
%sperm_tr_s(j,1)=sum(sum(Y(sperm_tridx,:).^2))/length(sperm_tridx);
%oocyte_tr_s(j,1)=sum(sum(Y(oocyte_tridx,:).^2))/length(oocyte_tridx);
tst_s(j,1)=sum(sum(Y_tst.^2))/size(Y_tst,1);
%sperm_tst_s(j,1)=sum(sum(Y_tst(sperm_tstidx,:).^2))/length(sperm_tstidx);
%oocyte_tst_s(j,1)=sum(sum(Y_tst(oocyte_tstidx,:).^2))/length(oocyte_tstidx);

for i=1:N
    i
%    b_s(i)=result.T(:,i)'*result.weights.u(:,i)/norm(result.weights.u(:,i));
   
    B_pls=result.weights.r(:,1:i)*result.weights.q(:,1:i)'; 

 
    Y1=X*B_pls;
    tr_s(j,i+1)=sum(sum((Y1-Y).^2))/size(X,1);
  %  sperm_tr_s(j,i+1)=sum(sum((Y1(sperm_tridx,:)-Y(sperm_tridx,:)).^2))/length(sperm_tridx);
  %  oocyte_tr_s(j,i+1)=sum(sum((Y1(oocyte_tridx,:)-Y(oocyte_tridx,:)).^2))/length(oocyte_tridx);
    

    Y_tst1=X_tst*B_pls;
    tst_s(j,i+1)=sum(sum((Y_tst1-Y_tst).^2))/size(X_tst,1);
    
 %   sperm_tst_s(j,i+1)=sum(sum((Y_tst1(sperm_tstidx,:)-Y_tst(sperm_tstidx,:)).^2))/length(sperm_tstidx);
 %   oocyte_tst_s(j,i+1)=sum(sum((Y_tst1(oocyte_tstidx,:)-Y_tst(oocyte_tstidx,:)).^2))/length(oocyte_tstidx);

end
%%%%%%%%%%%simpls_lasso%%%%%%%%%%


%save matfiles/PLS_filter_mot_count.mat;
save PLS_filter_mot_count_zscore_abs10.mat;
end





