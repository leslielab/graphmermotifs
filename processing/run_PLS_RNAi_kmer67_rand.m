%regular PLS with filtering motifs,6 and 7 mers

clear all;
N=10;
fold=800;
addpath('simpls');
dir='../../data/RNAi';

output_dir='../../experiments/';
output_file=['PLS_RNAi_kmer67.mat'];

load([dir,'/cexp.mat']);
    
%randomize cexp_real
ridx=randperm(size(cexp_real,1));
cexp_real=cexp_real(ridx,:);


for j=1:10

j
load([dir,'/cexp.mat']);

holdout_gene=fold*(j-1)+1:fold*j;
tr_gene=setdiff(1:size(cexp_real,1),holdout_gene);

mot_tr=mot(tr_gene,:);
mot_tst=mot(holdout_gene,:);
cexp_tr=cexp_real(tr_gene,:);
cexp_tst=cexp_real(holdout_gene,:);

idx=find(sum(mot_tr,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

idx=find(sum(mot_tst,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

idx=find(sum(mot_tst,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

motifs=motifs1;

m=mean(mot_tr);
s=std(mot_tr);
un=ones(size(mot_tst,1),1);
X_tst=(mot_tst-(un*m))./(un*s);

m=mean(cexp_tr);
s=std(cexp_tr);
un=ones(size(cexp_tst,1),1);
Y_tst=(cexp_tst-(un*m))./(un*s);
clear m s un;

%%%%%%%%%%%%%%%%%%PLS_simpls%%%%%%%%%%%%%%%%%%%%%%%%%%
%
[result,xcentr,ycentr]=csimpls(full(mot_tr),cexp_tr,'k',N);
Y=ycentr;
X=xcentr;
tr_s_rand(j,1)=sum(sum(Y.^2))/size(Y,1);
tst_s_rand(j,1)=sum(sum(Y_tst.^2))/size(Y_tst,1);

for i=1:N
    B_pls=result.weights.r(:,1:i)*result.weights.q(:,1:i)'; 
    Y1=X*B_pls;
    tr_s_rand(j,i+1)=sum(sum((Y1-Y).^2))/size(X,1);
    Y_tst1=X_tst*B_pls;
    tst_s_rand(j,i+1)=sum(sum((Y_tst1-Y_tst).^2))/size(X_tst,1);
end


save([output_dir,output_file],'-append','tr_s_rand','tst_s_rand');
end










