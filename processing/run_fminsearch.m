%global file;
%[x,fval,exitflag,output]=fminsearch(@run_PLS_fuse2_lasso_filter_mot_bkseq_count_fun,[3 4 0.25 0.3],optimset('display','iter'));

%file='matfiles/output_fmin1.txt';
%[x,fval,exitflag,output]=fminsearch(@run_PLS_fuse2_lasso_filter_mot_bkseq_count_fun,[4 5 0.5 0.3],optimset('display','iter'));

%file='matfiles/output_fmin2.txt';
%[x,fval,exitflag,output]=fminsearch(@run_PLS_fuse2_lasso_filter_mot_bkseq_count_fun,[2 6 0.7 0.4],optimset('display','iter'));

%file='matfiles/output_fmin3.txt';
%[x,fval,exitflag,output]=fminsearch(@run_PLS_fuse2_lasso_filter_mot_bkseq_count_fun,[5 2 0.3 0.2],optimset('display','iter'));

%file='matfiles/output_fmin4.txt';
%[x,fval,exitflag,output]=fminsearch(@run_PLS_fuse2_lasso_filter_mot_bkseq_count_fun,[7 5 0.2 0.3],optimset('display','iter'));

file='matfiles/fmin.mat';
N=500;
f=zeros(N,5);
%fuse 0.4 lasso 0.3 mot_qt 0.25 ham_cut 0.3
f_a=20;
f_b=30;
l_a=10;
l_b=23;
q_a=10;
q_b=30;
h_a=10;
h_b=23;


for i=1:N
%   r=rand(1,4);
%
   if(i~=1)
    r=[betarnd(f_a,f_b) betarnd(l_a,l_b) betarnd(q_a,q_b) betarnd(h_a,h_b)]; 
   else
   r=[0.4 0.3 0.25 0.3]; 
   end
   min_chi=run_PLS_fuse2_lasso_filter_mot_bkseq_count_fun(r);
    f(i,:)=[min_chi r];
      save(file,'f');  
end 
