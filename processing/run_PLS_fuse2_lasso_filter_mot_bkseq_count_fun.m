function min_chi=fuse2_lasso_filter_mot(x)
global file;

fuse=x(1);
lasso=x(2);
mot_qt=x(3);
ham_cutoff=x(4);

%regular PLS with filtering motifs
%shuffle the sequence and estimate a empirical distribution of count; estimate a zscore for each motif 

N=5; 
addpath('../simpls');
dir='../medusa';

load([dir,'/targets.mat'],'holdout_genes');
load([dir,'/cexp.mat']);

idx=[];
for i=1:length(motifs_names)
   if(length(motifs_names{i})>=5&length(motifs_names{i})<=6)
    idx(end+1)=i;
   end
end

mot=mot(:,idx);
motifs=motifs_names(idx);

%only first 2592 motifs are effective among the total 6552
mot=mot(:,1:2592);
motifs=motifs(1:2592);

remove_idx=[];
for i=1:length(motifs)
      if(length(strfind(motifs{i},'AAA'))>0|length(strfind(motifs{i},'CCC'))>0| length(strfind(motifs{i},'GGG'))>0|length(strfind(motifs{i},'TTT'))>0|sum(mot(:,i))<50|sum(mot(:,i))>5000)
      remove_idx(end+1)=i;
      end
end

idx=setdiff(1:length(motifs),remove_idx);
mot=mot(:,idx);
motifs=motifs(idx);

%%% replace binary matrix by count matrix
load([dir,'/mot_Nhit.mat']);
mot=mot_Nhit;

Nc=size(cexp_real,1);
Nidx=randperm(Nc);
Nf=floor(Nc/10);


%%%%%%%% estimate a zscore for each motif %%%%%%%%%%%%%%%

mot_count=sum(mot_Nhit,1);


%rand_count=zscore_mot(motifs,seq,N_sh);
load ../medusa/rand_count_bkseq.mat;
%zscore=(mot_count-rand_count(:,1)')./sqrt(rand_count(:,1)'); 

rand_count=rand_count(:,1:3);
zscore=(mot_count'-mean(rand_count,2))./sqrt(mean(rand_count,2)); 

%determine number of motifs filtered
mot_idx=find(zscore>quantile(zscore,mot_qt));
motifs=motifs(mot_idx);
mot_processed=mot(:,mot_idx);
%rand_count=zscore_mot(motifs,seq,N_sh);
%count=zscore_mot_test(motifs,seq,N_sh);

tst_s=zeros(10,N+1);
tr_s=zeros(10,N+1);

for j=1:3

load([dir,'/cexp.mat']);

holdout_gene=holdout_genes{j};
tr_gene=setdiff(1:size(cexp_real,1),holdout_gene);


[m,n]=ismember(holdout_gene,sperm_idx);
sperm_tstidx=find(m);

[m,n]=ismember(tr_gene,sperm_idx);
sperm_tridx=find(m);

[m,n]=ismember(holdout_gene,oocyte_idx);
oocyte_tstidx=find(m);

[m,n]=ismember(tr_gene,oocyte_idx);
oocyte_tridx=find(m);

mot_tr=mot_processed(tr_gene,:);
mot_tst=mot_processed(holdout_gene,:);
cexp_tr=cexp_real(tr_gene,:);
cexp_tst=cexp_real(holdout_gene,:);

idx=find(sum(mot_tr,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

idx=find(sum(mot_tst,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

idx=find(sum(mot_tst,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

motifs=motifs1;

m=mean(mot_tr);
s=std(mot_tr);
un=ones(size(mot_tst,1),1);
X_tst=(mot_tst-(un*m))./(un*s);

m=mean(cexp_tr);
s=std(cexp_tr);
un=ones(size(cexp_tst,1),1);
Y_tst=(cexp_tst-(un*m))./(un*s);

mot_ham=ones(length(motifs));
for i=1:length(motifs)
    for k=i+1:length(motifs)
       if(min(length(motifs{i}),length(motifs{k}))==4)
%
             mot_ham(i,k)=1;
       else
%
             mot_ham(i,k)=hamming(motifs{i},motifs{k});
       end
    end
end
[i,j1]=find(mot_ham<=ham_cutoff);
A=zeros(length(i),size(mot_ham,1));
k=1:length(i);

ind1=sub2ind(size(A),k',i);
A(ind1)=1;
ind2=sub2ind(size(A),k',j1);
A(ind2)=-1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%fuse_lasso

[result,xcentr,ycentr]=csimpls_fuse_lasso2(full(mot_tr),cexp_tr,A,lasso,fuse,'k',N);
r=result.weights.r;
nr=sqrt(sum(r.^2,1));
r=r./repmat(nr,[size(r,1) 1]);



Y=ycentr;
X=xcentr;
tr_l(j,1)=sum(sum(Y.^2))/size(Y,1);
tst_l(j,1)=sum(sum(Y_tst.^2))/size(Y_tst,1);

for i=1:N

    B_pls=result.weights.r(:,1:i)*result.weights.q(:,1:i)';

    Y1=X*B_pls;
    tr_l(j,i+1)=sum(sum((Y1-Y).^2))/size(X,1);  

    Y_tst1=X_tst*B_pls;
    tst_l(j,i+1)=sum(sum((Y_tst1-Y_tst).^2))/size(X_tst,1);

end

end

tst=mean(tst_l,1);
tst=tst/tst(1);
min_chi=min(tst);


%fid=fopen('output_fmin.txt','a');
%fid=fopen(file,'a');
%fprintf(fid,'min chi-square= %6.5f  fuse=%6.5f lasso=%6.5f mot_qt=%6.5f ham_cut=%6.5f\n',min_chi,fuse*0.1,lasso*0.1,mot_qt,ham_cutoff);
%fclose(fid);
