%regular PLS with filtering motifs
%shuffle the sequence and estimate a empirical distribution of count; estimate a zscore for each motif 

clear all;
N=15;
addpath('../simpls');
addpath('scan_motif');
dir='../medusa';

load([dir,'/targets.mat'],'holdout_genes');
load([dir,'/cexp.mat']);

idx=[];
for i=1:length(motifs_names)
   if(length(motifs_names{i})>=5&length(motifs_names{i})<=6)
    idx(end+1)=i;
   end
end

mot=mot(:,idx);
motifs=motifs_names(idx);

%only first 2592 motifs are effective among the total 6552
mot=mot(:,1:2592);
length(unique(motifs))
motifs=motifs(1:2592);
length(unique(motifs))

remove_idx=[];
for i=1:length(motifs)
      if(length(strfind(motifs{i},'AAA'))>0|length(strfind(motifs{i},'CCC'))>0| length(strfind(motifs{i},'GGG'))>0|length(strfind(motifs{i},'TTT'))>0|sum(mot(:,i))<50|sum(mot(:,i))>5000)
      remove_idx(end+1)=i;
      end
end

idx=setdiff(1:length(motifs),remove_idx);
mot=mot(:,idx);
motifs=motifs(idx);

%%% replace binary matrix by count matrix
load([dir,'/mot_Nhit.mat']);
mot=mot_Nhit;
length(unique(motifs))

Nc=size(cexp_real,1);
Nidx=randperm(Nc);
Nf=floor(Nc/10);


%%%%%%%% estimate a zscore for each motif %%%%%%%%%%%%%%%

%bkseq=textread('../medusa/background_seq.txt','%s');
%a1=char(bkseq); %concatenate all promoter sequences 
%load([dir,'/seqs.mat']);
%Nl=sum(sum(a<5));
%a1=a1(1:Nl);

%seq=zeros(length(a1),1);
%seq(find(a1=='a'))=1;
%seq(find(a1=='c'))=2;
%seq(find(a1=='g'))=3;
%seq(find(a1=='t'))=4;


%N_sh=100; %times of shuffling the sequence
mot_count=sum(mot_Nhit,1);


%rand_count=zscore_mot(motifs,seq,N_sh);
%save motif counts in random sequences:
load rand_count_bkseq.mat;
%zscore=(mot_count-rand_count(:,1)')./sqrt(rand_count(:,1)'); 

rand_count=rand_count(:,1:3);
zscore=(mot_count'-mean(rand_count,2))./sqrt(mean(rand_count,2)); 


mot_idx=find(zscore>quantile(zscore,0.25));
motifs=motifs(mot_idx);
mot_processed=mot(:,mot_idx);
%rand_count=zscore_mot(motifs,seq,N_sh);
%count=zscore_mot_test(motifs,seq,N_sh);

tst_s=zeros(10,N+1);
tr_s=zeros(10,N+1);


for j=1:10

load([dir,'/cexp.mat']);
file=['fold',num2str(j),'.mat'];
load([dir,'/',file],'kdmers');

holdout_gene=holdout_genes{j};
tr_gene=setdiff(1:size(cexp_real,1),holdout_gene);

mot_tr=mot_processed(tr_gene,:);
mot_tst=mot_processed(holdout_gene,:);
cexp_tr=cexp_real(tr_gene,:);
cexp_tst=cexp_real(holdout_gene,:);

idx=find(sum(mot_tr,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

idx=find(sum(mot_tst,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

idx=find(sum(mot_tst,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

motifs=motifs1;

m=mean(mot_tr);
s=std(mot_tr);
un=ones(size(mot_tst,1),1);
X_tst=(mot_tst-(un*m))./(un*s);

m=mean(cexp_tr);
s=std(cexp_tr);
un=ones(size(cexp_tst,1),1);
Y_tst=(cexp_tst-(un*m))./(un*s);
clear m s un;

%%%%%%%%%%%%%%%%%%PLS_simpls%%%%%%%%%%%%%%%%%%%%%%%%%%
[result,xcentr,ycentr]=csimpls(full(mot_tr),cexp_tr,'k',N);
Y=ycentr;
X=xcentr;
tr_s(j,1)=sum(sum(Y.^2))/size(Y,1);
tst_s(j,1)=sum(sum(Y_tst.^2))/size(Y_tst,1);

for i=1:N
    B_pls=result.weights.r(:,1:i)*result.weights.q(:,1:i)'; 
 
    Y1=X*B_pls;
    tr_s(j,i+1)=sum(sum((Y1-Y).^2))/size(X,1);
    Y_tst1=X_tst*B_pls;
    tst_s(j,i+1)=sum(sum((Y_tst1-Y_tst).^2))/size(X_tst,1);

end
%%%%%%%%%%%%%%%PLS_simpls by time point%%%%%%%%%%%%%%%
tr_t(j,1)=sum(sum(Y.^2))/size(Y,1);
tst_t(j,1)=sum(sum(Y_tst.^2))/size(Y_tst,1);
Nt=size(cexp_tr,2);

for i=1:N
   i
   for t=1:Nt
    t
    [result,xcentr,ycentr]=csimpls(full(mot_tr),cexp_tr(:,t),'k',i);
    B_pls=result.weights.r*result.weights.q'; 
 
    Y1(:,t)=X*B_pls;
    Y_tst1(:,t)=X_tst*B_pls;

  end
    tr_t(j,i+1)=sum(sum((Y1-Y).^2))/size(X,1);
    tst_t(j,i+1)=sum(sum((Y_tst1-Y_tst).^2))/size(X_tst,1);
  save PLS_filter_mot_count_bkseq_zscore_qt25.mat;
end

end





