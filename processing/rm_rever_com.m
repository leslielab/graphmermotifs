function remove_idx=rm_rever_com(motifs)
%remove motifs if the reverse complements are in previous motifs

Nm=length(motifs);
remove_idx=[];

for i=2:Nm
  motif_rc=rever_com(motifs{i});
%if the reverse complement of current ith motif is in the first (i-1)th motif  
%remove the ith motif
   if(sum(ismember(motifs(1:i-1),motif_rc)))
        remove_idx(end+1)=i;
    end
end
