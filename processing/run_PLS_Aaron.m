%regular PLS with filtering motifs,6 and 7 mers

clear all;
N=10;
fold=10;
addpath('simpls');
dir='../../data/';

output_dir='../../experiments/';
%
output_file=['PLS_Aaron.mat'];
load([dir,'/Aaron.mat']);
    
mot=counts;
cexp=labels;


%%%%%%%%%%%%%%%%%%PLS_simpls%%%%%%%%%%%%%%%%%%%%%%%%%%
%
[result,xcentr,ycentr]=csimpls(full(mot),cexp,'k',N);
Y=ycentr;
X=xcentr;
tr_s(1)=sum(sum(Y.^2))/size(Y,1);

for i=1:N
    B_pls=result.weights.r(:,1:i)*result.weights.q(:,1:i)'; 
    Y1=X*B_pls;
    tr_s(i+1)=sum(sum((Y1-Y).^2))/size(X,1);
end


save([output_dir,output_file]);










