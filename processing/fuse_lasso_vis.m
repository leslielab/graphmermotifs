load medusa_kmer_PLS_fuse2_lasso_count_bp.mat tst_l;
tst_l=squeeze(tst_l(2:2:8,2:2:8,10,:))/tst_l(2,2,10,1);

tst_l0=min(tst_l,[],3);

clf;
imagesc(tst_l0,[0.85 1]);
%axis([0. 0.8 0. 0.8]);
colorbar;
