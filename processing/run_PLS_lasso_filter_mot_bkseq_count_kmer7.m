%regular PLS with filtering motifs
%shuffle the sequence and estimate a empirical distribution of count; estimate a zscore for each motif 

clear all;
N=10;
addpath('../simpls');
dir='../medusa';
rfile='mot_Nhit_7mer.mat';
sfile='rand_count_bkseq_7mer.mat';
qt=0.5;

load([dir,'/targets.mat'],'holdout_genes');
load([dir,'/cexp.mat']);

idx=[];
for i=1:length(motifs_names)
   if(length(motifs_names{i})>=5&length(motifs_names{i})<=7)
    idx(end+1)=i;
   end
end

motifs=motifs_names(idx);


%filtering
remove_idx=[];
for i=1:length(motifs)
      if(length(strfind(motifs{i},'AAA'))>0|length(strfind(motifs{i},'CCC'))>0| length(strfind(motifs{i},'GGG'))>0|length(strfind(motifs{i},'TTT'))>0|sum(mot(:,i))<200|sum(mot(:,i))>5000)
      remove_idx(end+1)=i;
      end
end

idx=setdiff(1:length(motifs),remove_idx);
motifs=motifs(idx);
motifs=unique(motifs);
%%% replace binary matrix by count matrix
load([dir,'/',rfile]);
mot=mot_Nhit;


%%%%%%%% estimate a zscore for each motif %%%%%%%%%%%%%%%

mot_count=sum(mot_Nhit,1);


load([dir,'/',sfile]);
rand_count=rand_count(:,1:5);
zscore=(mot_count'-mean(rand_count,2))./sqrt(mean(rand_count,2)); 

%filter kmers
mot_idx=find(zscore>quantile(zscore,qt));
motifs=motifs(mot_idx);
mot_processed=mot(:,mot_idx);

tst_s=zeros(10,N+1);
tr_s=zeros(10,N+1);


for j=1:10

load([dir,'/cexp.mat']);

holdout_gene=holdout_genes{j};
tr_gene=setdiff(1:size(cexp_real,1),holdout_gene);

[m,n]=ismember(holdout_gene,sperm_idx);
sperm_tstidx=find(m);

[m,n]=ismember(tr_gene,sperm_idx);
sperm_tridx=find(m);

[m,n]=ismember(holdout_gene,oocyte_idx);
oocyte_tstidx=find(m);

[m,n]=ismember(tr_gene,oocyte_idx);
oocyte_tridx=find(m);


mot_tr=mot_processed(tr_gene,:);
mot_tst=mot_processed(holdout_gene,:);
cexp_tr=cexp_real(tr_gene,:);
cexp_tst=cexp_real(holdout_gene,:);

idx=find(sum(mot_tr,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

idx=find(sum(mot_tst,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

idx=find(sum(mot_tst,1)~=0);
mot_tr=mot_tr(:,idx);
mot_tst=mot_tst(:,idx);
motifs1=motifs(idx);

motifs=motifs1;

m=mean(mot_tr);
s=std(mot_tr);
un=ones(size(mot_tst,1),1);
X_tst=(mot_tst-(un*m))./(un*s);

m=mean(cexp_tr);
s=std(cexp_tr);
un=ones(size(cexp_tst,1),1);
Y_tst=(cexp_tst-(un*m))./(un*s);
clear m s un;




%%%%%%%%%%%%%%%%%%PLS_simpls%%%%%%%%%%%%%%%%%%%%%%%%%%
[result,xcentr,ycentr]=csimpls(full(mot_tr),cexp_tr,'k',N);
Y=ycentr;
X=xcentr;
tr_s(j,1)=sum(sum(Y.^2))/size(Y,1);
sperm_tr_s(j,1)=sum(sum(Y(sperm_tridx,:).^2))/length(sperm_tridx);
oocyte_tr_s(j,1)=sum(sum(Y(oocyte_tridx,:).^2))/length(oocyte_tridx);
tst_s(j,1)=sum(sum(Y_tst.^2))/size(Y_tst,1);
sperm_tst_s(j,1)=sum(sum(Y_tst(sperm_tstidx,:).^2))/length(sperm_tstidx);
oocyte_tst_s(j,1)=sum(sum(Y_tst(oocyte_tstidx,:).^2))/length(oocyte_tstidx);

for i=1:N
    i
   
    B_pls=result.weights.r(:,1:i)*result.weights.q(:,1:i)'; 

 
    Y1=X*B_pls;
    tr_s(j,i+1)=sum(sum((Y1-Y).^2))/size(X,1);
    sperm_tr_s(j,i+1)=sum(sum((Y1(sperm_tridx,:)-Y(sperm_tridx,:)).^2))/length(sperm_tridx);
    oocyte_tr_s(j,i+1)=sum(sum((Y1(oocyte_tridx,:)-Y(oocyte_tridx,:)).^2))/length(oocyte_tridx);
    

    Y_tst1=X_tst*B_pls;
    tst_s(j,i+1)=sum(sum((Y_tst1-Y_tst).^2))/size(X_tst,1);
    
    sperm_tst_s(j,i+1)=sum(sum((Y_tst1(sperm_tstidx,:)-Y_tst(sperm_tstidx,:)).^2))/length(sperm_tstidx);
     oocyte_tst_s(j,i+1)=sum(sum((Y_tst1(oocyte_tstidx,:)-Y_tst(oocyte_tstidx,:)).^2))/length(oocyte_tstidx);

end
%%%%%%%%%%%simpls_lasso%%%%%%%%%%
ww=zeros(9,length(motifs),N);
r_sparse=zeros(9,N);

for lasso=1:9

[result,xcentr,ycentr]=csimpls_lasso(full(mot_tr),cexp_tr,lasso*0.1,'k',N);
r=result.weights.r;
nr=sqrt(sum(r.^2,1));
r=r./repmat(nr,[size(r,1) 1]);
ww(lasso,:,:)=r;
r_sparse(lasso,:)=sum(abs(r)>1e-4,1);

disp(['lasso ',int2str(lasso)]);
Y=ycentr;
X=xcentr;
tr_l(lasso,j,1)=sum(sum(Y.^2))/size(Y,1);
sperm_tr_l(lasso,j,1)=sum(sum(Y(sperm_tridx,:).^2))/length(sperm_tridx);
oocyte_tr_l(lasso,j,1)=sum(sum(Y(oocyte_tridx,:).^2))/length(oocyte_tridx);
tst_l(lasso,j,1)=sum(sum(Y_tst.^2))/size(Y_tst,1);
sperm_tst_l(lasso,j,1)=sum(sum(Y_tst(sperm_tstidx,:).^2))/length(sperm_tstidx);
oocyte_tst_s(j,1)=sum(sum(Y_tst(oocyte_tstidx,:).^2))/length(oocyte_tstidx);

for i=1:N
    i
    B_pls=result.weights.r(:,1:i)*result.weights.q(:,1:i)';

    Y1=X*B_pls;
    tr_l(lasso,j,i+1)=sum(sum((Y1-Y).^2))/size(X,1);
    sperm_tr_l(lasso,j,i+1)=sum(sum((Y1(sperm_tridx,:)-Y(sperm_tridx,:)).^2))/length(sperm_tridx);
 Y_tst1=X_tst*B_pls;
    tst_l(lasso,j,i+1)=sum(sum((Y_tst1-Y_tst).^2))/size(X_tst,1);
    sperm_tst_l(lasso,j,i+1)=sum(sum((Y_tst1(sperm_tstidx,:)-Y_tst(sperm_tstidx,:)).^2))/length(sperm_tstidx);

    oocyte_tst_l(lasso,j,i+1)=sum(sum((Y_tst1(oocyte_tstidx,:)-Y_tst(oocyte_tstidx,:)).^2))/length(oocyte_tstidx);

end
end
  save(['matfiles/PLS_lasso_filter_mot_count_bkseq_zscore_qt',num2str(100*qt),'_kmer7.mat']);
end





