%extract the dominant factor for genes

no_sperm=setdiff(1:size(xcentr,1),sperm_tridx);

%w_x_corr=corr(r,xcentr(no_sperm,:)');
%w_x_sperm_corr=corr(r,xcentr(sperm_tridx,:)');

%[a,b]=max(abs(w_x_sperm_corr),[],1);
T=result.T;
%U=result.weights.u;
U=ycentr*result.weights.q;
w=result.weights.r;
c=result.weights.q;
flip=mean(w,1);
flip_idx=find(flip<0);
T(:,flip_idx)=-T(:,flip_idx);
w(:,flip_idx)=-w(:,flip_idx);
c(:,flip_idx)=-c(:,flip_idx);

%xw=xcentr*xcentr';
%xw_T=corr(xw,T);

%corr1(:,2:5)=-corr1(:,2:5);

%sort by T values. b is the dominant factor
[a,b]=max(T,[],2);
genes=cell(1,15);

for i=1:15
   genes{i}=targets_names(tr_gene(find(b==i)));
end

%subplot(2,1,1);
%hist(w_x_corr,50);
%axis([min([w_x_corr w_x_sperm_corr])  max([w_x_corr w_x_sperm_corr]) 0 800]);
%subplot(2,1,2);
%hist(w_x_sperm_corr,50);
%axis([min([w_x_corr w_x_sperm_corr])  max([w_x_corr w_x_sperm_corr]) 0 80]);

figure(1);
subplot(2,1,1);
hist(b(sperm_tridx),15);
axis([1 15 0 500]);
title('Distribution of most significant factors for sperm genes');
subplot(2,1,2);
hist(b(oocyte_tridx),15);
axis([1 15 0 500]);
title('Distribution of most significant factors for oocyte genes');
%hist(w_x_sperm_corr,50);
%axis([min([w_x_corr w_x_sperm_corr])  max([w_x_corr w_x_sperm_corr]) 0 80]);

%sort by U values
[a,b]=max(U,[],2);
figure(2);
subplot(2,1,1);
hist(b(sperm_tridx),15);
axis([1 15 0 500]);
title('Distribution of most significant factors for sperm genes');
subplot(2,1,2);
hist(b(oocyte_tridx),15);
axis([1 15 0 500]);
title('Distribution of most significant factors for oocyte genes');
%hist(w_x_sperm_corr,50);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
X=xcentr;
Y=ycentr;

tr=zeros(size(Y,1),size(c,2));

tr0=sum(Y.^2,2);


for i=1:size(c,2)
    Y1=xcentr*w(:,1:i)*c(:,1:i)';
 
    tr(:,i)=sum((Y1-Y).^2,2);
end

tr=tr./repmat(tr0,[1 size(c,2)]);
tr=[ones(size(Y,1),1) tr];

tr_dif=tr(:,1:end-1)-tr(:,2:end);
%% c is the factor giving the biggest chi-square reduction
[a,b]=max(tr_dif,[],2);

figure(3);
subplot(2,1,1);
hist(b(sperm_tridx),15);
axis([1 15 0 500]);
title('Distribution of most significant factors for sperm genes');
subplot(2,1,2);
hist(b(oocyte_tridx),15);
axis([1 15 0 500]);
title('Distribution of most significant factors for oocyte genes');



c_cexp=corr(c,ycentr');
%[max_corr,max_factor]=max(corr_f,[],1);
%sort by correlation between gene expression and c
[a,b]=max(c_cexp,[],1);
figure(4);
subplot(2,1,1);
hist(b(sperm_tridx),15);
title('Distribution of most significant factors for sperm genes');
axis([1 15 0 800]);
subplot(2,1,2);
hist(b(oocyte_tridx),15);
title('Distribution of most significant factors for oocyte genes');
axis([1 15 0 800]);


