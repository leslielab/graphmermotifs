%plot distribution of distance of ACGTG to coding region

load ../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67.mat;

addpath('../../../../');

factor=2;
r=result.weights.r(:,factor);
c=result.weights.q(:,factor);


%correlation with c_factor
%
cexp_corr=corr(c,cexp_real');
%load mot_enrich.mat cexp_corr;
anti_gene=find(cexp_corr<-0.6);


%extract motifs
[ig,idx]=sort(r);

N=1471;
mot=mot_processed;
%mot=motifs(idx(end-N:end));
N=50;
%higly negative
motifs1=motifs(idx(1:N));
%sorted weights;
w=ig;

%get the length of each promoter sequence
dir='medusa/';
pbg=textread([dir,'pbg.txt']);
dimers_maxgap=10;
Nhits=zeros(1,size(cexp_real,1));

M=size(cexp_real,1);
%for i=1:length(mot)
 for i=1:length(motifs)
       mot_idx=i;
      X=sum(mot(anti_gene,mot_idx));
      K=sum(mot(:,mot_idx));
      p(i)=1-hygecdf(full(X),M,full(K),length(anti_gene));
%        motif=mot{i};
%        pssm=[motif=='A';motif=='C';motif=='G';motif=='T'];
%        pssm=pssm+eps*ones(size(pssm));
%       pthr=sum(max(log(pssm./(pbg'*ones(1,size(pssm,2)))),[],1))-1;
%       mot_groupidx=0;
%       Nhit=scan_pssm(dir,mot_groupidx,pssm,pbg,pthr,dimers_maxgap);  
%       Nhits=Nhits+sum(Nhit,1);  

end


