
clear all;
%get top gene list for the 3 motifs in factor1
load('../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat','cexp_real','targets_names');;
seqs=textread('input/seq.txt','%s');



PC_idx=1;
file_factor='output/factor1_seq.fasta';


[coeff,score]=princomp(cexp_real);
%1st factor is flat,reverse the 2nd factor
corr1=corr(-coeff(:,PC_idx+1),cexp_real');

%sort gene by correlation with factor 3 (the 4th column in coeff)
factor_idx=find(corr1>0.9);
genes_factor=targets_names(factor_idx);
seqs_factor=seqs(factor_idx);


%print gene/seq file for alignace input
fid=fopen(file_factor,'w');
for i=1:length(genes_factor)
   fprintf(fid,'>%s\n',genes_factor{i});
   fprintf(fid,'%s\n',seqs_factor{i});
end
fclose(fid);
  










