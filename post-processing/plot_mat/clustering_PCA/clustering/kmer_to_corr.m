load ../matfiles/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat cexp_real targets_names sperm_idx sperm_tridx oocyte_idx oocyte_tridx result xcentr ycentr cexp_tr mot_tr motifs tridx;

T=result.T;
U=ycentr*result.weights.q;
w=result.weights.r;
c=result.weights.q;

X=xcentr;
Y=ycentr;
tr=zeros(size(Y,1),size(c,2));
tr0=sum(Y.^2,2);


for i=1:size(c,2)
    Y1=xcentr*w(:,1:i)*c(:,1:i)';
 
    tr(:,i)=sum((Y1-Y).^2,2);
end

tr=tr./repmat(tr0,[1 size(c,2)]);
tr=[ones(size(Y,1),1) tr];

tr_dif=tr(:,1:end-1)-tr(:,2:end);
%tr_var=sum(cexp_tr.^2,2);

%positive chi-square reduction
%c_corr=tr_dif;
%c_corr(find(tr_dif<0))=0;

%correlation with c
c_corr=corr(cexp_tr',c);
%dif_cutoff=2*sum(c_corr,2)./sum(c_corr>0,2);
%idx_cutoff=intersect(find(max(tr_dif,[],2)>dif_cutoff),find(sum(tr_dif,2)>0));
%idx_cutoff=find(max(c_corr,[],2)>0.8);

%% c is the factor giving the biggest chi-square reduction
%[a,idx_max]=max(c_corr,[],2);

%u=(c_corr>0.7);

for j=1:size(c_corr,1)
%      u1=find(u(j,:));
%       [su,su_idx]=sort(c_corr(j,:),'descend'); 
%       su_idx=su_idx(1:3);
       
%       u1_idx=find(c_corr(j,su_idx)>0.8);
 

%     if(length(u1_idx)>0)
%          idx_max(j)=min(su_idx(u1_idx));
%      else
%          idx_max(j)=0;
%      end

%study relationship between c_corr and k-mer hit
     w_j=w(:,j);
     [w_js,w_idx]=sort(w_j,'descend');
     w_idx=w_idx(1:50);
     mot_hit=sum(mot_tr(:,w_idx),2);

     gene_corr_idx=find(c_corr(:,j)>0.7);

%contain at least 10 k-mer hits
     cutoff_mot=10;
     cutoff_mot=quantile(mot_hit,0.9);
     %cutoff_mot=quantile(T(:,j),0.9);
     gene_mot_idx=find(mot_hit>cutoff_mot);
     gene_no_mot_idx=find(mot_hit<cutoff_mot);

     cexp_mot=cexp_tr(gene_mot_idx,:);
     figure(7);
     imagesc(cexp_mot,[-2 2]);
     colorbar('location','southoutside');

     figure(8);
     subplot(2,1,1);
     hist(c_corr(gene_no_mot_idx,j),50);
     title('all genes');
     subplot(2,1,2);
     hist(c_corr(gene_mot_idx,j),50);
     title('selected genes');
     gene_idx=intersect(gene_corr_idx,gene_mot_idx);

     oocyte_genes=intersect(gene_idx,oocyte_tridx);
     length(gene_idx)
     length(oocyte_genes)
     sperm_genes=intersect(gene_idx,sperm_tridx);
     length(sperm_genes)
     
    %divide all genes into two groups: first one enriched with k-mers and second one not. make cdfplot for correlation between gene expression in each group and factor 
    figure(6);
     clf
     %few motifs
     F1=cdfplot(c_corr(gene_no_mot_idx,j));
     hold on;
     %more motifs
     F2=cdfplot(c_corr(gene_mot_idx,j));
     set(F1,'Color','r');
     set(F2,'Color','b');
%     legend('Genes containing less than 10 hits of top 50 k-mers','Genes containing more than 10 hits of top 50 k-mers');
     h_legend=legend('Genes containing k-mer hits < 10','Genes containing k-mer hits > 10','Location','Northwest');
     h_ylabel=ylabel('accumulative distribution function');
     h_xlabel=xlabel(char(['correlation between gene expression and c',num2str(j)])); 
     h_title=title(char(['Empirical CDF for correlation between gene expression and c',num2str(j)])); 
%     pause;
    
     grid off;
  
  set(h_legend,'FontSize',14);
  set(h_title,'FontSize',14);
  set(h_xlabel,'FontSize',14); 
  set(h_ylabel,'FontSize',14); 
  set(gcf,'defaultuicontrolfontsize',16);
     switch j
       case 1 
           exportfig(gcf,'figs/corr_kmer_factor1.eps','color','rgb');
%          print -r1000 -djpeg figs/corr_kmer_factor1.jpg;
       case 2
           exportfig(gcf,'figs/corr_kmer_factor2.eps','color','rgb');
%          print -r1000 -djpeg figs/corr_kmer_factor2.jpg;
       case 3
           exportfig(gcf,'figs/corr_kmer_factor3.eps','color','rgb');
%          print -r1000 -djpeg figs/corr_kmer_factor3.jpg;
       case 4
           exportfig(gcf,'figs/corr_kmer_factor4.eps','color','rgb');
%          print -r1000 -djpeg figs/corr_kmer_factor4.jpg;
     end
end

%[a,idx_max]=max(T,[],2);

for i=1:10
   % idx_i=intersect(find(idx_max==i),idx_cutoff);
%    [st,idx_i]=sort(T(:,i),'descend');
%    idx_i=find(idx_max==i);
   % idx_i=intersect(find(c_corr(:,i)>0.7),find(tr_dif(:,i)>0.1));
    idx_i=find(c_corr(:,i)>0.8);

     [swi,idx_w]=sort(w(:,i),'descend'); 
%           wi=zeros(size(idx_w));
%           wi(idx_w(1:20))=w(idx_w(1:20),i);
%         Ti=mot_tr*wi;
%     tr_dif=sum(mot_tr(:,idx_w(1:10)),2);

    sperm_genes=intersect(idx_i,sperm_tridx);
    non_sperm=setdiff(idx_i,sperm_genes);
    oocyte_genes=intersect(idx_i,oocyte_tridx);
    non_oocyte=setdiff(idx_i,oocyte_genes);

M=size(mot_tr,1);
for j=1:length(swi)
          mot_idx=idx_w(j);
          Xo=sum(mot_tr(oocyte_genes,mot_idx));
          Xno=sum(mot_tr(non_oocyte,mot_idx));
          K=sum(mot_tr(:,mot_idx));
          p_o(j)=1-hygecdf(full(Xo),M,full(K),length(oocyte_genes)); 
          p_no(j)=1-hygecdf(full(Xno),M,full(K),length(non_oocyte)); 
end


   if(i==1)
   figure(4);
     clf;
     plot(log10(p_o(1:50)+eps),log10(p_no(1:50)+eps),'r.');
     xlabel('log10(p-val)  oocyte genes ');
     ylabel('log10(p-val)  non-oocyte genes ');
     title('Enrichment of top k-mers (w1) in genes highly correlated with c1');
     grid off;
     %exportfig(gcf,'figs/corr_kmer_factor1.eps','color','rgb');
    end
   
   if(i==2)
   figure(4);
     clf;
     plot(log10(p_o(1:50)+eps),log10(p_no(1:50)+eps),'r.');
     xlabel('log10(p-val)  sperm genes ');
     ylabel('log10(p-val)  non-sperm genes ');
     title('Enrichment of top k-mers (w2) in genes highly correlated with c2');
     grid off;
     %exportfig(gcf,'figs/corr_kmer_factor1.eps','color','rgb');
    end
    %figure(1);
   % clf;
    %figure(1);
   % clf;
   % subplot(2,1,1);
   % tr_dif_sperm=tr_dif(sperm_genes,j);
   % hist(tr_dif_sperm,50);
   % subplot(2,1,2);
   % tr_dif_non_sperm=tr_dif(non_sperm,j);
   % hist(tr_dif_non_sperm,50);
    
   % figure(2);
   % clf;
   % subplot(2,1,1);
   % tr_dif_oocyte=tr_dif(oocyte_genes,i);
   % hist(tr_dif_oocyte,50);
   % subplot(2,1,2);
   % tr_dif_non_oocyte=tr_dif(non_oocyte,i);
   % hist(tr_dif_non_oocyte,50);
    
    fprintf('factor %d; %d genes, %d sperm genes and %d oocyte genes\n',i,length(idx_i),length(sperm_genes),length(oocyte_genes));
   %pause;
end



%figure(3);
%subplot(2,1,1);
%hist(b(sperm_tridx),10);
%axis([1 10 0 500]);
%title('Distribution of most significant factors for sperm genes');
%subplot(2,1,2);
%hist(b(oocyte_tridx),10);
%axis([1 10 0 500]);
%title('Distribution of most significant factors for oocyte genes');

%sort by T values. b is the dominant factor
%[a,b]=max(T,[],2);
%genes=cell(1,10);

%for i=1:10
%   genes{i}=targets_names(tr_gene(find(b==i)));
%end


%figure(1);
%subplot(2,1,1);
%hist(b(sperm_tridx),10);
%axis([1 10 0 500]);
%title('Distribution of most significant factors for sperm genes');
%subplot(2,1,2);
%hist(b(oocyte_tridx),10);
%axis([1 10 0 500]);
%title('Distribution of most significant factors for oocyte genes');







  



