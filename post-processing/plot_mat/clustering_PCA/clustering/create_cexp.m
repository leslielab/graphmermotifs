load ../matfiles/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat cexp_real targets_names 

file='cexp.txt';
fid=fopen(file,'w');

for i=1:length(targets_names)
    fprintf(fid,'\n%s',targets_names{i});
    for j=1:12
    fprintf(fid,'\t%f',cexp_real(i,j));
    end
end 

fclose(fid);
