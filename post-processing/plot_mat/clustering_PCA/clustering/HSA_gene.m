load ../matfiles/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat cexp_real targets_names sperm_idx oocyte_idx result  mot mot_processed tr_gene;

N_max=15;
c=result.weights.q;
w=result.weights.r;

cexp_real(67,:)=0.1:0.1:1.2;
%T=clusterdata(cexp_real,'distance','corr','maxclust',N_max,'linkage','average');

Z=pdist(cexp_real,'corr');
Y=linkage(Z,'average');
%T=cluster(Y,'cutoff',0.3,'criterion','distance');
T=cluster(Y,'maxclust',N_max);


%T4=clusterdata(cexp_real(find(T==4),:),'distance','correlation','maxclust',10);
%for i=1:10
% sum(T4==i)
%end
  
%fid=fopen('test.txt','w');


%N_max=max(T)



for i=1:N_max
    idx=find(T==i);
    sperm_genes=intersect(idx,sperm_idx);

  %   c_corr=corr(cexp_real',c(:,1)); 
  %   oocyte_idx=find(c_corr>0.8);
    oocyte_genes=intersect(idx,oocyte_idx);
    non_oocyte=setdiff(idx,oocyte_genes);
  %  fprintf(fid,'cluster %d; %d genes, %d sperm genes and %d oocyte genes\n',i,length(idx),length(sperm_genes),length(oocyte_genes));
    fprintf('cluster %d; %d genes, %d sperm genes and %d oocyte genes\n',i,length(idx),length(sperm_genes),length(oocyte_genes));
   
%consider the oocyte gene cluster
if(length(oocyte_genes)>500)

[swi,idx_w]=sort(w(:,1),'descend'); 

M=size(mot_processed,1);
for j=1:length(swi)
          mot_idx=idx_w(j);
          Xo=sum(mot_processed(oocyte_genes,mot_idx));
          Xno=sum(mot_processed(non_oocyte,mot_idx));
          K=sum(mot_processed(:,mot_idx));
          p_o(j)=1-hygecdf(full(Xo),M,full(K),length(oocyte_genes)); 
          p_no(j)=1-hygecdf(full(Xno),M,full(K),length(non_oocyte)); 
end

%   figure(3);
%   subplot(2,1,1);
%   plot(swi(1:100),log10(p_o(1:100)+eps),'r.'); 
%   subplot(2,1,2);
%   plot(swi(1:100),log10(p_no(1:100)+eps),'r.'); 

  
   figure(4);
   clf;
    plot(log10(p_o(1:50)+eps),log10(p_no(1:50)+eps),'r.');
     xlabel('log10(p-val)  oocyte genes ');
     ylabel('log10(p-val)  non-oocyte genes ');
     title('Enrichment of top k-mers (w1) in genes of the cluster');
   
end
%   pause; 
    figure(1);
    imagesc(cexp_real(idx,:));
    title('Genes in the cluster');
%      subplot(2,1,1);
%      hist(corr(cexp_real(idx,:)',c(:,1)),50); 
%      subplot(2,1,2);
%      hist(corr(cexp_real(idx,:)',c(:,2)),50); %
   figure(2);
    imagesc(cexp_real(sperm_genes,:));
    title('Sperm genes in the cluster');
%    if(length(sperm_genes)>0)
%    hist(corr(cexp_real(sperm_genes,:)',c(:,2)),50); 
%    end
%
    figure(3);
    imagesc(cexp_real(oocyte_genes,:));
    title('Oocyte genes in the cluster');
%    if(length(oocyte_genes)>0)
%    hist(corr(cexp_real(oocyte_genes,:)',c(:,1)),50); 
%    end


%    cexp4=cexp_real(idx,:);
%    [k1,k2]=ismember(idx,oocyte_genes);
%    oocyte_genes1=find(k1);
      
%    T4=clusterdata(cexp4,'distance','correlation','maxclust',10,'linkage','average');
%   for j=1:10
%    idx=find(T4==j);
%    oocyte_genes2=intersect(idx,oocyte_genes1);
%    fprintf('cluster %d; %d genes,  %d oocyte genes\n',j,length(idx),length(oocyte_genes2));
%    end

 pause;
end

%fclose(fid);
   %plot(swi,log10(p_no+eps),'r.'); 





