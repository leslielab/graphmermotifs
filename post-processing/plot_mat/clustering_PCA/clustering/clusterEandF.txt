Cluster	Gene	Name
ClusterE	3R5.1	
ClusterE	AH6.5 	mex-6
ClusterE	B0019.2	
ClusterE	B0024.10	
ClusterE	B0024.13	
ClusterE	B0035.11	
ClusterE	B0035.3	
ClusterE	B0041.5	
ClusterE	B0207.6	
ClusterE	B0212.3	
ClusterE	B0238.10	
ClusterE	B0238.11	
ClusterE	B0238.9	
ClusterE	B0244.9	
ClusterE	B0273.2 	puf-7
ClusterE	B0280.10	
ClusterE	B0281.5	
ClusterE	B0285.5 	hse-5
ClusterE	B0303.9 	vps-33.1
ClusterE	B0304.2	
ClusterE	B0334.11 	ooc-3
ClusterE	B0334.4	
ClusterE	B0361.10	
ClusterE	B0361.2	
ClusterE	B0361.6	
ClusterE	B0361.7	
ClusterE	B0393.2 	rbg-3
ClusterE	B0393.3	
ClusterE	B0393.6	
ClusterE	B0414.3 	hil-5
ClusterE	B0414.5 	cpb-3
ClusterE	B0414.6 	glh-3
ClusterE	B0414.7 	mtk-1
ClusterE	B0414.8	
ClusterE	B0416.4	
ClusterE	B0432.10	
ClusterE	B0432.8	
ClusterE	B0462.3 	fbxb-119
ClusterE	B0464.8 	tag-342
ClusterE	B0491.6	
ClusterE	B0511.7	
ClusterE	B0511.9 	cdc-26
ClusterE	B0523.3 	pgl-2
ClusterE	B0546.2	
ClusterE	B0564.11	
ClusterE	C01B10.9	
ClusterE	C01F1.6	
ClusterE	C01F6.1	
ClusterE	C01F6.3 	cyp-31A1
ClusterE	C01F6.4 	fem-3
ClusterE	C01G5.5	
ClusterE	C01G5.6	
ClusterE	C01G5.8	
ClusterE	C01G6.3	
ClusterE	C01G6.5	
ClusterE	C01G6.8 	cam-1
ClusterE	C01G8.1	
ClusterE	C01G8.3 	dhs-1
ClusterE	C01G8.4 	dnj-4
ClusterE	C01H6.7 	tag-298
ClusterE	C02B10.2	
ClusterE	C02F5.3	
ClusterE	C02F5.4 	cids-1
ClusterE	C02F5.7	
ClusterE	C03C10.3 	rnr-2
ClusterE	C03D6.5 	asfl-1
ClusterE	C03D6.6	
ClusterE	C03E10.4 	gly-20
ClusterE	C03E10.5 	clec-223
ClusterE	C04A2.3 	egl-27
ClusterE	C04B4.2	
ClusterE	C04F12.5	
ClusterE	C04F6.3 	cht-1
ClusterE	C05C10.5	
ClusterE	C05C8.5	
ClusterE	C05C8.6	
ClusterE	C05C8.9	
ClusterE	C05D11.3 	tag-170
ClusterE	C05D2.10	
ClusterE	C05E11.7	
ClusterE	C06A5.3	
ClusterE	C06A5.6	
ClusterE	C06A5.8	
ClusterE	C06A5.9 	rnf-1
ClusterE	C06A6.3 	mvb-12
ClusterE	C06A8.2	
ClusterE	C06A8.4 	skr-17
ClusterE	C06G4.1	
ClusterE	C07A12.5 	spr-3
ClusterE	C07D10.2 	bath-44
ClusterE	C07E3.6	
ClusterE	C07G1.3 	pct-1
ClusterE	C07G2.1 	cpg-1
ClusterE	C08B11.1 	zyg-11
ClusterE	C08B11.6	
ClusterE	C08B6.9 	aos-1
ClusterE	C08C3.2 	bath-15
ClusterE	C08F1.6	
ClusterE	C08F8.3	
ClusterE	C09G4.3 	cks-1
ClusterE	C09G9.2	
ClusterE	C09G9.6 	oma-1
ClusterE	C09H10.6 	nasp-1
ClusterE	C09H6.3 	mau-2
ClusterE	C10C6.5 	wht-2
ClusterE	C10H11.1 	viln-1
ClusterE	C12C8.3 	lin-41
ClusterE	C12D8.10 	akt-1
ClusterE	C13F10.2	
ClusterE	C13F10.6	
ClusterE	C13F10.7	
ClusterE	C13G5.2	
ClusterE	C14B1.2	
ClusterE	C14B1.4 	tag-125
ClusterE	C14B1.5	
ClusterE	C14B1.8	
ClusterE	C14B1.9	
ClusterE	C14B9.8	
ClusterE	C14C11.2	
ClusterE	C15C6.4	
ClusterE	C15C8.4	
ClusterE	C15H11.6 	nxf-2
ClusterE	C16A11.6 	fbxc-44
ClusterE	C16C10.3	
ClusterE	C16C10.6 	ccdc-55
ClusterE	C16C2.3 	ocrl-1
ClusterE	C16C2.4	
ClusterE	C16C8.11	
ClusterE	C16C8.13	
ClusterE	C16C8.16	
ClusterE	C16C8.4	
ClusterE	C16C8.5	
ClusterE	C17E4.10	
ClusterE	C17E4.2	
ClusterE	C17E4.3	
ClusterE	C17E4.4	
ClusterE	C17E7.13	
ClusterE	C17E7.4	
ClusterE	C17F4.5 	fbxc-50
ClusterE	C17G10.1	
ClusterE	C17G10.4 	cdc-14
ClusterE	C17H11.4	
ClusterE	C17H12.13	
ClusterE	C17H12.2	
ClusterE	C18A3.1	
ClusterE	C18E3.9	
ClusterE	C18G1.5 	hil-4
ClusterE	C24D10.5	
ClusterE	C24G6.1 	syp-2
ClusterE	C24G6.3	
ClusterE	C24H12.1	
ClusterE	C25A1.12	
ClusterE	C25A1.5	
ClusterE	C25A1.9 	rsa-1
ClusterE	C25A11.2	
ClusterE	C25B8.4 	clec-266
ClusterE	C26B2.6 	elpc-4
ClusterE	C26C6.1 	pbrm-1
ClusterE	C26E6.2 	flh-2
ClusterE	C26E6.5 	fsn-1
ClusterE	C26F1.3	
ClusterE	C27A12.2	
ClusterE	C27A12.3 	tag-146
ClusterE	C27A12.6	
ClusterE	C27A2.1	
ClusterE	C27A2.3 	ify-1
ClusterE	C27A2.6 	dsh-2
ClusterE	C27A7.6	
ClusterE	C27B7.4 	rad-26
ClusterE	C27C12.3	
ClusterE	C27D8.4	
ClusterE	C27D9.1	
ClusterE	C27F2.10	
ClusterE	C27H6.3	
ClusterE	C28C12.2	
ClusterE	C28D4.2 	cka-1
ClusterE	C28D4.3 	gln-6
ClusterE	C28H8.1	
ClusterE	C29A12.1	
ClusterE	C29A12.2	
ClusterE	C29A12.3 	lig-1
ClusterE	C29F7.5 	fkh-4
ClusterE	C29G2.1	
ClusterE	C29H12.1 	rrt-2
ClusterE	C30A5.3	
ClusterE	C30B5.1 	tag-319
ClusterE	C30F12.4	
ClusterE	C31H1.8	
ClusterE	C32B5.10 	fbxc-32
ClusterE	C32B5.16 	sdz-4
ClusterE	C32D5.11	
ClusterE	C32D5.5 	set-4
ClusterE	C32E8.5	
ClusterE	C32F10.2 	lin-35
ClusterE	C32F10.5 	hmg-3
ClusterE	C32F10.6 	nhr-2
ClusterE	C33F10.2 	tbck-1
ClusterE	C33F10.4	
ClusterE	C33H5.10 	tag-322
ClusterE	C33H5.15 	sgo-1
ClusterE	C33H5.6	
ClusterE	C33H5.8	
ClusterE	C33H5.9 	sec-10
ClusterE	C34B2.2 	kbp-5
ClusterE	C34B7.2	
ClusterE	C34C12.2	
ClusterE	C34D4.12 	cyn-12
ClusterE	C34G6.5	
ClusterE	C35D10.13	
ClusterE	C35D10.7	
ClusterE	C35E7.5	
ClusterE	C36B1.11	
ClusterE	C36B1.12 	imp-1
ClusterE	C36B1.3 	rpb-3
ClusterE	C36B1.7 	dhfr-1
ClusterE	C36C9.1	
ClusterE	C36C9.3 	fbxa-170
ClusterE	C37A2.4 	cye-1
ClusterE	C37A2.8	
ClusterE	C37C3.1	
ClusterE	C37C3.9	
ClusterE	C38D4.4	
ClusterE	C38D4.6 	pal-1
ClusterE	C39E9.11	
ClusterE	C40A11.6	
ClusterE	C41C4.4 	ire-1
ClusterE	C41C4.7 	ctns-1
ClusterE	C41D11.4	
ClusterE	C41D11.7	
ClusterE	C41D11.9	
ClusterE	C41G7.1 	smn-1
ClusterE	C41G7.3	
ClusterE	C41H7.8 	sdz-5
ClusterE	C42C1.8	
ClusterE	C43G2.1	
ClusterE	C44B7.12	
ClusterE	C44B9.3	
ClusterE	C44E4.3	
ClusterE	C44E4.3	
ClusterE	C45B11.1 	pak-2
ClusterE	C45G9.2	
ClusterE	C45H4.14	
ClusterE	C46A5.5	
ClusterE	C46A5.9 	hcf-1
ClusterE	C46F11.3	
ClusterE	C47B2.1 	fbxa-140
ClusterE	C47E12.2	
ClusterE	C47G2.4	
ClusterE	C48B4.11	
ClusterE	C48B6.3	
ClusterE	C48B6.9	
ClusterE	C49C3.6	
ClusterE	C49C3.7	
ClusterE	C49F5.3	
ClusterE	C49H3.4	
ClusterE	C49H3.6	
ClusterE	C49H3.9	
ClusterE	C50B6.2 	nasp-2
ClusterE	C50B6.3	
ClusterE	C50B8.2 	bir-2
ClusterE	C50C3.1	
ClusterE	C50C3.7	
ClusterE	C50C3.8 	bath-42
ClusterE	C50E3.13	
ClusterE	C50E3.15	
ClusterE	C50E3.5	
ClusterE	C52D10.7 	skr-9
ClusterE	C52D10.8 	skr-13
ClusterE	C52D10.9 	skr-8
ClusterE	C52E12.3 	sqv-7
ClusterE	C52E12.4	
ClusterE	C53A5.2	
ClusterE	C53D5.4 	ztf-3
ClusterE	C54G4.6 	dod-18
ClusterE	C54G4.9	
ClusterE	C55A6.12	
ClusterE	C55A6.2 	ttll-5
ClusterE	C55A6.9	
ClusterE	C55B6.1	
ClusterE	C55B7.11	
ClusterE	C55B7.5 	uri-1
ClusterE	C55B7.8 	dbr-1
ClusterE	C55B7.9 	mdt-18
ClusterE	C55C3.5	
ClusterE	C56A3.4	
ClusterE	C56C10.1 	vps-33.2
ClusterE	C56C10.10	
ClusterE	C56C10.13 	dnj-8
ClusterE	C56C10.7	
ClusterE	C56C10.9	
ClusterE	C56E6.3	
ClusterE	D1007.18	
ClusterE	D1007.8	
ClusterE	D1014.8 	spr-1
ClusterE	D1022.7 	aka-1
ClusterE	D1037.4 	rab-8
ClusterE	D1043.1	
ClusterE	D1046.2	
ClusterE	D1054.13 	secs-1
ClusterE	D1054.14	
ClusterE	D1054.3	
ClusterE	D1081.6	
ClusterE	D1081.7	
ClusterE	D1081.8	
ClusterE	D1086.4	
ClusterE	D2024.5	
ClusterE	D2030.11	
ClusterE	D2030.3	
ClusterE	D2030.7	
ClusterE	D2030.9 	wdr-23
ClusterE	D2092.5	
ClusterE	D2096.4 	sqv-1
ClusterE	D2096.7	
ClusterE	DY3.7 	sup-17
ClusterE	E02H1.4 	pme-2
ClusterE	E02H4.6	
ClusterE	EEED8.1 	mel-47
ClusterE	EEED8.14	
ClusterE	EEED8.3	
ClusterE	F01D4.5	
ClusterE	F01F1.11	
ClusterE	F02E8.4	
ClusterE	F02E9.4 	sin-3
ClusterE	F02E9.7	
ClusterE	F02H6.2	
ClusterE	F02H6.3	
ClusterE	F02H6.4	
ClusterE	F07A5.1 	inx-14
ClusterE	F07H5.10	
ClusterE	F08B4.1 	dic-1
ClusterE	F08B4.5	
ClusterE	F08B4.6 	hst-1
ClusterE	F08D12.10 	sdz-9
ClusterE	F08F1.5 	ced-8
ClusterE	F08F3.6	
ClusterE	F08G12.10 	inx-2
ClusterE	F08G5.1	
ClusterE	F08H9.1 	coh-3
ClusterE	F09E5.13 	agt-2
ClusterE	F09G2.8	
ClusterE	F10B5.5 	pch-2
ClusterE	F10D11.2	
ClusterE	F10E9.3	
ClusterE	F10E9.5	
ClusterE	F10G2.4	
ClusterE	F10G8.7	
ClusterE	F11A3.2	
ClusterE	F11H8.1 	rfl-1
ClusterE	F12A10.8	
ClusterE	F12E12.1	
ClusterE	F12F6.3 	rib-1
ClusterE	F12F6.5 	srgp-1
ClusterE	F12F6.7	
ClusterE	F13A7.9 	skr-11
ClusterE	F13B12.1	
ClusterE	F13E9.1	
ClusterE	F13G3.6	
ClusterE	F13G3.9 	mif-3
ClusterE	F14B4.2	
ClusterE	F14B6.3	
ClusterE	F14D2.1 	bath-27
ClusterE	F14D2.11	
ClusterE	F14D2.13 	bath-28
ClusterE	F14D2.4 	bath-29
ClusterE	F14D2.4 	bath-29
ClusterE	F14D7.2	
ClusterE	F14H3.3	
ClusterE	F14H3.4	
ClusterE	F14H3.5	
ClusterE	F14H3.6	
ClusterE	F15A4.13 	fbxb-102
ClusterE	F15A4.2	
ClusterE	F15H10.3 	apc-10
ClusterE	F16A11.1	
ClusterE	F16B4.8 	cdc-25.2
ClusterE	F16D3.4	
ClusterE	F16H11.3	
ClusterE	F17A2.13	
ClusterE	F17A9.6 	ceh-49
ClusterE	F17C11.10	
ClusterE	F17C11.7	
ClusterE	F18A1.4 	lir-2
ClusterE	F18A1.7	
ClusterE	F18A11.1 	puf-6
ClusterE	F18C5.2 	wrn-1
ClusterE	F19B10.9 	sea-1
ClusterE	F19F10.10	
ClusterE	F19H6.4	
ClusterE	F20A1.9	
ClusterE	F20C5.1 	pme-3
ClusterE	F21C3.4 	rde-2
ClusterE	F21D5.1	
ClusterE	F21D5.2	
ClusterE	F21D5.5	
ClusterE	F21D5.6	
ClusterE	F21F3.4	
ClusterE	F21H12.1	
ClusterE	F22B3.4	
ClusterE	F22B5.1 	evl-20
ClusterE	F22B7.6 	polk-1
ClusterE	F22F4.2 	inx-3
ClusterE	F23F1.1 	nfyc-1
ClusterE	F23H11.1 	bra-2
ClusterE	F25B3.1 	ehbp-1
ClusterE	F25B3.6	
ClusterE	F25B5.2	
ClusterE	F25H2.7	
ClusterE	F25H2.8 	ubc-25
ClusterE	F25H5.5	
ClusterE	F25H9.6	
ClusterE	F26A1.1	
ClusterE	F26A3.3 	ego-1
ClusterE	F26A3.7	
ClusterE	F26D10.10 	gln-5
ClusterE	F26D10.12 	clec-196
ClusterE	F26E4.1 	sur-6
ClusterE	F26E4.10 	drsh-1
ClusterE	F26E4.11 	hrdl-1
ClusterE	F26F4.5	
ClusterE	F26F4.6	
ClusterE	F26G1.1	
ClusterE	F26G5.1	
ClusterE	F26H11.1 	kbp-3
ClusterE	F26H9.1 	prom-1
ClusterE	F26H9.4	
ClusterE	F27C8.6	
ClusterE	F27D4.2	
ClusterE	F28B3.5	
ClusterE	F28C6.1	
ClusterE	F28C6.2	
ClusterE	F28C6.3 	cpf-1
ClusterE	F28C6.4	
ClusterE	F28D1.2	
ClusterE	F28F8.6 	atx-3
ClusterE	F29B9.5	
ClusterE	F29D10.4 	hum-1
ClusterE	F29G9.2	
ClusterE	F29G9.7	
ClusterE	F30A10.3	
ClusterE	F30A10.6	
ClusterE	F30B5.4	
ClusterE	F30F8.1	
ClusterE	F30F8.3	
ClusterE	F30F8.8 	taf-5
ClusterE	F31B9.3	
ClusterE	F31C3.4	
ClusterE	F31C3.5	
ClusterE	F31E3.3 	rfc-4
ClusterE	F31F6.1	
ClusterE	F31F6.3	
ClusterE	F32A5.1 	ada-2
ClusterE	F32B6.3	
ClusterE	F32D1.6	
ClusterE	F32D1.7	
ClusterE	F32E10.5	
ClusterE	F32E10.6	
ClusterE	F32H2.2 	mdt-31
ClusterE	F32H2.4 	thoc-3
ClusterE	F33D11.10	
ClusterE	F33D11.9	
ClusterE	F33G12.2	
ClusterE	F33G12.4 	lrr-1
ClusterE	F33H1.2 	gpd-4
ClusterE	F33H1.3	
ClusterE	F33H1.4	
ClusterE	F35B12.5 	sas-5
ClusterE	F35C11.5	
ClusterE	F35C8.7 	chtl-1
ClusterE	F35G12.10 	asb-1
ClusterE	F35G12.12	
ClusterE	F35G12.4	
ClusterE	F35H10.7	
ClusterE	F35H8.3 	zfp-2
ClusterE	F36A2.2	
ClusterE	F36A2.9	
ClusterE	F36D4.5	
ClusterE	F36F12.8	
ClusterE	F36H1.4 	lin-3
ClusterE	F36H2.2	
ClusterE	F37A4.8 	isw-1
ClusterE	F37B12.3	
ClusterE	F37D6.1 	mus-101
ClusterE	F37D6.2	
ClusterE	F38E1.7 	mom-2
ClusterE	F38H4.10	
ClusterE	F39H11.3 	cdk-8
ClusterE	F39H2.4 	syp-3
ClusterE	F39H2.5	
ClusterE	F40F11.4	
ClusterE	F40G12.11	
ClusterE	F40G9.11 	mxl-2
ClusterE	F41E6.4 	smk-1
ClusterE	F41G3.6	
ClusterE	F41H10.10 	htp-1
ClusterE	F41H10.4	
ClusterE	F42A8.3	
ClusterE	F42A9.8	
ClusterE	F42H10.2	
ClusterE	F43D2.1	
ClusterE	F43D9.4 	sip-1
ClusterE	F43E2.4 	haf-2
ClusterE	F43G6.10	
ClusterE	F43G6.3	
ClusterE	F43G6.5	
ClusterE	F43G6.7	
ClusterE	F43G9.4	
ClusterE	F43G9.9 	cpn-1
ClusterE	F44A6.2 	sex-1
ClusterE	F44E2.7	
ClusterE	F44F4.1	
ClusterE	F44F4.2 	egg-3
ClusterE	F45B8.1 	rgs-11
ClusterE	F45B8.2 	rgs-10
ClusterE	F45C12.15	
ClusterE	F45C12.7 	btb-6
ClusterE	F45D11.9 	fbxc-42
ClusterE	F45E4.10 	gfi-4
ClusterE	F45E4.9 	hmg-5
ClusterE	F45F2.11	
ClusterE	F45G2.3	
ClusterE	F45G2.7	
ClusterE	F46A9.4 	skr-2
ClusterE	F46B6.3 	smg-4
ClusterE	F46B6.5	
ClusterE	F46E10.2	
ClusterE	F46F11.2 	cey-2
ClusterE	F46F11.6	
ClusterE	F47D12.4 	hmg-1.2
ClusterE	F47D12.9	
ClusterE	F47G3.3	
ClusterE	F47H4.1	
ClusterE	F48C1.5	
ClusterE	F48E3.6	
ClusterE	F48E8.7 	skpt-1
ClusterE	F49D11.9 	tag-296
ClusterE	F49E11.1 	mbk-2
ClusterE	F52A8.6	
ClusterE	F52B11.1	
ClusterE	F52B5.2	
ClusterE	F52B5.5 	cep-1
ClusterE	F52C6.11 	bath-2
ClusterE	F52C6.2	
ClusterE	F52C6.3	
ClusterE	F52D2.2 	rgs-8.1
ClusterE	F52D2.4 	gei-12
ClusterE	F52E1.1 	pos-1
ClusterE	F52E1.10 	vha-18
ClusterE	F52E1.13	
ClusterE	F52E10.1 	sdc-1
ClusterE	F52F12.3 	mom-4
ClusterE	F52F12.4 	lsl-1
ClusterE	F52F12.6 	ztf-11
ClusterE	F52H3.4	
ClusterE	F53A3.2 	polh-1
ClusterE	F53B2.9 	sru-10
ClusterE	F53F4.14	
ClusterE	F53F4.3	
ClusterE	F53F8.3	
ClusterE	F53G12.5 	mex-3
ClusterE	F54C1.2 	dom-3
ClusterE	F54C1.3 	mes-3
ClusterE	F54C8.2 	cpar-1
ClusterE	F54C8.3 	emb-30
ClusterE	F54C8.4	
ClusterE	F54C9.8 	puf-5
ClusterE	F54D10.1 	skr-15
ClusterE	F54D10.5	
ClusterE	F54D10.7	
ClusterE	F54D5.11	
ClusterE	F54D5.14	
ClusterE	F54D5.5	
ClusterE	F54D5.9	
ClusterE	F54E12.2	
ClusterE	F54F2.2 	zfp-1
ClusterE	F54F7.5 	mes-1
ClusterE	F54F7.6	
ClusterE	F55A11.4	
ClusterE	F55A11.8	
ClusterE	F55A12.1	
ClusterE	F55A12.5	
ClusterE	F55A3.2	
ClusterE	F55A8.1 	egl-18
ClusterE	F55C12.5	
ClusterE	F55C5.4	
ClusterE	F55C5.7	
ClusterE	F55F8.6	
ClusterE	F55G1.6	
ClusterE	F55G1.8 	plk-3
ClusterE	F55H2.7	
ClusterE	F56A3.1	
ClusterE	F56A3.2	
ClusterE	F56A8.8	
ClusterE	F56B6.2 	rgs-7
ClusterE	F56C9.3	
ClusterE	F56C9.6	
ClusterE	F56D1.1	
ClusterE	F56D1.7 	daz-1
ClusterE	F56D2.2	
ClusterE	F56D5.5	
ClusterE	F56F11.4	
ClusterE	F56F3.1 	pqn-45
ClusterE	F56F4.1 	sdz-22
ClusterE	F56G4.2 	pes-2.1
ClusterE	F56G4.3 	pes-2.2
ClusterE	F56H1.5	
ClusterE	F57A10.4	
ClusterE	F57B1.2 	sun-1
ClusterE	F57B10.12 	mei-2
ClusterE	F57B10.4	
ClusterE	F57B9.7 	flap-1
ClusterE	F57B9.7 	flap-1
ClusterE	F57C2.1 	btb-20
ClusterE	F57C2.3	
ClusterE	F57C2.6 	spat-1
ClusterE	F57C9.4	
ClusterE	F57C9.5 	htp-3
ClusterE	F57C9.7	
ClusterE	F58A4.10 	ubc-7
ClusterE	F58A4.3 	hcp-3
ClusterE	F58A4.4 	pri-1
ClusterE	F58A4.8 	tbg-1
ClusterE	F58B3.6	
ClusterE	F58B3.9 	ttr-50
ClusterE	F58E1.9 	fbxb-19
ClusterE	F58F6.4 	rfc-2
ClusterE	F58G11.3	
ClusterE	F59A1.7 	fbxa-108
ClusterE	F59A3.9 	pup-3
ClusterE	F59A6.5	
ClusterE	F59A7.12	
ClusterE	F59B2.6 	zif-1
ClusterE	F59C6.4 	exos-3
ClusterE	F59E12.10 	ddl-1
ClusterE	F59E12.11	
ClusterE	F59E12.13 	fut-3
ClusterE	F59H6.11 	bath-5
ClusterE	H02I12.1 	cbd-1
ClusterE	H02I12.5	
ClusterE	H02I12.8 	cyp-31A2
ClusterE	H04M03.3	
ClusterE	H06I04.1	
ClusterE	H14A12.3	
ClusterE	H21P03.2	
ClusterE	H26D21.1 	hus-1
ClusterE	H26D21.2 	msh-2
ClusterE	H34C03.2	
ClusterE	H38K22.1 	evl-14
ClusterE	H43I07.1	
ClusterE	JC8.4	
ClusterE	JC8.6 	lin-54
ClusterE	K01C8.3 	tdc-1
ClusterE	K01C8.5 	gei-14
ClusterE	K01G5.1 	tag-331
ClusterE	K01G5.9	
ClusterE	K02B12.2	
ClusterE	K02B12.8 	zhp-3
ClusterE	K02B2.1	
ClusterE	K02B2.4 	inx-7
ClusterE	K02B9.1 	meg-1
ClusterE	K02B9.2 	meg-2
ClusterE	K02C4.3	
ClusterE	K02E7.9 	btb-10
ClusterE	K02F2.4 	ulp-5
ClusterE	K03B4.2	
ClusterE	K03B4.3 	taf-10
ClusterE	K03B8.4	
ClusterE	K03H1.7	
ClusterE	K03H4.2	
ClusterE	K04C1.5	
ClusterE	K04C2.3	
ClusterE	K04D7.2	
ClusterE	K04F10.3	
ClusterE	K04F10.6 	mut-2
ClusterE	K04F10.7	
ClusterE	K04G2.10	
ClusterE	K04G2.2	
ClusterE	K04G2.8 	1-Apr
ClusterE	K04G7.3 	ogt-1
ClusterE	K05C4.7	
ClusterE	K05F6.4	
ClusterE	K05F6.5 	fbxb-44
ClusterE	K05F6.7 	fbxb-54
ClusterE	K06A5.7 	cdc-25.1
ClusterE	K06G5.2 	cyp-13B2
ClusterE	K06H7.4 	grp-1
ClusterE	K06H7.6 	apc-2
ClusterE	K06H7.7	
ClusterE	K07A1.11 	rba-1
ClusterE	K07A1.17	
ClusterE	K07A1.9	
ClusterE	K07C11.2 	air-1
ClusterE	K07C5.8 	cash-1
ClusterE	K07F5.12	
ClusterE	K07G5.1 	crml-1
ClusterE	K07G5.2 	xpa-1
ClusterE	K07H8.1	
ClusterE	K07H8.2	
ClusterE	K08A8.1 	mek-1
ClusterE	K08B4.1 	lag-1
ClusterE	K08B4.2	
ClusterE	K08E3.3 	toca-2
ClusterE	K08E7.3 	let-99
ClusterE	K08F4.3	
ClusterE	K08F9.2 	tag-216
ClusterE	K08F9.4	
ClusterE	K08H10.7 	rde-1
ClusterE	K08H2.3	
ClusterE	K09E3.7	
ClusterE	K09H9.2	
ClusterE	K10B2.1 	lin-23
ClusterE	K10B2.3 	clec-88
ClusterE	K10B2.5 	ani-2
ClusterE	K10C3.2	
ClusterE	K10C3.4	
ClusterE	K10D2.1	
ClusterE	K10D2.2 	pup-2
ClusterE	K10D2.4	
ClusterE	K10D3.3 	taf-11.2
ClusterE	M01A10.1	
ClusterE	M01D1.3 	btb-11
ClusterE	M01D1.8 	fbxb-41
ClusterE	M01E11.1	
ClusterE	M01E11.5 	cey-3
ClusterE	M01E5.5 	top-1
ClusterE	M02B1.3	
ClusterE	M02B7.1 	fbxc-47
ClusterE	M02B7.2	
ClusterE	M03A1.1 	vab-1
ClusterE	M03D4.1 	zen-4
ClusterE	M03E7.5 	gosr-2.2
ClusterE	M04B2.2	
ClusterE	M04B2.3 	gfl-1
ClusterE	M05B5.5 	hlh-2
ClusterE	M05D6.2	
ClusterE	M106.2	
ClusterE	M110.3	
ClusterE	M18.3	
ClusterE	M18.6	
ClusterE	M7.2 	klc-1
ClusterE	R01H2.3 	egg-2
ClusterE	R02D3.7	
ClusterE	R02D3.8	
ClusterE	R02F2.4	
ClusterE	R03D7.4	
ClusterE	R03D7.7 	nos-1
ClusterE	R04D3.2	
ClusterE	R04D3.3	
ClusterE	R04D3.4	
ClusterE	R04F11.3	
ClusterE	R05D11.8 	edc-3
ClusterE	R05D3.11 	met-2
ClusterE	R05D3.4 	rfp-1
ClusterE	R05F9.1	
ClusterE	R05F9.9	
ClusterE	R05G9.3	
ClusterE	R05H5.3	
ClusterE	R06C7.1	
ClusterE	R06C7.2	
ClusterE	R06C7.4 	cpg-3
ClusterE	R06C7.6	
ClusterE	R06C7.7 	lin-61
ClusterE	R06F6.1 	cdl-1
ClusterE	R06F6.4 	set-14
ClusterE	R07B7.1 	clh-6
ClusterE	R07B7.2	
ClusterE	R07C3.6 	fbxc-28
ClusterE	R07C3.9 	fbxc-31
ClusterE	R07E4.5	
ClusterE	R07E5.14 	rnp-4
ClusterE	R07E5.3	
ClusterE	R07E5.7	
ClusterE	R07E5.8 	cku-80
ClusterE	R07H5.1 	prx-14
ClusterE	R09A8.2	
ClusterE	R09B3.2	
ClusterE	R09F10.8	
ClusterE	R107.4 	ikke-1
ClusterE	R107.5	
ClusterE	R10A10.2 	rbx-2
ClusterE	R10D12.12	
ClusterE	R10D12.13	
ClusterE	R10E11.3	
ClusterE	R10E4.11	
ClusterE	R10H10.7	
ClusterE	R11.4	
ClusterE	R11A5.1 	apb-3
ClusterE	R11A5.2 	nud-2
ClusterE	R11A5.3	
ClusterE	R11D1.1	
ClusterE	R11H6.2	
ClusterE	R11H6.5	
ClusterE	R11H6.6	
ClusterE	R12E2.10 	egg-5
ClusterE	R13F6.1 	kbp-1
ClusterE	R13G10.2 	amx-1
ClusterE	R144.6	
ClusterE	R148.4	
ClusterE	R52.1 	sdz-28
ClusterE	R53.6	
ClusterE	R53.7	
ClusterE	R74.5 	asd-1
ClusterE	R74.8	
ClusterE	T01B7.3 	rab-21
ClusterE	T01B7.5	
ClusterE	T01C3.2	
ClusterE	T01C3.3	
ClusterE	T01C3.9	
ClusterE	T01E8.4	
ClusterE	T01G5.7	
ClusterE	T01H3.4	
ClusterE	T01H8.1 	rskn-1
ClusterE	T02E1.3 	gla-3
ClusterE	T02G5.11	
ClusterE	T02G5.6 	msh-4
ClusterE	T02G6.5	
ClusterE	T03F6.3	
ClusterE	T04A8.15	
ClusterE	T04A8.7	
ClusterE	T04A8.8	
ClusterE	T04A8.9 	dnj-18
ClusterE	T04B8.3	
ClusterE	T04C4.1	
ClusterE	T04D3.1	
ClusterE	T04D3.2 	sdz-30
ClusterE	T04D3.5	
ClusterE	T04H1.2	
ClusterE	T04H1.4 	rad-50
ClusterE	T04H1.5	
ClusterE	T05B9.1	
ClusterE	T05E11.4 	spo-11
ClusterE	T05E7.3	
ClusterE	T05F1.2	
ClusterE	T05G11.1 	pzf-1
ClusterE	T05G5.10 	iff-1
ClusterE	T05G5.3 	cdk-1
ClusterE	T05G5.4	
ClusterE	T05G5.7 	rmd-1
ClusterE	T05H10.2 	apn-1
ClusterE	T05H10.4	
ClusterE	T05H4.14 	gad-1
ClusterE	T06C12.4 	fbxa-197
ClusterE	T06D4.1	
ClusterE	T06D8.7	
ClusterE	T06E6.2 	cyb-3
ClusterE	T07A9.12	
ClusterE	T07A9.5 	eri-1
ClusterE	T07A9.6 	daf-18
ClusterE	T07C12.12	
ClusterE	T07C4.6 	tbx-9
ClusterE	T07C4.8 	ced-9
ClusterE	T07E3.5 	brc-2
ClusterE	T07F10.3	
ClusterE	T07F8.3 	gld-3
ClusterE	T07F8.4	
ClusterE	T07G12.11 	zim-3
ClusterE	T07G12.11 	zim-3
ClusterE	T07G12.12 	him-8
ClusterE	T07G12.6 	zim-1
ClusterE	T08A9.4	
ClusterE	T08B2.4	
ClusterE	T08D10.1 	nfya-1
ClusterE	T08D10.2 	lsd-1
ClusterE	T08E11.6 	fbxb-10
ClusterE	T08G5.5 	vps-39
ClusterE	T09B4.1	
ClusterE	T09B4.10 	chn-1
ClusterE	T09B4.2	
ClusterE	T09B4.5	
ClusterE	T09F3.3 	gpd-1
ClusterE	T09F3.5	
ClusterE	T10B11.7	
ClusterE	T10B11.8	
ClusterE	T10B5.6 	knl-3
ClusterE	T10C6.5	
ClusterE	T10C6.7	
ClusterE	T10C6.8	
ClusterE	T10E9.1	
ClusterE	T10E9.2	
ClusterE	T10F2.3 	ulp-1
ClusterE	T10H9.3	
ClusterE	T11B7.1	
ClusterE	T11F8.1	
ClusterE	T11F8.3 	rme-2
ClusterE	T12A2.3	
ClusterE	T12D8.7 	taf-9
ClusterE	T12E12.1	
ClusterE	T12F5.1	
ClusterE	T12G3.6	
ClusterE	T13F2.6	
ClusterE	T13F2.8 	cav-1
ClusterE	T14B4.3	
ClusterE	T14F9.2	
ClusterE	T14G10.7	
ClusterE	T16G12.4	
ClusterE	T17A3.3 	fbxb-81
ClusterE	T17A3.4 	fbxb-82
ClusterE	T17A3.7 	fbxb-84
ClusterE	T18H9.6 	mdt-27
ClusterE	T18H9.7 	tag-232
ClusterE	T19B10.11 	mxl-1
ClusterE	T19B10.6	
ClusterE	T19B10.7 	ima-1
ClusterE	T19B10.8	
ClusterE	T19B4.7 	unc-40
ClusterE	T19H12.2	
ClusterE	T20B12.2 	tbp-1
ClusterE	T20B12.8 	hmg-4
ClusterE	T20D3.7 	vps-26
ClusterE	T20D3.8	
ClusterE	T20H4.4 	adr-2
ClusterE	T21B10.4	
ClusterE	T21C9.1	
ClusterE	T21C9.13	
ClusterE	T21D12.3	
ClusterE	T21E3.1 	egg-4
ClusterE	T22A3.3 	lst-1
ClusterE	T22B7.4	
ClusterE	T22C1.10 	rbg-2
ClusterE	T22C1.5	
ClusterE	T22D1.5	
ClusterE	T22F3.2	
ClusterE	T23B12.1	
ClusterE	T23B12.6	
ClusterE	T23B12.7 	dnj-22
ClusterE	T23B3.1	
ClusterE	T23D8.1 	mom-5
ClusterE	T23D8.7	
ClusterE	T23G11.2 	gna-2
ClusterE	T23G11.3 	gld-1
ClusterE	T23G11.5 	rlbp-1
ClusterE	T23G11.7	
ClusterE	T23G7.5 	pir-1
ClusterE	T24A11.1 	mtm-3
ClusterE	T24B8.2	
ClusterE	T24C4.1 	ucr-2.3
ClusterE	T24C4.5	
ClusterE	T24C4.7 	ztf-18
ClusterE	T24D1.2	
ClusterE	T24D1.3	
ClusterE	T24D5.5	
ClusterE	T24G10.2	
ClusterE	T24H10.4	
ClusterE	T25E12.5	
ClusterE	T25G3.2 	chs-1
ClusterE	T26A5.2	
ClusterE	T26A5.6	
ClusterE	T26A5.7 	set-1
ClusterE	T26A5.8	
ClusterE	T26C11.7 	ceh-39
ClusterE	T27A8.2	
ClusterE	T27C10.3 	mop-25.3
ClusterE	T27D1.1 	cyn-9
ClusterE	T27E9.3 	cdk-5
ClusterE	T27F6.4	
ClusterE	T27F7.1	
ClusterE	T28A8.3	
ClusterE	T28F3.3 	hke-4.1
ClusterE	VC5.4 	mys-1
ClusterE	VF36H2L.1 	aph-1
ClusterE	VH15N14R.1	
ClusterE	W01A11.2	
ClusterE	W01A8.5	
ClusterE	W01B6.9 	ndc-80
ClusterE	W01G7.4	
ClusterE	W02A2.7 	mex-5
ClusterE	W02B12.8 	rga-1
ClusterE	W02D3.9 	unc-37
ClusterE	W02D7.6	
ClusterE	W02F12.3	
ClusterE	W02F12.4	
ClusterE	W02F12.6 	sna-1
ClusterE	W03C9.2	
ClusterE	W03C9.7 	mex-1
ClusterE	W04A8.5	
ClusterE	W04B5.5	
ClusterE	W05F2.3	
ClusterE	W05F2.6	
ClusterE	W05H9.2	
ClusterE	W06B4.1	
ClusterE	W06D11.1	
ClusterE	W06D11.5	
ClusterE	W09C5.2 	unc-59
ClusterE	W09G12.1 	dsl-2
ClusterE	W10C8.2 	pop-1
ClusterE	W10D5.3 	gei-17
ClusterE	Y102A5C.1 	fbxa-206
ClusterE	Y102A5C.18 	efl-1
ClusterE	Y106G6A.2	
ClusterE	Y106G6A.2	
ClusterE	Y106G6A.5	
ClusterE	Y106G6D.1	
ClusterE	Y106G6H.15	
ClusterE	Y106G6H.16	
ClusterE	Y106G6H.6	
ClusterE	Y110A2AR.1	
ClusterE	Y110A7A.4	
ClusterE	Y113G7B.4 	ftr-1
ClusterE	Y113G7B.5 	fog-2
ClusterE	Y116A8C.17 	dct-13
ClusterE	Y11D7A.12 	flh-1
ClusterE	Y11D7A.13 	flh-3
ClusterE	Y11D7A.7	
ClusterE	Y17G9B.3 	cyp-31A3
ClusterE	Y17G9B.8	
ClusterE	Y17G9B.9	
ClusterE	Y18D10A.11	
ClusterE	Y20C6A.4	
ClusterE	Y24F12A.1	
ClusterE	Y24F12A.3	
ClusterE	Y32F6A.1 	set-22
ClusterE	Y32F6A.3 	pap-1
ClusterE	Y32H12A.2	
ClusterE	Y37D8A.13 	unc-71
ClusterE	Y37E11B.3	
ClusterE	Y37E11B.6	
ClusterE	Y38A10A.6	
ClusterE	Y39A1A.1	
ClusterE	Y39A1A.11 	dhs-11
ClusterE	Y39A1A.12	
ClusterE	Y39A1A.13	
ClusterE	Y39B6A.10	
ClusterE	Y39B6A.13	
ClusterE	Y39E4B.5	
ClusterE	Y39G8B.5	
ClusterE	Y40D12A.1	
ClusterE	Y43C5A.5 	thk-1
ClusterE	Y43C5A.6 	rad-51
ClusterE	Y43E12A.1 	cyb-2.1
ClusterE	Y43F11A.4	
ClusterE	Y43F4B.4 	npp-18
ClusterE	Y44E3B.1 	zip-4
ClusterE	Y45F10A.2 	puf-3
ClusterE	Y45F10C.3 	fbxa-215
ClusterE	Y46B2A.1 	fbxc-51
ClusterE	Y47D3A.28	
ClusterE	Y47D7A.1 	skr-7
ClusterE	Y47D7A.8 	skr-14
ClusterE	Y47G6A.2 	inx-22
ClusterE	Y47G6A.25	
ClusterE	Y47G6A.31	
ClusterE	Y47G6A.6 	pcaf-1
ClusterE	Y48G1A.6 	mbtr-1
ClusterE	Y48G1BL.3 	puf-10
ClusterE	Y48G8AL.7	
ClusterE	Y49A3A.3	
ClusterE	Y49E10.14 	pie-1
ClusterE	Y49F6C.3 	bath-9
ClusterE	Y49F6C.4 	bath-10
ClusterE	Y49F6C.8	
ClusterE	Y4C6A.3	
ClusterE	Y4C6B.1	
ClusterE	Y51B9A.4	
ClusterE	Y51H7C.7	
ClusterE	Y53C10A.12 	hsf-1
ClusterE	Y53C10A.13 	fbxa-122
ClusterE	Y53C12A.1 	wee-1.3
ClusterE	Y53F4B.8	
ClusterE	Y53H1A.2	
ClusterE	Y54E10BR.8 	ztf-23
ClusterE	Y54G11A.1	
ClusterE	Y55D5A.5 	daf-2
ClusterE	Y55D9A.2	
ClusterE	Y55F3AM.9	
ClusterE	Y56A3A.14 	sdz-33
ClusterE	Y56A3A.15 	fbxb-24
ClusterE	Y57A10A.20 	sinh-1
ClusterE	Y57G11C.25 	ccch-5
ClusterE	Y5F2A.4	
ClusterE	Y62E10A.14	
ClusterE	Y62E10A.15 	cyp-31A5
ClusterE	Y62E10A.17	
ClusterE	Y62F5A.1 	mdt-8
ClusterE	Y65B4BL.2 	deps-1
ClusterE	Y65B4BR.8	
ClusterE	Y67D8C.3	
ClusterE	Y69A2AR.30 	mdf-2
ClusterE	Y6G8.3 	ztf-25
ClusterE	Y71A12B.12	
ClusterE	Y71F9AL.8	
ClusterE	Y71F9AR.3	
ClusterE	Y71H2AM.17	
ClusterE	Y71H2B.6 	mdt-19
ClusterE	Y75B12B.1	
ClusterE	Y75B8A.17 	gmn-1
ClusterE	Y75B8A.31	
ClusterE	Y75D11A.2	
ClusterE	Y87G2A.7	
ClusterE	Y95D11A.3	
ClusterE	Y97E10AL.2	
ClusterE	Y97E10AR.3	
ClusterE	Y97E10AR.4	
ClusterE	ZC168.3	
ClusterE	ZC308.4	
ClusterE	ZC317.7	
ClusterE	ZC328.4 	san-1
ClusterE	ZC395.6 	gro-1
ClusterE	ZC395.8 	ztf-8
ClusterE	ZC404.8 	spn-4
ClusterE	ZC434.6 	aph-2
ClusterE	ZC477.5	
ClusterE	ZC504.3	
ClusterE	ZC513.5	
ClusterE	ZC513.6 	oma-2
ClusterE	ZC53.7 	rgs-9
ClusterE	ZK1098.2	
ClusterE	ZK1127.1 	nos-2
ClusterE	ZK1127.11 	him-14
ClusterE	ZK1128.6 	ttll-4
ClusterE	ZK1248.11	
ClusterE	ZK1248.13	
ClusterE	ZK1248.13	
ClusterE	ZK1248.15	
ClusterE	ZK1307.5 	sqv-8
ClusterE	ZK1307.9	
ClusterE	ZK177.1	
ClusterE	ZK177.4	
ClusterE	ZK177.5 	cyp-44A1
ClusterE	ZK177.6 	fzy-1
ClusterE	ZK287.5 	rbx-1
ClusterE	ZK353.1	
ClusterE	ZK353.7 	cutc-1
ClusterE	ZK370.3 	hipr-1
ClusterE	ZK381.1 	him-3
ClusterE	ZK418.4 	lin-37
ClusterE	ZK484.3	
ClusterE	ZK484.4	
ClusterE	ZK507.3	
ClusterE	ZK546.15 	try-1
ClusterE	ZK546.2	
ClusterE	ZK546.2	
ClusterE	ZK546.5	
ClusterE	ZK593.8	
ClusterE	ZK632.12	
ClusterE	ZK632.5	
ClusterE	ZK632.7	
ClusterE	ZK632.8 	arl-5
ClusterE	ZK637.11 	cdc-25.3
ClusterE	ZK643.2	
ClusterE	ZK652.10 	tag-307
ClusterE	ZK652.6	
ClusterE	ZK666.1	
ClusterE	ZK673.10 	sdz-37
ClusterE	ZK675.1 	ptc-1
ClusterE	ZK686.1	
ClusterE	ZK688.9	
ClusterE	ZK809.2 	acl-3
ClusterE	ZK829.5 	tbx-36
ClusterE	ZK856.1 	cul-5
ClusterE	ZK856.13 	tag-315
ClusterE	ZK858.4 	mel-26
ClusterE	ZK858.5	
ClusterE	ZK858.8	
ClusterE	ZK863.4	
ClusterE	ZK863.7 	rnp-1
ClusterE	ZK909.4 	ces-2
ClusterE	ZK938.7 	rnh-1.2
ClusterE	ZK973.11	
ClusterF	AC8.1 	pme-6
ClusterF	B0001.2	
ClusterF	B0001.3	
ClusterF	B0001.7	
ClusterF	B0035.1	
ClusterF	B0035.12	
ClusterF	B0035.6	
ClusterF	B0205.1	
ClusterF	B0240.4 	npp-22
ClusterF	B0280.9	
ClusterF	B0334.8 	age-1
ClusterF	B0336.3	
ClusterF	B0336.5	
ClusterF	B0336.6 	abi-1
ClusterF	B0336.7	
ClusterF	B0365.1	
ClusterF	B0379.3 	mut-16
ClusterF	B0457.1 	lat-1
ClusterF	B0464.2	
ClusterF	B0464.6	
ClusterF	B0464.9	
ClusterF	B0491.1	
ClusterF	B0511.12	
ClusterF	B0523.5 	fli-1
ClusterF	C01B10.8	
ClusterF	C01C7.1 	ark-1
ClusterF	C01G10.7	
ClusterF	C01G5.2 	prg-2
ClusterF	C01H6.9	
ClusterF	C02B10.4	
ClusterF	C02B10.5	
ClusterF	C02F4.1 	ced-5
ClusterF	C02F5.1 	knl-1
ClusterF	C03B8.4 	lin-13
ClusterF	C03D6.3 	cel-1
ClusterF	C03D6.4 	npp-14
ClusterF	C04A2.7 	dnj-5
ClusterF	C04F5.1 	sid-1
ClusterF	C04H5.1	
ClusterF	C05B10.1 	lip-1
ClusterF	C05C10.2	
ClusterF	C05C10.6 	ufd-3
ClusterF	C05C8.4 	gei-6
ClusterF	C05D11.2 	vps-16
ClusterF	C05D11.7	
ClusterF	C05D2.5 	gak-1
ClusterF	C06A1.1 	cdc-48.1
ClusterF	C06A5.1	
ClusterF	C06A5.10	
ClusterF	C06C3.1 	mel-11
ClusterF	C06G3.2 	klp-18
ClusterF	C07A9.3 	tlk-1
ClusterF	C07A9.7 	set-3
ClusterF	C07E3.1 	stip-1
ClusterF	C07H6.4	
ClusterF	C07H6.5 	cgh-1
ClusterF	C08B11.2 	hda-2
ClusterF	C08B11.3	
ClusterF	C09D4.4	
ClusterF	C09G4.5 	mes-6
ClusterF	C10C6.1 	kin-4
ClusterF	C10H11.10 	kca-1
ClusterF	C10H11.9 	let-502
ClusterF	C13B4.1	
ClusterF	C13B4.2 	usp-14
ClusterF	C13G3.3 	pptr-2
ClusterF	C14A4.11	
ClusterF	C14A4.3	
ClusterF	C14B1.7	
ClusterF	C14B9.4 	plk-1
ClusterF	C14C11.6 	mut-14
ClusterF	C15F1.3 	tra-2
ClusterF	C16A11.2	
ClusterF	C16A3.7 	tag-182
ClusterF	C16A3.8 	thoc-2
ClusterF	C16C10.1	
ClusterF	C16C10.2	
ClusterF	C16C10.4	
ClusterF	C17B7.7	
ClusterF	C17B7.7	
ClusterF	C17E4.6	
ClusterF	C18A3.2	
ClusterF	C18E3.7 	ppw-1
ClusterF	C18E9.3 	pqn-14
ClusterF	C18F10.7	
ClusterF	C18F3.2 	sax-7
ClusterF	C18G1.4 	pgl-3
ClusterF	C23G10.4 	rpn-2
ClusterF	C23G10.8	
ClusterF	C24H12.2	
ClusterF	C24H12.5	
ClusterF	C25G4.5 	dpy-26
ClusterF	C25H3.6	
ClusterF	C26B2.1 	dnc-4
ClusterF	C26D10.1 	ran-3
ClusterF	C26E6.3	
ClusterF	C26E6.7	
ClusterF	C26H9A.1 	vha-7
ClusterF	C27B7.5	
ClusterF	C27C7.1	
ClusterF	C28A5.1	
ClusterF	C28A5.2	
ClusterF	C28H8.9	
ClusterF	C29E4.2 	kle-2
ClusterF	C29E4.4 	npp-15
ClusterF	C29H12.5	
ClusterF	C30B5.4	
ClusterF	C30G12.6	
ClusterF	C32D5.10	
ClusterF	C32D5.3	
ClusterF	C32E8.8 	ptr-2
ClusterF	C33H5.4 	klp-10
ClusterF	C33H5.7	
ClusterF	C34B2.7 	sdha-2
ClusterF	C34C6.2	
ClusterF	C34E10.8	
ClusterF	C35D10.9 	ced-4
ClusterF	C35E7.8	
ClusterF	C36A4.4	
ClusterF	C36A4.8 	brc-1
ClusterF	C36B1.5 	prp-4
ClusterF	C36B1.8	
ClusterF	C36F7.2	
ClusterF	C37A2.2 	pqn-20
ClusterF	C38C10.5 	rgr-1
ClusterF	C38D4.1	
ClusterF	C38D4.3 	mel-28
ClusterF	C38D4.5 	tag-325
ClusterF	C39E9.12	
ClusterF	C39E9.13 	rfc-3
ClusterF	C41H7.6	
ClusterF	C43E11.10 	cdc-6
ClusterF	C43E11.3 	met-1
ClusterF	C43E11.8 	exoc-7
ClusterF	C44B9.4 	athp-1
ClusterF	C44E4.8	
ClusterF	C45G3.1 	aspm-1
ClusterF	C46F11.4	
ClusterF	C47D12.2	
ClusterF	C47D12.8	
ClusterF	C47E8.8 	set-5
ClusterF	C47G2.5	
ClusterF	C48G7.3 	tag-333
ClusterF	C49H3.10 	xpo-3
ClusterF	C50F2.2	
ClusterF	C50F2.3	
ClusterF	C53A5.3 	hda-1
ClusterF	C53B4.4	
ClusterF	C53D6.4	
ClusterF	C54G10.2 	rfc-1
ClusterF	C55B7.1 	glh-2
ClusterF	CD4.4 	vps-37
ClusterF	D1007.7 	nrd-1
ClusterF	D1022.1 	ubc-6
ClusterF	D1044.6	
ClusterF	D1054.15 	tag-135
ClusterF	D2005.4	
ClusterF	D2013.5 	eat-3
ClusterF	D2023.6	
ClusterF	D2045.2	
ClusterF	D2092.2 	ppfr-2
ClusterF	D2096.12	
ClusterF	E01A2.4	
ClusterF	E01G4.4 	pqn-27
ClusterF	E03A3.2 	rcq-5
ClusterF	EEED8.5 	mog-5
ClusterF	F01F1.4 	rabn-5
ClusterF	F01F1.7 	ddx-23
ClusterF	F01G4.4	
ClusterF	F02A9.6 	glp-1
ClusterF	F07A11.2	
ClusterF	F07A11.3 	npp-5
ClusterF	F07C6.4	
ClusterF	F08F1.9	
ClusterF	F08F3.2 	acl-6
ClusterF	F08F8.10	
ClusterF	F09E8.3 	msh-5
ClusterF	F09G2.4 	cpsf-2
ClusterF	F09G2.9	
ClusterF	F10B5.6 	emb-27
ClusterF	F10B5.7 	rrf-3
ClusterF	F10C2.4	
ClusterF	F10C5.1 	mat-3
ClusterF	F10E7.11	
ClusterF	F10E7.8 	farl-11
ClusterF	F10E9.8 	sas-4
ClusterF	F10G7.3 	unc-85
ClusterF	F10G7.4 	scc-1
ClusterF	F11A10.1 	lex-1
ClusterF	F11A10.5	
ClusterF	F11A10.8 	cpsf-4
ClusterF	F11E6.7	
ClusterF	F12F6.1	
ClusterF	F13D12.5	
ClusterF	F13H10.4	
ClusterF	F15D4.1 	btf-1
ClusterF	F16D3.2 	rsd-6
ClusterF	F17A9.2	
ClusterF	F18A1.5 	rpa-1
ClusterF	F18A1.6	
ClusterF	F18E2.3 	scc-3
ClusterF	F19F10.9	
ClusterF	F20D12.1 	csr-1
ClusterF	F20D12.2	
ClusterF	F20D12.4 	czw-1
ClusterF	F20G4.1 	smgl-1
ClusterF	F20G4.2	
ClusterF	F20G4.3 	nmy-2
ClusterF	F20H11.1	
ClusterF	F21G4.2 	mrp-4
ClusterF	F22B5.7 	zyg-9
ClusterF	F22D6.6 	ekl-1
ClusterF	F22E5.9	
ClusterF	F23B2.6 	aly-2
ClusterF	F23C8.4 	ubxn-1
ClusterF	F23F1.5	
ClusterF	F23H11.2	
ClusterF	F23H11.4	
ClusterF	F25D7.2 	tag-353
ClusterF	F25D7.4	
ClusterF	F25G6.9	
ClusterF	F25H2.13 	bch-1
ClusterF	F25H8.2	
ClusterF	F26B1.2	
ClusterF	F26B1.3 	ima-2
ClusterF	F26E4.4	
ClusterF	F26F4.1 	cee-1
ClusterF	F26F4.7 	nhl-2
ClusterF	F26G5.9 	tam-1
ClusterF	F26H11.2 	nurf-1
ClusterF	F28B12.3 	vrk-1
ClusterF	F28B3.7 	him-1
ClusterF	F28D1.10 	gex-3
ClusterF	F29C12.3	
ClusterF	F29C12.3	
ClusterF	F29D11.2	
ClusterF	F29G9.1	
ClusterF	F30A10.10	
ClusterF	F31C3.2	
ClusterF	F31C3.3	
ClusterF	F31E3.4	
ClusterF	F31E8.6	
ClusterF	F32A11.4	
ClusterF	F32A5.1 	ada-2
ClusterF	F32A7.4	
ClusterF	F32A7.5	
ClusterF	F32D1.10 	mcm-7
ClusterF	F32E10.1 	nol-10
ClusterF	F32E10.2	
ClusterF	F32H2.1 	gei-11
ClusterF	F32H2.3 	spd-2
ClusterF	F33E11.3	
ClusterF	F33G12.3	
ClusterF	F33H2.1 	dog-1
ClusterF	F33H2.2	
ClusterF	F33H2.5	
ClusterF	F35G12.11	
ClusterF	F35G12.3 	sel-5
ClusterF	F35G12.8 	smc-4
ClusterF	F36A2.1 	cids-2
ClusterF	F36A2.13	
ClusterF	F37A4.6	
ClusterF	F37E3.1 	ncbp-1
ClusterF	F38A5.13 	dnj-11
ClusterF	F38B7.5 	duo-1
ClusterF	F39B2.1	
ClusterF	F39B2.4 	sur-2
ClusterF	F39F10.3	
ClusterF	F40E3.2	
ClusterF	F40F8.11	
ClusterF	F41E6.4 	smk-1
ClusterF	F41H10.3	
ClusterF	F41H10.6	
ClusterF	F42A6.3	
ClusterF	F42A6.5	
ClusterF	F42A9.2 	lin-49
ClusterF	F42G9.6	
ClusterF	F42H10.7	
ClusterF	F42H11.2 	lem-3
ClusterF	F43G6.1 	dna-2
ClusterF	F43G6.9 	patr-1
ClusterF	F43G9.12	
ClusterF	F43G9.5	
ClusterF	F44A2.1 	tag-153
ClusterF	F44B9.6 	lin-36
ClusterF	F44B9.7 	pqn-38
ClusterF	F44C4.4 	gon-14
ClusterF	F44G4.4 	tag-169
ClusterF	F45E12.2 	brf-1
ClusterF	F45E12.3 	cul-4
ClusterF	F45F2.10	
ClusterF	F46F11.10	
ClusterF	F48A11.4	
ClusterF	F48A11.5 	ubxn-3
ClusterF	F49C12.9	
ClusterF	F52B5.3	
ClusterF	F52B5.6 	rpl-25.2
ClusterF	F52C12.1	
ClusterF	F52C6.10 	bath-7
ClusterF	F52C6.4	
ClusterF	F52C6.9 	bath-6
ClusterF	F52C9.7	
ClusterF	F52G2.1 	dcap-2
ClusterF	F52H3.2	
ClusterF	F53A2.8 	mtm-6
ClusterF	F53F10.5 	npp-11
ClusterF	F54D11.3	
ClusterF	F54D5.2	
ClusterF	F54E7.3 	par-3
ClusterF	F54F2.5 	ztf-1
ClusterF	F55A11.2 	syn-3
ClusterF	F55A11.3 	hrd-1
ClusterF	F55A11.7	
ClusterF	F55A3.3	
ClusterF	F55A3.3	
ClusterF	F55F8.4 	cir-1
ClusterF	F55H2.4 	lrg-1
ClusterF	F56A3.3 	npp-6
ClusterF	F56A3.4 	spd-5
ClusterF	F56A6.1 	sago-2
ClusterF	F56C9.10	
ClusterF	F56C9.11	
ClusterF	F56D1.4 	clr-1
ClusterF	F56D12.6	
ClusterF	F56D2.7 	ced-6
ClusterF	F56G4.4	
ClusterF	F56H1.4 	rpt-5
ClusterF	F57A10.1 	str-9
ClusterF	F57B10.6 	xpg-1
ClusterF	F57B9.10 	rpn-6
ClusterF	F57B9.2 	let-711
ClusterF	F57C9.4	
ClusterF	F58A4.9	
ClusterF	F58B3.7	
ClusterF	F58B6.3 	par-2
ClusterF	F58G1.1	
ClusterF	F58G1.1	
ClusterF	F58G1.2	
ClusterF	F58G11.1 	letm-1
ClusterF	F58G11.5 	tag-65
ClusterF	F59A2.1 	npp-9
ClusterF	F59A3.2	
ClusterF	F59A3.4	
ClusterF	F59B2.7 	rab-6.1
ClusterF	F59E10.1 	orc-2
ClusterF	F59E12.1	
ClusterF	F59G1.3 	vps-35
ClusterF	F59H5.3 	bath-12
ClusterF	F59H6.10 	bath-3
ClusterF	F59H6.3	
ClusterF	F59H6.7 	cya-2
ClusterF	F59H6.9 	bath-1
ClusterF	H04D03.1	
ClusterF	H04D03.2	
ClusterF	H05C05.2	
ClusterF	H06H21.6 	ubxn-6
ClusterF	H11L12.1	
ClusterF	H12C20.2 	pms-2
ClusterF	H20J04.2	
ClusterF	H28O16.2	
ClusterF	H38K22.2 	dcn-1
ClusterF	H43I07.2	
ClusterF	JC8.10 	unc-26
ClusterF	K02B12.5	
ClusterF	K02E7.3	
ClusterF	K02F2.1 	dpf-3
ClusterF	K02F6.7	
ClusterF	K03D7.1	
ClusterF	K03D7.1	
ClusterF	K04B12.3	
ClusterF	K04C2.4 	brd-1
ClusterF	K04G2.6	
ClusterF	K04G7.1	
ClusterF	K06A5.1	
ClusterF	K06A5.4 	knl-2
ClusterF	K06B9.2	
ClusterF	K06B9.2	
ClusterF	K06B9.4	
ClusterF	K06B9.4	
ClusterF	K07A1.12 	lin-53
ClusterF	K07A12.2	
ClusterF	K07C5.6	
ClusterF	K07F5.13 	npp-1
ClusterF	K08B12.5 	tag-59
ClusterF	K08E3.5	
ClusterF	K08E4.1 	spt-5
ClusterF	K09H11.3 	rga-3
ClusterF	K10D2.3 	cid-1
ClusterF	K11D12.2 	pqn-51
ClusterF	K11H3.4	
ClusterF	K12D12.1 	top-2
ClusterF	K12D12.1 	top-2
ClusterF	K12D12.2 	npp-3
ClusterF	K12D12.5	
ClusterF	K12H4.8 	dcr-1
ClusterF	M01B12.4	
ClusterF	M01E11.3	
ClusterF	M01E11.6 	klp-15
ClusterF	M01E5.3	
ClusterF	M01E5.4	
ClusterF	M01F1.8	
ClusterF	M03C11.2	
ClusterF	M03C11.8	
ClusterF	M03F8.3	
ClusterF	M04B2.1 	mep-1
ClusterF	M106.1 	mix-1
ClusterF	M116.5	
ClusterF	M151.3	
ClusterF	M151.7	
ClusterF	M18.8	
ClusterF	M4.1	
ClusterF	PAR2.4 	mig-22
ClusterF	R01H10.7	
ClusterF	R05D3.8	
ClusterF	R05H10.3	
ClusterF	R06A4.2	
ClusterF	R06F6.5 	npp-19
ClusterF	R06F6.8	
ClusterF	R07E5.1	
ClusterF	R07G3.3 	npp-21
ClusterF	R07G3.9 	oig-4
ClusterF	R08C7.10 	wapl-1
ClusterF	R08C7.3 	htz-1
ClusterF	R08D7.2	
ClusterF	R09A1.1 	ergo-1
ClusterF	R09B3.1 	exo-3
ClusterF	R10D12.14	
ClusterF	R10E4.4 	mcm-5
ClusterF	R10E4.5 	nth-1
ClusterF	R119.1	
ClusterF	R119.7 	rnp-8
ClusterF	R11A8.1	
ClusterF	R11E3.6 	eor-1
ClusterF	R11E3.7 	dpf-7
ClusterF	R12B2.4 	him-10
ClusterF	R12C12.2 	ran-5
ClusterF	R12E2.3 	rpn-8
ClusterF	R13A5.1 	cup-5
ClusterF	R13H4.4 	hmp-1
ClusterF	R151.8	
ClusterF	R17.2	
ClusterF	T01B11.3 	syn-4
ClusterF	T01C3.1	
ClusterF	T01C3.8 	mut-15
ClusterF	T01D3.5	
ClusterF	T01G9.4 	npp-2
ClusterF	T01G9.5 	mei-1
ClusterF	T02C12.2	
ClusterF	T02C12.3	
ClusterF	T02E1.2	
ClusterF	T02G5.12	
ClusterF	T03F1.9 	hcp-4
ClusterF	T04A11.6 	him-6
ClusterF	T05A12.3	
ClusterF	T05C12.5 	dylt-3
ClusterF	T05C12.6 	mig-5
ClusterF	T05E8.3	
ClusterF	T05F1.6 	hsr-9
ClusterF	T05G5.8	
ClusterF	T05G5.9	
ClusterF	T05H10.1	
ClusterF	T05H10.5 	ufd-2
ClusterF	T05H4.1 	acl-8
ClusterF	T06D8.8 	rpn-9
ClusterF	T07C4.3	
ClusterF	T07D4.4 	ddx-19
ClusterF	T07E3.3	
ClusterF	T08B2.5	
ClusterF	T08D2.3	
ClusterF	T08D2.3	
ClusterF	T08D2.5	
ClusterF	T08D2.5	
ClusterF	T09A5.10 	lin-5
ClusterF	T09A5.8	
ClusterF	T09E8.2 	him-17
ClusterF	T09F3.2	
ClusterF	T10G3.5 	eea-1
ClusterF	T12A2.8	
ClusterF	T12D8.1 	set-16
ClusterF	T12E12.2	
ClusterF	T12E12.3	
ClusterF	T12E12.4 	drp-1
ClusterF	T12G3.7	
ClusterF	T13C2.6	
ClusterF	T16G12.3	
ClusterF	T16G12.5 	ekl-6
ClusterF	T16G12.6	
ClusterF	T17E9.1 	kin-18
ClusterF	T19B4.2 	npp-7
ClusterF	T19B4.5	
ClusterF	T19C3.8 	fem-2
ClusterF	T19E10.1 	ect-2
ClusterF	T20F5.6	
ClusterF	T20F5.7	
ClusterF	T20G5.11 	rde-4
ClusterF	T21B10.3	
ClusterF	T21B10.5 	set-17
ClusterF	T22A3.5 	pash-1
ClusterF	T22D1.10 	ruvb-2
ClusterF	T22D1.9 	rpn-1
ClusterF	T22F3.3	
ClusterF	T23B5.1	
ClusterF	T23D8.9 	sys-1
ClusterF	T23E1.2	
ClusterF	T23G5.1 	rnr-1
ClusterF	T23G5.2	
ClusterF	T23H2.3	
ClusterF	T24B8.7	
ClusterF	T24D1.1 	sqv-5
ClusterF	T24F1.2	
ClusterF	T24H10.1	
ClusterF	T24H10.3 	dnj-23
ClusterF	T26A5.4	
ClusterF	T26A5.5	
ClusterF	T26A8.4	
ClusterF	T27A3.7	
ClusterF	T27F2.1 	skp-1
ClusterF	T28A8.4	
ClusterF	T28A8.6	
ClusterF	T28A8.7 	mlh-1
ClusterF	T28D9.2 	rsp-5
ClusterF	T28F3.1	
ClusterF	VW02B12L.3 	ebp-2
ClusterF	W01D2.5	
ClusterF	W01G7.5 	lem-2
ClusterF	W02A2.6 	rec-8
ClusterF	W02D3.4	
ClusterF	W02D9.1 	pri-2
ClusterF	W02D9.3	
ClusterF	W03A3.2 	polq-1
ClusterF	W03A3.2 	polq-1
ClusterF	W03A5.6	
ClusterF	W03D2.4 	pcn-1
ClusterF	W03F9.5 	ttb-1
ClusterF	W03G1.6 	pig-1
ClusterF	W03G9.2	
ClusterF	W04A4.5	
ClusterF	W04A8.1	
ClusterF	W04D2.4	
ClusterF	W04D2.6	
ClusterF	W05F2.2	
ClusterF	W05G11.2	
ClusterF	W06D4.6 	rad-54
ClusterF	W06H3.1	
ClusterF	W07A8.2	
ClusterF	W08F4.8 	cdc-37
ClusterF	W09B6.3 	eri-3
ClusterF	W09B6.3 	eri-3
ClusterF	W09D10.2 	tat-3
ClusterF	W09G10.4 	apd-3
ClusterF	W09G3.6	
ClusterF	W10C6.1 	mat-2
ClusterF	Y102E9.2	
ClusterF	Y105E8A.15	
ClusterF	Y105E8A.17 	ekl-4
ClusterF	Y105E8A.8	
ClusterF	Y105E8B.2 	exoc-8
ClusterF	Y106G6D.7	
ClusterF	Y106G6H.12 	duo-3
ClusterF	Y110A2AL.13	
ClusterF	Y110A7A.17 	mat-1
ClusterF	Y110A7A.8	
ClusterF	Y110A7A.9	
ClusterF	Y111B2A.3	
ClusterF	Y116A8C.36 	itsn-1
ClusterF	Y119D3B.11	
ClusterF	Y14H12B.1	
ClusterF	Y14H12B.2	
ClusterF	Y17G7A.1 	hmg-12
ClusterF	Y17G7B.5 	mcm-2
ClusterF	Y18D10A.17 	car-1
ClusterF	Y18D10A.20 	pfn-1
ClusterF	Y18H1A.6 	pif-1
ClusterF	Y18H1A.7	
ClusterF	Y23H5B.6	
ClusterF	Y2H9A.1 	mes-4
ClusterF	Y32B12B.2	
ClusterF	Y32B12B.4	
ClusterF	Y34D9A.3	
ClusterF	Y34D9A.4 	spd-1
ClusterF	Y37A1B.2 	lst-4
ClusterF	Y37D8A.12	
ClusterF	Y37D8A.12	
ClusterF	Y37D8A.12	
ClusterF	Y37D8A.9 	mrg-1
ClusterF	Y38A8.3 	ulp-2
ClusterF	Y39A1A.15 	cnt-2
ClusterF	Y39A1B.3 	dpy-28
ClusterF	Y39B6A.49	
ClusterF	Y39G10AR.13 	icp-1
ClusterF	Y39G10AR.14 	mcm-4
ClusterF	Y39G10AR.7	
ClusterF	Y39G10AR.9	
ClusterF	Y39H10A.7 	chk-1
ClusterF	Y40B1B.6 	spr-5
ClusterF	Y40B1B.8	
ClusterF	Y41D4B.12 	set-23
ClusterF	Y41D4B.19 	npp-8
ClusterF	Y41D4B.4	
ClusterF	Y41E3.12 	srt-47
ClusterF	Y43F4B.3 	set-25
ClusterF	Y43F4B.6 	klp-19
ClusterF	Y43H11AL.3 	pqn-85
ClusterF	Y44A6C.2	
ClusterF	Y45F10D.9 	sas-6
ClusterF	Y46H3C.4	
ClusterF	Y47D3A.26 	smc-3
ClusterF	Y47D3A.29	
ClusterF	Y47G6A.12 	1-Sep
ClusterF	Y47G6A.27	
ClusterF	Y47G6A.28 	tag-63
ClusterF	Y47G6A.4	
ClusterF	Y47G6A.8 	crn-1
ClusterF	Y48A6B.11 	rsa-2
ClusterF	Y48C3A.8	
ClusterF	Y48C3A.9	
ClusterF	Y48G10A.4	
ClusterF	Y48G1A.5 	xpo-2
ClusterF	Y48G1C.1	
ClusterF	Y48G1C.8	
ClusterF	Y49E10.3 	pph-4.2
ClusterF	Y49E10.4	
ClusterF	Y49F6B.4 	smu-2
ClusterF	Y49F6B.9	
ClusterF	Y51H1A.7	
ClusterF	Y51H4A.12 	set-26
ClusterF	Y52B11A.9	
ClusterF	Y53C10A.6	
ClusterF	Y53C12A.6	
ClusterF	Y53C12B.3 	nos-3
ClusterF	Y53F4B.9	
ClusterF	Y53G8AR.2	
ClusterF	Y54E10A.12	
ClusterF	Y54E10A.15 	cdt-1
ClusterF	Y54E10BR.2	
ClusterF	Y54E2A.12 	tbc-20
ClusterF	Y54E5A.4 	npp-4
ClusterF	Y54E5A.7	
ClusterF	Y54E5B.3 	let-49
ClusterF	Y55D9A.1 	efa-6
ClusterF	Y55F3AM.4 	atg-3
ClusterF	Y55F3AM.4 	atg-3
ClusterF	Y55F3BR.1	
ClusterF	Y55F3BR.4 	lgc-33
ClusterF	Y56A3A.17 	npp-16
ClusterF	Y56A3A.20 	ccf-1
ClusterF	Y56A3A.29 	ung-1
ClusterF	Y57A10A.13	
ClusterF	Y57A10A.19 	rsr-2
ClusterF	Y57A10A.25	
ClusterF	Y57A10A.31	
ClusterF	Y57A10A.4	
ClusterF	Y57G11C.13 	arl-8
ClusterF	Y65B4A.1	
ClusterF	Y65B4BL.5 	acs-1
ClusterF	Y66A7A.5	
ClusterF	Y67D8A.2	
ClusterF	Y69H2.7	
ClusterF	Y71F9B.10 	sop-3
ClusterF	Y71F9B.10 	sop-3
ClusterF	Y71F9B.6	
ClusterF	Y71F9B.7 	plk-2
ClusterF	Y73B6A.4 	smg-7
ClusterF	Y75B12A.1	
ClusterF	Y76A2A.2 	cua-1
ClusterF	Y76A2B.1 	pod-1
ClusterF	Y76B12C.2 	xpc-1
ClusterF	Y76B12C.6	
ClusterF	Y76G2A.1	
ClusterF	Y77E11A.6	
ClusterF	Y77E11A.7	
ClusterF	ZC302.1 	mre-11
ClusterF	ZC308.1 	gld-2
ClusterF	ZC308.1 	gld-2
ClusterF	ZC376.6	
ClusterF	ZC404.3 	spe-39
ClusterF	ZC404.9 	gck-2
ClusterF	ZK1055.1 	hcp-1
ClusterF	ZK1058.5	
ClusterF	ZK1067.3	
ClusterF	ZK1128.5 	tag-246
ClusterF	ZK1248.10 	tbc-2
ClusterF	ZK1248.14 	fzo-1
ClusterF	ZK1248.3 	ehs-1
ClusterF	ZK1248.7	
ClusterF	ZK1248.9	
ClusterF	ZK328.4	
ClusterF	ZK353.8 	ubxn-4
ClusterF	ZK370.4	
ClusterF	ZK381.4 	pgl-1
ClusterF	ZK546.1 	zyg-12
ClusterF	ZK546.13 	mdt-4
ClusterF	ZK550.4	
ClusterF	ZK632.1 	mcm-6
ClusterF	ZK637.7 	lin-9
ClusterF	ZK686.4	
ClusterF	ZK742.1 	xpo-1
ClusterF	ZK930.1	
ClusterF	ZK973.3	
ClusterF	ZK973.5 	acr-6
