addpath('../GO/');
load ../matfiles/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat cexp_real targets_names sperm_idx oocyte_idx result  mot mot_processed tr_gene;

w=result.weights.r;
c=result.weights.q;



E_genes=textread('clusterE_gene.txt','%s');
[e,ign]=ismember(targets_names,E_genes);
E_idx=find(e);
F_genes=textread('clusterF_gene.txt','%s');
[f,ign]=ismember(targets_names,F_genes);
F_idx=find(f);


M=size(mot_processed,1);
[swi,idx_w]=sort(w(:,1),'descend'); 
%
for i=1:2
%study the 2 clusters
if(i==1)
  cluster_idx=E_idx;
else
%  cluster_idx=[E_idx;F_idx];
  cluster_idx=F_idx;
end

oocyte_genes=intersect(cluster_idx,oocyte_idx);
non_oocyte=setdiff(cluster_idx,oocyte_genes);

if(i==1)
oocyte_corr1=corr(cexp_real(oocyte_genes,:)',c(:,1));
non_oocyte_corr1=corr(cexp_real(non_oocyte,:)',c(:,1));
else
oocyte_corr2=corr(cexp_real(oocyte_genes,:)',c(:,1));
non_oocyte_corr2=corr(cexp_real(non_oocyte,:)',c(:,1));
end
%figure(3);
%subplot(2,1,1);
%hist(oocyte_corr,20);
%subplot(2,1,2);
%hist(non_oocyte_corr,20);


for j=1:length(swi)
          mot_idx=idx_w(j);
          Xo=sum(mot_processed(oocyte_genes,mot_idx));
          Xno=sum(mot_processed(non_oocyte,mot_idx));
          K=sum(mot_processed(:,mot_idx));
          p_o(j)=1-hygecdf(full(Xo),M,full(K),length(oocyte_genes)); 
          p_no(j)=1-hygecdf(full(Xno),M,full(K),length(non_oocyte)); 
end

   figure(1);
   imagesc(cexp_real(oocyte_genes,:),[-3 3]);
   colorbar;
   figure(2);
   imagesc(cexp_real(non_oocyte,:),[-3 3]);
   colorbar;


   figure(2);
   clf
   F2=cdfplot(log10(p_no(1:50)));
   hold on;
   F1=cdfplot(log10(p_o(1:50)));
   set(F2,'Color','r');   
   set(F1,'Color','b');   
   h_legend=legend('non-oocyte','oocyte','location','NorthWest');
   h_xlabel=xlabel('log10(p-val)');
   h_ylabel=ylabel('accumulative distribution function');
   grid off;
  
  set(h_legend,'FontSize',14);
  set(h_xlabel,'FontSize',14); 
  set(h_ylabel,'FontSize',14); 

  if(i==1)
   h_title=title('Empirical CDF for log10(p-val) measuring k-mer enrichment in cluster E');
   exportfig(gcf,'../GO/figs/cluster_E.eps','color','rgb');
  else
 %i==2
   h_title=title('Empirical CDF for log10(p-val) measuring k-mer enrichment in cluster F');
   exportfig(gcf,'../GO/figs/cluster_F.eps','color','rgb');
  end

  set(h_title,'FontSize',14); 
  set(gcf,'defaultuicontrolfontsize',16);
end




  










  



