addpath('../');
clear all;
%get top gene list for the 3 motifs in factor1
load('../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat','cexp_real','targets_names','result');;

%[coeff,score]=princomp(zscore(cexp_real,[],2));
[coeff,score]=princomp(cexp_real);
coeff(:,4)=[0.02 0.04 0.47 -0.43 -0.22 0.18 0.06 0.5 0.09 0.12 -0.2 -0.57];
coeff(:,5)=[0.32 -0.38 0.17 -0.06 -0.47 0.58 -0.07 -0.09 -0.2 -0.1 -0.04 0.34];

c=result.weights.q;

figure(1);
clf;
p2=plot(1:12,-coeff(:,2),'k.-','LineWidth',5,'Markersize',25);
set(p2,'Color',[0.5 0.5 0.5]);
hold on;
p2=plot(1:12,coeff(:,3),'k.-','LineWidth',5,'Markersize',25);
hold on;
p2=plot(1:12,coeff(:,4),'k.-','LineWidth',5,'Markersize',25);
set(p2,'Color',[0.5 0.5 0.90]);
hold on;
p2=plot(1:12,coeff(:,5),'k.-','LineWidth',5,'Markersize',25);
set(p2,'Color',[0.5 0.85 0.5]);

axis([1 12 -1 1]);
h_legend=legend('PC1','PC2','PC3','PC4','location','NorthWest');
h_title=title('First 4 PCA principal components');
h_xlabel=xlabel('Time point');


set(gca,'YTick',-1:0.2:1);
set(gca,'XTick',1:12);
set(gca,'FontSize',18);
set(h_title,'FontSize',24);
set(h_xlabel,'FontSize',24);
set(h_legend,'FontSize',20);
exportfig(gcf,'../figs1/PCA_PLS1.eps','color','rgb');


figure(2);
clf;
p2=plot(1:12,c(:,1),'k.-','LineWidth',5,'Markersize',25);
set(p2,'Color',[0.5 0.5 0.5]);
hold on;
p2=plot(1:12,c(:,2),'k.-','LineWidth',5,'Markersize',25);
hold on;
p2=plot(1:12,c(:,3),'k.-','LineWidth',5,'Markersize',25);
set(p2,'Color',[0.5 0.5 0.90]);
hold on;
p2=plot(1:12,c(:,4),'k.-','LineWidth',5,'Markersize',25);
set(p2,'Color',[0.5 0.85 0.5]);

%
axis([1 12 -30 50]);
h_legend=legend('c1','c2','c3','c4','location','NorthWest');
h_title=title('First 4 PLS weight vectors');
h_xlabel=xlabel('Time point');

set(gca,'YTick',-30:10:50);
set(gca,'XTick',1:12);
set(gca,'FontSize',18);
set(h_title,'FontSize',24);
set(h_xlabel,'FontSize',24);
set(h_legend,'FontSize',20);
exportfig(gcf,'../figs1/PCA_PLS2.eps','color','rgb');


































