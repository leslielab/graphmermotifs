%post-process cluster3.0 results, output gene cluster expression patterns

addpath('../');
%export figs
s=1/32;
r=[zeros(33,1);[s:s:1]'];
g=r(end:-1:1);
map=[r g zeros(65,1)];

file='../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat';
load(file,'cexp_real','sperm_idx','oocyte_idx','result','targets_names');
seqs=textread('input/seq.txt','%s');

%cexp values written to file
file='input/gene_cexp.txt';
cluster_file='input/gene_cexp.gtr';
corr_file='input/cluster_corr.gtr';
corr_cutoff=0.8;
 
fid=fopen(file,'w');
fprintf(fid,'YORF\ttime1\ttime2\ttime3\ttime4\ttime5\ttime6\ttime7\ttime8\ttime9\ttime10\ttime11\ttime12\n');
for i=1:length(targets_names)
  fprintf(fid,'%s',targets_names{i});
  for j=1:12
    fprintf(fid,'\t%f',cexp_real(i,j));
  end
  fprintf(fid,'\n');
end
fclose(fid);
 
%read cluster3.0 output gtr file
cluster3_result=textread(cluster_file,'%s');
cluster3_result=reshape(cluster3_result,[4 length(cluster3_result)/4]);
cluster=cluster3_result(1:3,:)';
corr=textread(corr_file);

%set a correlation coefficient cutoff
idx=find(corr>corr_cutoff);
corr=corr(idx,:);
cluster=cluster(idx,:);
Node_mat=cluster(:,1);
Gene_mat=cluster(:,2:3);

%Each gene is assigned to a node/cluster
clusters=zeros(size(targets_names));

%Assign every gene to a cluster
%for i=1:length(targets_names)
%    gene=char(['GENE',num2str(i-1),'X']);
%    node=gene;
%    loc=0;
%    ign=1;

%      while ign==1
%         [ign,loc1]=ismember(node,Gene_mat(:));
%          loc=mod(loc1-1,size(Gene_mat,1))+1;

%         if(ign==1)
%             node=Node_mat(loc);
%             clusters(i)=loc;
%           end
%      end
%end

%save cluster3.mat;

load output/cluster3_80.mat;

cluster_idx=unique(clusters);
cluster_count=zeros(size(cluster_idx));
cluster_genes=cell(size(cluster_idx));

for i=1:length(cluster_idx)
    cluster_count(i)=sum(clusters==cluster_idx(i));
    cluster_genes{i}=find(clusters==cluster_idx(i));
end

%find big clusters
idx=find(cluster_count>400);

for i=length(idx):-1:1
 %for i=1:length(idx)
 
 cluster=idx(i);
 cluster
 figure(1);
 clf;
 
 switch cluster;
 case 566
%sperm gene cluster
   cluster_num=3;
 case 355
%ooycte gene cluster1
   cluster_num=1;
 case 519
%oocyte gene cluster2
   cluster_num=2;
 otherwise
   continue;
 end

 imagesc(cexp_real(cluster_genes{cluster},:),[-2 2]);
 h_title=title(char(['Cluster ',num2str(cluster_num),' gene expression']));
 h_xlabel=xlabel('Time point');
 h_ylabel=ylabel('Genes');
 set(gca,'YTick',200:200:600);
% set(gca,'XTick',0:2:12);
 set(gca,'FontSize',24);
 set(h_title,'FontSize',30);
 set(h_xlabel,'FontSize',28);
 set(h_ylabel,'FontSize',28);
 colorbar('location','southoutside');
 colormap(map);
%produce a gene expression fig 
 exportfig(gcf,char(['cluster_figs/cluster_',num2str(cluster_num),'.eps']),'color','rgb');


end

sperm_set=566;
sperm_idx=cluster_genes{sperm_set};
gene_sperm=targets_names(sperm_idx);
seq_sperm=seqs(sperm_idx);
file_sperm='output/cluster_sperm.fasta';
fid=fopen(file_sperm,'w');
%output seq file for alignace
for i=1:length(gene_sperm)
  fprintf(fid,'>%s\n',gene_sperm{i});
  fprintf(fid,'%s\n',seq_sperm{i});
end

%for oocyte genes. more
oocyte1_set=519;
oocyte1_idx=cluster_genes{oocyte1_set};
gene_oocyte1=targets_names(oocyte1_idx);
seq_oocyte1=seqs(oocyte1_idx);
file_sperm
file_oocyte1='output/cluster_oocyte1.fasta';
fid=fopen(file_oocyte1,'w');
for i=1:length(gene_oocyte1);
  fprintf(fid,'>%s\n',gene_oocyte1{i});
  fprintf(fid,'%s\n',seq_oocyte1{i});
end

oocyte2_set=355;
