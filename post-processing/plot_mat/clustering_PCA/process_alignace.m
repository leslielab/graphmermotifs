%post-process alignace output. create logo pics for alignace found motifs


%folder='cluster_sperm';
folder='PCA_factor1';
%folder='cluster_oocyte2';
kmer_file=char(['logos/',folder,'/txt/'])
pssm_logo=char(['logos/',folder,'/pics/']);
kmers=textread(char([kmer_file,'seq.ace']),'%s');


motif_idx=find(strcmp('Motif',kmers))+2;
map_idx=find(strcmp('MAP',kmers))-10;
score_idx=map_idx+12;

score_file=char([pssm_logo,'MAP.txt']);
fileWrite(kmers(score_idx),score_file);



N=length(motif_idx);
for i=1:N
   motif_file=char([kmer_file,'motif',num2str(i),'.ace']);
%output kmers to kmer txt file
   fileWrite(kmers(motif_idx(i):map_idx(i)),motif_file);
end



cd ../HSA/weblogo;

for i=1:N
kmer_file1=char(['../../clustering_PCA/',kmer_file,'motif',num2str(i),'.ace']);
pssm_logo1=char(['../../clustering_PCA/',pssm_logo,'motif',num2str(i),'.pdf']);
%kmer_file1=char(['txt/','motif',num2str(i),'.ace']);
%pssm_logo1=char(['pics/','motif',num2str(i),'.pdf']);
eval(char(['!./seqlogo -F pdf -f ',kmer_file1, '>', pssm_logo1,' -c']))
end

cd ../../clustering_PCA;
