/*****************************************************************
# Copyright (C) 2000, President and Fellows of Harvard University.
# All rights reserved. 
# 
# This software should not be redistributed or used for any commercial
# purpose, without written permission from Harvard University.
# 
# This software is provided ``AS IS'' and any express or implied
# warranties, including, but not limited to, the implied warranties of
# merchantability and fitness for a particular purpose, are disclaimed.
# In no event shall the authors or Harvard University or its agents be
# liable for any direct, indirect, incidental, special, exemplary, or
# consequential damages (including, but not limited to, procurement of
# substitute goods or services; loss of use, data, or profits; or
# business interruption) however caused and on any theory of liability,
# whether in contract, strict liability, or tort (including negligence
# or otherwise) arising in any way out of the use of this software, even
# if advised of the possibility of such damage. 
#****************************************************************/
May 13, 2004

AlignACE is a command-line program.  The default $PATH for Linux, which it
uses to find executables, does not include the current directory.  If you want
to run the program from the directory in which it resides, type ./AlignACE.
Alternatively, move it to a directory in your path.

To recompile AlignACE and CompareACE, simply execute 'make' in this directory.
This will create both executables in the current directory.

If you run AlignACE without options, it will return a list of all possible
options.

A sample input file for AlignACE, GAL.seq, is included.  This is a
FASTA-formatted file containing sequences upstream of a number of galactose
utilization genes in Saccharomyces cerevisiae.  To run AlignACE on these
sequences, execute the following command:

$ ./AlignACE -i GAL.seq > test.ace

The AlignACE output file contains a version number, a copy of the input
command line, and the values for all relevant parameters.  This is followed by
a list of the names of the input sequences.  The numbers associated with these
names are used in the subsequent motif descriptions to refer to the input
sequences.  Motifs are then listed in order of descending MAP score, the
metric for motif strength used by AlignACE.

The fields in AlignACE output following Motif x are:

1:  site sequence (*'s below indicate 'active' motif columns)

2:  number of the sequence from which the site was found (listed at the
top of the output file)

3:  position of the site in that sequence (specifically, the position of
the site column nearest the beginning of the input sequence)

4:  strand (1=forward, 0=reverse)

As mentioned above, the *'s indicate active columns.  

Questions and comments should be referred to Jason Hughes at
jhughes@post.harvard.edu.

