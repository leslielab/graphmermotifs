function [] = motifmaps(selectgnames,selectgcomnames,selectmotifs,motcolormap,imagemapflag,seqlen,idirname,ifname,odirname,ofname)
% function [] = motifmaps(selectgnames,selectgcomnames,selectmotifs,motcolormap,imagemapflag,seqlen,idirname,ifname,odirname,ofname)
% selectgnames: names of select genes for whom u want the motif maps
% selectgcomnames: common names of select genes for whom u want the motif maps
% selectmotifs: names of motifs/pssms for whom u want the motif maps
% motcolormap: colors for the motifs (nmots X 1 X 3) ... u can simply use rand(nmots,1,3)
% imagemapflag: 1 if u want to creat an imagemap out of it
% seqlen: length of sequence
% idirname: directory containing main pssm position file
% ifname: name of pssm position file
% odirname: directory where u want to save the image
% ofname: filename of saved image

if (length(selectgnames) > 300)
    error('Please restrict the number of genes to < 300');
end

close all;
load([idirname,'/',ifname],'hitgenenames', 'hitlength', 'hitstart', 'isdimers', 'isseq','avggap','pssm_words');

% Filter out unwanted pssms
hitgenenames = hitgenenames(selectmotifs);
hitlength = hitlength(selectmotifs); % length of pssm/monomer part of dimer
hitstart = hitstart(selectmotifs); % start position of hits
isdimers = isdimers(selectmotifs); % whether the pssm is a dimer
isseq = isseq(selectmotifs); % whether the pssm is a sequence
avggap = avggap(selectmotifs);
pssm_words = pssm_words(selectmotifs);
[selectgcomnames,sidx] = sort(selectgcomnames);
selectgnames = selectgnames(sidx);

% Plot the axes
left = 0.03;
% left = 0.05;
bottom = 0.05;
width = 0.89;
% width = 0.85
height = 0.94;
hm = axes('position',[left bottom width height]);

% Plot the upstream regions as lines
xoffset = 0;
rectwidth = 0.6; %width of rectangles
recthdist = 0.4; %horizontal distance between rectangles
rectheight = 0.7; %height of rectangles
rectvdist = 0.3; % vertical distance between rectangles
maxlim = (rectwidth+recthdist)*(seqlen); % total length of the upstream regions
% maxlim = seqlen+xoffset; % maximum length of the line (normalized units)

ng = length(selectgnames); % number of genes
linex = zeros(2,ng); % 2 X ng (x-coordinates of the lines)
liney = linex;       % 2 X ng (Y-coordinates of the lines)
set(gca,'xlim',[0 maxlim]); % set X axis limits so as to normalize units
linex(2,:) = maxlim-2*xoffset; % right-most coordinates of the lines
linex = linex+xoffset; % add the xoffset
set(gca,'ylim',[0 ng]); % set Y axis limits so as to normalize units
temp = [0.5:1:ng]; % equally spaced Y-coordinates of the lines
liney = repmat(temp,2,1); % Y-coordinates of the lines
liney = liney; % add offset
t = line(linex,liney,'Color','k','linewidth',0.25); % plot the lines


% Print gene names
set(gca,'xaxislocation','bottom','yaxislocation','left');
set(gca,'xticklabelmode','manual','yticklabelmode','manual','tickdirmode','manual','xtickmode','manual','ytickmode','manual');
set(gca,'xtick',[0:(rectwidth+recthdist)*50:maxlim]);
set(gca,'ytick',[0.5:1:ng]);
set(gca,'fontunits','normalized');
set(gca,'fontsize',9.5/(10*ng));
set(gca,'fontunits','pixels');
fs = get(gca,'fontsize');
fs = min(fs,4);
% fs = min(fs,6);
set(gca,'fontsize',fs);
set(gca,'yticklabel',selectgcomnames);
set(gca,'xticklabel',[seqlen:-50:0]);

% Now plot the pssms
nmots = length(selectmotifs);
rcolors = motcolormap; % color for each motif

% For each motif
for count = 1:nmots
    if isdimers(count)
        plen = 2*hitlength(count)+avggap(count);
    else
        plen = hitlength(count);
    end
    mgnames = hitgenenames{count};
    [tf,lineidx] = ismember(mgnames,selectgnames);
    valids = find(tf);
    currstart = hitstart{count};
    if isseq(count) % if it is a sequence
        currstart = currstart(valids);
        hstart = [];
        lidx = [];
        lineidx = lineidx(valids);
        for g = 1:length(lineidx)
            numhits = length([currstart{g}]);
            hstart = [hstart,currstart{g}];
            lidx = [lidx,lineidx(g)*ones(1,numhits)];           
        end        
    else % if it is a pssm
        tempidx = find(ismember(currstart(:,1),valids));
        lidx = lineidx(currstart(tempidx,1))';
        hstart = currstart(tempidx,2)';
    end
    % Now use hstart to get x coordinates and lidx to get y-coordinates of
    % the rectangles
    numrects = length(lidx);
    y1 = liney(1,lidx) - rectheight/2;
    y2 = liney(1,lidx) + rectheight/2;
    x1 = (hstart-1)*(rectwidth+recthdist);
    x2 = x1+(rectwidth+recthdist)*(plen-1)+rectwidth;
    patch([x1;x1;x2;x2],[y1;y2;y2;y1], squeeze(rcolors(count,1,:))','Edgecolor','none','FaceAlpha',0.8);
    tx = (x1+(x2-x1)/2)';
    ty = (y1+(y2-y1)/2)';
    text(tx,ty,num2str(selectmotifs(count)),'HorizontalAlignment','center','VerticalAlignment','middle','FontUnits','pixels','Fontsize',max(2,fs),'color','w');    
end

% Print the legend
left = 0.923;
bottom = 0.05;
width = 0.01;
height = 0.94;
hl = axes('position',[left bottom width height]);
[sm2 sidx] = sort(selectmotifs); % sort the motif names
rcolors = rcolors(sidx,:,:);
image(rcolors);
set(hl,'xaxislocation','bottom','yaxislocation','right');
set(hl,'xticklabelmode','manual','yticklabelmode','manual','tickdirmode','manual','xtickmode','manual','ytickmode','manual','ticklength',[0 0]);
set(hl,'xtick',[]);
set(hl,'ytick',[1:1:nmots]);
fs = 8/(10*nmots);
set(hl,'fontunits','normalized');
set(hl,'fontsize',fs);
set(hl,'fontunits','pixels');
fs = min(get(hl,'fontsize'),2.5);
% fs = min(get(hl,'fontsize'),5);
set(hl,'fontsize',7);
set(hl,'yticklabel',pssm_words(sidx));

resol = ['-r',num2str(min(max(400,ng*2.2),1300))];
fprintf('Printing file: %s/%s.jpg\n',odirname,ofname);
print('-djpeg95',resol,'-opengl',[odirname,'/',ofname]);
%close all;
