load ../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat motifs sperm_tridx oocyte_tridx cexp_tr result sperm_tst_l oocyte_tst_l tst_l oocyte_tst_s cexp_real;

%color map
s=1/32;
r=[zeros(33,1);[s:s:1]'];
g=r(end:-1:1);
map=[r g zeros(65,1)];


c=result.weights.q;
figure(1);
clf;
subplot(2,1,1);
imagesc(cexp_tr(oocyte_tridx,:),[-2 2]);
colorbar('location','southoutside');
h_title=title('Oocyte gene expression');
h_xlabel=xlabel('Time point');
h_ylabel=ylabel('Oocyte genes');

set(gca,'FontSize',20);
set(h_title,'FontSize',24);
set(h_xlabel,'FontSize',24);
set(h_ylabel,'FontSize',24);
subplot(2,1,2);
p2=plot(1:12,c(:,1),'k.-','LineWidth',2,'Markersize',20);
set(p2,'Color',[0.5 0.5 0.5]);

axis([0 12 0 40]);
h_title=title('Weight vector c1');
h_xlabel=xlabel('Time point');
h_ylabel=ylabel('c1');
set(gca,'XTick',0:2:12);
colormap(map);
set(gca,'FontSize',20);
set(h_title,'FontSize',24);
set(h_xlabel,'FontSize',24);
set(h_ylabel,'FontSize',24);

exportfig(gcf,'figs1/oocyte_gene.eps','color','rgb');



figure(3);
clf;
subplot(2,1,1);
imagesc(cexp_tr(sperm_tridx,:),[-2 2]);
colorbar('location','southoutside');
h_title=title('Sperm gene expression');
h_xlabel=xlabel('Time point');
h_ylabel=ylabel('Sperm genes');

set(gca,'FontSize',20);
set(h_title,'FontSize',24);
set(h_xlabel,'FontSize',24);
set(h_ylabel,'FontSize',24);

subplot(2,1,2);
plot(1:12,c(:,2),'k.-','LineWidth',2,'Markersize',20);
%h_title=title('The 2nd PLS weight vector c2 in gene expression space');
axis([0 12 0 40]);
h_title=title('Weight vector c2');
h_xlabel=xlabel('Time point');
h_ylabel=ylabel('c2')
colormap(map);

set(gca,'XTick',0:2:12);
set(gca,'FontSize',20);
set(h_title,'FontSize',24);
set(h_xlabel,'FontSize',24);
set(h_ylabel,'FontSize',24);
exportfig(gcf,'figs1/sperm_gene.eps','color','rgb');

















