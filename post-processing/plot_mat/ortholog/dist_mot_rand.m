%get randomized rate -> rate_rand.mat

load ../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat;
addpath('../../../');
dir='../medusa/';
dir_e='elegans/';
dir_b='briggsae/';
file='output/rate_rand.mat';
%get the length of each promoter sequence

load([dir,'seqs.mat'],'a');
load([dir,'cexp.mat'],'motifs_names');
load([dir,'targets.mat'],'targets_names');

len_e=500*ones(1,size(a,1));
a1=a;
a1(:,end)=1;
x=(a1==5);
x1=x(:,2:end)-x(:,1:end-1);
[m,n]=find(x1);
len_e(m)=n;
%%c.elegans%%%
a_e=a;
gnames_e=targets_names;

%%c.briggsae%%%
load([dir_b,'seqs.mat'],'a','targets_names');
len_b=500*ones(1,size(a,1));
a1=a;
a1(:,end)=1;
x=(a1==5);
x1=x(:,2:end)-x(:,1:end-1);
[m,n]=find(x1);
len_b(m)=n;
a_b=a;
gnames_b=targets_names;

%%only gnames_b; gnames_b is a subset of gnames_e%%%
 [i,idx]=ismember(gnames_b,gnames_e(sperm_idx));
 sperm_idx=find(i);
 [i,idx]=ismember(gnames_b,gnames_e(oocyte_idx));
 oocyte_idx=find(i);
 [i,idx]=ismember(gnames_b,gnames_e);
 gnames_e=gnames_e(idx);
 a_e=a_e(idx,:);
 len_e=len_e(idx);
%%new dir for c.elegans
eval(['!mkdir elegans']);
a=a_e;
save([dir_e,'seqs.mat'],'a');
eval(['!cp ', dir,'pbg.txt ', dir_e,'.']); 



pbg_e=textread([dir_e,'pbg.txt']);
pbg_b=textread([dir_b,'pbg.txt']);
dimers_maxgap=10;

%sample random motifs
Nm=500;
idx_kmer6=[];
idx_kmer7=[];
for i=1:length(motifs_names)
   if(length(motifs_names{i})==6)
    idx_kmer6(end+1)=i;
   elseif(length(motifs_names{i})==7)
    idx_kmer7(end+1)=i;
   else
   end
end

kmer6=unique(motifs_names(idx_kmer6));
kmer7=unique(motifs_names(idx_kmer7));

idx6=randperm(length(kmer6));
idx7=randperm(length(kmer7));

%500 6kmers and 500 7mers, as random distribution
mot=[kmer6(idx6(1:Nm)) kmer7(idx7(1:Nm))];

for i=1:length(mot)
        i
        if(mod(i,20)==0)
           save(file);
        end
        motif=mot{i};
        pssm=[motif=='A';motif=='C';motif=='G';motif=='T'];
        pssm=pssm+eps*ones(size(pssm));
       pthr_e=sum(max(log(pssm./(pbg_e'*ones(1,size(pssm,2)))),[],1))-1;
       pthr_b=sum(max(log(pssm./(pbg_b'*ones(1,size(pssm,2)))),[],1))-1;
       mot_groupidx=0;
      %Nhit is  count matrix (length of promoter*number of genes) 
      %c.elegans
      Nhit_e=scan_pssm(dir_e,mot_groupidx,pssm,pbg_e,pthr_e,dimers_maxgap);  
      Nhit_b=scan_pssm(dir_b,mot_groupidx,pssm,pbg_b,pthr_b,dimers_maxgap);  
      
      %genome-wide
      [r,c]=find(Nhit_e);
      gene_e=unique(c);
      [r,c]=find(Nhit_b);
      gene_b=unique(c);

      rate_all_rand(i)=length(intersect(gene_e,gene_b))/length(gene_e);
      %sperm genes
      Nhit_e_sperm=Nhit_e(:,sperm_idx);
      [r,c]=find(Nhit_e_sperm);
      gene_e_sperm=unique(c);
      Nhit_b_sperm=Nhit_b(:,sperm_idx);
      [r,c]=find(Nhit_b_sperm);
      gene_b_sperm=unique(c);

      rate_s_rand(i)=length(intersect(gene_e_sperm,gene_b_sperm))/length(gene_e_sperm);
      %non-sperm genes
      no_idx=setdiff(1:size(a,1),sperm_idx);
      Nhit_e_no=Nhit_e(:,no_idx);
      [r,c]=find(Nhit_e_no);
      gene_e_no=unique(c);
      Nhit_b_no=Nhit_b(:,no_idx);
      [r,c]=find(Nhit_b_no);
      gene_b_no=c;
      rate_s_no_rand(i)=length(intersect(gene_e_no,gene_b_no))/length(gene_e_no);
      
      %oocyte genes
      Nhit_e_oocyte=Nhit_e(:,oocyte_idx);
      [r,c]=find(Nhit_e_oocyte);
      gene_e_oocyte=unique(c);
      Nhit_b_oocyte=Nhit_b(:,oocyte_idx);
      [r,c]=find(Nhit_b_oocyte);
      gene_b_oocyte=unique(c);
      rate_o_rand(i)=length(intersect(gene_e_oocyte,gene_b_oocyte))/length(gene_e_oocyte);
      %non-sperm genes
      no_idx=setdiff(1:size(a,1),oocyte_idx);
      Nhit_e_no=Nhit_e(:,no_idx);
      [r,c]=find(Nhit_e_no);
      gene_e_no=unique(c);
      Nhit_b_no=Nhit_b(:,no_idx);
      [r,c]=find(Nhit_b_no);
      gene_b_no=c;
      rate_o_no_rand(i)=length(intersect(gene_e_no,gene_b_no))/length(gene_e_no);



end





