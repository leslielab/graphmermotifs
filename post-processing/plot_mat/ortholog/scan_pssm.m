function mhit=scan_pssm(dir,mot_groupidx,pssm,pbg,pthr,dimers_maxgap)
%scan a given pssm across all gene promoter sequences
%pssm_hit:1*number of genes

load([dir,'/seqs.mat'],'a');



P=log(pssm./(pbg'*ones(1,size(pssm,2))));
% number of target genes
Nseq = size(a,1);
% length of longest promoter sequence
Nb = size(a,2);

% Np: length of pssm
Np = size(P,2);
P(5,:)=-100+repmat(min(P(:)),[1 Np]);

N = Nb-Np+1;
pos = ones(N,1)*(1:Np) + ((0:(N-1))')*ones(1,Np);
pos = repmat(pos,[1 1 Nseq]);
seqno = shiftdim(1:Nseq,-1);
seqno = repmat(seqno,[N,Np,1]);

seqidx = (pos-1)*Nseq + seqno;
clear pos seqno;
seq = a(seqidx);
clear seqidx;

%pos2 = repmat(4*(0:Np-1),[N,1,Nseq]);
pos2 = repmat(5*(0:Np-1),[N,1,Nseq]);

% here is where idx is defined.
% idx is a W x L x G tensor, 
% L is length of pssm being studied
% (length of "P")
% where W is length of promoter+1-L,
% and G is the number of promoters being analyzed.

idx = pos2 + seq;

% here's an example of the funniness manuel had to do
% to save on memory:
%clear pos2 seq;

%when a contains 0
Prc = P([end-1:-1:1 end],end:-1:1);
%Prc = P(end:-1:1,end:-1:1);

% this all could be done with a "case" statement:
if mot_groupidx==0
	sc = [sum(P(idx),2);sum(Prc(idx),2)];
	clear idx;
        sc=reshape(sc,[size(sc,1) size(sc,3)]);	

        [i,j]=find(sc>=pthr);
        i=mod(i,size(sc,1)/2);
        i(find(i==0))=size(sc,1)/2;

% Nb: number of base pairs
% size(sc,2): number of genes        
        mhit=zeros(Nb,size(sc,2));
        midx=(j-1)*Nb+i;
        mhit(midx)=1;


%        for k=1:Np-1
%            mhit(midx+k)=1;
%        end
%      mhit(:,setdiff([1:size(sc,2)],gidx))=0;
else
   [idx1,idx2]=dimers_getidx(Nb,dimers_maxgap,Np);
   if mot_groupidx==1
       sc=sum(P(idx),2);
       scrc=sum(Prc(idx),2);
       sc=sc(idx1,:,:)+sc(idx2,:,:);
       scrc=scrc(idx1,:,:)+scrc(idx2,:,:);
   elseif mot_groupidx==2
       P2=P(:,end:-1:1);
      P2rc=P2([end-1:-1:1 end],end:-1:1);
   %    P2rc=P2(end:-1:1,end:-1:1);
       
       sc=sum(P(idx),2);
       scrc=sum(Prc(idx),2);
       sc2=sum(P2(idx),2);
       sc2rc=sum(P2rc(idx),2);
       sc=sc(idx1,:,:)+sc2(idx2,:,:);
       scrc=scrc(idx1,:,:)+sc2rc(idx2,:,:);
    elseif mot_groupidx==3
       P2=P([end-1:-1:1 end],:);
       P2rc=P2([end-1:-1:1 end],end:-1:1);
   %    P2=P(end:-1:1,:);
   %    P2rc=P2(end:-1:1,end:-1:1);
       
       sc=sum(P(idx),2);
       scrc=sum(Prc(idx),2);
       sc2=sum(P2(idx),2);
       sc2rc=sum(P2rc(idx),2);
       sc=sc(idx1,:,:)+sc2(idx2,:,:);
       scrc=scrc(idx1,:,:)+sc2rc(idx2,:,:);
      
    elseif mot_groupidx==4
       sc=sum(P(idx),2);
       scrc=sum(Prc(idx),2);
       sc=sc(idx1,:,:)+scrc(idx2,:,:);
       scrc=scrc(idx1,:,:)+sc(idx2,:,:);
    end

       sc2=[sc;scrc];
       sc2=reshape(sc2,[size(sc2,1) size(sc2,3)]);	
        [i,j]=find(sc2>=pthr);
        i=mod(i,size(sc2,1)/2);
        i(find(i==0))=size(sc2,1)/2;
      
        i1=idx1(i); 
        i2=idx2(i); 

% Nb: number of base pairs
% size(sc2,2): number of genes
        mhit=zeros(Nb,size(sc2,2));
        midx1=(j-1)*Nb+i1;
        midx2=(j-1)*Nb+i2;
        mhit(midx1)=1;
        mhit(midx2)=1;
%        for k=1:Np-1
%            mhit(midx1+k)=1;
%            mhit(midx2+k)=1;
%        end
        
   %   mhit(:,setdiff([1:size(sc2,2)],gidx))=0;
end  



function [idx1,idx2] = dimers_getidx(Nb,maxgap,Np)

idxend = Nb-maxgap-2*Np+1;
idx1 = 	ones(maxgap,1)*(1:idxend);
idx2 = ones(maxgap,1)*(2:(idxend+1)) + Np + (0:(maxgap-1))'*ones(1,idxend);
idxend2 = idxend+1;
idx3 = tril(ones(maxgap,1)*(idxend2:(idxend2+maxgap-1)),-1);
idxend4 = idxend+Np+1; 
idxend5 = Nb-Np+1;
idx4 = tril((idxend4:(idxend4+maxgap-1))'*ones(1,maxgap),-1);
idx1 = [idx1(:);idx3(find(idx3))];
idx2 = [idx2(:);idx4(find(idx4))];

	


