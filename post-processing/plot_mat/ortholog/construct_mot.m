% get real rates -> rate.mat

load ../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat;

addpath('../../../');
dir='../medusa/';
dir_e='elegans/';
dir_b='briggsae/';
%file='output/rate.mat';
file='output/rate_top50_motifs.mat';
mot=motifs;
w=result.weights.r;
[sw,w_idx]=sort(w(:,1),'descend');
mot=mot(w_idx(1:50));

%get the length of each promoter sequence
load([dir,'seqs.mat'],'a');
load([dir,'targets.mat'],'targets_names');
len_e=500*ones(1,size(a,1));
a1=a;
a1(:,end)=1;
x=(a1==5);
x1=x(:,2:end)-x(:,1:end-1);
[m,n]=find(x1);
len_e(m)=n;
%%c.elegans%%%
a_e=a;
gnames_e=targets_names;

%%c.briggsae%%%
load([dir_b,'seqs.mat'],'a','targets_names');
len_b=500*ones(1,size(a,1));
a1=a;
a1(:,end)=1;
x=(a1==5);
x1=x(:,2:end)-x(:,1:end-1);
[m,n]=find(x1);
len_b(m)=n;
a_b=a;
gnames_b=targets_names;

%%only gnames_b; gnames_b is a subset of gnames_e%%%
 [i,idx]=ismember(gnames_b,gnames_e(sperm_idx));
 sperm_idx=find(i);
 [i,idx]=ismember(gnames_b,gnames_e(oocyte_idx));
 oocyte_idx=find(i);
 [i,idx]=ismember(gnames_b,gnames_e);
 gnames_e=gnames_e(idx);
 a_e=a_e(idx,:);
 len_e=len_e(idx);
%%new dir for c.elegans
eval(['!mkdir elegans']);
a=a_e;
save([dir_e,'seqs.mat'],'a');
eval(['!cp ', dir,'pbg.txt ', dir_e,'.']); 



pbg_e=textread([dir_e,'pbg.txt']);
pbg_b=textread([dir_b,'pbg.txt']);
dimers_maxgap=10;

mot_e=zeros(length(gnames_e),length(mot));
mot_b=zeros(length(gnames_b),length(mot));

for i=1:length(mot)
        i
        if(mod(i,20)==0)
          save(file,'mot','gnames_e','gnames_b','mot_b','mot_e'); 
       end       

        motif=mot{i};
        pssm=[motif=='A';motif=='C';motif=='G';motif=='T'];
        pssm=pssm+eps*ones(size(pssm));
       pthr_e=sum(max(log(pssm./(pbg_e'*ones(1,size(pssm,2)))),[],1))-1;
       pthr_b=sum(max(log(pssm./(pbg_b'*ones(1,size(pssm,2)))),[],1))-1;
       mot_groupidx=0;
      %Nhit is  count matrix (length of promoter*number of genes) 
      %c.elegans
      Nhit_e=scan_pssm(dir_e,mot_groupidx,pssm,pbg_e,pthr_e,dimers_maxgap);  
      Nhit_b=scan_pssm(dir_b,mot_groupidx,pssm,pbg_b,pthr_b,dimers_maxgap);  

      mot_e(:,i)=sum(Nhit_e,1)';
      mot_b(:,i)=sum(Nhit_b,1)';

end





