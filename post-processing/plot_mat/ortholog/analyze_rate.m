%estimate sperm/oocyte MCS - non-sperm/oocyte MCS
addpath('../GO/');
addpath('../');

load ../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat;
%run dist_mot.m
load output/rate.mat;
%run dist_mot_rand.m
load output/rate_rand.mat;


%random rates for 6mers
mer6=mean(rate_all_rand(1:500));
%random rates for 7mers
mer7=mean(rate_all_rand(501:end));

for i=1:length(rate_all)
   if(length(motifs(i))==6)
   %motif length==6
   %mer6: expected conservation rate
   %N:Number of genes 
    rate_all1(i)=sqrt(N_all(i))*(rate_all(i)-mer6)/sqrt(mer6*(1-mer6));
    rate_s1(i)=sqrt(N_s(i))*(rate_s(i)-mer6)/sqrt(mer6*(1-mer6));
    rate_s_no1(i)=sqrt(N_s_no(i))*(rate_s_no(i)-mer6)/sqrt(mer6*(1-mer6));
    rate_o1(i)=sqrt(N_o(i))*(rate_o(i)-mer6)/sqrt(mer6*(1-mer6));
    rate_o_no1(i)=sqrt(N_o_no(i))*(rate_o_no(i)-mer6)/sqrt(mer6*(1-mer6));
  else
    rate_all1(i)=sqrt(N_all(i))*(rate_all(i)-mer7)/sqrt(mer7*(1-mer7));
    rate_s1(i)=sqrt(N_s(i))*(rate_s(i)-mer7)/sqrt(mer7*(1-mer7));
    rate_s_no1(i)=sqrt(N_s_no(i))*(rate_s_no(i)-mer7)/sqrt(mer7*(1-mer7));
    rate_o1(i)=sqrt(N_o(i))*(rate_o(i)-mer7)/sqrt(mer7*(1-mer7));
    rate_o_no1(i)=sqrt(N_o_no(i))*(rate_o_no(i)-mer7)/sqrt(mer7*(1-mer7));
  end


end

%sperm MCS - non-sperm MCS
%Ns=#non-sperm/#sperm
Ns=(length(targets_names)-length(sperm_idx))/length(sperm_idx);
rate_s1=rate_s1*sqrt(Ns);
rate_s2=rate_s1-rate_s_no1;
%rate_s2=rate_s1./rate_s_no1;
%oocyte MCS - non-oocyte MCS
%No=#non-oocyte/#oocyte
No=(length(targets_names)-length(oocyte_idx))/length(oocyte_idx);
rate_o1=rate_o1*sqrt(No);
rate_o2=rate_o1-rate_o_no1;
%rate_o2=rate_o1./rate_o_no1;

%rate_s2=rate_s1-rate_s_no1;
%rate_o2=rate_o1-rate_o_no1;

r=result.weights.r;
r=r(1:length(rate_all),:);



[r_sperm,idx]=sort(r(:,2),'descend');
rate_s2_s=rate_s2(idx);

%get the motifs
motifs_s=motifs(idx(1:50));
rate_s=rate_s2(idx(1:50));
[ign,idx_s]=sort(rate_s,'descend');
motifs_s(idx_s(1:20))


[h,p_sperm,stat_sperm]=kstest2(rate_s2_s(51:end),rate_s2_s(1:50),[],'large');


figure(2);
clf;
range=-20:20;

a=hist(rate_s2_s(51:end),range)/(length(rate_s2_s)-50);
h=bar(range,a);
set(h,'facecolor','k','edgecolor','w');
hold on;

a=hist(rate_s2_s(1:50),range)/50;
h=bar(range,a);
set(h,'facecolor','r','edgecolor','w');

h_title1=title('Conservation of 2nd factor k-mers');
h_xlabel1=xlabel('Sperm MCS - non-sperm MCS');
h_ylabel1=ylabel('Fraction of k-mers')
h_legend1=legend('All k-mers (excluding top 50)','Top 50 k-mers','location','NorthWest');
axis([-25 25 0 0.2]);

%h_title2=title('MCS for all k-mers (excluding top 50) in w2'); 
%h_xlabel2=xlabel('sperm MCS - non-sperm MCS');
%h_ylabel2=ylabel('Fraction of k-mers');

set(gca,'YTick',0:0.04:0.2);
set(gca,'XTick',-20:5:20);
set(gca,'FontSize',20);
set(h_title1,'FontSize',24);
%set(h_title2,'FontSize',24);
set(h_xlabel1,'FontSize',24);
%set(h_xlabel2,'FontSize',24);
set(h_ylabel1,'FontSize',24);
%set(h_ylabel2,'FontSize',24);
set(h_legend1,'FontSize',22);
exportfig(gcf,'../figs1/sperm_MCS.eps','color','rgb');


[r_oocyte,idx]=sort(r(:,1),'descend');
rate_o2_s=rate_o2(idx);

%get the motifs
motifs_o=motifs(idx(1:50));
rate_o=rate_o2(idx(1:50));
[ign,idx_o]=sort(rate_o,'descend');
motifs_o(idx_o(1:20))


[h,p_oocyte,stat_oocyte]=kstest2(rate_o2_s(51:end),rate_o2_s(1:50),[],'larger');

figure(4);
clf;
range=-20:20;

h=bar(range,hist(rate_o2_s(51:end),range)/(length(rate_o2_s)-50));
%h=bar(range,a);
set(h,'facecolor','k','edgecolor','w');
hold on;
h=bar(range,hist(rate_o2_s(1:50),range)/50);
%h=bar(range,a);
set(h,'facecolor','r','edgecolor','w');
%set(h,'facecolor','k','edgecolor','w');
%h_title1=title('MCS for top 50 k-mers in w1');
h_title1=title('Conservation of 1st factor k-mers');
h_xlabel1=xlabel('Oocyte MCS - non-oocyte MCS');
h_ylabel1=ylabel('Fraction of k-mers');
h_legend1=legend('All k-mers (excluding top 50)','Top 50 k-mers','location','NorthWest');
axis([-25 25 0 0.2]);
%h_xlabel2=xlabel('oocyte MCS - non-oocyte MCS');
%h_ylabel2=ylabel('Fraction of k-mers');

%subplot(2,1,2);
%a=hist(rate_o2_s(51:end),range);
%h=bar(range,a);
%set(h,'facecolor','k','edgecolor','w');
%h_title2=title('MCS for all k-mers (excluding top 50) in w1');
%h_xlabel1=xlabel('oocyte MCS - non-oocyte MCS');
%h_ylabel1=ylabel('Fraction of k-mers');

set(gca,'YTick',0:0.04:0.2);
set(gca,'XTick',-20:5:20);
set(gca,'FontSize',20);
set(h_title1,'FontSize',24);
%set(h_title2,'FontSize',24);
set(h_xlabel1,'FontSize',24);
%set(h_xlabel2,'FontSize',24);
set(h_ylabel1,'FontSize',24);
set(h_legend1,'FontSize',22);
%set(h_ylabel2,'FontSize',24);
exportfig(gcf,'../figs1/oocyte_MCS.eps','color','rgb');
