%look up wb ids for gene names -> wb_gene.txt
%look up orthologo wb ids for wb ids  -> wb_orth.txt
path='briggsae'; 


addpath(path);

a=textread([path,'/wb_gene.txt'],'%s');
a=reshape(a,[2 length(a)/2]);
%wb id
wb1=a(1,:);
%gene
gene=a(2,:);

a=textread([path,'/wb_orth.txt'],'%s');
a=reshape(a,[2 length(a)/2]);
%wb id
wb2=a(2,:);
%orth wb id
orth=a(1,:);


wb=intersect(wb1,wb2);
[i1,j1]=ismember(wb,wb1);
[i2,j2]=ismember(wb,wb2);

gene=gene(j1);
orth=orth(j2);
%output orth
fileWrite(orth,[path,'/orth.txt']);
%obtain sequence promoter.txt;
init_sequence(path,[path,'/gnames.txt'],[path,'/promoter.txt']);


%get gene names
load([path,'/seqs.mat']);
[i,j]=ismember(gnames,orth);
targets_names=gene(j);
wbnames=gnames;
save([path,'/seqs.mat'],'a','targets_names','wbnames');
