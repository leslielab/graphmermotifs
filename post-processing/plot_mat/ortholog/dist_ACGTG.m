load ../matfiles/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt25_fuse=23.mat;

addpath('../../../');
dir='../medusa/';
dir_e='elegans/';
dir_b='briggsae/';
%get the length of each promoter sequence
load([dir,'seqs.mat'],'a');
load([dir,'targets.mat'],'targets_names');
len_e=500*ones(1,size(a,1));
a1=a;
a1(:,end)=1;
x=(a1==5);
x1=x(:,2:end)-x(:,1:end-1);
[m,n]=find(x1);
len_e(m)=n;
%%c.elegans%%%
a_e=a;
gnames_e=targets_names;

%%c.briggsae%%%
load([dir_b,'seqs.mat'],'a','targets_names');
len_b=500*ones(1,size(a,1));
a1=a;
a1(:,end)=1;
x=(a1==5);
x1=x(:,2:end)-x(:,1:end-1);
[m,n]=find(x1);
len_b(m)=n;
a_b=a;
gnames_b=targets_names;

%%only gnames_b; gnames_b is a subset of gnames_e%%%
 [i,idx]=ismember(gnames_b,gnames_e(sperm_idx));
 sperm_idx=find(i);
 [i,idx]=ismember(gnames_b,gnames_e);
 gnames_e=gnames_e(idx);
 a_e=a_e(idx,:);
 len_e=len_e(idx);
%%new dir for c.elegans
eval(['!mkdir elegans']);
a=a_e;
save([dir_e,'seqs.mat'],'a');
eval(['!cp ', dir,'pbg.txt ', dir_e,'.']); 

Nhits=cell(1,6);
mot=cell(1,4);
mot{4}='ACGTGG';
mot{3}='ACGTGA';
mot{2}='AACGTG';
mot{1}='CACGTG';

%mot{5}='CACGTA';
%mot{6}='AACGTA';
%mot{7}='ACGTAA';
pbg_e=textread([dir_e,'pbg.txt']);
pbg_b=textread([dir_b,'pbg.txt']);
dimers_maxgap=10;

for i=1:length(mot)

        i
        motif=mot{i};
        pssm=[motif=='A';motif=='C';motif=='G';motif=='T'];
        pssm=pssm+eps*ones(size(pssm));
       pthr_e=sum(max(log(pssm./(pbg_e'*ones(1,size(pssm,2)))),[],1))-1;
       pthr_b=sum(max(log(pssm./(pbg_b'*ones(1,size(pssm,2)))),[],1))-1;
       mot_groupidx=0;
      %Nhit is  count matrix (length of promoter*number of genes) 
      %c.elegans
      Nhit_e=scan_pssm(dir_e,mot_groupidx,pssm,pbg_e,pthr_e,dimers_maxgap);  
      Nhit_b=scan_pssm(dir_b,mot_groupidx,pssm,pbg_b,pthr_b,dimers_maxgap);  

     if(i==1)
         Nhit_e_all=Nhit_e;
         Nhit_b_all=Nhit_b;
     else
         Nhit_e_all=Nhit_e_all+Nhit_e;
         Nhit_b_all=Nhit_b_all+Nhit_b;
     end
  
 %      c_u=unique(c);
%       hitgenenames{i}=c_u;
%       for g=1:length(c_u)
     %motif i's position in sperm gene g in c_u
%           hitstart{i}{g}=500-dist_sperm(find(c==c_u(g)))+2*(i-1);
%       end 
     % length of motif i
%      hitlength(i)=length(mot{i});

           

end

      len_e_sperm=len_e(sperm_idx);
      Nhit_e_sperm=Nhit_e_all(:,sperm_idx);
      [r,c]=find(Nhit_e_sperm);
      gene_e_sperm=unique(c);
      %distance to coding region of sperm genes
      dist_e_sperm=len_e_sperm(c)-r';      
      
      len_b_sperm=len_b(sperm_idx);
      Nhit_b_sperm=Nhit_b_all(:,sperm_idx);
      [r,c]=find(Nhit_b_sperm);
      gene_b_sperm=unique(c);
      %distance to coding region of sperm genes
      dist_b_sperm=len_b_sperm(c)-r';      

      %length(intersect(gene_e_sperm,gene_b_sperm))/length(union(gene_e_sperm,gene_b_sperm))
      length(intersect(gene_e_sperm,gene_b_sperm))/length(gene_e_sperm)



      no_idx=setdiff(1:size(a,1),sperm_idx);
      len_e_no=len_e(no_idx);
      Nhit_e_no=Nhit_e_all(:,no_idx);
      [r,c]=find(Nhit_e_no);
      gene_e_no=unique(c);
      %distance to coding region of no-sperm genes
      dist_e_no=len_e_no(c)-r';      
      
      len_b_no=len_b(no_idx);
      Nhit_b_no=Nhit_b_all(:,no_idx);
      [r,c]=find(Nhit_b_no);
      gene_b_no=c;
      %distance to coding region of no-sperm genes
      dist_b_no=len_b_no(c)-r';      

      %length(intersect(gene_e_no,gene_b_no))/length(union(gene_e_no,gene_b_no))
      length(intersect(gene_e_no,gene_b_no))/length(gene_e_no)
      
      figure(1);
      subplot(2,1,1);
      hist(dist_e_sperm,20);
      title('Distribution of distance of ACGTG to coding region of sperm genes in C.elegans ')
      subplot(2,1,2);
      hist(dist_e_no,20);
      title('Distribution of distance of ACGTG to coding region of non-sperm genes in C.elegans ')
      figure(2);
      subplot(2,1,1);
      hist(dist_b_sperm,20);
      title('Distribution of distance of ACGTG to coding region of sperm genes in C.briggsae ')
      subplot(2,1,2);
      hist(dist_b_no,20);
      title('Distribution of distance of ACGTG to coding region of non-sperm genes in C.briggsae ')
%      pause;      

      gene_eb_sperm=intersect(gene_e_sperm,gene_b_sperm); 
      len_e_sperm=len_e_sperm(gene_eb_sperm);
      len_b_sperm=len_b_sperm(gene_eb_sperm);
      Nhit_e_sperm=Nhit_e_sperm(:,gene_eb_sperm);
      Nhit_b_sperm=Nhit_b_sperm(:,gene_eb_sperm);
      dist=zeros(size(gene_eb_sperm));
      for i=1:length(gene_eb_sperm)
           idx_e=len_e_sperm(i)-find(Nhit_e_sperm(:,i));
           idx_b=len_b_sperm(i)-find(Nhit_b_sperm(:,i));
 
           m=repmat(idx_e',[length(idx_b) 1])-repmat(idx_b,[1 length(idx_e)]);
           dist(i)=min(min(abs(m)));
       end
    
      gene_eb_no=intersect(gene_e_no,gene_b_no); 
      len_e_no=len_e_no(gene_eb_no);
      len_b_no=len_b_no(gene_eb_no);
      Nhit_e_no=Nhit_e_no(:,gene_eb_no);
      Nhit_b_no=Nhit_b_no(:,gene_eb_no);
      dist_no=zeros(size(gene_eb_no));
      for i=1:length(gene_eb_no)
           idx_e=len_e_no(i)-find(Nhit_e_no(:,i));
           idx_b=len_b_no(i)-find(Nhit_b_no(:,i));
 
           m=repmat(idx_e',[length(idx_b) 1])-repmat(idx_b,[1 length(idx_e)]);
           dist_no(i)=min(min(abs(m)));
       end
    
      figure(3);
      subplot(2,1,1);
      hist(dist,10);
      title('Minimum distance between occurrences of ACGTG in sperm genes of the two species');

      subplot(2,1,2);
      hist(dist_no,10);
      title('Minimum distance between occurrences of ACGTG in non-sperm genes of the two species');
     %sperm genes hit by motif i
     %sperm genes hit by motif i
