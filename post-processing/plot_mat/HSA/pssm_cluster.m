function pssm = pssm_cluster(motifs,gene_idx,N_gene,counts,path)
% performs hierarchical sequence agglomeration (or motif clustering)
% INPUT:
%	motifs = list of motif-sequences to be agglomerated
%       counts = percentage of hits of motifs
%       N_gene= total number of genes
%       gene_idx = cell vector containing indices of genes hit by motif
%       counts = percentage of genes hit by motif 

global Nn Na epsilon2 pbg MaxLength counts gene_idx N_gene;

smoothing = .05; % initial smoothing with background to convert sequences to PSSMs


Na = 4;
epsilon2 = 10^-30;
MaxLength = 15;
pbg = load([path,'/pbg.txt']);


s = strvcat(motifs);
Ns = size(s,1);
Nn = size(s,2);
P = zeros(4*Ns,Nn);
idx = 1:4:4*Ns;
letters = {'A','C','G','T'};

for i=1:4
	P(idx+(i-1),:) = (s==letters{i});
end
P2=[];
for i=1:Ns
	P2{i} = P(4*(i-1)+1:(4*(i-1)+4),find(sum(P(4*(i-1)+1:(4*(i-1)+4),:)))); 
	P2{i} = ((1-smoothing)*P2{i} + smoothing*pbg'*ones(1,size(P2{i},2)));
end


Y = pssm_pdist(P2,counts);

pssm = linkage(Y,P2);


function Y = pssm_pdist(P,counts)
% calculates distances between all pairs of PSSMs


N = length(P);
Y = sparse([],[],[],N,N);

for i=1:N-1
	for j=i+1:N
		Y(i,j) = dist(P{i},P{j},counts(i),counts(j));
	end
end

function [d,stx,sty,endx,endy,dir] = dist(x,y,wx,wy)
% defines the distance function based on the minimum over all offsets of the JS entropy

global Nn Na epsilon2 pbg MaxLength;
Ny = size(y,2);
Nx = size(x,2);
nx = MaxLength - Nx;
ny = MaxLength - Ny;
Ns = 2*nx + Nx;
npos = Ns - Ny + 1;
nx1 = nx; nx2 = nx;
ny1 = ny; ny2 = ny;
ws = wx + wy;
wx = wx/ws;
wy = wy/ws;

p = zeros(npos,Ns,Na);
q = zeros(npos,Ns,Na);
qrev = zeros(npos,Ns,Na);
cyc = cycleindex(Ny,npos);
for i=1:Na
	xtmp = [pbg(i)*ones(1,nx1),x(i,:),pbg(i)*ones(1,nx2)];
	ptmp = ones(npos,1)*xtmp;
	ytmp = y(i,:);
	ytmprev = y(Na-i+1,end:-1:1);
	qtmp = pbg(i)*ones(npos,Ns);
	qtmp(cyc{1})=ytmp(cyc{2});
	p(:,:,i)=ptmp;
	q(:,:,i)=qtmp;
	qtmp(cyc{1}) = ytmprev(cyc{2});
	qrev(:,:,i) = qtmp;
end
		
pq = wx*p + wy*q;
JS1 = sum(sum(-pq.*log(pq+epsilon2) + wx*p.*log(p+epsilon2) + wy*q.*log(q+epsilon2),2),3);
pqrev = wx*p + wy*qrev;
JS2 = sum(sum(-pqrev.*log(pqrev+epsilon2) + wx*p.*log(p+epsilon2) + wy*qrev.*log(qrev+epsilon2),2),3);
[d1,mnpos1] = min(JS1);
[d2,mnpos2] = min(JS2);

if d1<=d2
	d = d1;
	dir = 1;
	mnpos = mnpos1;
else
	d = d2;
	dir = 2;
	mnpos = mnpos2;
end	



if nargout>1
	start = mnpos - nx1;
	if start>0
		stx=0;
		sty=start-1;
	elseif start<=0
		stx=1-start;
		sty=0;
	end
	ending = (nx1 + size(x,2)) - (mnpos + size(y,2) - 1);
	if ending>=0
		endx=0;
		endy=ending;
	elseif ending<0
		endx=-ending;
		endy=0;
	end
end


function Pssm = linkage(Y,P);
% performs the hierarchical agglomeration

global clust_targidx counts gene_idx N_gene;
Ns = length(Y);
W = counts;
Ts = zeros(Ns);
Tcurr = 1:Ns;
Ts(1,:) = Tcurr;
Z = zeros(Ns-1,3);
Zlabel = 1:Ns;
motidxmin = [];
dklpbg = [];

for i=2:Ns
	fprintf(['hierarchical motif clustering: iteration ',num2str(i-1),'\n']);
	mn = min(Y(find(Y)));
	[j,k] = find(Y==mn);
	take = ceil(rand*length(j));
	j = j(take);
	k = k(take);
	Z(i-1,:) = [Zlabel(j) Zlabel(k) mn];
	Zlabel(j) = Ns+i-1;
	P{j} = pssm_add(P{j},P{k},W(j),W(k));
	dklpbg(i-1) = dkl(P{j});

        gene_idx{j}=union(gene_idx{j},gene_idx{k});
%        W(j)=W(j)+W(k);
        W(j)=length(gene_idx{j})/N_gene;
	if W(j)==0, W(j) = eps; end
	W(k) = 0;
	Tcurr(find(Tcurr==k)) = j;
	Ts(i,:) = Tcurr;
	update = find(W ~= 0);
	above = find(update>j);
	below = find(update<j);
	for m = 1:length(above) 
		Y(j,update(above(m)))=dist(P{update(above(m))},P{j},W(update(above(m))),W(j));
	end
	for m = 1:length(below)
		Y(update(below(m)),j)=dist(P{update(below(m))},P{j},W(update(below(m))),W(j));
	end
	Y(k,:)=0;Y(:,k)=0;
        Pssm=P{j}
end

function d = dkl(P)
% Kullback-Leibler divergence

global pbg;
P2 = P./(pbg'*ones(1,size(P,2)));
P3 = P.*log2(P2);
d = sum(P3(:));

function z = pssm_add(x,y,wx,wy)
% joins two PSSMs

global pbg;
wsum = wx + wy;
wx = wx/wsum;
wy = wy/wsum;
[ign,stx,sty,endx,endy,dir] = dist(x,y,wx,wy);

if dir==1
	z = wx*[pbg'*ones(1,stx),x,pbg'*ones(1,endx)] + wy*[pbg'*ones(1,sty),y,pbg'*ones(1,endy)];
elseif dir==2
	z = wx*[pbg'*ones(1,stx),x,pbg'*ones(1,endx)] + wy*[pbg'*ones(1,sty),y(end:-1:1,end:-1:1),pbg'*ones(1,endy)];
end




