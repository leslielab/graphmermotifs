N_f=5
%number of factors
kmer_dir='kmer67';
output_dir='HSA_output/pssms/';
pssm_file=char([output_dir,kmer_dir,'/germline_',kmer_dir,'.txt']);


%loop over factors
for j=1:N_f 

cd HSA_output/pssms/kmer67/;

%file=char([output_dir,kmer_dir,'/factor',num2str(j),'_*.txt'])
file=char(['factor',num2str(j),'_*.txt'])

kmer_files1=dir(file);
N=length(kmer_files1);

cd ../../../;
for i=1:N
  kmer_file=kmer_files1(i).name; 
 %read in k-mer sequences
  s=textread(char([output_dir,kmer_dir,'/',kmer_file]),'%s');
  s1=strvcat(s);
  pssm=[sum(s1=='A',1);sum(s1=='C',1);sum(s1=='G',1);sum(s1=='T',1)]/size(s1,1);
  dlmwrite(pssm_file,'<p>','delimiter','','-append');
  dlmwrite(pssm_file,j,'delimiter','','-append');
  dlmwrite(pssm_file,pssm,'delimiter',' ','-append');
  dlmwrite(pssm_file,'</p>','delimiter','','-append');
   
%   !echo '</p>' >> tf.txt;
end
end
