function seqcell=pssmseq(pssm,N)

lpssm=size(pssm,2);
cumpssm=cumsum(pssm,1);
cumpssm(2:4,:)=cumpssm(1:3,:);
cumpssm(1,:)=zeros(1,lpssm);

seqcell=cell(N,1);

for i=1:N 
  seq=repmat(rand(1,lpssm),[4 1]);
  posneg=cumsum(seq-cumpssm,1);
  [seqnum,seqidx]=max(posneg,[],1);
  
  seqi(find(seqidx==1))='A';
  seqi(find(seqidx==2))='C';
  seqi(find(seqidx==3))='G';
  seqi(find(seqidx==4))='T';

  seqcell{i}=seqi;
end
