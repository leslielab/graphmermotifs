%cluster motifs in a subnetwork from mcode analysis
%output logos and text pssms, input text  for matalign 
clear all;

file='../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat';

load(file);
output_dir='kmer67';

%loop over all mcode output text files


%first get kmer subnetworks from mcode
!cut -f3 mcode_output/mcode_oocyte_onlyfuse_negative.txt > mcode_output/N.txt;
!sed -e 's/[0-9|,|.]//g' mcode_output/mcode_oocyte_onlyfuse_negative.txt > mcode_output/mot.txt;
file1='/mcode_oocyte_onlyfuse_negative_';


all_Ns=[0; cumsum(textread('mcode_output/N.txt'))];
all_mots=textread('mcode_output/mot.txt','%s');

%loop over all networks
for i=2:length(all_Ns)
     i 
      mots=all_mots(all_Ns(i-1)+1:all_Ns(i));
%file to which sequences are output
       N_seq=200;
       kmer_file=char(['HSA_output/pssms/',output_dir,file1,num2str(i-1),'.txt']);
       kmer_file1=char(['../HSA_output/pssms/',output_dir,file1,num2str(i-1),'.txt']);
     pssm_logo=char(['../HSA_output/logos/',output_dir,file1,num2str(i-1),'.png'
]);
     

gene_idx=cell(size(mots));
hit_per=zeros(size(mots));


addpath('../');
dir='../../../../data/';
pbg=textread([dir,'pbg.txt']);
dimers_maxgap=10;

for i=1:length(mots)

        i
        motif=mots{i};
        pssm=[motif=='A';motif=='C';motif=='G';motif=='T'];
        pssm=pssm+eps*ones(size(pssm));
       pthr=sum(max(log(pssm./(pbg'*ones(1,size(pssm,2)))),[],1))-1;
       mot_groupidx=0;



           Nhit=scan_pssm(dir,mot_groupidx,pssm,pbg,pthr,dimers_maxgap);
           hit_per(i)=sum(sum(Nhit,1)>0)/size(Nhit,2);        
    
           gene_idx{i}=find(sum(Nhit,1)>0);
end 

N_gene=size(Nhit,2);
%HSA. cluster kmers into a pssm. output 
pssm=pssm_cluster(mots,gene_idx,N_gene,hit_per,dir);

!echo '<p>' >> mcode_oocyte1.txt;
dlmwrite('mcode_oocyte1.txt',pssm,'delimiter',' ','-append');
!echo '</p>' >> mcode_oocyte1.txt;

%generate the logo file
pssmlogo(pssm,N_seq,kmer_file);

cd weblogo;
eval(char(['!./seqlogo -F PNG -f ',kmer_file1, '>', pssm_logo,' -c']));
cd ../;

%generate the pssm file

clear global;
end

!rm mcode_output/N.txt;
!rm mcode_output/mot.txt;

