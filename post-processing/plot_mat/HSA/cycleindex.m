function cyc = cycleindex(nb,npos)
% function cyc = cycleindex(nb,npos)
%
% creates index-matrix for pssm_cluster.m
%
% (c) Manuel Middendorf, 2005

N = npos + nb - 1;
a = (1:nb)'*ones(1,npos);
b = ones(nb,1)*(N+1)*(0:(npos-1)) + a;
c = zeros(N,npos);
c(b(:)) = a(:);
c = c';
[idx,ign,val] = find(c(:));
cyc{1} = idx;
cyc{2} = val;


