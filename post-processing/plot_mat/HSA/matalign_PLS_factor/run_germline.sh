#!/bin/bash

if [ $# -eq 3 ]; then
     TF=$1;
     input=$2; 
     top=$3;
   # perl  matalignmlb_TF.pl -rulesfile=input/$input.txt -pssmfile=input/TR_S_In_dimer.txt -output=output/$TF  -html=$TF.html -top=$top;
     perl  matalignmlb_PLS.pl -rulesfile=input/$input.txt -pssmfile=input/wormbook.txt -output=output/$TF  -html=$TF.html -top=$top;
else
    echo "Usage: $0 <TF file> < Number of top TF pssms >";
    exit 0;
fi 



