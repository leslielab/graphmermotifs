#!/usr/bin/perl  

use strict;
my @args = @ARGV;
my $rules;
my $TFpssms;
my $output;
my $html;
my $top;
#my @pindices;

unless( $#args==4 ){
   die("Usage: perl matalign.pl [required options]\n\nRequired options:\n\n-rulesfile=rules file   path to input rules file from MEDUSA\n-pssmfile=pssm file    path to input pssm file having the format of one line of transcription factor name followed by four lines of its pssm matrix\n-output=output directory    subdirectory where output files will be written\n-html=html file   name of the html file written in the output diretory\n-top=N  save to html file only the top N matches comparing each pssm from rules file against pssms from pssm file\n");
}
if ($args[0]=~m/-rulesfile=(.*)/)
 { $rules = $1;} 
if ($args[1]=~/-pssmfile=(.*)/)
 { $TFpssms=$1;}
if ($args[2]=~/-output=(.*)/)
 { $output=$1;} 
if ($args[3]=~/-html=(.*)/)
 { $html=$1; }
if ($args[4]=~/-top=(.*)/)
 { $top=$1; }


my ($pssms_ref,$fs_ref)=parse($rules); 
my @pssms=@$pssms_ref;
my @fs=@$fs_ref;

my %tfpssms=readin($TFpssms);
my @tfpssms=convert(\%tfpssms);
system("if [ ! -d $output ]; then mkdir $output; fi");
PssmLogo1(\@pssms,$output,"pssm");
PssmLogo2(\@tfpssms,$output,"tfpssm");
my @align=matalign(\@pssms,\%tfpssms,$top);
#sort by p-value
my @sortalign=sort {$a->[4]<=>$b->[4]} (@align);
#sort by ruler id
#my @sortalign=sort {$a->[2]<=>$b->[2]} (@align);

WriteHtml(\@sortalign,\@fs,$output,$html);
Printalign(\@sortalign,\@tfpssms,\@pssms,$output);
#system "rm matalign.txt $output/pssmseq.txt $output/putative $output/tf";

sub parse()
{  my $rulefile=$_[0];
   my $s='';
   my @pssms;
   open(RULES,$rulefile); 
   while(<RULES>){
        $s .= $_;
   }
   close(RULES);
  
#read in factors 
   my @fs = ($s =~ /<p>([0-9]*)\n.*?\n<\/p>/gs);
    
   my @ps = ($s =~ /<p>[0-9]*\n(.*?)\n<\/p>/gs);
   my $p;
# read in pssms
   foreach $p (@ps){
#         my @pssm2idx = split /\n/, $p;
#         my @pssm=@pssm2idx[1..4];
#         push @pssms, \@pssm;
#         push @pindices, $pssm2idx[0];
          my @pssm = split /\n/, $p;
          push @pssms, \@pssm;
   } 
   return (\@pssms,\@fs);
}

sub readin()
{ my $tffile=$_[0];
  my $i=0;
  my $TF=0;
  my %tfpssms;
  my @pssm; 

  open(TFFILE,$tffile);
  while(<TFFILE>)
 {  chomp; 
    if(/^\s*$/)
      {next;}
    elsif(/^\s*([A-Z].*)$/i)
      {
        $i=0;
        $TF=$1;
      } 
    else
      {$pssm[$i]=$_;
       if($i<3){ 
          $i+=1; }
       else{
          $i=0;
          push  @{$tfpssms{$TF}},[ @pssm ];
          }
      }  
 }
     

    return %tfpssms; 
} 
  
sub PrintPssms(){
    my $pssmfile=$_[1];
    my $pssms_ref=$_[0];
    my @pssms=@$pssms_ref; 

    open(PSSM,">>$pssmfile");
    foreach my $pssm_ref (@pssms){
       my @pssm=@$pssm_ref; 
       print PSSM  "$pssm[0]\n$pssm[1]\n$pssm[2]\n$pssm[3]\n\n";
    }
    close(PSSM);
}
 
  
sub PrintTFpssms(){
    my $tfpssms_ref=$_[0];
    my $tffile=$_[1];
    my %tfpssms=%{$tfpssms_ref}; 
   
    open(TF,">>$tffile");
    foreach my $TF (sort keys %tfpssms)
    { 
       print TF "$TF\n";
       my @pssms=@{$tfpssms{$TF}}; 
       for my $i ( 0..$#pssms ){
           my @pssm=@{$pssms[$i]};
           print TF "$pssm[0]\n$pssm[1]\n$pssm[2]\n$pssm[3]\n\n";
       }
    }   
    close(TF);
}

sub convert(){
  my %tfpssms=%{$_[0]};
  my @tfpssms;
    foreach my $TF (sort keys %tfpssms)
    {
       my @tpssms=@{$tfpssms{$TF}};
       for my $j ( 0..$#tpssms ){
           my @tpssm=@{$tpssms[$j]};
           push @tfpssms,[ @tpssm ];
       }
    }
     return @tfpssms;
}


sub PssmLogo1(){
   my @pssms=@{$_[0]};
   my $output=$_[1];
   my $name=$_[2];
   
   for my $i (0..$#pssms){
        my @pssm=@{$pssms[$i]};
        PssmSeq(\@pssm,"$output/pssmseq.txt");
#        system "perl weblogo/seqlogo -f $output/pssmseq.txt -F GIF -c -w 4.5 -h 2 > $output/$name$pindices[$i]\.gif";
        system "perl weblogo/seqlogo -f $output/pssmseq.txt -F GIF -c -w 4.5 -h 2 > $output/$name$i\.gif";
    } 
}
sub PssmLogo2(){
   my @pssms=@{$_[0]};
   my $output=$_[1];
   my $name=$_[2];
   
   for my $i (0..$#pssms){
        my @pssm=@{$pssms[$i]};
        PssmSeq(\@pssm,"$output/pssmseq.txt");
#      system "perl weblogo/seqlogo -f $output/pssmseq.txt -F GIF -c -w 4.5 -h 2 > $output/$name$pindices[$i]\.gif";
        system "perl weblogo/seqlogo -f $output/pssmseq.txt -F GIF -c -w 4.5 -h 2 > $output/$name$i\.gif";
    } 
}

sub PssmSeq(){
     my @pssm=@{$_[0]};
     my $seqfile=$_[1];
     my $Nseq=200;
     my @inpssm;
     my $pssmlen;
   
     for my $i ( 0..3 ){
        my @line=split ' ',$pssm[$i];
        push @inpssm,\@line; 
        $pssmlen=$#line;          
     }
     
     system "if [ -e $seqfile ]; then rm $seqfile; fi";
     open(PSSMSEQ,">>$seqfile");
     
     for my $N ( 0..$Nseq){
          my $seq; 
          for my $pos ( 0..$pssmlen){
              my $ran=rand();
              if($ran < $inpssm[0]->[$pos])
                 {$seq.='A';}
              elsif($ran < ($inpssm[0]->[$pos] + $inpssm[1]->[$pos]))
                 { $seq.='C';}
              elsif($ran < ($inpssm[0]->[$pos] + $inpssm[1]->[$pos] + $inpssm[2]->[$pos]))
                { $seq.='G';}
              else 
                 { $seq.='T';}
          } 
          print PSSMSEQ "$seq\n";  
     } 
     close(PSSMSEQ);
}

sub matalign(){
  my @pssms=@{$_[0]};
  my %tfpssms=%{$_[1]};
  my $top=$_[2]; 
  my @align;
  my $ALLR;
  my $pvalue;

   foreach my $pidx ( 0..$#pssms ){
           my @pssm=@{$pssms[$pidx]};
           my @pssmalign;
           open(Rule,">$output/putative");
                print Rule "A | $pssm[0]\nC | $pssm[1]\nG | $pssm[2]\nT | $pssm[3]\n";
           close(Rule);
           
           my $tfidx=0;
           foreach my $TF (sort keys %tfpssms)
           { 
              
             my @tpssms=@{$tfpssms{$TF}}; 
               for my $j ( 0..$#tpssms ){
                   my @tpssm=@{$tpssms[$j]};
                   open(TF,">$output/tf");
                      print TF "A | $tpssm[0]\nC | $tpssm[1]\nG | $tpssm[2]\nT | $tpssm[3]\n";
                   close(TF);
           
                   system ("./matalign-v2a -f1 $output/putative -f2 $output/tf -t1 1 -t2 1 -g > matalign.txt");
                   
             
                   open(Align,"matalign.txt");  
                   while(<Align>){
                      if(/ALLR = (.*)$/) 
                        { $ALLR=$1;} 
                      if(/P_value = (.*)$/)
                        { $pvalue=$1;}
                    } 
                   close(Align);
            
                   push @{$pssmalign[$tfidx]}, $TF ;
                   push @{$pssmalign[$tfidx]}, $tfidx ;
                   push @{$pssmalign[$tfidx]}, $pidx;
 #                  push @{$pssmalign[$tfidx]}, $pindices[$pidx];
                   push @{$pssmalign[$tfidx]}, $ALLR;
                   push @{$pssmalign[$tfidx]}, $pvalue;
                   $tfidx+=1;
            }
          }
       
       my @sortpssm=sort {$a->[4]<=>$b->[4]} (@pssmalign); 
#       my @sortpssm=sort {$a->[2]<=>$b->[2]} (@pssmalign); 
       my @tem; 
      if ($top==0)
          { @tem=(@align,@sortpssm);} 
       elsif ($top<=$tfidx && $top>0)
          { @tem=(@align,@sortpssm[0..($top-1)]);}
       else
          {die("Not a valid number of top TF pssms for each Medusa pssm!\n");}  
       @align=@tem;
  }
       return @align;   
         
}

sub Printalign(){
    my @align=@{$_[0]};
    my @tfpssms=@{$_[1]};
    my @pssms=@{$_[2]};
    my $output=$_[3];
     for my $count ( 0..$#align ){
#        print "TF: $align[$count][0]\n";
#        print "TFpssm:\n";
        PrintPssm($tfpssms[$align[$count][1]],$output);  
#        print "pssm:\n";
#        PrintPssm($pssms[$align[$count][2]]);  
#        print "motifidx: $align[$count][2]\n";
#        print "ALLR: $align[$count][3]\n";
#        print "pvalue: $align[$count][4]\n\n";
    }
}

sub PrintPssm(){
        my @pssm=@{$_[0]};
        my $output=$_[1];
        open(TF,">>$output/tfpssm.txt");
        print TF "$pssm[0]\n$pssm[1]\n$pssm[2]\n$pssm[3]\n";
       close(TF);
#        system "cat $output/tfpssm.txt >> $output/tfpssms.txt";
    
}

sub WriteHtml(){
       my @align=@{$_[0]};
       my @fs=@{$_[1]};
       my $output=$_[2];
       my $html=$_[3];   
       my $left=1; 
 
foreach my $f (@fs){
      print  "$f\n";
   }
       
       system("if [ -e $output/$html ]; then rm $output/$html; fi");
       open(HTML,">$output/$html");
       print HTML "<html>\n<body>\n<table BORDER=1 align=\"center\" width=\"452\">\n";
       print HTML "<tr>\n";
       print HTML "<th width=\"50\" align=\"center\"><font size=\"2\">TFNAME</font></th>\n";
       print HTML "<th width=\"126\" align=\"center\"><font size=\"2\">TFPSSM<\/font><\/th>\n";
       print HTML "<th width=\"126\" align=\"center\"><font size=\"2\">PSSM<\/font><\/th>\n";
       print HTML "<th  width=\"50\" align=\"center\"><font size=\"2\">Latent Factor<\/font><\/th>\n";
       print HTML "<th  width=\"50\"  align=\"center\"><font size=\"2\">ALLR<\/font><\/th>\n";
       print HTML "<th  width=\"50\" align=\"center\"><font size=\"2\">Pvalue<\/font><\/th>\n";
       print HTML "</tr>\n";
       
       for my $count ( 0..$#align ){
           print HTML "<tr>\n";
           print HTML "<td width=\"50\" height=\"40\"  align=\"center\"><font size=\"2\">$align[$count][0]<\/font></td>\n";
           print HTML "<td  width=\"126\" height=\"40\" align=\"left\"><img src=\"tfpssm$align[$count][1]\.gif\" width=\"126\" height=\"40\"></td>\n";  
           print HTML "<td  width=\"126\" height=\"40\" align=\"left\"><img src=\"pssm$align[$count][2]\.gif\"  width=\"126\" height=\"40\" ></td>\n";  
         #
 #
    # 
 my $ruleid=$fs[$align[$count][2]];
 #   
        #
 # 
# my $ruleid=$align[$count][2]+1;
      print "$ruleid\n"; 
           print HTML "<td width=\"50\" height=\"40\"  align=\"center\"><font size=\"2\">$ruleid<\/font></td>\n";
           print HTML "<td width=\"50\" height=\"40\"  align=\"center\"><font size=\"2\">$align[$count][3]<\/font></td>\n";
           print HTML "<td width=\"50\" height=\"40\"  align=\"center\"><font size=\"2\">$align[$count][4]<\/font></td>\n";
           print HTML "</tr>\n";
           
    }
    
       print HTML "<\/table>\n<\/body>\n<\/html>\n";
       close(HTML);
}
