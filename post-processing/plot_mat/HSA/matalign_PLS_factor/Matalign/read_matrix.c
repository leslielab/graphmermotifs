/****************************************************************
 * MatrixAligner (version 2a)					*
 * Copyright (C) 2004, 2005					*
 * Washington University, St. Louis, MO, USA.			*
 * All Rights Reserved.						*
 * Author:							*
 *  Ting Wang							*
 *  Department of Genetics					*
 *  Washington University in St. Louis				*
 *  Campus Box 8232						*
 *  St. Louis MO 63110						*
 *								*
 *  twang@ural.wustl.edu					*
 ****************************************************************/

 
#include "matalign_options.h"


void     init_profiles();
PROFILE *make_mirror_profile( PROFILE * );

/* Initialize two profiles */
void init_profiles()
{
  int i;
  PROFILE *read_matrix();
  PROFILE *make_mirror_profile();

  profile = (PROFILE **)tw_calloc( 4,
  				   sizeof(PROFILE *),
				   "profile",
				   "init_profiles()" );

  profile[0] = read_matrix( file1, type1 ); 
  profile[1] = read_matrix( file2, type2 );
  profile[2] = make_mirror_profile( profile[1] );
  profile[3] = (PROFILE *)NULL;
  
  hsp = (HSP **)tw_calloc( 4,
  			   sizeof(HSP *),
  			   "hsp",
  			   "init_profiles()" );
  for ( i=0; i<4; ++i )
  {
    hsp[i] = (HSP *)NULL;
  }
  
}


/* Read matrix */
PROFILE *read_matrix( char *file, int type )
{
  PROFILE  *profile;
  COLUMN  **matrix;
  int     **int_matrix;
  double  **double_matrix;
  int      *dcode;
  int       length;
  int       num_seq;
  int       i, j;
  
  int     **read_int_matrix();
  double  **read_double_matrix();
  COLUMN   *alloc_column();
  
  /* Read in matrix */
  if ( type == 0 )
  {
    int_matrix = read_int_matrix( file, &length );
  }
  else if ( type == 1 )
  {
    double_matrix = read_double_matrix( file, &length );
  } 

  matrix = (COLUMN **)tw_calloc( length,
  			      	 sizeof(COLUMN *),
			      	 "matrix",
			      	 "read_matrix()" );
  dcode = (int *)tw_calloc( length,
  			    sizeof(int),
  			    "dcode",
  			    "read_matrix()" );
  
  /* Construct COLUMN **matrix */
  for ( i=0; i<length; ++i )
  {
    matrix[i] = alloc_column();    
    num_seq = 0;
    for ( j=0; j<A_size; ++j )
    {
      if ( type == 0 )
      {
        (matrix[i]->column)[j]    = int_matrix[j][i];
	(matrix[i]->frequency)[j] = 0.0;
	num_seq 		 += int_matrix[j][i];
      }
      else if ( type == 1 )
      {
        (matrix[i]->column)[j]    = 0;
	(matrix[i]->frequency)[j] = double_matrix[j][i];
	num_seq			  = Num_f;
      }
    }
    matrix[i]->num_seqs = num_seq;
    dcode[i] = -1;
  }
  
  /* Construct profile */
  profile = (PROFILE *)tw_calloc( 1, 
  				  sizeof(PROFILE),
				  "profile",
				  "read_matrix()" );
  profile->matrix     = matrix;
  profile->dcode      = dcode;
  profile->type       = type;
  profile->ALLR_score = 0;
  profile->width      = length;
  profile->peak_start = 0;
  profile->peak_width = length;
  profile->num_seq    = matrix[0]->num_seqs;
  profile->crude_info = 0;

  if ( type == 0 )
  {
    for ( i=0; i<A_size; ++i )
    {
      tw_free( int_matrix[i], "int_matrix[]", "read_matrix()" );
    }
    tw_free( int_matrix, "int_matrix", "read_matrix()" );
  }
  
  else if ( type == 1 )
  {
    for ( i=0; i<A_size; ++i )
    {
      tw_free( double_matrix[i], "double_matrix[]", "read_matrix()" );
    }
    tw_free( double_matrix, "double_matrix", "read_matrix()" );
  } 
  
  return ( profile );

}


/* Read an integer matrix */
int **read_int_matrix( char *file, int *ret_length )
{
  int   **matrix;
  FILE   *fp;
  char    buffer[MAXLINE];
  int     firstline[MAXLINE/10];
  char   *s;
  int     length;
  int     i;
  int     let_idx;
  
  /* Open matrix file */
  if ( ( fp=fopen( file, "r" ) ) == NULL )
  {
    fprintf( stderr, "Error in opening file %s\n", file );
    exit (1);
  }

  /* Get the length of the matrix and first letter index */
  length = -1; 		/* format: A | 10 ... */
  fgets ( buffer, MAXLINE, fp );
  s = strtok( buffer, " \t\n" );
  /* Find the letter of the first line */
  for ( i=0; i<A_size; ++i )
  {
    if ( toascii((int)s[0]) == A[i] )
    {
      let_idx = i;
      break;
    }
  }
  while ( ( s = strtok( NULL, " \t\n" ) ) != NULL )
  {
    length++;
    /* Record the first line, starting from '|'. */
    if ( length>0 )
    {
      firstline[length-1] = atoi( s );
    }
  }

  /* Allocate space for the counts */
  matrix = (int **)tw_calloc( A_size,
  			      sizeof( int * ),
			      "matrix",
			      "read_int_matrix()" );
  for ( i=0; i<A_size; ++i )
  {
    matrix[i] = (int *)tw_calloc( length,
    				  sizeof( int ),
				  "matrix[]",
				  "read_int_matrix()" );
  }
  
  /* Read one line */
  for ( i=0; i<length; ++i )
  {
    matrix[let_idx][i] = firstline[i];
  }

  while ( (fgets( buffer, MAXLINE, fp ) != NULL) && 
  	  ( (s = strtok( buffer, " \t\n" )) != NULL) )
  {
    for ( i=0; i<A_size; ++i )
    {
      if ( toascii((int)s[0]) == A[i] )
      {
        let_idx = i;
        break;
      }
    }

    for ( i=-1; i<length; ++i )
    {
      s = strtok( NULL, " \t\n" );
      
      if ( s!=NULL && s[0]!='|' )
      {
        matrix[let_idx][i] = atoi( s );
      }
    }
  }

  fclose (fp);
  
  /*
  free_error( buffer, "buffer", "read_int_matrix()" );
  free_error( firstline, "firstline", "read_int_matrix()" );
  */

  *ret_length = length;
 
  return ( matrix );
    
}  


/* Read a double matrix */
double ** read_double_matrix( char *file, int *ret_length )   
{
  double **matrix;
  FILE    *fp;
  char     buffer[MAXLINE];
  double   firstline[MAXLINE/10];
  char    *s;
  int      length;
  int      i;
  int      let_idx;
  
  /* Open matrix file */
  if ( ( fp=fopen( file, "r" ) ) == NULL )
  {
    fprintf( stderr, "Error in opening file %s\n", file );
    exit (1);
  }

  /* Get the length of the matrix and first letter index */
  length = -1; 		/* format: A | 10 ... */
  fgets ( buffer, MAXLINE, fp );
  s = strtok( buffer, " \t\n" );
  /* Find the letter of the first line */
  for ( i=0; i<A_size; ++i )
  {
    if ( toascii((int)s[0]) == A[i] )
    {
      let_idx = i;
      break;
    }
  }
  while ( ( s = strtok( NULL, " \t\n" ) ) != NULL )
  {
    length++;
    /* Record the first line, starting from '|'. */
    if ( length>0 )
    {
      firstline[length-1] = atof( s );
    }
  }

  /* Allocate space for the counts */
  matrix = (double **)tw_calloc( A_size,
  				 sizeof( double * ),
				 "matrix",
				 "read_int_matrix()" );
  for ( i=0; i<A_size; ++i )
  {
    matrix[i] = (double *)tw_calloc( length,
    				     sizeof( double ),
				     "matrix[]",
				     "read_int_matrix()" );
  }
  
  /* Read one line */
  for ( i=0; i<length; ++i )
  {
    matrix[let_idx][i] = firstline[i];
  }

  while ( (fgets( buffer, MAXLINE, fp ) != NULL) && 
  	  ( (s = strtok( buffer, " \t\n" )) != NULL) )
  {
    for ( i=0; i<A_size; ++i )
    {
      if ( toascii((int)s[0]) == A[i] )
      {
        let_idx = i;
        break;
      }
    }

    for ( i=-1; i<length; ++i )
    {
      s = strtok( NULL, " \t\n" );
      
      if ( s!=NULL && s[0]!='|' )
      {
        matrix[let_idx][i] = atof( s );
      }
    }
  }

  fclose (fp);
  
  /*
  free_error( buffer, "buffer", "read_int_matrix()" );
  free_error( firstline, "firstline", "read_int_matrix()" );
  */

  *ret_length = length;
 
  return ( matrix );
}


/* Allocate a column */
COLUMN *alloc_column()
{
  int     i;
  COLUMN *column; 

  column = (COLUMN *)tw_calloc( 1, 
  				sizeof(COLUMN), 
				"column", 
				"alloc_column()" );
  column->column = (int *)tw_calloc( A_size, 
  				     sizeof(int),
				     "column", 
				     "alloc_column()" );
  column->frequency = (double *)tw_calloc( A_size,
  					   sizeof(double),
					   "frequency",
					   "alloc_column()" );
  for ( i = 0; i < A_size; ++i ) 
  {
    (column->column)[i]    = 0;
    (column->frequency)[i] = 0.0;
  }
  column->num_seqs = 0;
  column->info     = 0.0;

  return(column);

}



PROFILE * make_mirror_profile( PROFILE *profile )
{
  PROFILE    *mirror;
  COLUMN    **matrix;
  COLUMN    **p_matrix;
  int         type;
  double      ALLR_score;
  int        *dcode;
  int         width;
  int         peak_start;
  int         peak_width;
  int         num_seq;
  double      crude_info;
  int         i, j;
  int         comp_l;
  int         comp_a;
  
  COLUMN     *alloc_column();
  
  p_matrix     = profile->matrix;
  width        = profile->width;
  peak_start   = width - profile->peak_start - profile->peak_width;
  peak_width   = profile->peak_width;
  num_seq      = profile->num_seq;
  ALLR_score   = profile->ALLR_score;
  type         = profile->type;

  mirror = (PROFILE *)tw_malloc( sizeof(PROFILE), 
  				 "mirror", 
  				 "make_mirror_profile()" );
  
  matrix = (COLUMN **)tw_calloc( width,
  				 sizeof(COLUMN *),
				 "matrix",
				 "make_mirror_profile()" );
  
  dcode = (int *)tw_calloc( width, 
  			    sizeof(int),
  			    "dcode",
  			    "make_mirror_profile()" );
  
  for ( i=0; i<width; ++i )
  {
    comp_l = width - i - 1;
    matrix[i] = alloc_column();
    for ( j=0; j<A_size; ++j )
    {
      comp_a = A_comp[j];
      (matrix[i]->column)[j]    = (p_matrix[comp_l]->column)[comp_a];
      (matrix[i]->frequency)[j] = (p_matrix[comp_l]->frequency)[comp_a];
    }
    //matrix[i]->info = cal_info( matrix[i]->frequency );
    matrix[i]->info = 0;
    matrix[i]->num_seqs = p_matrix[comp_l]->num_seqs;
    dcode[i] = -1;
  }
  
  crude_info = 0;

  mirror->type          = type;
  mirror->ALLR_score    = ALLR_score;
  mirror->matrix        = matrix;
  mirror->width         = width;
  mirror->peak_start    = peak_start;
  mirror->peak_width    = peak_width;
  mirror->num_seq       = num_seq;
  mirror->crude_info    = crude_info;
  mirror->dcode         = dcode;
  
  return mirror;
}

