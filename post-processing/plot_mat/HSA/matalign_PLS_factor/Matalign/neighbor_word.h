/****************************************************************
 * PhyloNet (version 2b)					*
 * Copyright (C) 2004, 2005 					*
 * Washington University, St. Louis, MO, USA.			*
 * All Rights Reserved.						*
 * Author:							*
 *  Ting Wang							*
 *  Department of Genetics					*
 *  Washington University in St. Louis				*
 *  Campus Box 8232						*
 *  St. Louis MO 63110						*
 *								*
 *  twang@ural.wustl.edu					*
 ****************************************************************/

/* Declaration of variables and functions for generating
 * neighborhood word hash.
 */


extern int     C_size; /* Degenerate Code (new alphabet) size */
extern int     W_size; /* Length of each neighbourhood word */
extern char*   Word_seed;   /* Position seed */
extern int     Word_len;
extern int   **All_word;
extern int     All_word_size;

extern int     Word_threshold;

extern int         Hash_size;
extern int         Hash_num;
extern HASH_NODE **WordHash;

extern char  Dcode[15];
extern int   SM1[15][15];
extern int   SM2[15][15];


extern void build_word_index();

extern void       init_hash();
extern void       init_all_word();
extern void       free_all_word();
extern HASH_NODE *alloc_HASH_NODE();
extern void       check_word_seed();
extern void       index_promoter( PROMOTER * );
extern void       hash_insert( int *, int );
extern int        hash_function( int * );
extern HASH_NODE *hash_find( int * );
extern void       print_hash_table();
extern void       index_neighbors( int *, int );
extern int        align_word( int *, int * );
extern void       print_word( int *, int );


/**************************
 * End of neighbor_word.h *
 **************************/

