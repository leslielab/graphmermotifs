/****************************************************************
 * MatrixAligner (version 2a)					*
 * Copyright (C) 2003, 2004, 2005                               *
 * Washington University, St. Louis, MO, USA.                   *
 * All Rights Reserved.						*
 * Author:							*
 *  Ting Wang                                                   *
 *  Laboratory of Dr. Gary D. Stormo	                        *
 *  Department of Genetics					*
 *  Washington University in St. Louis				*
 *  Campus Box 8232						*
 *  St. Louis MO 63110						*
 *								*
 *  twang@ural.wustl.edu					*
 ****************************************************************/


#include "matalign_options.h"

void compare_profiles();

void     complete_profile( PROFILE * );
HSP     *compare_2_profiles( PROFILE *, PROFILE * );
PROFILE *merge_HSP( HSP * );
void     print_profile( PROFILE * );
void     print_HSP( HSP * );
double   get_ALLR_E_value ( double );
double   e_value_to_p_value( double );


void compare_profiles()
{
  complete_profile( profile[0] );
  complete_profile( profile[1] );
  complete_profile( profile[2] );
  
  hsp[0] = compare_2_profiles( profile[0], profile[0] );
  hsp[1] = compare_2_profiles( profile[1], profile[1] );
  hsp[2] = compare_2_profiles( profile[0], profile[1] );
  if ( Comp_status )
  {
    hsp[3] = compare_2_profiles( profile[0], profile[2] );
  }
  if ( Comp_status && ( hsp[3]->hsp_score>hsp[2]->hsp_score ) )
  {
    print_HSP( hsp[3] );
    profile[3] = merge_HSP( hsp[3] );
    ALLR = hsp[3]->hsp_score;
    Dist = hsp[0]->hsp_score + hsp[1]->hsp_score - 2*hsp[3]->hsp_score;
  }
  else
  {
    print_HSP( hsp[2] );
    profile[3] = merge_HSP( hsp[2] );
    ALLR = hsp[2]->hsp_score;
    Dist = hsp[0]->hsp_score + hsp[1]->hsp_score - 2*hsp[2]->hsp_score;
  }
  
  M = profile[0]->width;
  N = profile[1]->width;
  E_value = get_ALLR_E_value( ALLR*100 );
  P_value = e_value_to_p_value( E_value );
  printf ( "Comparison scores:\n" );
  printf ( "ALLR = %.4f\n", ALLR );
  printf ( "Distance = %.4f\n", Dist );
  printf ( "E_value = %.4g\n", E_value );
  printf ( "P_value = %.4g\n", P_value );
  print_profile ( profile[3] );
  
}


/* Compare two profiles, generate one HSP */
HSP *compare_2_profiles( PROFILE *profile_1, PROFILE *profile_2 )
{
  /* Variables to hold information about 2 profiles
   */
  COLUMN  **matrix_1;
  COLUMN  **matrix_2;
  int       width_1;
  int       width_2;
  int       num_seq_1;
  int       num_seq_2;
  int       i;
  
  /* Variables to hold scores 
   */
  double  **score;
  double  **dp_score;
  
  /* Variables to hold information about HSPs 
   */
  HSP     *hsp;
  
  double **score_function();
  double **DP_function();
  double **DP_function_g();
  HSP    **trace_back();
  HSP    **trace_back_g();
  void     free_scores();
  
  matrix_1   = profile_1->matrix;
  width_1    = profile_1->width;
  num_seq_1  = profile_1->num_seq;
  
  matrix_2   = profile_2->matrix;
  width_2    = profile_2->width;
  num_seq_2  = profile_2->num_seq;

  /* Calculate pairwise comparison scores */
  score = score_function( matrix_1, width_1, matrix_2, width_2 );
  
  /* Dynamic programming function */
  if ( Global == 0 )
  {
    dp_score = DP_function( matrix_1, width_1, matrix_2, width_2, score );
  }
  else
  {
    dp_score = DP_function_g( matrix_1, width_1, matrix_2, width_2, score );
  }
  
  /* HSP generated from this comparison */
  if ( Global == 0 )
  {
    hsp = trace_back( dp_score, score, width_1, width_2 );
  }
  else
  {
    hsp = trace_back_g( dp_score, score, width_1, width_2 );
  }
  hsp->profile_1 = profile_1;
  hsp->profile_2 = profile_2;

  /* Free the space used for comparison */
  free_scores( score, width_1, width_2 );
  free_scores( dp_score, width_1, width_2 );
  
  return hsp;
  
}  


/* Calculate pair-wise position score */
double **score_function( 
            COLUMN **matrix_1, 
	    int      m, 
	    COLUMN **matrix_2, 
	    int      n 
	    )
{
  int      i, j;
  double **STscore;
  
  double   ALLR_function();
  
  STscore = (double **)tw_calloc( m, 
                                  sizeof(double *),
				  "STscore",
				  "score_function()" );

  for ( i=0; i<m; ++i )
  {
    STscore[i] = (double *)tw_calloc( n,
    				      sizeof(double),
				      "STscore[]",
				      "score_function()" );
  }
    
  for ( i=0; i<m; ++i )
  {
    for ( j=0; j<n; ++j )
    {  
      STscore[i][j] = ALLR_function( matrix_1[i]->frequency, 
      				     matrix_1[i]->num_seqs,
				     matrix_2[j]->frequency,
				     matrix_2[j]->num_seqs );
    }
  }
  return ( STscore );
}


/* Dynamic programming function */
double **DP_function(  
            COLUMN **matrix_1, 
	    int      m, 
	    COLUMN ** matrix_2, 
	    int      n,
	    double **score 
	    )
{
  int        i, j;
  double     s = 0;
  double   **DP;
  
  DP = (double **)tw_calloc( m, sizeof(double *), "DP", "DP_function" );
  
  for ( i=0; i<m; ++i )
  {
    DP[i] = (double *)tw_calloc( n,
    				 sizeof(double),
				 "DP[]",
				 "DP_function()" );
  }     
  
  for ( i=0; i<m; ++i )
  {
    DP[i][0] = (score[i][0]>=0) ? score[i][0] : 0;
  }
  for ( j=0; j<n; ++j )
  {
    DP[0][j] = (score[0][j]>=0) ? score[0][j] : 0;
  }
  
  for ( i=1; i<m; ++i )
  {
    for ( j=1; j<n; ++j )
    {
      s = DP[i-1][j-1] + score[i][j];
      DP[i][j] = (s>=0) ? s : 0;
    }
  }
  return ( DP );
}

/* Global alignment version */
double **DP_function_g(  
            COLUMN **matrix_1, 
	    int      m, 
	    COLUMN **matrix_2, 
	    int      n,
	    double **score 
	    )
{
  int        i, j;
  double     s = 0;
  double   **DP;
  
  DP = (double **)tw_calloc( m, sizeof(double *), "DP", "DP_function" );
  
  for ( i=0; i<m; ++i )
  {
    DP[i] = (double *)tw_calloc( n,
    				 sizeof(double),
				 "DP[]",
				 "DP_function()" );
  }     
  
  for ( i=0; i<m; ++i )
  {
    DP[i][0] = score[i][0]; //(score[i][0]>=0) ? score[i][0] : 0;
  }
  for ( j=0; j<n; ++j )
  {
    DP[0][j] = score[0][j]; //(score[0][j]>=0) ? score[0][j] : 0;
  }
  
  for ( i=1; i<m; ++i )
  {
    for ( j=1; j<n; ++j )
    {
      s = DP[i-1][j-1] + score[i][j];
      DP[i][j] = s;
    }
  }
  return ( DP );
}


/* Trace back */
HSP *trace_back( double **DP, double **score, int m, int n )
{
  int       i, j;
  double    max;
  int       i_start, j_start;
  int       i_end, j_end;
  HSP      *hsp;
  
  hsp = (HSP *)tw_calloc( 1, sizeof(HSP), "hsp", "trace_back()" );
  
  max = 0;
  
  for ( i=0; i<m; ++i )
  {
    for ( j=0; j<n; ++j )
    {
      if ( DP[i][j]>=max )
      {
        max = DP[i][j];
	i_end = i;
	j_end = j;
      }
    }
  }

  i = i_end;
  j = j_end;
  
  while ( (i>0) && (j>0) )
  {
    if ( DP[i][j]>0 )
    {
      --i;
      --j;
    }
    else
    {
      ++i;
      ++j;
      break;
    }
  }
  
  i_start = i;
  j_start = j;
  
  hsp->profile_1 = NULL;
  hsp->profile_2 = NULL;
  hsp->hsp_score = max;
  hsp->m = m;
  hsp->n = n;
  hsp->i_start = i_start;
  hsp->j_start = j_start;
  hsp->i_end = i_end;
  hsp->j_end = j_end;
  hsp->length = i_end - i_start + 1;
  
  hsp->match = (int *)tw_calloc( hsp->length,
  				 sizeof(int),
  				 "hsp->match",
  				 "trace_back()" );
  for ( i=0; i<hsp->length; ++i )
  {
    if ( score[ i_start+i ][ j_start+i ] >= 0 )
    {
      hsp->match[i] = 1;
    }
    else
    {
      hsp->match[i] = 0;
    }
  }
  
  return hsp;
}

/* Trace back, global version */
HSP *trace_back_g( double **DP, double **score, int m, int n )
{
  int       i, j;
  double    max;
  int       i_start, j_start;
  int       i_end, j_end;
  HSP      *hsp;
  
  hsp = (HSP *)tw_calloc( 1, sizeof(HSP), "hsp", "trace_back()" );
  
  max = DP[m-1][n-1];
  i_end = m-1;
  j_end = n-1;
  for ( i=0; i<m; ++i )
  {
    if ( DP[i][n-1] >= max )
    {
      max = DP[i][n-1];
      i_end = i;
      j_end = n-1;
    }
  }
  for ( j=0; j<n; ++j )
  {
    if ( DP[m-1][j] >= max )
    {
      max = DP[m-1][j];
      i_end = m-1;
      j_end = j;
    }
  }
  
  i = i_end;
  j = j_end;
  
  while ( (i>0) && (j>0) )
  {
    --i;
    --j;
  }
  
  i_start = i;
  j_start = j;
  
  
  hsp->profile_1 = NULL;
  hsp->profile_2 = NULL;
  hsp->hsp_score = max;
  hsp->m = m;
  hsp->n = n;
  hsp->i_start = i_start;
  hsp->j_start = j_start;
  hsp->i_end = i_end;
  hsp->j_end = j_end;
  hsp->length = i_end - i_start + 1;
  
  hsp->match = (int *)tw_calloc( hsp->length,
  				 sizeof(int),
  				 "hsp->match",
  				 "trace_back_g()" );
  for ( i=0; i<hsp->length; ++i )
  {
    if ( score[ i_start+i ][ j_start+i ] > 0 )
    {
      hsp->match[i] = 1;
    }
    else
    {
      hsp->match[i] = 0;
    }
  }
  
  return hsp;
}


/* Free score matrix space */
void free_scores( double **score, int m, int n )
{
  int i, j;
  for ( i=0; i<m; ++i )
  {
    tw_free ( score[i], "score[]", "free_scores()" );
  }
  tw_free( score, "score", "free_scores()" );
}    


/* Create a profile based on a HSP */     
PROFILE *merge_HSP( HSP * hsp )
{
  int        i, i1, i2, j;
  PROFILE   *new_profile;
  PROFILE   *profile_1;
  PROFILE   *profile_2;
  int        pre_extend;
  int        post_extend;
  int        width;
  int        peak_start;
  int        peak_width;
  int        num_seq;
  double     crude_info;
  COLUMN   **matrix;
  int       *dcode;


  COLUMN    *alloc_column();
  //PROFILE   *make_mirror_profile();
  void       count_2_frequency();
  double     cal_info();

  new_profile = (PROFILE *)tw_calloc( 1, 
  				      sizeof(PROFILE),
                                      "new_profile", 
				      "merge_HSP()" );
  
  profile_1   = hsp->profile_1;
  profile_2   = hsp->profile_2;
  					  
  pre_extend  = ( hsp->i_start <= hsp->j_start ) ? hsp->i_start : hsp->j_start;
  post_extend = ( (hsp->m - hsp->i_end) <= (hsp->n - hsp->j_end) ) ? 
                  (hsp->m - hsp->i_end - 1) : (hsp->n - hsp->j_end - 1);
  
  pre_extend  = ( pre_extend <= 12 ) ? pre_extend : 12;
  post_extend = ( post_extend <= 12 ) ? post_extend : 12;
  
  peak_width  = hsp->length;
  peak_start  = pre_extend;
  
  width       = hsp->length + pre_extend + post_extend;
  
  num_seq     = profile_1->num_seq + profile_2->num_seq;
  
  matrix = (COLUMN **)tw_calloc( width, 
  				 sizeof(COLUMN *),
                                 "int_matrix", 
				 "merge_HSP()" );
  
  dcode = (int *)tw_calloc( width,
  			    sizeof(int),
  			    "dcode",
  			    "merge_HSP()" );
  
  for ( i = 0, i1 = hsp->i_start-pre_extend, i2 = hsp->j_start-pre_extend;
        i<width; 
	++i, ++i1, ++i2 )
  {
    matrix[i] = alloc_column();
    
    matrix[i]->num_seqs = (profile_1->matrix)[i1]->num_seqs +
    			  (profile_2->matrix)[i2]->num_seqs;

    for ( j=0; j<A_size; ++j )
    {
      (matrix[i]->column)[j] = ((profile_1->matrix)[i1]->column)[j] +
                               ((profile_2->matrix)[i2]->column)[j];
    }
    count_2_frequency( matrix[i]->column, 
    		       matrix[i]->frequency,
		       matrix[i]->num_seqs );      
    matrix[i]->info = cal_info( matrix[i]->frequency );
    dcode[i] = map_column_dcode( matrix[i]->frequency );
  }
  
  crude_info = 0.0;
  for ( i=peak_start; i<peak_start+peak_width; ++i )
  {
    crude_info += matrix[i]->info;
  }

  new_profile->type         = 0;
  new_profile->ALLR_score   = hsp->hsp_score;
  new_profile->matrix       = matrix;
  new_profile->width        = width;
  new_profile->peak_start   = peak_start;
  new_profile->peak_width   = peak_width;
  new_profile->num_seq      = num_seq;
  new_profile->crude_info   = crude_info;
  new_profile->dcode        = dcode;
  
  return new_profile;
}


/* Complete a profile record */
void complete_profile( PROFILE *profile )
{
  int      i;
  
  void     count_2_frequency();
  void     frequency_2_count();
  int      map_column_dcode( double * );
  double   cal_info();
  
  if ( profile->type == 0 )
  {
    for ( i=0; i<profile->width; ++i )
    {
      count_2_frequency( (profile->matrix[i])->column, 
    		         (profile->matrix[i])->frequency,
			 (profile->matrix[i])->num_seqs );
    }
  }
  else if ( profile->type == 1 )
  {
    for ( i=0; i<profile->width; ++i )
    {
      frequency_2_count( (profile->matrix[i])->frequency,
      			 (profile->matrix[i])->column,
			 (profile->matrix[i])->num_seqs );
      count_2_frequency( (profile->matrix[i])->column, 
    		         (profile->matrix[i])->frequency,
			 (profile->matrix[i])->num_seqs );
      
    }
  }
  
  profile->crude_info = 0;
  for ( i=0; i<profile->width; ++i )
  {
    (profile->matrix[i])->info = 
    		cal_info( (profile->matrix[i])->frequency );
    profile->dcode[i] = map_column_dcode( (profile->matrix[i])->frequency );
    profile->crude_info += (profile->matrix[i])->info;
  }
  
}				    	  


void count_2_frequency( int *count, double *frequency, int num_seq )
{
  int    i;
  double pseudo = 1.0;
  
  for ( i=0; i<A_size; ++i )
  {
    frequency[i] = ( count[i] + P[i]*(double)pseudo )
    		   / (double)(num_seq+pseudo);
  }
}


void frequency_2_count( double *frequency, int *count, int num_seq )
{
  int  i;
  int  to_nearest_int();  
  
  for ( i=0; i<A_size; ++i )
  {
    count[i] = to_nearest_int( num_seq*frequency[i] );
  }
}


double cal_info( double *frequency )
{
  int i;
  double info;
  
  info = 0.0;
  for ( i=0; i<A_size; ++i )
  {
    info += frequency[i]*( log (frequency[i]/P[i]) )/LN2;
  }
  
  return info;
}


int to_nearest_int( double d )
{
  int i;
  int floor;
  
  floor = (int)d;
  
  if ( fabs( d-floor ) < 0.5 )
  {
    i = d;
  }
  else
  {
    i = d+1;
  }
  return ( i );
}
  

  


void print_profile( PROFILE * profile )
{
  int        i, j;

  COLUMN   **mat_matrix;
  double     ALLR_score;
  int        width;
  int        peak_start;
  int        peak_width;
  int        num_seq;
  double     crude_info;
  

  mat_matrix = profile->matrix;
  ALLR_score = profile->ALLR_score;
  width      = profile->width;
  peak_start = profile->peak_start;
  peak_width = profile->peak_width;
  num_seq    = profile->num_seq;
  crude_info = profile->crude_info;
  
  //printf ( "ALLR score: %.4f\n", ALLR_score );
  printf ( "NEW MATRIX:\n" );
  printf ( "number of sequences = %d\n", num_seq );
  printf ( "width = %d\n", peak_width );
  printf ( "crude information = %.4f (bits)\n", crude_info );

  if ( Comp_flag != 1 )
  {
    for ( i=0; i<A_size; i++ )
    {
      (Ascii == YES) ? printf ( "%c |", A[i] ) : printf ( "%3d", A[i] );
      for ( j=peak_start; j<peak_start+peak_width; ++j )
      {
        printf ( " %3d", (mat_matrix[j]->column)[i] );
      }
      printf ( "\n" );
    }
  }
  /* If alphabet has complements, and no letter is its own complement,
   * rearrange rows so that the alphabet is symmetrical. */
  else
  {
    for ( i=0; i<A_size; i+=2 )
    {
      /* Print the current row. */
      (Ascii == YES) ? printf( "%c |", A[i] ) : printf( "%3d |", A[i] );
      for ( j=peak_start; j<peak_start+peak_width; ++j )
      {
        printf( " %3d", (mat_matrix[j]->column)[i] );
      }
      printf( "\n" );
    }
    for ( i=A_size-1; i>0; i-=2 )
    {
      /* Print the current row. */
      (Ascii == YES) ? printf( "%c |", A[i] ) : printf( "%3d |", A[i] );
      for ( j=peak_start; j<peak_start+peak_width; ++j )
      {
        printf( " %3d", (mat_matrix[j]->column)[i] );
      }
      printf("\n");
    }
  }
  printf ( "Consensus: " );
  for ( i=peak_start; i<peak_start+peak_width; ++i )
  {
    printf ( "%c", Dcode[ profile->dcode[i] ] );
  }
  printf ( "\n" );
}  


void print_HSP (HSP *hsp )
{
  int      i;
  PROFILE *profile_1;
  PROFILE *profile_2;
  int      indent_1 = 0;
  int      indent_2 = 0;
  int      indent   = 0;
  int      length_1;
  int      length_2; 

  if ( hsp->i_start >= hsp->j_start )
  {
    indent_2 = hsp->i_start - hsp->j_start;
  }
  else
  {
    indent_1 = hsp->j_start - hsp->i_start;
  }
  indent = hsp->i_start + indent_1;
  
  profile_1 = hsp->profile_1;
  profile_2 = hsp->profile_2;
  length_1=hsp->m;
  length_2=hsp->n;
   
  printf ( "Matrices Alignment:\n" );
  
  print_MATRIX ( profile_1, indent_1 );
  //printf ( "\n" );
  printf ( "MATCH:     " );
  print_indent( indent );
  for ( i=0; i<hsp->length; ++i )
  {
    if ( hsp->match[i] == 0 )
    {
      printf ( "%6s", "x" );
    }
    else if ( hsp->match[i] == 1 )
    {
      printf ( "%6s", "|" );
    }
  }
  printf ( "\n" );
  print_MATRIX ( profile_2, indent_2 );
  print_BOTH(profile_1,profile_2,indent_1,indent_2,length_1,length_2); 
}

void print_BOTH(PROFILE *profile_1, PROFILE *profile_2, int indent_1, int indent_2, int length_1, int length_2)
{ int i,j;  
  COLUMN **mat_matrix1;
  COLUMN **mat_matrix2;
  mat_matrix1=profile_1->matrix;
  mat_matrix2=profile_2->matrix;
  int sum;
  int L[4];

  for (i=0;i<A_size;i++)
  {  if( A[i]=='A')
     { L[0]=i;}
  }
  for (i=0;i<A_size;i++)
  {  if( A[i]=='C')
     { L[1]=i;}
  }
  for (i=0;i<A_size;i++)
  {  if( A[i]=='G')
     { L[2]=i;}
  }
  for (i=0;i<A_size;i++)
  {  if( A[i]=='T')
     { L[3]=i;}
  }
  
  printf("Merged PSSM:\n");
  if(indent_1>0)
   { for ( i=0;i<A_size;i++) 
     {  
//        printf ( "%c |", A[L[i]] );
        for ( j=0; j<indent_1; ++j )
         {printf ( " %5d", (mat_matrix2[j]->column)[L[i]]*2);}
        
        if(indent_1+length_1>length_2)
        {  for ( j=indent_1;j<length_2;++j)
            {printf ( " %5d", (mat_matrix2[j]->column)[L[i]]+(mat_matrix1[j-indent_1]->column)[L[i]] );}
          for ( j=length_2;j<(length_1+indent_1);++j)
          {printf ( " %5d", (mat_matrix1[j-indent_1]->column)[L[i]]*2);}
           printf ( "\n" );
        } 
 
        else
        {  for ( j=indent_1;j<(length_1+indent_1);++j)
            {printf ( " %5d", (mat_matrix2[j]->column)[L[i]]+(mat_matrix1[j-indent_1]->column)[L[i]] );}
          for ( j=(length_1+indent_1);j<length_2;++j)
          {printf ( " %5d", (mat_matrix2[j]->column)[L[i]]*2);}
           printf ( "\n" );
        } 
          
    } 
   }  

  else
   { for ( i=0;i<A_size;i++) 
     {
//      printf ("%c |", A[L[i]] ); 
        for ( j=0; j<indent_2; ++j )
         {printf ( " %5d", (mat_matrix1[j]->column)[L[i]]*2);}
       
        if(indent_2+length_2>length_1)
          {for ( j=indent_2;j<length_1;++j)
            {printf ( " %5d", (mat_matrix1[j]->column)[L[i]]+(mat_matrix2[j-indent_2]->column)[L[i]] );}
          for ( j=length_1;j<(length_2+indent_2);++j)
            {printf ( " %5d", (mat_matrix2[j-indent_2]->column)[L[i]]*2);}
            printf ( "\n" );
          } 
       
         else 
          {for ( j=indent_2;j<(length_2+indent_2);++j)
            {printf ( " %5d", (mat_matrix1[j]->column)[L[i]]+(mat_matrix2[j-indent_2]->column)[L[i]] );}
          for ( j=(length_2+indent_2);j<length_1;++j)
            {printf ( " %5d", (mat_matrix1[j]->column)[L[i]]*2);}
            printf ( "\n" );
          } 
    } 
   }  

}

void print_MATRIX( PROFILE *profile, int indent )
{
  int i, j;
  int peak_start;
  int peak_width;
  COLUMN **mat_matrix;
  
  void print_indent( int );
  
  
  peak_start = profile->peak_start;
  peak_width = profile->peak_width;
  mat_matrix = profile->matrix;
  printf ( "MATRIX: " );
  
  if ( Comp_flag != 1 )
  {
    for ( i=0; i<A_size; i++ )
    {
      if ( i!=0 )
      {
        printf ( "        " );
      }
      print_indent( indent );
      printf ( "%c |", A[i] );
      for ( j=peak_start; j<peak_start+peak_width; ++j )
      {
        printf ( " %5d", (mat_matrix[j]->column)[i] );
      }
      printf ( "\n" );
    }
  }
  /* If alphabet has complements, and no letter is its own complement,
   * rearrange rows so that the alphabet is symmetrical. */
  else
  {
    for ( i=0; i<A_size; i+=2 )
    {
      if ( i!=0 )
      {
        printf ( "        " );
      }
      print_indent( indent );
      printf( "%c |", A[i] );
      for ( j=peak_start; j<peak_start+peak_width; ++j )
      {
        printf( " %5d", (mat_matrix[j]->column)[i] );
      }
      printf( "\n" );
    }
    for ( i=A_size-1; i>0; i-=2 )
    {
      if ( i!=0 )
      {
        printf ( "        " );
      }
      print_indent( indent );
      printf( "%c |", A[i] );
      for ( j=peak_start; j<peak_start+peak_width; ++j )
      {
        printf( " %5d", (mat_matrix[j]->column)[i] );
      }
      printf("\n");
    }
  }
  
  printf ( "Consensus: " );
  print_indent( indent );
  for ( i=peak_start; i<peak_start+peak_width; ++i )
  {
    printf ( " %5c", Dcode[ profile->dcode[i] ] );
  }
  printf ( "\n" );
  
}




void print_indent( int indent )
{
  int i;
  for ( i=0; i<indent; ++i )
  {
    printf ( "%6s", " " );
  }
}
  
