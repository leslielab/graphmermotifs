/****************************************************************
 * MatrixAligner (version 2a)					*
 * Copyright (C) 2003, 2004, 2005                               *
 * Washington University, St. Louis, MO, USA.                   *
 * All Rights Reserved.						*
 * Author:							*
 *  Ting Wang                                                   *
 *  Laboratory of Dr. Gary D. Stormo	                        *
 *  Department of Genetics					*
 *  Washington University in St. Louis				*
 *  Campus Box 8232						*
 *  St. Louis MO 63110						*
 *								*
 *  twang@ural.wustl.edu					*
 ****************************************************************/

#include "matalign_options.h"

/* The text of the directions. */
void text_directions(FILE *fp)
{
  fprintf ( fp, "###########################\n" ); 
  fprintf ( fp, "# README FOR MATALIGN-V2A #\n" ); 
  fprintf ( fp, "###########################\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "Copyright 2002--2005 Ting Wang and Gary Stormo\n" ); 
  fprintf ( fp, "May be copied for noncommercial purposes.\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "Author:\n" ); 
  fprintf ( fp, "  Ting Wang and Gary Stormo\n" ); 
  fprintf ( fp, "  Department of Genetics\n" ); 
  fprintf ( fp, "  Washington University in St. Louis\n" ); 
  fprintf ( fp, "  Campus Box 8232\n" ); 
  fprintf ( fp, "  St. Louis, MO 63110\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "  stormo@ural.wustl.edu\n" ); 
  fprintf ( fp, "  twang@ural.wustl.edu\n" ); 
  fprintf ( fp, "  \n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "MatrixAligner-v2a\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "#################\n" ); 
  fprintf ( fp, "# BASIC OPTIONS #\n" ); 
  fprintf ( fp, "#################\n" ); 
    fprintf ( fp, "\n" ); 
  fprintf ( fp, "-f1 <Name of matrix file 1>\n" ); 
  fprintf ( fp, "-f2 <Name of matrix file 2>\n" ); 
  fprintf ( fp, "-t1 <Type of matrix 1 (0: count matrix; 1: frequency matrix)>\n" ); 
  fprintf ( fp, "-t2 <Type of matrix 2 (0: count matrix; 1: frequency matrix)>\n" ); 
  fprintf ( fp, "[-h <Print directions>]\n" ); 
  fprintf ( fp, "[-n <Number of sequence assumed for frequency matrix, (default: 10)>]\n" ); 
  fprintf ( fp, "[-c0 <Compare only the forward orientation>]\n" ); 
  fprintf ( fp, "[-c1 <Compare both orientations (default)>]\n" ); 
  fprintf ( fp, "[-g <Global alignment, (default: 0, for local alignment)>]\n" ); 
  fprintf ( fp, "[-a <Name of ascii alphabet file>]\n" ); 
  fprintf ( fp, "[-A <Ascii alphabet information on the command line>]\n" ); 
  fprintf ( fp, "[-CS <Ascii alphabet is case sensitive (default: ascii alphabets are case insensitive)>]\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "#######################\n" ); 
  fprintf ( fp, "# GENERAL INFORMATION #\n" ); 
  fprintf ( fp, "#######################\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "Matrix Aligner is a program to compare two positional specific matrices.\n" ); 
  fprintf ( fp, "The precursor of this program is \"CompareTwo\". Matalign-v2a made several \n" ); 
  fprintf ( fp, "improvements over the previous establishment of the program.\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "Two input PSSMs should be formatted according to consensus output. They \n" ); 
  fprintf ( fp, "can be either count matrices, or frequency matrices. If a frequency matrix\n" ); 
  fprintf ( fp, "is used, a user-defined \"total count\" is assumed (default = 10). \n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "The scoring function between two positions of the two matrices is ALLR\n" ); 
  fprintf ( fp, "statistic. The alignment algorithm can be either \"local alignment\" or \n" ); 
  fprintf ( fp, "\"global alignment\". Both options do not allow gaps.\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "The comparison between the two matrices results in the following output:\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "1) An alignment between two input matrices. \n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "2) An ALLR score of the comparison. In general, the higher the ALLR score,\n" ); 
  fprintf ( fp, "   the more similar are the two matrices.\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "3) A distance score. Distance between A and B is defined as:\n" ); 
  fprintf ( fp, "   ALLR(A,A) + ALLR(B,B) - 2xALLR(A,B).\n" ); 
  fprintf ( fp, "   In general, the smaller the distance, the more similar are the two\n" ); 
  fprintf ( fp, "   matrices.\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "4) P-value and E-value of the observed ALLR score. These significance values\n" ); 
  fprintf ( fp, "   are calculated based on Karlin-Altschul statistics. The meaning of the \n" ); 
  fprintf ( fp, "   p-value is, given two random PSSMs, the probability of observing an \n" ); 
  fprintf ( fp, "   equal or higher ALLR score.\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "5) The aligned parts of the two matrices will be merged into one new \n" ); 
  fprintf ( fp, "   matrix and printed as output.\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "########################\n" ); 
  fprintf ( fp, "# COMMAND LINE OPTIONS #\n" ); 
  fprintf ( fp, "########################\n" ); 
  fprintf ( fp, "\n" ); 
    fprintf ( fp, " 0) -h: print these directions.\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, " 1) Required options\n" ); 
  fprintf ( fp, "    -f1 filename\n" ); 
  fprintf ( fp, "        \"filename\" contains the one matrix.\n" ); 
  fprintf ( fp, "    \n" ); 
  fprintf ( fp, "    -f2 filename\n" ); 
  fprintf ( fp, "       \"filename\" contains the second matrix.\n" ); 
  fprintf ( fp, "    \n" ); 
  fprintf ( fp, "    -t1 integer: the type of matrix 1. 0 means count matrix, 1 means\n" ); 
  fprintf ( fp, "                 frequency matrix.\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "    -t2 integer: the type of matrix 1. 0 means count matrix, 1 means\n" ); 
  fprintf ( fp, "                 frequency matrix.\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, " 2) Alphabet options\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "    -a filename: file containing the alphabet and normalization \n" ); 
  fprintf ( fp, "       information.\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "       Each line contains a letter (a symbol in the alphabet) followed \n" ); 
  fprintf ( fp, "       by an optional normalization number (default: 1.0).  The \n" ); 
  fprintf ( fp, "       normalization is based on the relative prior probabilities of \n" ); 
  fprintf ( fp, "       the letters.  For nucleic acids, this might be be the genomic \n" ); 
  fprintf ( fp, "       frequency of the bases; however, if the \"-d\" option is not used, \n" ); 
  fprintf ( fp, "       the frequencies observed in your own sequence data are used.  \n" ); 
  fprintf ( fp, "       In nucleic acid alphabets, a letter and its complement appear \n" ); 
  fprintf ( fp, "       on the same line, separated by a colon (a letter can be its own \n" ); 
  fprintf ( fp, "       complement, e.g. when using a dimer alphabet). Complementary \n" ); 
  fprintf ( fp, "       letters may use the same normalization number.  Only the standard \n" ); 
  fprintf ( fp, "       26 letters are permissible; however, when the \"-CS\" option is\n" ); 
  fprintf ( fp, "       used, the alphabet is case sensitive so that a total of 52 \n" ); 
  fprintf ( fp, "       different characters are possible.\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "       POSSIBLE LINE FORMATS WITHOUT COMPLEMENTARY LETTERS:\n" ); 
  fprintf ( fp, "       letter\n" ); 
  fprintf ( fp, "       letter normalization\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "       POSSIBLE LINE FORMATS WITH COMPLEMENTARY LETTERS:\n" ); 
  fprintf ( fp, "       letter:complement\n" ); 
  fprintf ( fp, "       letter:complement normalization\n" ); 
  fprintf ( fp, "       letter:complement normalization:complement's_normalization\n" ); 
  fprintf ( fp, "       \n" ); 
  fprintf ( fp, "       Example alphabet file 1:\n" ); 
  fprintf ( fp, "           A:T\n" ); 
  fprintf ( fp, "           C:G\n" ); 
  fprintf ( fp, "       Example alphabet file 2:\n" ); 
  fprintf ( fp, "           A:T 0.3\n" ); 
  fprintf ( fp, "           C:G 0.2\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, " 3) Options for handling the complement of the matrices ---\n" ); 
  fprintf ( fp, "    -c0: ignore the complement \n" ); 
  fprintf ( fp, "    -c1: compare both orientation (the default option)\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, " 4) Algorithm options\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "    -n: if a frequency matrix is compared, a total count is assumed. \n" ); 
  fprintf ( fp, "        \"n\" is the user-defined total count. Default is 10.\n" ); 
  fprintf ( fp, "    \n" ); 
  fprintf ( fp, "    -g: global alignment. Default is local alignment.\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "###########\n" ); 
  fprintf ( fp, "# Example #\n" ); 
  fprintf ( fp, "###########\n" ); 
  fprintf ( fp, "\n" ); 
    fprintf ( fp, "(bifrost)[3:11pm]src_v2a 207>>more matrix1 \n" ); 
  fprintf ( fp, "A |   1   0   0   0   0   0   0\n" ); 
  fprintf ( fp, "C |   0  14   2   0   0   0   9\n" ); 
  fprintf ( fp, "G |  15   2  14  16  16  16   2\n" ); 
  fprintf ( fp, "T |   0   0   0   0   0   0   5\n" ); 
  fprintf ( fp, " \n" ); 
  fprintf ( fp, "(bifrost)[3:23pm]src_v2a 208>>more matrix2 \n" ); 
  fprintf ( fp, "A |   0   0   1   0   1   0   0\n" ); 
  fprintf ( fp, "C |   3  22   0   0   0   1  15\n" ); 
  fprintf ( fp, "G |  18   1  23  24  23  23   1\n" ); 
  fprintf ( fp, "T |   3   1   0   0   0   0   8\n" ); 
  fprintf ( fp, " \n" ); 
  fprintf ( fp, "(bifrost)[3:23pm]src_v2a 209>>./matalign-v2a -f1 matrix1 -f2 matrix2 -t1 0 -t2 0 -g\n" ); 
  fprintf ( fp, "COMMAND LINE: ./matalign-v2a -f1 matrix1 -f2 matrix2 -t1 0 -t2 0 -g\n" ); 
  fprintf ( fp, " \n" ); 
  fprintf ( fp, "***** PID: 4450 *****\n" ); 
  fprintf ( fp, "Algorithm options:\n" ); 
  fprintf ( fp, "  Compare both orientations.\n" ); 
  fprintf ( fp, "  Global alignment.\n" ); 
  fprintf ( fp, "Matrix 1 type: count matrix\n" ); 
  fprintf ( fp, "Matrix 2 type: count matrix\n" ); 
  fprintf ( fp, "Matrices Alignment:\n" ); 
  fprintf ( fp, "MATRIX: A |     1     0     0     0     0     0     0\n" ); 
  fprintf ( fp, "        C |     0    14     2     0     0     0     9\n" ); 
  fprintf ( fp, "        G |    15     2    14    16    16    16     2\n" ); 
  fprintf ( fp, "        T |     0     0     0     0     0     0     5\n" ); 
  fprintf ( fp, "Consensus:      G     C     G     G     G     G     c\n" ); 
  fprintf ( fp, "MATCH:          |     |     |     |     |     |     |\n" ); 
  fprintf ( fp, "MATRIX: A |     0     0     1     0     1     0     0\n" ); 
  fprintf ( fp, "        C |     3    22     0     0     0     1    15\n" ); 
  fprintf ( fp, "        G |    18     1    23    24    23    23     1\n" ); 
  fprintf ( fp, "        T |     3     1     0     0     0     0     8\n" ); 
  fprintf ( fp, "Consensus:      G     C     G     G     G     G     c\n" ); 
  fprintf ( fp, "Comparison scores:\n" ); 
  fprintf ( fp, "ALLR = 8.3894\n" ); 
  fprintf ( fp, "Distance = 1.9606\n" ); 
  fprintf ( fp, "E_value = 3.01e-07\n" ); 
  fprintf ( fp, "P_value = 3.01e-07\n" ); 
  fprintf ( fp, "NEW MATRIX:\n" ); 
  fprintf ( fp, "number of sequences = 40\n" ); 
  fprintf ( fp, "width = 7\n" ); 
  fprintf ( fp, "crude information = 9.7450 (bits)\n" ); 
  fprintf ( fp, "A |   1   0   1   0   1   0   0\n" ); 
  fprintf ( fp, "C |   3  36   2   0   0   1  24\n" ); 
  fprintf ( fp, "G |  33   3  37  40  39  39   3\n" ); 
  fprintf ( fp, "T |   3   1   0   0   0   0  13\n" ); 
  fprintf ( fp, "Consensus: GCGGGGc\n" ); 
  fprintf ( fp, "\n" ); 
  fprintf ( fp, "\n" );

}
