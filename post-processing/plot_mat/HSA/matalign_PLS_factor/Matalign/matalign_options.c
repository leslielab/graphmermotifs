/****************************************************************
 * MatrixAligner (version 2a)					*
 * Copyright (C) 2004, 2005					*
 * Washington University, St. Louis, MO, USA.			*
 * All Rights Reserved.						*
 * Author:							*
 *  Ting Wang							*
 *  Department of Genetics					*
 *  Washington University in St. Louis				*
 *  Campus Box 8232						*
 *  St. Louis MO 63110						*
 *								*
 *  twang@ural.wustl.edu					*
 ****************************************************************/


/* External global variables corresponding to various options,
 * the matrix of sequences, and the list of the summary matrices. */

#include <stdio.h>
#include "matalign_options.h"



int   PID;

char *file1 = (char *)NULL;	/* Name of matrix file 1 */
char *file2 = (char *)NULL; 	/* Name of matrix file 2 */

int   type1  = 0;
int   type2  = 0;
int   Num_f  = 100;
int   Global = 0;
int   Comp_status = 1;
double ALLR;
double Dist;
double P_value;
double E_value;

int Comp_status;

PROFILE **profile; /* profile[0]: matrix 1
		    * profile[1]: matrix 2
		    * profile[2]: reverse-complement of matrix 2
		    * profile[3]: merged profile
		    */
HSP     **hsp;	   /* hsp[0]: matrix 1 against itself
		    * hsp[1]: matrix 2 against itself
		    * hsp[2]: matrix 1 against matrix 2
		    * hsp[3]: matrix 1 against matrix 2's complement
		    */


/* Information for ALLR statistic */
char   Dcode[15];		/* Degenerate code array */
double Dcode_f[15];		/* Degenerate letter frequency */
int    SM1[15][15];		/* ALLR scoring matrix 1 */
int    SM2[15][15];		/* ALLR scoring matrix 2 */

/* Information for Karlin/Altschul statistic */
double M;
double N;
double K;			/* Parameter K */
double Lambda;			/* Parameter Lambda */
double H;			/* Parameter H */
