/****************************************************************
 * MatrixAligner (version 2a)					*
 * Copyright (C) 2003, 2004, 2005                               *
 * Washington University, St. Louis, MO, USA.                   *
 * All Rights Reserved.						*
 * Author:							*
 *  Ting Wang                                                   *
 *  Laboratory of Dr. Gary D. Stormo	                        *
 *  Department of Genetics					*
 *  Washington University in St. Louis				*
 *  Campus Box 8232						*
 *  St. Louis MO 63110						*
 *								*
 *  twang@ural.wustl.edu					*
 ****************************************************************/


#include <stdio.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#include "tw_alloc.h"
#include "tw_error.h"
#include "matalign_struct.h"
#include "alpha.h"



#define NO 0
#define YES 1
#define LN2 0.6931471805599453094172321214581765680755   /* ln(2) */
#define MAXLINE 4096

extern char *file1;
extern char *file2;
extern int   type1;
extern int   type2;
extern int   Num_f;
extern int   Global;
extern double ALLR;
extern double Dist;
extern double P_value;
extern double E_value;


extern int Comp_status;

extern PROFILE **profile;
extern HSP     **hsp;

extern int   PID;



/* Information for ALLR statistic */
extern char  Dcode[15];
extern double Dcode_f[15];
extern int   SM1[15][15];
extern int   SM2[15][15];

/* Information for Karlin/Altschul statistic */
extern double M;
extern double N;
extern double K;
extern double Lambda;
extern double H;



