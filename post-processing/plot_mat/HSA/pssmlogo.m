function seq_cell=pssmseq(pssm,N,seq_txt)
%Sample short sequences from pssm and output to the seq_txt
%pssm: Given pssm matrix
%N: number of sequences to be sampled
%seq_txt: sequence output file

lpssm=size(pssm,2);
cumpssm=cumsum(pssm,1);
cumpssm(2:4,:)=cumpssm(1:3,:);
cumpssm(1,:)=zeros(1,lpssm);

seq_cell=cell(N,1);

%sample N sequences
for i=1:N 
  seq=repmat(rand(1,lpssm),[4 1]); %generate random numbers
  posneg=cumsum(seq-cumpssm,1);
  [seqnum,seqidx]=max(posneg,[],1);%decide seqidx of maximum posneg 
  
  seqi(find(seqidx==1))='A';
  seqi(find(seqidx==2))='C';
  seqi(find(seqidx==3))='G';
  seqi(find(seqidx==4))='T';

  seq_cell{i}=seqi;
end
         
 filewrite(seq_cell,seq_txt); %write sampled sequences (seq_cell) to sequence file (seq_txt)
