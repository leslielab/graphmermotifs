%Cluster k-mers in each motif subnetwork from mcode analysis output
%Input: One Mcode output text file (not original, first few lines are deleted)
%Output:sequence files (seq_output_file) in HSA_output/pssms and corresponding logos (logo_output_file) in HSA_output/logos for Mcode found motif patterns

clear all;

%specify data_dir to read in information for clustering
addpath('../');
data_dir='../../../../data/';
pbg=textread([data_dir,'pbg.txt']);
dimers_maxgap=10;

%define Mcode input and output
mcode_input_file="mcode_hill_factor3.txt";
output_name='/mcode_hill_factor3_';
%define output dir in HSA_output/pssms and HSA_output/logos 
output_dir='kmer67';

%define number of short sequences written to sequence file       
N_seq=200;

%first read in kmer subnetworks from mcode
!cut -f3 mcode_input/mcode_hill_factor3.txt > mcode_input/N.txt;
!sed -e 's/[0-9|,|.]//g' mcode_input/mcode_hill_factor3.txt > mcode_input/mot.txt;

%all_Ns match to all_kmers; (e.g. all_Ns=[3 7], then the first 3 k-mers are for 1st motif subnetwork, the next 4 for 2nd motif subnetwork)
all_Ns=[0; cumsum(textread('mcode_input/N.txt'))];
all_kmers=textread('mcode_input/mot.txt','%s');

%loop over all motif subnetworks
for i=2:length(all_Ns)
      kmers=all_kmers(all_Ns(i-1)+1:all_Ns(i)); % k-mers in the ith motif subnetwork
       seq_output_file=char(['../HSA_output/pssms/',output_dir,output_name,num2str(i-1),'.txt']);  %sequence output file
      logo_output_file=char(['../HSA_output/logos/',output_dir,output_name,num2str(i-1),'.png']); %logo output file
     

      gene_idx=cell(size(kmers));
      hit_per=zeros(size(kmers));
      %loop over all k-mers in the motif subnetworks. scan each k-mer in promoter sequences
      for i=1:length(kmers)

         motif=kmers{i};
         pssm=[motif=='A';motif=='C';motif=='G';motif=='T'];
         pssm=pssm+eps*ones(size(pssm)); %turn k-mer into pssm for calculation's purpose
         pthr=sum(max(log(pssm./(pbg'*ones(1,size(pssm,2)))),[],1))-1;
         mot_groupidx=0;
        
         Nhit=scan_pssm(data_dir,mot_groupidx,pssm,pbg,pthr,dimers_maxgap); %scan the k-mer; output hit matrix (Nhit(i,j)=1 means the k-mer is found in gene j at site i)
         hit_per(i)=sum(sum(Nhit,1)>0)/size(Nhit,2);    %percentage of genes that have the k-mer    
        gene_idx{i}=find(sum(Nhit,1)>0); %gene list that has the k-mer
     end 

     N_gene=size(Nhit,2); %total number of genes
     %Cluster k-mers into a single pssm
     pssm=pssm_cluster(kmers,gene_idx,N_gene,hit_per,data_dir);

     %Prepare the sequence file for seqlogo to generate pssm logo 
     pssmlogo(pssm,N_seq,seq_output_file);
     %Generate pssm logo
     cd weblogo;
     eval(char(['!./seqlogo -F PNG -f ',seq_output_file, '>', logo_output_file,' -c']));
     cd ../;


     clear global;
     %!rm mcode_pssm.txt;
     %!echo '<p>' >> mcode_pssm.txt;
     %dlmwrite('mcode_pssm.txt',pssm,'delimiter',' ','-append');
     %!echo '</p>' >> mcode_pssm.txt;
end


%remove used temporary files
!rm mcode_input/N.txt;
!rm mcode_input/mot.txt;

