
%sort k-mers by weights. take the top N k-mers and createthe input file for matalign 


load ../../../../experiments/PLS_fuse2_hill_mot=qt75_gene=qt25_kmer67_l4f2_1.mat; 
filename='../cytoscape_files/mat_factor3_hill.txt';

factor=3;
r=result.weights.r(:,factor);
nit=1;
tst=zeros(1,nit);
tst0=sum(sum(Y_tst.^2))/size(Y_tst,1);

N=50;
[ig,idx]=sort(r,'descend');

mot=motifs(idx(1:N));
%sorted weight
weight=r(idx(1:N));

%eval(['!rm ',filename ]);

           
% fid=fopen(filename,'w');
 for i=1:length(mot)
      motif=mot{i};
      pssm=[motif=='A';motif=='C';motif=='G';motif=='T'];

      !echo '<p>' >> test.txt;
      dlmwrite('test.txt',i,'-append');
      dlmwrite('test.txt',pssm,'delimiter',' ','-append');
      !echo '</p>' >> test.txt;
 end 
 
  eval(['!mv test.txt ',filename]);          
% fclose(fid);
