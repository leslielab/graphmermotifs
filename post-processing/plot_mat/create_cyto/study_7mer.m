%study hits of 7mers

%load ../matfiles/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt25_fuse=23.mat;

load ../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer7_l4f2.mat;



factor=1;
r=result.weights.r(:,factor);
%r=-r;
[ig,idx]=sort(r,'descend');

N=300;
%sort motifs
mots=motifs(idx(1:N));


addpath('../');
dir='../../../../data/';
pbg=textread([dir,'pbg.txt']);
dimers_maxgap=10;

for i=1:length(mots)

        i
        motif=mots{i};
        pssm=[motif=='A';motif=='C';motif=='G';motif=='T'];
        pssm=pssm+eps*ones(size(pssm));
       pthr=sum(max(log(pssm./(pbg'*ones(1,size(pssm,2)))),[],1))-1;
       mot_groupidx=0;



           Nhit=scan_pssm(dir,mot_groupidx,pssm,pbg,pthr,dimers_maxgap);
           if(size(pssm,2)==7)
             is_7mer(i)=1;
           end
           hit_per(i)=sum(sum(Nhit,1)>0)/size(Nhit,2);        
    
           Nhit_oocyte=Nhit(:,oocyte_idx);
           hit_per_oocyte(i)=sum(sum(Nhit_oocyte,1)>0)/size(Nhit_oocyte,2);        
      
end 
