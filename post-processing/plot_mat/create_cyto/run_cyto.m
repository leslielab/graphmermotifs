
%sort k-mers by weights. take the top 50 k-mers and create the cytoscape file
load ../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat;
filename='../cytoscape_files/cyto_factor4_kmer67.txt';

factor=4;
N=50;

r=result.weights.r(:,factor);
nit=1;

[ig,idx]=sort(r,'descend');

mot=motifs(idx(1:N));
%sorted weight
weight=r(idx(1:N));
%hamming distance matrix
ham=sparse(N);


            fid=fopen(filename,'w');
for i=1:N
    for j=i+1:N
        ham(i,j)=hamming(mot{i},mot{j});      
        ham(j,i)=ham(i,j);      
        if(length(mot{i})>4 && length(mot{j})>4)
            if(ham(i,j)<0.30)
               fprintf(fid,'%s %6.2f %s\n',mot{i},ham(i,j),mot{j});
            end     
         end
    end
end

        fclose(fid);
