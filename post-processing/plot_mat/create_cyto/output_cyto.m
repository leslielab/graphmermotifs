
%sort k-mers by weights. take the top 50 k-mers and create the cytoscape file

function [c_avg,N]=output_cyto(matfile,filename,factor,N,sum_w,order,choose_w)
load(char(matfile),'result','motifs'); 
r=result.weights.r(:,factor);
nit=1;
[ig,idx]=sort(r,char(order));


if(choose_w==1)
  if(order=='descend')
      total_w=sum(r(find(r>0)));
  else
      total_w=sum(r(find(r<0)));
  end
      sr=cumsum(r(idx));
      idx=find(sr>total_w*sum_w);
      N=idx(1);
end
N

mot=motifs(idx(1:N));
%sorted weight
weight=r(idx(1:N));
ham=sparse(N);



       %     fid=fopen(filename,'w');
for i=1:N
    for j=i+1:N
        ham(i,j)=hamming(mot{i},mot{j});      
        ham(j,i)=ham(i,j);      
        if(length(mot{i})>4 && length(mot{j})>4)
            
        if(ham(i,j)<0.30)
        %      fprintf(fid,'%s %6.2f %s\n',mot{i},ham(i,j),mot{j});
        end     
         end
    end
end

      %  fclose(fid);
ham(find(ham>0.3))=0;
1;gcetfg%weight=weight/sum(weight);
%c_avg=clustercoeff(ham,weight);
c_avg=clustercoeff(ham);
