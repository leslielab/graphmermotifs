function d=hamming(str1,str2)

mot1=zeros(length(str1),1);
mot2=zeros(length(str2),1);

mot1(find(str1=='A'))=1;
mot1(find(str1=='C'))=2;
mot1(find(str1=='G'))=3;
mot1(find(str1=='T'))=4;

mot2(find(str2=='A'))=1;
mot2(find(str2=='C'))=2;
mot2(find(str2=='G'))=3;
mot2(find(str2=='T'))=4;



%mot1 is longer than mot2, swap mot1 and mot2
if(length(mot1)<length(mot2))
   mot=mot1;
   mot1=mot2;
   mot2=mot;
   str2=str1;
end

%%% reverse complement of mot2
mot2_r(find(str2=='A'))=4;
mot2_r(find(str2=='C'))=3;
mot2_r(find(str2=='G'))=2;
mot2_r(find(str2=='T'))=1;

mot2_r=mot2_r(end:-1:1);
%extend mot1
  mot1_ex=[0; mot1; 0];
  for i=1:length(mot1_ex)-length(mot2)+1
     mot2_ex=zeros(size(mot1_ex));
     mot2_ex(i:i+length(mot2)-1)=mot2;
     hamms(i,1)=sum(mot1_ex-mot2_ex~=0)/sum(mot1_ex+mot2_ex>0);  
     
     %reverse complement
     mot2_r_ex=zeros(size(mot1_ex));
     mot2_r_ex(i:i+length(mot2)-1)=mot2_r;
     hamms(i,2)=sum(mot1_ex-mot2_r_ex~=0)/sum(mot1_ex+mot2_r_ex>0);  
  end

d=min(hamms(:));
%d=min(hamms(:,1));

%if two 7mers or 6,7mers share the same core part (e.g. GATAAGC, CCGATAA),set d=0.29
if(length(mot1)==7&length(mot2)>=6)
for i=1:length(mot1)-4
  for j=1:length(mot2)-4
      if(is_same(mot1(i:i+4),mot2(j:j+4)))
        d=0.29;
        break;
      end
  end
end
end


function d=is_same(mot1,mot2)
mot2_r=5-mot2;
mot2_r=mot2_r(end:-1:1);
if(nnz(mot1-mot2)==0|nnz(mot1-mot2_r)==0)
 d=1;
else
 d=0;
end

