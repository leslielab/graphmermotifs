load ../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat result cexp_tr tr_gene gene_idx oocyte_tridx mot_tr X Y sperm_tridx;
addpath('GO/');


xcenter=X;
ycenter=Y;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%understand if top motifs in first pc are enriched in oocyte genes
factor=1;
W=result.weights.r;
W1=W(:,factor);
[sW1,idx]=sort(W1);

M=size(mot_tr,1);
N_oocyte=length(oocyte_tridx);
N_sperm=length(sperm_tridx);

for i=1:length(sW1)
    mot_idx=idx(i);
    
    %X: number of mot hits in oocyte genes; K: number of mot hits in sperm genes
    X=sum(mot_tr(oocyte_tridx,mot_idx));
    K=sum(mot_tr(:,mot_idx));
    p_oocyte(i)=1-hygecdf(full(X),M,full(K),N_oocyte);
  
end


   log10_p=-log10(p_oocyte+eps);
   log10_p(find(isnan(log10_p)))=1;

   Pearson_corr=corr(log10_p',sW1);

   log10_p0=log10_p(find(sW1>0));
   sW10=sW1(find(sW1>0));

   Pearson_corr0=corr(log10_p0',sW10); 

    figure(1);
    clf;
    plot(sW1,-log10(p_oocyte+eps),'k.','MarkerSize',15);
    h_title1=title(['Hypergeometric k-mer enrichment p-value versus w1']);
    h_xlabel1=xlabel('W1');
    h_ylabel1=ylabel('-log10(p-value)');
    axis([-6e-4 6e-4 0 18]);

    set(gca,'FontSize',18);
    set(gca,'XTick',-6e-4:2e-4:6e-4);
    set(gca,'YTick',0:2:18);
    set(h_title1,'FontSize',22);
    set(h_xlabel1,'FontSize',22);
    set(h_ylabel1,'FontSize',22);
      

   
    exportfig(gcf,'figs1/oocyte_pval.eps','color','rgb'); 








