%plot distribution of distance of ACGTG to TSS
load ../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt25_fuse=23.mat;

addpath('../../../../');
addpath('GO/'); %exportfig

factor=3;
r=result.weights.r(:,factor);
nit=1;
tst=zeros(1,nit);
tst0=sum(sum(Y_tst.^2))/size(Y_tst,1);

    [ig,idx]=sort(r);

N=1471;
%mot=motifs(idx(end-N:end));
%N=50
mot=motifs(idx(1:N));
%sorted weights;
w=ig;

%get the length of each promoter sequence
dir='medusa/';
load([dir,'seqs.mat'],'a');
len=500*ones(1,size(a,1));
a1=a;
a1(:,end)=1;
x=(a1==5);
x1=x(:,2:end)-x(:,1:end-1);
[m,n]=find(x1);

len(m)=n;



pbg=textread([dir,'pbg.txt']);
dimers_maxgap=10;
JS_ent=zeros(length(mot),1);


%load matfiles/mot_dist.mat;
%%JS_ent w mot rank
%rank=1:length(w);
%[JS_ent_s,idx_s]=sort(JS_ent,'descend');
%mot_s=mot(idx_s);
%rank_s=rank(idx_s);
%w_s=w(idx_s);

%      figure(1);
%      clf;
%      plot(JS_ent,-w,'r.');
%      title('JS entropy versus weight vector latent factor');
%      xlabel('JS entropy');
%      ylabel('weight'); 
 

%for i=1:length(mot)

        i=1;
        motif=mot{i};
        pssm=[motif=='A';motif=='C';motif=='G';motif=='T'];
        pssm=pssm+eps*ones(size(pssm));
       pthr=sum(max(log(pssm./(pbg'*ones(1,size(pssm,2)))),[],1))-1;
       mot_groupidx=0;
       Nhit=scan_pssm(dir,mot_groupidx,pssm,pbg,pthr,dimers_maxgap);  

      len_sperm=len(sperm_idx);
      Nhit_sperm=Nhit(:,sperm_idx);
      [r,c]=find(Nhit_sperm);


      %only count the kmer closest to the TSS
      [k1,k2]=ismember(unique(c),c);
      r=r(k2);
      c=c(k2);

      %Distance to TSS in sperm genes
      dist_sperm=len_sperm(c)-r';      
 
      no_idx=setdiff(1:size(Nhit,2),sperm_idx);
      len_no=len(no_idx);
      Nhit_no=Nhit(:,no_idx);
      [r,c]=find(Nhit_no);
      
      [k1,k2]=ismember(unique(c),c);
      r=r(k2);
      c=c(k2);
      %Distance to TSS in non-sperm genes
      dist_no=len_no(c)-r';      


      [N_no,x]=hist(dist_no,10);
      N_no=N_no/sum(N_no);
      N_sperm=hist(dist_sperm,x,10);
      N_sperm=N_sperm/sum(N_sperm);
      if(length(dist_sperm)>100)
         JS_ent(i)=ent((N_no+N_sperm)/2)-ent(N_no)/2-ent(N_sperm)/2;
      else
         JS_ent(i)=0;
      end

      range=0:10:500;
      figure(2);
      clf;
      subplot(2,1,1); 

      
      a=hist(dist_sperm,range)/length(dist_sperm);
      h=bar(range,a);
      set(h,'FaceColor','k','EdgeColor','w');

%      hist(dist_sperm,range); 
   %
  axis([0 500 0 0.1]);
set(gca,'XTick',0:50:500);      
set(gca,'FontSize',16);

     h_title1=title(['Distance of ',motif,' to TSS in sperm genes']);
    h_xlabel1=xlabel('Distance to TSS (bps)'); 
   h_ylabel1=ylabel('Fraction of k-mers');
     subplot(2,1,2); 
      h=bar(range,hist(dist_no,range)/length(dist_no),'r');
      set(h,'FaceColor','k','EdgeColor','w');
%     hist(dist_no,range);
  %
    axis([0 500 0 0.1]);
      h_title2=title(['Distance of ',motif,' to TSS in non-sperm genes']);
  h_xlabel2=xlabel('Distance to TSS (bps)'); 

  h_ylabel2=ylabel('Fraction of k-mers');

set(gca,'XTick',0:50:500);      
set(gca,'FontSize',16);
set(h_title1,'FontSize',22);
set(h_title2,'FontSize',22);
set(h_xlabel1,'FontSize',22);
set(h_xlabel2,'FontSize',22);
set(h_ylabel1,'FontSize',22);
set(h_ylabel2,'FontSize',22);

     exportfig(gcf,'figs1/ACGTG.eps','color','rgb');
      
%pause;
%end


