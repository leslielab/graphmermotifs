function H=ent(p);
p=p(find(p));
p=p/sum(p);
H=-sum(p.*log(p));
H=H/log(2);
