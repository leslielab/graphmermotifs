load ../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat result cexp_tr tr_gene gene_idx sperm_tridx mot_tr X Y sperm_tridx;
addpath('GO/');


xcenter=X;
ycenter=Y;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%understand if top motifs in first pc are enriched in sperm genes
factor=2;
W=result.weights.r;
W1=W(:,factor);
[sW2,idx]=sort(W1);

M=size(mot_tr,1);
N_sperm=length(sperm_tridx);
N_sperm=length(sperm_tridx);

for i=1:length(sW2)
    mot_idx=idx(i);
    
    %X: number of mot hits in sperm genes; K: number of mot hits in sperm genes
    X=sum(mot_tr(sperm_tridx,mot_idx));
    K=sum(mot_tr(:,mot_idx));
    p_sperm(i)=1-hygecdf(full(X),M,full(K),N_sperm);
  
end

   log10_p=-log10(p_sperm+eps);
   log10_p(find(isnan(log10_p)))=1;
   Pearson_corr=corr(log10_p',sW2);
   log10_p0=log10_p(find(sW2>0));
   sW20=sW2(find(sW2>0));
   Pearson_corr0=corr(log10_p0',sW20); 
   

   figure(1);
    clf;
    plot(sW2,-log10(p_sperm+eps),'k.','MarkerSize',15);
    h_title1=title(['Hypergeometric k-mer enrichment p-value versus w2']);
    h_xlabel1=xlabel('W2');
    h_ylabel1=ylabel('-log10(p-value)');
    axis([-6e-4 6e-4 0 18]);

    set(gca,'FontSize',18);
    set(gca,'XTick',-6e-4:2e-4:6e-4);
    set(gca,'YTick',0:2:18);
    set(h_title1,'FontSize',22);
    set(h_xlabel1,'FontSize',22);
    set(h_ylabel1,'FontSize',22);
   
    exportfig(gcf,'figs1/sperm_pval.eps','color','rgb'); 





