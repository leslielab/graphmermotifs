figure(1);
clf;

for qt=0.25:0.25:0.75

load(['../../../experiments/PLS_hill_mot=qt25','_gene=qt',num2str(qt*100),'_kmer67_1_one_random_model.mat'], 'tst_s');
tst_s2=mean(tst_s,1);
tst_s2=tst_s2/tst_s2(1);

load(['../../../experiments/PLS_hill_mot=qt50','_gene=qt',num2str(qt*100),'_kmer67_1_one_random_model.mat'], 'tst_s');
tst_s5=mean(tst_s,1);
tst_s5=tst_s5/tst_s5(1);

load(['../../../experiments/PLS_hill_mot=qt75','_gene=qt',num2str(qt*100),'_kmer67_1_one_random_model.mat'], 'tst_s');
tst_s8=mean(tst_s,1);
tst_s8=tst_s8/tst_s8(1);

figure(1);

switch qt
case 0.25
 subplot(1,3,1);
case 0.5
 subplot(1,3,2);
case 0.75
 subplot(1,3,3);
end


%plot(0:10,tst_s,'k.-','LineWidth',2,'Markersize',20);
%hold on;
plot(0:10,tst_s2,'b.-','LineWidth',2,'Markersize',20);
%plot(0:10,tst_s2_1,'b','LineWidth',2,'Markersize',20);
hold on;
plot(0:10,tst_s5,'r.-','LineWidth',2,'Markersize',20);
%plot(0:10,tst_s5_1,'r','LineWidth',2,'Markersize',20);
hold on;
plot(0:10,tst_s8,'g.-','LineWidth',2,'Markersize',20);
%plot(0:10,tst_s8_1,'g','LineWidth',2,'Markersize',20);
%legend('PLS',char(['regularized PLS ','lasso=0.',num2str(lasso),' fuse=0.',num2str(fuse)]));

xlabel('number of factors');
ylabel('chi-square');
title([num2str(100*qt),'% genes filtered)']);
axis([0 10 0.9 1.5]);
end
