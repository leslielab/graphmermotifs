#!/usr/bin/perl
# scans shuffled promoter sequences for all k-mers for k=1,...,$ARGV[1]
# usage: perl geti
# usage: perl scan_kmers.pl filename kmin kmax seqfile 
# where filename is the stem of the files which will contain the scanning results, and kmax is the maximum k.
#output: motif (#of counts in all sequences)



$filename = $ARGV[0];
$Nmin=$ARGV[1];
$Nmax=$ARGV[2];
$seqfile=$ARGV[3];
$rand=$ARGV[4];

open(IN,"<$seqfile") || die "could not open utr-file";


$orflabel=0;
$analyze=0;
$seq = "";

while(<IN>){
	chomp;
	if (m/^>/){
#		m/^>(.*?)\ /;
		m/^>(.*)range=.*$/;
                $orflabel++;
                if($orflabel>1) {$analyze=1;} 

	} else {
		$seq = $seq.$_;
	}

       if ($analyze==1) {
           &getmers($orflabel-1); 
        }
}
           &getmers($orflabel); 

close(IN);

print "$filename";

open(MOT,">$filename.mot") || die "could not open utr-file";
open(COUNT,">$filename.count") || die "could not open utr-file";
foreach $motif (sort keys %mers){
       print MOT "$motif\n";
       print COUNT "$mers{$motif}\n";
}
close(MOT);
close(COUNT);

sub getmers {
	my $idx = $_[0];
                print STDOUT "$idx\n";

      #shuffle sequence 
      if($rand==1) 
      { my @seq1=split('',$seq);
        my @sh_seq1=shuffle(\@seq1);
        my $sh_seq=join('',@sh_seq1);
        $seq=$sh_seq;
      }
	$bp = length($seq);
        $N1=$Nmin;
 	$N2 = $Nmax;
	for ($i=0;$i<$bp-$N1+1;$i++){
		$j=$i+1;
		if ($bp-$i+1<$N){ $N2 = $bp-$i+1;}
		for ($ii=$N1;$ii<=$N2;$ii++){
			$motif = substr($seq,$i,$ii);
			$motif = substr($seq,$i,$ii);
			if(!$mers{$motif}){
				$mers{$motif} = 0;
			}
                        $mers{$motif}=$mers{$motif}+1;
           }
	}

      $analyze=0;
      $seq="";
}

			


sub shuffle{
    my($input)=@_;
    my @new;
 
     while(@$input){
     push (@new,splice(@$input,rand(@$input),1));
}

    return @new;
}
