function [motifs,motifs_names,orf_names] = kmers_matrix(path,Nmin,Nmax,seqfile)
% function [motifs,motifs_names,orf_names] = kmers_matrix(path,Nk)
%
% calls perl scripts to scan promoter sequences for k-mers and calculates the resulting motif-matrix
% INPUT:
%	path = path to run-directory (string)
%	Nk = maximum k for the k-mers to scan
% OUTPUT:
%	motifs = motif-matrix
%	motifs_names = motif names (cell array of strings)
%	orf_names = gene names associated with the motif-matrix (cell array of strings)
%


file = [path,'kmers_scan'];
eval(['!perl get_kmers.pl ',file,' ',num2str(Nmin),' ',num2str(Nmax),' ', seqfile]);
fprintf('calculating motif matrix for k-mers...');
%eval(['!perl identify_revcom.pl ',file]);

A = load([file,'.matrix']);
orf = A(:,1);
mot = A(:,2);
pos = A(:,3);
M = max(mot);

[idxmot,nmmot] = textread([file,'.motifs'],'%d\t%s\n');
motifs_names = cell(1,max(idxmot));
motifs_names(idxmot) = nmmot;

[idxorf,nmorf] = textread([file,'.orfs'],'%d\t%s\n');
orf_names = cell(1,max(idxorf));
orf_names(idxorf) = nmorf;

map = load([file,'.motifs.identify']);
mapidx = 1:M;
mapidx(map(:,1)) = map(:,2);

motifs = sparse(orf,mapidx(mot),ones(1,length(orf)));

mgood = unique(mapidx);
motifs_names = motifs_names(mgood);

motifs = motifs(:,mgood);

save([file,'.mat'],'motifs','motifs_names','orf_names');
fprintf('done\n');

%motifs = (motifs>0);
