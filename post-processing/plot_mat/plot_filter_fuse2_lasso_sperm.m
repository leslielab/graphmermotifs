%load ../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2_1.mat motifs sperm_tridx oocyte_tridx cexp_tr result sperm_tst_l oocyte_tst_l tst_l oocyte_tst_s motifs;
load ../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer7_l4f2_weightedA.mat;

c=result.weights.q;
figure(1);
subplot(2,1,1);
imagesc(cexp_tr(oocyte_tridx,:),[-2 2]);
colorbar('location','southoutside');
title('oocyte gene expression');
xlabel('time point');
ylabel('oocyte genes');
subplot(2,1,2);
plot(c(:,1),'r.-');
title('weight vector c for the 1st latent factor');
xlabel('time point');
ylabel('c1');

figure(3);
subplot(2,1,1);
imagesc(cexp_tr(sperm_tridx,:),[-2 2]);
colorbar('location','southoutside');
title('sperm gene expression');
xlabel('time point');
ylabel('sperm genes');
subplot(2,1,2);
plot(-c(:,2),'r.-');
title('weight vector c for the 2nd latent factor');
xlabel('time point');
ylabel('c2')










tst_l=squeeze(tst_l(4,2,:,:));
sperm_tst_l=squeeze(sperm_tst_l(4,2,:,:));
oocyte_tst_l=squeeze(oocyte_tst_l(4,2,:,:));


tst0=mean(tst_l,1);
tst0=tst0(1);
sperm_tst0=mean(sperm_tst_l,1);
sperm_tst0=sperm_tst0(1);
oocyte_tst0=mean(oocyte_tst_s,1);
oocyte_tst0=oocyte_tst0(1);

%oocyte_tst_l(:,1)=oocyte_tst_s(:,1);
lasso=4;


figure(2);
clf;
plot(0:5,mean(tst_l,1)/tst0,'k.-');
hold on;
plot(0:5,mean(sperm_tst_l,1)/sperm_tst0,'r.-');
hold on;
plot(0:5,mean(oocyte_tst_l,1)/oocyte_tst0,'g.-');

legend('all genes','sperm genes','oocyte genes');
%legend('PLS','random PLS');
xlabel('number of factors');
ylabel('chi-square');
title('Normalized squared mean error on test data for regularized PLS');
