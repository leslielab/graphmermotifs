load matfiles/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt25_fuse=2.mat;
w=result.weights.r;
c=result.weights.q;
T=result.T;

nit=size(w,2);

tr=zeros(1,nit+1);
tr(1)=sum(sum(Y.^2))/size(Y,1);
tr_t=zeros(1,nit+1);
tr_t(1)=sum(sum(Y.^2))/size(Y,1);

tst=zeros(1,nit+1);
tst(1)=sum(sum(Y_tst.^2))/size(Y_tst,1);
tst_t=zeros(1,nit+1);
tst_t(1)=sum(sum(Y_tst.^2))/size(Y_tst,1);

for lamda=0:0.1:0.5
lamda

for k=1:nit
    wk=w(:,1:k);
    ck=c(:,1:k);
    tk=T(:,1:k);
    B_pls=wk*ck';
    
    Y_tr1=X*B_pls;
   tr(k+1)=sum(sum((Y-Y_tr1).^2))/size(Y,1);
    Y_tst1=X_tst*B_pls;
   tst(k+1)=sum(sum((Y_tst-Y_tst1).^2))/size(Y_tst,1);
    
    B_pls=wk*inv(tk'*tk+lamda*eye(k))*ck';
    Y_tr1_t=X*B_pls;
   tr_t(k+1)=sum(sum((Y-Y_tr1_t).^2))/size(Y_tr1_t,1);
    Y_tst1_t=X_tst*B_pls;
   tst_t(k+1)=sum(sum((Y_tst-Y_tst1_t).^2))/size(Y_tst1_t,1);

end

figure(1);
clf;
subplot(2,1,1);
plot(0:nit,tr/tr(1),'g.-');
hold on;
plot(0:nit,tr_t/tr(1),'r.-');
legend('B=W*C^T','B=W*inv(T^T*T+bI)*C^T');
title(char(['chi-square on training data (b=',num2str(lamda),')']));
subplot(2,1,2);
plot(0:nit,tst/tst(1),'g.-');
hold on;
plot(0:nit,tst_t/tst(1),'r.-');
legend('B=W*C^T','B=W*inv(T^T*T+bI)*C^T');
title('chi-square on test data');
pause;
end
