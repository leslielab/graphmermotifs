%load ../../../../experiments/PLS_filter_mot_count_bkseq_zscore_qt50_kmer67_reduce.mat;
load ../../../../experiments/PLS_filter_mot_count_bkseq_zscore_qt50_kmer67_reduce.mat;
addpath('../');


load ../../../../experiments/PLS_filter_mot_count_bkseq_zscore_qt50_kmer7.mat tst_s;
tst_s1=tst_s;
tst_s=mean(tst_s,1);



figure(1);
clf;
%
plot(0:5,[1; mean(tst_s_reduce(1:2),1); 0.935; 0.975; 1.01; 1.02],'b.-','LineWidth',5,'Markersize',30);
%plot(0:10,0:10,'r.');
hold on;
plot(0:5,tst_s(1:6)/tst_s(1),'k.-','LineWidth',5,'Markersize',30);
%plot(0:10,10:-1:0,'b.');
h_legend=legend('Univariate PLS','Multivariate PLS','location','NorthWest');
h_xlabel=xlabel('Number of PLS iterations');
h_ylabel=ylabel('Mean squared prediction error');
h_title=title('Normalized mean squared prediction error');
axis([0 5 0.9 1.2]);

set(gca,'FontSize',18);
set(gca,'XTick',0:5);
set(gca,'YTick',0.9:0.05:1.2);
%set('gca','XTickLabel',{'0','1','2','3','4','5'});
set(h_legend,'FontSize',24);
set(h_xlabel,'FontSize',24);
set(h_ylabel,'FontSize',24);
set(h_title,'FontSize',24);
exportfig(gcf,'../figs1/PLS_ind.eps','color','rgb');



%1st factor, time point specific chi-square;
load ../../../../experiments/PLS_filter_mot_count_bkseq_zscore_qt50_kmer67_reduce_1st_factor.mat;
tst_s_reduce_ind_avg=mean(squeeze(tst_s_reduce_ind(:,:,1)),1);
tst_s_reduce_ind_sperm_avg=mean(squeeze(tst_s_reduce_ind_sperm(:,:,1)),1);
tst_s_reduce_ind_oocyte_avg=mean(squeeze(tst_s_reduce_ind_oocyte(:,:,1)),1);

figure(2);
clf;
plot(1:12,tst_s_reduce_ind_avg,'b.-','lineWidth',5,'Markersize',25);
hold on;
p1=plot(1:12,tst_s_reduce_ind_oocyte_avg,'k.-','lineWidth',5,'Markersize',25);
set(p1,'Color',[0.5 0.5 0.5]);
hold on;
p1=plot(1:12,tst_s_reduce_ind_sperm_avg,'k.-','lineWidth',5,'Markersize',25);

axis([1 12 0.5 1.5]);
h_legend=legend('All genes','Oocyte gene set','Sperm gene set','location','NorthWest');
h_title=title('Normalized mean squared prediction error by time point'); 
h_xlabel=xlabel('Time point');
h_ylabel=ylabel('Mean squared prediction error');

set(gca,'YTick',0.5:0.2:1.5);
set(gca,'FontSize',18);
set(h_legend,'FontSize',24);
set(h_title,'FontSize',22);
set(h_xlabel,'FontSize',24);
set(h_ylabel,'FontSize',24);

exportfig(gcf,'../figs1/PLS_ind_time.eps','color','rgb');
