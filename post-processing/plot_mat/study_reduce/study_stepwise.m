load ../../../experiments/PLS_filter_mot_count_bkseq_zscore_qt50_kmer67.mat motifs rs_ind;
rs_reduce=rs_ind;
load ../../../experiments/PLS_filter_mot_count_bkseq_zscore_qt50_kmer67_test1.mat tr_reduce tst_reduce;

load ../../../experiments/PLS_filter_mot_count_bkseq_zscore_qt50_kmer67_stepwise.mat rs_stepwise tr_stepwise tst_stepwise; 

N_time=length(rs_reduce);

for i=1:N_time
    N_f=size(rs_reduce{i},2);
    
        rs=rs_reduce{i};
   %first factor
         j=1;
        [rs1,idx_reduce]=sort(rs(:,j),'descend');
        fprintf('\n\n\ntime point %d; single time point PLS; factor %d\n',i,j);
        for k=1:50
           fprintf('%s ',motifs{idx_reduce(k)});
        end
       
        [p1,idx_stepwise]=sort(rs_stepwise(:,i)); %rank by p-val in stepwise regression
        fprintf('\n\ntime point %d; single time point stepwise regression\n',i);
        for k=1:50
           fprintf('%s ',motifs{idx_stepwise(k)});
        end
         

       pause;
end

