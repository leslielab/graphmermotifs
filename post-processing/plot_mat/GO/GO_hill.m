clear all;
%extract the dominant factor for genes
%
%load ../../../../experiments/PLS_hill_mot=qt75_gene=qt25_kmer67_1_one_random_model.mat;
load ../../../../experiments/PLS_fuse2_hill_mot=qt75_gene=qt25_kmer67_l4f2_1.mat;

addpath('../');

T=result.T;
U=result.weights.u;
w=result.weights.r;
c=result.weights.q;

%for i=1:4
% [sw,idx]=sort(w(:,i),'descend');
% T1(:,i)=sum(mot_tr(:,idx(1:50)),2);
%end

idx=setdiff(1:length(targets_names),holdout_gene);
targets_names=targets_names(idx);
cexp_real=cexp_real(idx,:);
%U=corr(c,zscore(cexp_real)')';
U=corr(c,(cexp_real)')';

%rank factors by TU
TU=T.*U;
idx=intersect(find(TU(:)>0),find(T(:))<0);
TU(idx)=-TU(idx);
[i,j]=max(TU,[],2);


%find GO terms enriched for gene groups


for k=1:3
k
   for q1=8
     
    q=quantile(i,0.1*q1);
     genes=targets_names(find((j==k).*(i>q)));
    [p,gid,term,numGenesWithAnno,numClusterWithAnno,numGenes,numCluster]=termFinder_hill(genes,'gene','completeAssociationArray.mat',0.001);
     ps{k}=p;
     terms{k}=term;
%some genes are not found in termfinder, scale numClusterWithAnno
    numClusterWithAnno=numClusterWithAnno*length(genes)/numCluster;



 fid=fopen(char(['../output/factor',num2str(k),'_gene_GO.txt']),'w');
 for i1=1:length(term)
   fprintf(fid,'%s\t%e\t%d\n',term{i1},p(i1),numClusterWithAnno(i1));
 end

 fclose(fid);
  end
end


figure(1);
clf;
plot(c(:,1),'k.-','LineWidth',10,'Markersize',60);
%h_title=title('c1');
%set(h_title,'Fontsize',30);
h_xlabel=xlabel('Developmental time');
set(h_xlabel,'Fontsize',32);
exportfig(gcf,'../figs1/hill_c1.eps','color','rgb');

figure(2);
clf;
plot(c(:,2),'k.-','LineWidth',10,'Markersize',60);
%title('c2');
%set(h_title,'Fontsize',30);
h_xlabel=xlabel('Developmental time');
set(h_xlabel,'Fontsize',32);
exportfig(gcf,'../figs1/hill_c2.eps','color','rgb');

figure(3);
clf;
plot(c(:,3),'k.-','LineWidth',10,'Markersize',60);
%title('c3');
%set(h_title,'Fontsize',30);
h_xlabel=xlabel('Developmental time');
set(h_xlabel,'Fontsize',32);
exportfig(gcf,'../figs1/hill_c3.eps','color','rgb');
