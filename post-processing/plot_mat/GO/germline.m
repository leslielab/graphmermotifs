
clear all;
%extract the dominant factor for genes
load ../matfiles/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt25_fuse=23.mat;

genes=targets_names(sperm_idx);

pval=0.1;

[p,gids,terms]=termFinder(genes,'gene','completeAssociationArray.mat',pval);
