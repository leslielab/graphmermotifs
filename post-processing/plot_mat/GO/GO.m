clear all;
%extract the dominant factor for genes
load ../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat;

T=result.T;
U=result.weights.u;
w=result.weights.r;
c=result.weights.q;
%flip=mean(w,1);
%flip_idx=find(flip<0);
%T(:,flip_idx)=-T(:,flip_idx);
%U(:,flip_idx)=-U(:,flip_idx);
%w(:,flip_idx)=-w(:,flip_idx);
%c(:,flip_idx)=-c(:,flip_idx);

%rank factors by TU
TU=T.*U;
idx=intersect(find(TU(:)>0),find(T(:))<0);
TU(idx)=-TU(idx);
TU(idx)=0;
[i,j]=max(TU,[],2);
q=quantile(i,0.5);
TU_s=sort(TU,2,'descend');
i1=TU_s(:,1)./TU_s(:,2);


%find GO terms enriched for gene groups
%sperm_genes=targets_names(oocyte_idx);
targets_names=targets_names(setdiff(1:length(targets_names),holdout_gene));
for k=2;
  k
%  for cutoff=2:2:8
  %  q=quantile(i,cutoff*0.1);
% genes=targets_names(find((j==k).*(i>q).*(i1>2)));
   %
 idx=find((j==k).*(i>q));
%    idx=find((j==k));
    genes=targets_names(idx);

 fid=fopen('../output/Genes.txt','w');
 for i=1:length(genes)
   fprintf(fid,'%s\n',genes{i});
 end
 fclose(fid);



   if(k==1)
     N_k=length(intersect(idx,oocyte_tridx));
     q_k=N_k/length(oocyte_tridx);
     q_group=N_k/length(idx);
  %   fprintf('number of genes=%d\t percentage of oocyte genes=%f5.2 \t percentage of group genes=%f5.2\n',N_k,q_k,q_group);
   else  
     N_k=length(intersect(idx,sperm_tridx));
     q_k=N_k/length(sperm_tridx);
     q_group=N_k/length(idx);
  %   fprintf('number of genes=%d\t percentage of sperm genes=%f5.2 \t percentage of group genes=%f5.2\n',N_k,q_k,q_group);
   end

  
  
    [p,gid,term,numGenesWithAnno,numClusterWithAnno,numGenes,numCluster]=termFinder(genes,'gene','completeAssociationArray.mat',0.001,targets_names);
    ps{k}=p;
    terms{k}=term;

   fid=fopen('../output/GO.txt','w');
   for i=1:length(term)
     fprintf(fid,'%s\t%e\n',term{i},p(i));
    end
   fclose(fid);

%  end
end







