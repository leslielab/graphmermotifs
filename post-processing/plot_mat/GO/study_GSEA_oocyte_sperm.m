clear all;
%extract the dominant factor for genes
addpath('../');
load ../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat oocyte_tridx oocyte_tridx tr_gene targets_names cexp_tr result mot_tr;

T=result.T;
U=result.weights.u;
%U=ycentr*result.weights.q;
w=result.weights.r;
c=result.weights.q;
factor=1;

targets_tr=targets_names(tr_gene);
oocyte_genes=targets_tr(oocyte_tridx);
[is,js]=ismember(targets_tr,oocyte_genes);

%Oocyte genes or not
svals=-ones(size(targets_tr))';
svals(find(is))=1;
rvals=corr(c(:,factor),cexp_tr');
rvals(find(isnan(rvals)))=0;

[sw,w_idx]=sort(w(:,factor),'descend');
wvals=sum(mot_tr(:,w_idx(1:50)),2);

  figure(1);
  clf;
  r1=wvals(find(svals==1));
  r2=wvals(find(svals==-1));
  F2=cdfplot(r2);
  hold on;
  F1=cdfplot(r1);
  set(F1,'Color','b','lineWidth',5);
  set(F2,'Color','r','lineWidth',5);
  h_legend=legend('Non-oocyte genes','Oocyte genes','Location','Southeast'); 
  h_title=title(char(['Empirical CDF for hits of top 50 k-mers ranked by w',num2str(factor)])); 
   h_xlabel=xlabel('Hits of top 50 k-mers'); 
  h_ylabel= ylabel('Accumulative distribution function');
  grid off;
  
  set(gca,'YTick',0:0.2:1);
  set(gca,'FontSize',22);
  set(h_legend,'FontSize',24);
  set(h_title,'FontSize',22);
  set(h_xlabel,'FontSize',24);
  set(h_ylabel,'FontSize',24);
  
exportfig(gcf,'../figs1/oocyte_kmer.eps','color','rgb');
  
  figure(2);
  clf;
  r1=rvals(find(svals==1));
  r2=rvals(find(svals==-1));
  F2=cdfplot(r2);
  hold on;
  F1=cdfplot(r1);
  set(F1,'Color','b','lineWidth',5);
  set(F2,'Color','r','lineWidth',5);
  
  h_legend=legend('Non-oocyte genes','Oocyte genes','Location','Northwest'); 
  h_title=title(char(['Empirical CDF for correlation between gene expression and c',num2str(factor)])); 
  h_xlabel=xlabel(char(['Correlation between gene expression and c',num2str(factor)]));
  h_ylabel=ylabel('Accumulative distribution function');

  set(gca,'YTick',0:0.2:1);
  set(gca,'FontSize',22);
  set(h_legend,'FontSize',24);
  set(h_title,'FontSize',22);
  set(h_xlabel,'FontSize',24);
  set(h_ylabel,'FontSize',24);

  grid off;

  opts=struct('FontSize',5,'Resolution',1000);
  exportfig(gcf,'../figs1/oocyte_corr.eps','color','rgb');


