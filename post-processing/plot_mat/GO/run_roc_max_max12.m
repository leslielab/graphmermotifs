clear all;

for ks=1:15
for ko=1:15

%% ks=3;
%% ko=1;
%% % for debugging, set these to other values:
%% ks=6;
%% ko=7;
%% k=3;

%extract the dominant factor for genes
load ../matfiles/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt25_fuse=23.mat;


%[a,b]=max(abs(w_x_sperm_corr),[],1);
T=result.T;
U=result.weights.u;
%U=ycentr*result.weights.q;
w=result.weights.r;
c=result.weights.q;
flip=mean(w,1);
flip_idx=find(flip<0);
T(:,flip_idx)=-T(:,flip_idx);
U(:,flip_idx)=-U(:,flip_idx);
w(:,flip_idx)=-w(:,flip_idx);
c(:,flip_idx)=-c(:,flip_idx);


TU=T.*U;
idx=intersect(find(T<0),find(U<0));
TU(idx)=-TU(idx);

%TU=U;

%max T
[roc_max_s,q_max12_s,roc_max12_s,q_max_s,p_s]=roc_max_max12(TU,ks,sperm_tridx);
[roc_max_s_abs,q_max12_s_abs,roc_max12_s_abs,q_max_s_abs,p_s_abs]=roc_max_max12(abs(TU),ks,sperm_tridx);

figure(1);
clf;
plot(q_max12_s,roc_max_s,'r.-');
hold on;
plot(q_max12_s_abs,roc_max_s_abs,'r');
xlabel(sprintf('cutoff of max(TU,%d)/2nd max(TU,%d)',ks,ks));
ylabel('AUC');
title(sprintf('max(TU,%d) AUC for sperm genes',ks));
legend('TU','|TU|');

figure(2);
clf;
plot(q_max_s,roc_max12_s,'r.-');
hold on;
plot(q_max_s_abs,roc_max12_s_abs,'r');
xlabel(sprintf('cutoff of max(TU,%d)',ks));
ylabel('AUC');
title(sprintf('max(TU,%d)/2nd max(TU,%d) AUC for oocyte genes',ks,ks));
legend('TU','|TU|');

[roc_max_o,q_max12_o,roc_max12_o,q_max_o,p_o]=roc_max_max12(TU,ko,oocyte_tridx);

[roc_max_o_abs,q_max12_o_abs,roc_max12_o_abs,q_max_o_abs,p_o_abs]=roc_max_max12(abs(TU),ko,oocyte_tridx);

figure(3);
clf;
plot(q_max12_o,roc_max_o,'r.-');
hold on;
plot(q_max12_o_abs,roc_max_o_abs,'r');
%xlabel('cutoff of max(TU,1)/2nd max(TU,1)');
xlabel(sprintf('cutoff of max(TU,%d)/2nd max(TU,%d)',ko,ko));
ylabel('AUC');
title(sprintf('max(TU,%d) AUC for oocyte genes',ko));
legend('TU','|TU|');

figure(4);
clf;
plot(q_max_o,roc_max12_o,'r.-');
hold on;
plot(q_max_o_abs,roc_max12_o_abs,'r');
xlabel(sprintf('cutoff of max(TU,%d)',ko));
ylabel('AUC');
title(sprintf('max(TU,%d)/2nd max(TU,%d) AUC for oocyte genes',ko,ko));
legend('TU','|TU|');



disp([ko ks p_o_abs p_o p_s_abs p_s])
disp([ max(roc_max12_o_abs) max(roc_max_o_abs) max(roc_max12_o) max(roc_max_o) max(roc_max12_s_abs) max(roc_max_s_abs) max(roc_max12_s) max(roc_max_s)])

pause;
end 
end
