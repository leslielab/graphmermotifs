load ../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat cexp_real targets_names sperm_idx sperm_tridx oocyte_idx oocyte_tridx result xcentr ycentr cexp_tr mot_tr motifs tridx;

addpath('../');

T=result.T;
U=ycentr*result.weights.q;
w=result.weights.r;
c=result.weights.q;

X=xcentr;
Y=ycentr;
tr=zeros(size(Y,1),size(c,2));
tr0=sum(Y.^2,2);


for i=1:size(c,2)
    Y1=xcentr*w(:,1:i)*c(:,1:i)';
 
    tr(:,i)=sum((Y1-Y).^2,2);
end

tr=tr./repmat(tr0,[1 size(c,2)]);
tr=[ones(size(Y,1),1) tr];

tr_dif=tr(:,1:end-1)-tr(:,2:end);
c_corr=corr(cexp_tr',c);


%study relationship between c_corr and k-mer hit
for j=1:4
     w_j=w(:,j);
     [w_js,w_idx]=sort(w_j,'descend');
     w_idx=w_idx(1:50);
     mot_hit=sum(mot_tr(:,w_idx),2);

     gene_corr_idx=find(c_corr(:,j)>0.7);

%contain at least 10 k-mer hits
     cutoff_mot=10;
     cutoff_mot=quantile(mot_hit,0.9);
     %cutoff_mot=quantile(T(:,j),0.9);
     gene_mot_idx=find(mot_hit>cutoff_mot);
     gene_no_mot_idx=find(mot_hit<cutoff_mot);

     cexp_mot=cexp_tr(gene_mot_idx,:);
     figure(7);
     imagesc(cexp_mot,[-2 2]);
     colorbar('location','southoutside');

     figure(8);
     subplot(2,1,1);
     hist(c_corr(gene_no_mot_idx,j),50);
     title('all genes');
     subplot(2,1,2);
     hist(c_corr(gene_mot_idx,j),50);
     title('selected genes');
     gene_idx=intersect(gene_corr_idx,gene_mot_idx);

     oocyte_genes=intersect(gene_idx,oocyte_tridx);
     sperm_genes=intersect(gene_idx,sperm_tridx);
     
    %divide all genes into two groups: first one enriched with k-mers and second one not. make cdfplot for correlation between gene expression in each group and factor 
    figure(6);
     clf
     %few motifs
     F1=cdfplot(c_corr(gene_no_mot_idx,j));
     hold on;
     %more motifs
     F2=cdfplot(c_corr(gene_mot_idx,j));
     set(F1,'Color','r','lineWidth',5);
     set(F2,'Color','b','lineWidth',5);
     h_legend=legend('Genes containing k-mer hits < 10','Genes containing k-mer hits > 10','Location','Northwest');
     h_ylabel=ylabel('Accumulative distribution function');
     h_xlabel=xlabel(char(['Correlation between gene expression and c',num2str(j)])); 
     h_title=title(char(['Empirical CDF for correlation between gene expression and c',num2str(j)])); 
%     pause;
    
     grid off;
  
  set(h_legend,'FontSize',18);
  set(h_title,'FontSize',20);
  set(h_xlabel,'FontSize',20); 
  set(h_ylabel,'FontSize',20); 
      switch j
       case 1 
       exportfig(gcf,'../figs1/corr_kmer_factor1.eps','color','rgb');
       case 2
       exportfig(gcf,'../figs1/corr_kmer_factor2.eps','color','rgb');
       case 3
         exportfig(gcf,'../figs1/corr_kmer_factor3.eps','color','rgb');
       case 4
           exportfig(gcf,'../figs1/corr_kmer_factor4.eps','color','rgb');
     end
end

%[a,idx_max]=max(T,[],2);

for i=1:10
    idx_i=find(c_corr(:,i)>0.8);

     [swi,idx_w]=sort(w(:,i),'descend'); 

    sperm_genes=intersect(idx_i,sperm_tridx);
    non_sperm=setdiff(idx_i,sperm_genes);
    oocyte_genes=intersect(idx_i,oocyte_tridx);
    non_oocyte=setdiff(idx_i,oocyte_genes);

M=size(mot_tr,1);
for j=1:length(swi)
          mot_idx=idx_w(j);
          Xo=sum(mot_tr(oocyte_genes,mot_idx));
          Xno=sum(mot_tr(non_oocyte,mot_idx));
          K=sum(mot_tr(:,mot_idx));
          p_o(j)=1-hygecdf(full(Xo),M,full(K),length(oocyte_genes)); 
          p_no(j)=1-hygecdf(full(Xno),M,full(K),length(non_oocyte)); 
end


   if(i==1)
   figure(4);
     clf;
     plot(log10(p_o(1:50)+eps),log10(p_no(1:50)+eps),'r.');
     xlabel('log10(p-val)  oocyte genes ');
     ylabel('log10(p-val)  non-oocyte genes ');
     title('Enrichment of top k-mers (w1) in genes highly correlated with c1');
     grid off;
     %exportfig(gcf,'figs/corr_kmer_factor1.eps','color','rgb');
    end
   
   if(i==2)
   figure(4);
     clf;
     plot(log10(p_o(1:50)+eps),log10(p_no(1:50)+eps),'r.');
     xlabel('log10(p-val)  sperm genes ');
     ylabel('log10(p-val)  non-sperm genes ');
     title('Enrichment of top k-mers (w2) in genes highly correlated with c2');
     grid off;
    end
    
    fprintf('factor %d; %d genes, %d sperm genes and %d oocyte genes\n',i,length(idx_i),length(sperm_genes),length(oocyte_genes));
end









  



