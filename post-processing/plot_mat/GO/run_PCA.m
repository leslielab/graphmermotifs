
clear all;
%get top gene list for the 3 motifs in factor1
load('../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat','cexp_real','targets_names');;

PC_idx=3;
N_gene=100;




[coeff,score]=princomp(cexp_real);
%1st factor is flat
corr1=corr(coeff(:,PC_idx+1),cexp_real');

%sort gene by correlation with factor 3 (the 4th column in coeff)
[sort_corr1,sort_idx]=sort(corr1,'descend');
genes_sort=targets_names(sort_idx(1:N_gene));




 
%look for enriched GO terms in genes correlated with PC
[p,gid,term,numGenesWithAnno,numClusterWithAnno,numGenes,numCluster]=termFinder_hill(genes_sort,'gene','completeAssociationArray.mat',0.001);


  










