clear all;
%get top gene list for the 3 motifs in factor1
load ../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat;
load ortholog/output/rate_top50_motifs.mat;


T=result.T;
U=result.weights.u;
w=result.weights.r;
c=result.weights.q;

%rank factors by TU to choose genes for factor 1
TU=T.*U;
idx=intersect(find(TU(:)>0),find(T(:))<0);
TU(idx)=-TU(idx);
TU(idx)=0;
[i,j]=max(TU,[],2);
q=quantile(i,0.8);

%sort motifs for factor1
[sw,w_idx]=sort(w(:,1),'descend');
mots=motifs(w_idx(1:50));
mot_tr=mot_tr(:,w_idx(1:50));


%top 3 motif patterns from cytoscape
mot1_idx=[21 10 42 18 5 36 45];
mot2_idx=[13 19 22 1 25 32 15 28 8 47 27 30 16 2 24 3 48 31];
mot3_idx=[14 44 4];
mot4_idx=[mot1_idx mot2_idx mot3_idx];

figure(1);
clf;
mot_idx=mot1_idx;
plot(sum(mot_e(:,mot_idx),2),sum(mot_b(:,mot_idx),2),'b.');
exportfig(gcf,'figs1/mot1.eps','color','rgb');

figure(2);
clf;
mot_idx=mot2_idx;
plot(sum(mot_e(:,mot_idx),2),sum(mot_b(:,mot_idx),2),'b.');
exportfig(gcf,'figs1/mot2.eps','color','rgb');

figure(3);
clf;
mot_idx=mot3_idx;
plot(sum(mot_e(:,mot_idx),2),sum(mot_b(:,mot_idx),2),'b.');
exportfig(gcf,'figs1/mot3.eps','color','rgb');

%find GO terms enriched for gene groups
oocyte_genes=targets_names(oocyte_idx);
targets_names=targets_names(setdiff(1:length(targets_names),holdout_gene));

 
   
    for m_idx=1:4
            
          if(m_idx==1) 
             mot_idx=mot1_idx;
             fid=fopen('output/factor1_mot1.txt','w');
         elseif(m_idx==2)
             mot_idx=mot2_idx;
             fid=fopen('output/factor1_mot2.txt','w');
          elseif(m_idx==3)
             mot_idx=mot3_idx;
             fid=fopen('output/factor1_mot3.txt','wt');
          else
             mot_idx=mot4_idx;
             fid=fopen('output/factor1_allmot.txt','w');
          end

             fprintf(fid,'k-mers:\t');
             for m=1:length(mot_idx)
               fprintf(fid,'%s\t',mots{mot_idx(m)});
             end 
             fprintf(fid,'\n\n');
          
          
             fprintf(fid,'gene_name\tcounts_in_c.elegans\tcounts_in_c.briggsae\toocyte gene?\n');
 
           %genes associated with 1st group     
           k=1;
           idx=find((j==k).*(i>q));
           genes=targets_names(idx);
           mot_tr_genes=mot_tr(idx,:);
       
          mot_counts=sum(mot_tr_genes(:,mot_idx),2);
          mot_counts_e=sum(mot_e(:,mot_idx),2);
          mot_counts_b=sum(mot_b(:,mot_idx),2); 
          
          [mot_counts_sort,mot_num]=sort(mot_counts,'descend');
          if_oocyte=ismember(genes(mot_num(1:100)),oocyte_genes);

         sum(if_oocyte)          
          for n=1:100
               gene_num=mot_num(n);
               fprintf(fid,'%s\t\t%d\t\t',genes{gene_num},mot_counts_sort(n));
          
               [ign,gene_num_b]=ismember(genes(gene_num),gnames_b);
               if(ign==1)
                  fprintf(fid,'%d\t',mot_counts_b(gene_num_b));
               else
                 fprintf(fid,'N/A\t');
               end               
               [if_oocyte,ign]=ismember(genes(gene_num),oocyte_genes);
                 fprintf(fid,'%d\n',if_oocyte);
                         
          end
         fclose(fid); 
    end 

     N_k=length(intersect(idx,oocyte_tridx));
     q_k=N_k/length(oocyte_tridx);
     q_group=N_k/length(idx);


  







