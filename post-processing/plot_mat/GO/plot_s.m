
figure(1);
clf;
plot(1:15,N_s1,'g.-');
hold on;
plot(1:15,N_s_abs1,'g');
legend('TU number of sperm genes','|TU| number of sperm genes');
xlabel('factor');

figure(4);
clf;
plot(1:15,roc_max12_s1,'r.-');
hold on;
plot(1:15,roc_max12_s_abs1,'r');
xlabel('factor');
ylabel('AUC');
title(sprintf('max(TU,%d)/2nd max(TU,%d) AUC for sperm genes',ks,ks));
legend('TU','|TU|');

