%function [p,gids,terms,numGenesWithAnno,numClusterWithAnno,numGenes,numCluster] = termFinder(clustGNames,orfOrGene,completeArrayfile,pthr,all_genes)
function [p,gids,terms,numGenesWithAnno,numClusterWithAnno,numGenes,numCluster] = termFinder(clustGNames,orfOrGene,completeArrayfile,pthr)
% function [p,gids,terms,numGenesWithAnno,numClusterWithAnno,numGenes,numCluster] = termFinder(clustGNames,orfOrGene,completeArrayfile)
% Calculates hypergeometric p-values of GO annotation enrichment for a
% user-defined cluster of genes
%
% OUTPUT
% p: pvalues of enrichment
% gids: GO term ids
% terms: GO term
% numGenesWithAnno: Number of genes in genome associated with that term
% numClusterWithAnno: Number of genes in user defined cluster associated with that term
% numGenes: Number of genes in the genome
% numCluster: Number of genes in the user defined cluster
% 
% PARAMETERS:
% clustGNames: cell array of names of genes (orfname or common name or both)
% orfOrGene: takes value = 'orf' or 'gene'. 
%            Flag indicating whether the clustGNames are ORFnames or common names.
%            If they are ALL ORFnames then use 'orf' else use 'gene'
% completeArrayfile: Optional parameter. Default value = completeAssociationArray.mat
%                    a standardized mat file containing all the Gene Ontology details.

if ~exist('completeArrayfile','var')
    completeArrayfile = 'sgd/completeAssociationArray2005.mat';
end
load(completeArrayfile,'completeArray');




% remove repetitive genes. e.g. ACT-1, act-1; replace public names by sequence names
%[rowgNames,idx]=unique(lower(completeArray.rowgNames));
%completeArray.array=completeArray.array(idx,:);
  rowgNames=lower(completeArray.rowgNames);
 %gene_pub(1,:): public name ; gene_pub(2,:): sequence name

    gene_pub=textread('test.txt','%s');
    gene_pub=reshape(gene_pub,[2 length(gene_pub)/2]);
    [i,j]=ismember(rowgNames,lower(gene_pub(1,:)));
    
 %replace public names by sequence names
    rowgNames(find(i))=lower(gene_pub(2,j(find(i))));
    completeArray.rowgNames=rowgNames;

 %find all targets_names in GO set
 %   tf = find(ismember(lower(completeArray.rowgNames),lower(all_genes)));
 %total number of genes with each GO annotation;
 %   numGenesWithAnno=sum(completeArray.array(tf,:),1); 
 %total number of genes
 %   numGenes=length(tf);

    
% Total number of genes in genome with each GO annotation
numGenesWithAnno = sum(completeArray.array,1);
% Total number of genes in genome
numGenes = length(completeArray.rowgNames);


% Find rows in the main association array that correspond to the cluster of
% genes
if strcmpi(orfOrGene,'orf')
    tf = find(ismember(lower(completeArray.rowOrfNames),lower(clustGNames)));
elseif strcmpi(orfOrGene,'gene')
    tf = find(ismember(lower(completeArray.rowgNames),lower(clustGNames)));
end

% Extract array corresponding to cluster
miniarray = completeArray.array(tf,:);

% Number of genes in the cluster
numCluster = size(miniarray,1);

% Number of genes in the cluster with particular annotations
numClusterWithAnno = sum(miniarray,1);

% Remove annotations that have a cluster count <= 1
validgids = find(numClusterWithAnno>1);
numGenesWithAnno = numGenesWithAnno(validgids);
numClusterWithAnno = numClusterWithAnno(validgids);
gids = completeArray.colGid(validgids);
terms = completeArray.colterm(validgids);

% Calculate Pvalue
%

%numCluster=length(clustGNames)
p = 1-hygecdf(full(numClusterWithAnno-1),numGenes,numCluster,full(numGenesWithAnno));

% Sort the pvalues in ascending order
[p idx] = sort(p);
%
idx=idx(find(p<pthr));
%idx=idx(find(p>1-pthr));
p=p(find(p<pthr));
gids = gids(idx);
terms = terms(idx);
numGenesWithAnno = numGenesWithAnno(idx);
numClusterWithAnno = numClusterWithAnno(idx);





