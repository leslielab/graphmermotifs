function [roc_max,q_max12,roc_max12,q_max,p_k,N_k]=roc_max_max12(TU,k,gene_idx)
%k is factor


% max TU
[i,j]=max(TU,[],2);
TU_s=sort(TU,2,'descend');
i12=TU_s(:,1)./TU_s(:,2);

%auc 

%genes dominant factor k
idxk=find(j==k);
[idxk_s,g]=ismember(idxk,gene_idx);
%gene genes dominant factor k
gene_idxk=idxk(find(idxk_s));
%non-gene genes dominant factor k
nogene_idxk=idxk(find(~idxk_s));
%percentage of genes
p_k=length(gene_idxk)/length(idxk);
N_k=length(gene_idxk);

%maximum value
gene_i=[i(gene_idxk); i(nogene_idxk)];
%maximum/2nd maximum
gene_i12=[i12(gene_idxk); i12(nogene_idxk)];
%labels: gene vs. non-gene
gene_t=[ones(length(gene_idxk),1);  zeros(length(nogene_idxk),1)];
%roc(gene_y,gene_t)

qidx=1;

for q=0.1:0.2:0.9
    q_max12(qidx)=quantile(gene_i12,q);
    idx12=find(gene_i12>quantile(gene_i12,q));
    roc_max(qidx)=roc(gene_i(idx12),gene_t(idx12));

    q_max(qidx)=quantile(gene_i,q);
    idx=find(gene_i>quantile(gene_i,q));
    roc_max12(qidx)=roc(gene_i12(idx),gene_t(idx));

    qidx=qidx+1;
end





