addpath('GO/');


clear all;
%get top gene list for the 3 motifs in factor1
load ../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat;

factor_idx=3;
N_gene=100;

T=result.T;
U=result.weights.u;
w=result.weights.r;
c=result.weights.q;

%rank factors by TU to choose genes for factor 1
TU=T.*U;
idx=intersect(find(TU(:)>0),find(T(:))<0);
TU(idx)=-TU(idx);
TU(idx)=0;
[i,j]=max(TU,[],2);
q=quantile(i,0.8);

%sort motifs for factor
[sw,w_idx]=sort(w(:,factor_idx),'descend');
mots=motifs(w_idx(1:50));
%only keep top ranked motifs
mot_tr=mot_tr(:,w_idx(1:50));

%top 3 motif patterns from cytoscape

%factor 1
%mot1_idx=[21 10 42 18 5 36 45];
%mot2_idx=[13 19 22 1 25 32 15 28 8 47 27 30 16 2 24 3 48 31];
%mot3_idx=[14 44 4];
%mot4_idx=[mot1_idx mot2_idx mot3_idx];

%factor 2
%mot1_idx=[46 1 5 13 15 29 30 14 3 19 25 34 38];
%mot2_idx=[31 2 11];
%mot3_idx=[49 4 16];
%mot4_idx=[21 50 6];
%mot_all_idx=[mot1_idx mot2_idx mot3_idx mot4_idx];

%factor 3
mot1_idx=[47 11 16 44 18];
mot2_idx=[48 19 23 34];
mot3_idx=[36 32 10 28 14 21 38];
mot4_idx=[46 20 33];
mot_all_idx=[mot1_idx mot2_idx mot3_idx mot4_idx];

%factor 4
%mot1_idx=[49 2 3 4 5 13 16 22 24 27 41 44 48];
%mot2_idx=[29 1 7 10 14 17 19 23];
%mot3_idx=[15 8 30 12];
%mot4_idx=[42 9 38];
%mot_all_idx=[mot1_idx mot2_idx mot3_idx mot4_idx];


%find GO terms enriched for gene groups
oocyte_genes=targets_names(oocyte_idx);
sperm_genes=targets_names(sperm_idx);
targets_names=targets_names(setdiff(1:length(targets_names),holdout_gene));

    for m_idx=1:5
           
             switch m_idx
               case 1 
                 mot_idx=mot1_idx;
                 fid=fopen(['output/factor',num2str(factor_idx)','_mot',num2str(m_idx),'.txt'],'w');
               case 2
                 mot_idx=mot2_idx;
                 fid=fopen(['output/factor',num2str(factor_idx)','_mot',num2str(m_idx),'.txt'],'w');
               case 3
                 mot_idx=mot3_idx;
                 fid=fopen(['output/factor',num2str(factor_idx)','_mot',num2str(m_idx),'.txt'],'w');
               case 4
                 mot_idx=mot4_idx;
                 fid=fopen(['output/factor',num2str(factor_idx)','_mot',num2str(m_idx),'.txt'],'w');
               otherwise  
                 mot_idx=mot_all_idx;
                 fid=fopen(['output/factor',num2str(factor_idx)','_mot_all.txt'],'w');
               end

             fprintf(fid,'k-mers:\t');
             for m=1:length(mot_idx)
               fprintf(fid,'%s\t',mots{mot_idx(m)});
             end 
             fprintf(fid,'\n\n');
          
          
%             fprintf(fid,'gene_name\tcounts_in_c.elegans\tcounts_in_c.briggsae\toocyte gene?\n');
             fprintf(fid,'gene_name\tcounts_in_c.elegans\toocyte gene?\tsperm gene?\n');
 
           %genes associated with factor_idx
           k=factor_idx;
           idx=find((j==k).*(i>q));
           genes=targets_names(idx);
           mot_tr_genes=mot_tr(idx,:);
       
          mot_counts=sum(mot_tr_genes(:,mot_idx),2);
  %        mot_counts_e=sum(mot_e(:,mot_idx),2);
  %        mot_counts_b=sum(mot_b(:,mot_idx),2); 
          
          [mot_counts_sort,mot_num]=sort(mot_counts,'descend');
          genes_sort=genes(mot_num(1:N_gene));
  %        if_oocyte=ismember(genes(mot_num(1:N_gene)),oocyte_genes);
  %        if_sperm=ismember(genes(mot_num(1:N_gene)),sperm_genes);
          
          for n=1:N_gene
               gene_num=mot_num(n);
               fprintf(fid,'%s\t\t%d\t\t',genes{gene_num},mot_counts_sort(n));
          
   %            [ign,gene_num_b]=ismember(genes(gene_num),gnames_b);
   %            if(ign==1)
   %               fprintf(fid,'%d\t',mot_counts_b(gene_num_b));
   %            else
   %              fprintf(fid,'N/A\t');
   %            end               
   %     
               [if_oocyte,ign]=ismember(genes(gene_num),oocyte_genes);
               [if_sperm,ign]=ismember(genes(gene_num),sperm_genes);
               fprintf(fid,'%d\t%d\n',if_oocyte,if_sperm);
                         
          end
         fclose(fid); 

           
        if(m_idx==5)
            [p,gid,term,numGenesWithAnno,numClusterWithAnno,numGenes,numCluster]=termFinder_hill(genes_sort,'gene','completeAssociationArray.mat',0.001);
                
             fid=fopen(['output/factor',num2str(factor_idx)','_mot_all_GO.txt'],'w');
            for i1=1:length(term)
               fprintf(fid,'%s\t%e\n',term{i1},p(i1));
            end
            fclose(fid);
        end

   end 


  










