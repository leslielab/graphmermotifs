clear all;
%extract the dominant factor for genes
load ../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat;

T=result.T;
U=result.weights.u;
w=result.weights.r;
c=result.weights.q;

idx=setdiff(1:length(targets_names),holdout_gene);
targets_names=targets_names(idx);
cexp_real=cexp_real(idx,:);
%U=corr(c,zscore(cexp_real)')';
U=corr(c,(cexp_real)')';

%rank factors by TU
TU=T.*U;
idx=intersect(find(TU(:)>0),find(T(:))<0);
TU(idx)=-TU(idx);
[i,j]=max(TU,[],2);


%find GO terms enriched for gene groups


for k=1:4
k
   for q1=6
     
    q=quantile(i,0.1*q1);
     genes=targets_names(find((j==k).*(i>q)));
    [p,gid,term,numGenesWithAnno,numClusterWithAnno,numGenes,numCluster]=termFinder_hill(genes,'gene','completeAssociationArray.mat',0.001);
     ps{k}=p;
     terms{k}=term;

 fid=fopen(char(['../output/factor',num2str(k),'_gene_GO_val.txt']),'w');
 for i1=1:length(term)
   fprintf(fid,'%s\t%e\n',term{i1},p(i1));
 end

 fclose(fid);
  end
end


