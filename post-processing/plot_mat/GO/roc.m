function auc=roc(y,t)


%sort by classifier output

[Y,idx]=sort(-y);
t=t(idx);

%compute tp and fp

tp=cumsum(t)/sum(t);
fp=cumsum(~t)/sum(~t);

%add end-points

tp=[0;tp;1];
fp=[0;fp;1];

auc=sum((fp(2:end)-fp(1:end-1)).*(tp(2:end)+tp(1:end-1)))/2;
