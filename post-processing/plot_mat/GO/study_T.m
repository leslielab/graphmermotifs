clear all;
%extract the dominant factor for genes
%load ../matfiles/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt25_fuse=23.mat;
load ../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat;

T=result.T;
U=result.weights.u;
%U=ycentr*result.weights.q;
w=result.weights.r;
c=result.weights.q;
flip=mean(w,1);
flip_idx=find(flip<0);
%
%T(:,flip_idx)=-T(:,flip_idx);
%U(:,flip_idx)=-U(:,flip_idx);
%w(:,flip_idx)=-w(:,flip_idx);
%c(:,flip_idx)=-c(:,flip_idx);


%max T
[i,j]=max(T,[],2);
T_s=sort(T,2,'descend');
i1=T_s(:,1)./T_s(:,2);


js=j(sperm_tridx);
is=i(sperm_tridx);
is1=i1(sperm_tridx);
jo=j(oocyte_tridx);
io=i(oocyte_tridx);
io1=i1(oocyte_tridx);
figure(2);

m=0;
for quant=0.1:0.1:0.9
   m=m+1; 
   idx=find(is>quantile(i,quant));
    ps(m)=sum(js(idx)==2)/length(idx);
    ps1(m)=sum((js(idx)==2).*(is1(idx)>2))/sum(is1(idx)>2); 
  idx=find(io>quantile(i,quant));
    po(m)=sum(jo(idx)==1)/length(idx);
    po1(m)=sum((jo(idx)==1).*(io1(idx)>2))/sum(io1(idx)>2); 
    q(m)=quantile(i,quant);
end
clf;
plot(q,ps,'r.-');
hold on;
plot(q,po,'b.-');
hold on;
plot(q,ps1,'r-');
hold on;
plot(q,po1,'b-');
xlabel('cutoff of max(T)');
title('rank factor by T');
axis([q(1) q(end) 0 1]);
legend('#sperm genes dominated by factor 2/#sperm genes','#oocyte genes dominated by factor 1/#oocyte genes','#sperm genes/#genes of factor 2','#oocyte genes/#genes of factor 1');


% max U
[i,j]=max(U,[],2);
U_s=sort(U,2,'descend');
i1=U_s(:,1)./U_s(:,2);

js=j(sperm_tridx);
is=i(sperm_tridx);
is1=i1(sperm_tridx);
jo=j(oocyte_tridx);
io=i(oocyte_tridx);
io1=i1(oocyte_tridx);

figure(3);
m=0;
for quant=0.1:0.1:0.9
   m=m+1; 
   idx=find(is>quantile(i,quant));
    ps(m)=sum(js(idx)==2)/length(idx);
    ps1(m)=sum((js(idx)==2).*(is1(idx)>2))/sum(is1(idx)>2); 
  idx=find(io>quantile(i,quant));
    po(m)=sum(jo(idx)==1)/length(idx);
    po1(m)=sum((jo(idx)==1).*(io1(idx)>2))/sum(io1(idx)>2); 
%    p1(m)=sum(js(idx)==2)/sum((i>quantile(i,quant)).*(j==2));
    q(m)=quantile(i,quant);
end
clf;
plot(q,ps,'r.-');
hold on;
plot(q,po,'b.-');
hold on;
plot(q,ps1,'r-');
hold on;
plot(q,po1,'b-');
legend('#sperm genes dominated by factor 2/#sperm genes','#oocyte genes dominated by factor 1/#oocyte genes','#sperm genes/#genes of factor 2','#oocyte genes/#genes of factor 1');
xlabel('cutoff of max(U)');
title('rank factor by U');
axis([q(1) q(end) 0 1]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
X=xcentr;
Y=ycentr;

tr=zeros(size(Y,1),size(c,2));

tr0=sum(Y.^2,2);


for i=1:size(c,2)
    Y1=xcentr*w(:,1:i)*c(:,1:i)';
 
    tr(:,i)=sum((Y1-Y).^2,2);
end

tr=tr./repmat(tr0,[1 size(c,2)]);
tr=[ones(size(Y,1),1) tr];

tr_dif=tr(:,1:end-1)-tr(:,2:end);
%% c is the factor giving the biggest chi-square reduction
[i,j]=max(tr_dif,[],2);

js=j(sperm_tridx);
is=i(sperm_tridx);
jo=j(oocyte_tridx);
io=i(oocyte_tridx);

figure(4);
m=0;
for quant=0.1:0.1:0.9
   m=m+1; 
   idx=find(is>quantile(i,quant));
    ps(m)=sum(js(idx)==2)/length(idx);
    ps1(m)=sum(js(idx)==2)/sum((i>quantile(i,quant)).*(j==2));
   idx=find(io>quantile(i,quant));
    po(m)=sum(jo(idx)==1)/length(idx);
    po1(m)=sum(jo(idx)==1)/sum((i>quantile(i,quant)).*(j==1));
%    p1(m)=sum(js(idx)==2)/sum((i>quantile(i,quant)).*(j==2));
    q(m)=quantile(i,quant);
end
clf;
plot(q,ps,'r.-');
hold on;
plot(q,po,'b.-');
hold on;
plot(q,ps1,'r-');
hold on;
plot(q,po1,'b-');
title('rank factor by chi-square reduction');
xlabel('cutoff of max(chi-square)');
axis([q(1) q(end) 0 1]);
legend('#sperm genes dominated by factor 2/#sperm genes','#oocyte genes dominated by factor 1/#oocyte genes','#sperm genes/#genes of factor 2','#oocyte genes/#genes of factor 1');



% max TU
TU=T.*U;
idx=intersect(find(T<0),find(U<0));
TU(idx)=-TU(idx);
[i,j]=max(TU,[],2);
TU_s=sort(TU,2,'descend');
i1=TU_s(:,1)./TU_s(:,2);

%i1(find(i1>10))=10;
%i1(find(i1<-10))=-10;

js=j(sperm_tridx);
is=i(sperm_tridx);
is1=i1(sperm_tridx);
jo=j(oocyte_tridx);
io=i(oocyte_tridx);
io1=i1(oocyte_tridx);

%auc 

%genes dominant factor 2
idx3=find(j==2);
[idx3_s,g]=ismember(idx3,sperm_tridx);
%sperm genes dominant factor 2
sperm_idx3=idx3(find(idx3_s));
%non-sperm genes dominant factor 2
nosperm_idx3=idx3(find(~idx3_s));

%maximum value
sperm_i=[i(sperm_idx3); i(nosperm_idx3)];
%maximum/2nd maximum
sperm_i1=[i1(sperm_idx3); i1(nosperm_idx3)];
%labels: sperm vs. non-sperm
sperm_t=[ones(length(sperm_idx3),1);  zeros(length(nosperm_idx3),1)];
disp('auc1');
%roc(sperm_y,sperm_t)




sperm_y=[i1(sperm_idx3); i1(nosperm_idx3)];
sperm_t=[ones(length(sperm_idx3),1);  zeros(length(nosperm_idx3),1)];
disp('auc1');
roc(sperm_y,sperm_t)




figure(1);
m=0;
for quant=0.1:0.1:0.9
   m=m+1; 
   idx=find(is>quantile(i,quant));
     i11=is1(idx);
    ps(m)=sum(js(idx)==2)/length(idx);
    is11=i11(find(js(idx)==2));
    is12=i11(find(js(idx)~=3));
%   ps1(m)=sum(js(idx)==2)/sum((i>quantile(i,quant)).*(j==2));
    ps1(m)=sum((js(idx)==2).*(is1(idx)>2))/sum(is1(idx)>2); 
    ps2(m)=sum((js(idx)==2).*(is1(idx)>5))/sum(is1(idx)>5); 
  idx=find(io>quantile(i,quant));
    po(m)=sum(jo(idx)==1)/length(idx);
    po1(m)=sum((jo(idx)==1).*(io1(idx)>2))/sum(io1(idx)>2); 
    po2(m)=sum((jo(idx)==1).*(io1(idx)>5))/sum(io1(idx)>5); 

%    po1(m)=sum(jo(idx)==1)/sum((i>quantile(i,quant)).*(j==1));
    q(m)=quantile(i,quant);
end
clf;
plot(q,ps,'r-');
hold on;
plot(q,po,'b-');
hold on;
plot(q,ps1,'r.-');
hold on;
plot(q,po1,'b.-');
hold on;
plot(q,ps2,'r-o');
hold on;
plot(q,po2,'b-o');
title('rank factor by TU');
xlabel('cutoff of max(TU)');

axis([q(1) q(end) 0 1]);
%legend('#sperm genes dominated by factor 2/#sperm genes','#oocyte genes dominated by factor 1/#oocyte genes','#sperm genes/#genes of factor 2','#oocyte genes/#genes of factor 1');
legend('#sperm genes dominated by factor 2/#sperm genes','#oocyte genes dominated by factor 1/#oocyte genes','#sperm genes/#genes of factor 2 (1st/2nd >2)','#oocyte genes/#genes of factor 1 (1st/2nd >2)','#sperm genes/#genes of factor 2 (1st/2nd >5)','#oocyte genes/#genes of factor 1 (1st/2nd >5)');

% max abs(TU)
TU=T.*U;
idx=intersect(find(T<0),find(U<0));
TU(idx)=-TU(idx);
TU=abs(TU);
[i,j]=max(TU,[],2);
TU_s=sort(TU,2,'descend');
i1=TU_s(:,1)./TU_s(:,2);

%i1(find(i1>10))=10;
%i1(find(i1<-10))=-10;

js=j(sperm_tridx);
is=i(sperm_tridx);
is1=i1(sperm_tridx);
jo=j(oocyte_tridx);
io=i(oocyte_tridx);
io1=i1(oocyte_tridx);

%auc 

%genes dominant factor 2
idx3=find(j==2);
[idx3_s,g]=ismember(idx3,sperm_tridx);
%sperm genes dominant factor 2
sperm_idx3=idx3(find(idx3_s));
%non-sperm genes dominant factor 2
nosperm_idx3=idx3(find(~idx3_s));

%maximum value
sperm_i=[i(sperm_idx3); i(nosperm_idx3)];
%maximum/2nd maximum
sperm_i1=[i1(sperm_idx3); i1(nosperm_idx3)];
%labels: sperm vs. non-sperm
sperm_t=[ones(length(sperm_idx3),1);  zeros(length(nosperm_idx3),1)];
disp('auc1');
%roc(sperm_y,sperm_t)




sperm_y=[i1(sperm_idx3); i1(nosperm_idx3)];
sperm_t=[ones(length(sperm_idx3),1);  zeros(length(nosperm_idx3),1)];
disp('auc1');
roc(sperm_y,sperm_t)




figure(5);
m=0;
for quant=0.1:0.1:0.9
   m=m+1; 
   idx=find(is>quantile(i,quant));
     i11=is1(idx);
    ps(m)=sum(js(idx)==2)/length(idx);
    is11=i11(find(js(idx)==2));
    is12=i11(find(js(idx)~=3));
%   ps1(m)=sum(js(idx)==2)/sum((i>quantile(i,quant)).*(j==2));
    ps1(m)=sum((js(idx)==2).*(is1(idx)>2))/sum(is1(idx)>2); 
    ps2(m)=sum((js(idx)==2).*(is1(idx)>5))/sum(is1(idx)>5); 
  idx=find(io>quantile(i,quant));
    po(m)=sum(jo(idx)==1)/length(idx);
    po1(m)=sum((jo(idx)==1).*(io1(idx)>2))/sum(io1(idx)>2); 
    po2(m)=sum((jo(idx)==1).*(io1(idx)>5))/sum(io1(idx)>5); 

%    po1(m)=sum(jo(idx)==1)/sum((i>quantile(i,quant)).*(j==1));
    q(m)=quantile(i,quant);
end
clf;
plot(q,ps,'r-');
hold on;
plot(q,po,'b-');
hold on;
plot(q,ps1,'r.-');
hold on;
plot(q,po1,'b.-');
hold on;
plot(q,ps2,'r-o');
hold on;
plot(q,po2,'b-o');
title('rank factor by |TU|');
xlabel('cutoff of max(|TU|)');

axis([q(1) q(end) 0 1]);
%legend('#sperm genes dominated by factor 2/#sperm genes','#oocyte genes dominated by factor 1/#oocyte genes','#sperm genes/#genes of factor 2','#oocyte genes/#genes of factor 1');
legend('#sperm genes dominated by factor 2/#sperm genes','#oocyte genes dominated by factor 1/#oocyte genes','#sperm genes/#genes of factor 2 (1st/2nd >2)','#oocyte genes/#genes of factor 1 (1st/2nd >2)','#sperm genes/#genes of factor 2 (1st/2nd >5)','#oocyte genes/#genes of factor 1 (1st/2nd >5)');
