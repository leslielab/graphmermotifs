%extract the dominant factor for genes
%load ../matfiles/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt25_fuse=23.mat;
load ../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat;

T=result.T;
U=result.weights.u;
%U=ycentr*result.weights.q;
w=result.weights.r;
c=result.weights.q;
flip=mean(w,1);
flip_idx=find(flip<0);
%T(:,flip_idx)=-T(:,flip_idx);
%U(:,flip_idx)=-U(:,flip_idx);
%w(:,flip_idx)=-w(:,flip_idx);
%c(:,flip_idx)=-c(:,flip_idx);


%max T
[i,j]=max(T,[],2);
T_s=sort(T,2,'descend');

js=j(sperm_tridx);
is=i(sperm_tridx);
jo=j(oocyte_tridx);
io=i(oocyte_tridx);

figure(1);
subplot(2,1,1);
hist(js);
title('T sperm genes');
subplot(2,1,2);
hist(jo);
title('T oocyte genes');

% max U
[i,j]=max(U,[],2);
U_s=sort(U,2,'descend');
i1=U_s(:,1)./U_s(:,2);

js=j(sperm_tridx);
is=i(sperm_tridx);
jo=j(oocyte_tridx);
io=i(oocyte_tridx);

figure(2);
subplot(2,1,1);
hist(js);
title('U sperm genes');
subplot(2,1,2);
hist(jo);
title('U oocyte genes');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
X=xcentr;
Y=ycentr;

tr=zeros(size(Y,1),size(c,2));

tr0=sum(Y.^2,2);


for i=1:size(c,2)
    Y1=xcentr*w(:,1:i)*c(:,1:i)';
 
    tr(:,i)=sum((Y1-Y).^2,2);
end

tr=tr./repmat(tr0,[1 size(c,2)]);
tr=[ones(size(Y,1),1) tr];

tr_dif=tr(:,1:end-1)-tr(:,2:end);
%% c is the factor giving the biggest chi-square reduction
[i,j]=max(tr_dif,[],2);

js=j(sperm_tridx);
is=i(sperm_tridx);
jo=j(oocyte_tridx);
io=i(oocyte_tridx);

figure(3);
subplot(2,1,1);
hist(js);
title('chi-square sperm genes');
subplot(2,1,2);
hist(jo);
title('chi-square oocyte genes');

% max TU
TU=T.*U;
idx=intersect(find(T<0),find(U<0));
TU(idx)=-TU(idx);
[i,j]=max(TU,[],2);
TU_s=sort(TU,2,'descend');


js=j(sperm_tridx);
is=i(sperm_tridx);
jo=j(oocyte_tridx);
io=i(oocyte_tridx);

figure(4);
subplot(2,1,1);
hist(js);
title('TU sperm genes');
subplot(2,1,2);
hist(jo);
title('TU oocyte genes');

% max abs(TU)
TU=T.*U;
idx=intersect(find(T<0),find(U<0));
TU(idx)=-TU(idx);
TU=abs(TU);
[i,j]=max(TU,[],2);
TU_s=sort(TU,2,'descend');

js=j(sperm_tridx);
is=i(sperm_tridx);
jo=j(oocyte_tridx);
io=i(oocyte_tridx);

figure(5);
subplot(2,1,1);
hist(js);
title('abs(TU) sperm genes');
subplot(2,1,2);
hist(jo);
title('abs(TU) oocyte genes');
