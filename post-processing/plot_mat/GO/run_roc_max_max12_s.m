clear all;

load ../matfiles/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt25_fuse=23.mat;
for ks=1:15

%extract the dominant factor for genes
ks

%[a,b]=max(abs(w_x_sperm_corr),[],1);
T=result.T;
U=result.weights.u;
%U=ycentr*result.weights.q;
w=result.weights.r;
c=result.weights.q;
flip=mean(w,1);
flip_idx=find(flip<0);
T(:,flip_idx)=-T(:,flip_idx);
U(:,flip_idx)=-U(:,flip_idx);
w(:,flip_idx)=-w(:,flip_idx);
c(:,flip_idx)=-c(:,flip_idx);


TU=T.*U;
idx=intersect(find(T<0),find(U<0));
TU(idx)=-TU(idx);

%TU=U;

%max T
[roc_max_s,q_max12_s,roc_max12_s,q_max_s,p_s,N_s]=roc_max_max12(TU,ks,sperm_tridx);
[roc_max_s_abs,q_max12_s_abs,roc_max12_s_abs,q_max_s_abs,p_s_abs,N_s_abs]=roc_max_max12(abs(TU),ks,sperm_tridx);

%figure(1);
%clf;
%plot(q_max12_s,roc_max_s,'r.-');
%hold on;
%plot(q_max12_s_abs,roc_max_s_abs,'r');
%xlabel(sprintf('cutoff of max(TU,%d)/2nd max(TU,%d)',ks,ks));
%ylabel('AUC');
%title(sprintf('max(TU,%d) AUC for sperm genes',ks));
%legend('TU','|TU|');

%figure(2);
%clf;
%plot(q_max_s,roc_max12_s,'r.-');
%hold on;
%plot(q_max_s_abs,roc_max12_s_abs,'r');
%xlabel(sprintf('cutoff of max(TU,%d)',ks));
%ylabel('AUC');
%title(sprintf('max(TU,%d)/2nd max(TU,%d) AUC for oocyte genes',ks,ks));
%legend('TU','|TU|');



N_s1(ks)=N_s;
N_s_abs1(ks)=N_s_abs;
roc_max12_s_abs1(ks)=max(roc_max12_s_abs);
roc_max_s_abs1(ks)=max(roc_max_s_abs);
roc_max12_s1(ks)=max(roc_max12_s);
roc_max_s1(ks)=max(roc_max_s);

%disp([ko ks p_o_abs p_o p_s_abs p_s])
%disp([ max(roc_max12_s_abs) max(roc_max_s_abs) max(roc_max12_s) max(roc_max_s)])

end
