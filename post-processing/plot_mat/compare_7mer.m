%
load ../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat tst_l sperm_tst_l oocyte_tst_l;
tst_l_67=squeeze(tst_l(4,2,1,:));
sperm_tst_l_67=squeeze(sperm_tst_l(4,2,1,:));
oocyte_tst_l_67=squeeze(oocyte_tst_l(4,2,1,:));

load ../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer7_l4f2_weightedA.mat tst_l sperm_tst_l oocyte_tst_l;
tst_l_57=squeeze(tst_l(4,2,1,:));
sperm_tst_l_57=squeeze(sperm_tst_l(4,2,1,:));
oocyte_tst_l_57=squeeze(oocyte_tst_l(4,2,1,:));
oocyte_tst_l_57(1)=oocyte_tst_l_67(1);

load ../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt25_fuse=23.mat tst_l sperm_tst_l oocyte_tst_l;
tst_l_56=squeeze(tst_l(4,3,1,1:11));
sperm_tst_l_56=squeeze(sperm_tst_l(4,3,1,1:11));
oocyte_tst_l_56=squeeze(oocyte_tst_l(4,3,1,1:11));
oocyte_tst_l_56(1)=oocyte_tst_l_67(1);




tst_l0=tst_l_56(1);
sperm_tst_l0=sperm_tst_l_56(1);
oocyte_tst_l0=oocyte_tst_l_56(1);


figure(2);
clf;
plot(0:10,tst_l_56/tst_l0,'r.-');
hold on;
plot(0:10,tst_l_67/tst_l0,'g.-');
hold on;
plot(0:10,tst_l_57/tst_l0,'k.-');
legend('5-6mers','6-7mers','5-7mers');
xlabel('number of factors');
ylabel('chi-square');
title('Normalized squared mean error on test data for regularized PLS');

figure(1);
clf;
plot(0:10,sperm_tst_l_56/sperm_tst_l0,'r.-');
hold on;
plot(0:10,sperm_tst_l_67/sperm_tst_l0,'g.-');
hold on;
plot(0:10,sperm_tst_l_57/sperm_tst_l0,'k.-');
legend('5-6mers','6-7mers','5-7mers');
xlabel('number of factors');
ylabel('chi-square');
title('Normalized squared mean error on test data for regularized PLS (sperm genes)');

figure(3);
clf;
plot(0:10,oocyte_tst_l_56/oocyte_tst_l0,'r.-');
hold on;
plot(0:10,oocyte_tst_l_67/oocyte_tst_l0,'g.-');
hold on;
plot(0:10,oocyte_tst_l_57/oocyte_tst_l0,'k.-');
legend('5-6mers','6-7mers','5-7mers');
xlabel('number of factors');
ylabel('chi-square');
title('Normalized squared mean error on test data for regularized PLS (oocyte genes)');
