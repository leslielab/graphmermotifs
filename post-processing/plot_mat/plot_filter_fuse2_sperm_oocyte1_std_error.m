load ../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67.mat motifs sperm_tridx oocyte_tridx cexp_tr result sperm_tst_l oocyte_tst_l tst_l oocyte_tst_s;

tst_l1=tst_l;
sperm_tst_l1=sperm_tst_l;
oocyte_tst_l1=oocyte_tst_l;

load ../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat sperm_tst_l oocyte_tst_l tst_l;

tst_l1(4,2,1,:)=tst_l(4,2,1,:);
sperm_tst_l1(4,2,1,:)=sperm_tst_l(4,2,1,:);
oocyte_tst_l1(4,2,1,:)=oocyte_tst_l(4,2,1,:);
tst_l=tst_l1;
sperm_tst_l=sperm_tst_l1;
oocyte_tst_l=oocyte_tst_l1;



lasso=4;
tst_l=squeeze(tst_l(4,2,:,:));
sperm_tst_l=squeeze(sperm_tst_l(4,2,:,:));
oocyte_tst_l=squeeze(oocyte_tst_l(4,2,:,:));


tst0=mean(tst_l,1);
tst0=tst0(1);
sperm_tst0=mean(sperm_tst_l,1);
sperm_tst0=sperm_tst0(1);
oocyte_tst0=mean(oocyte_tst_l,1);
oocyte_tst0=oocyte_tst0(1);

%remove 0 for CV set 2
sperm_tst_l(2,:)=[];
oocyte_tst_l(2,:)=[];


figure(2);
clf;
%plot(0:10,mean(sperm_tst_l,1)/sperm_tst0,'k.-','LineWidth',2,'Markersize',20);
%hold on;
%p2=plot(0:10,mean(oocyte_tst_l,1)/oocyte_tst0,'b.-','LineWidth',2,'Markersize',20);

sperm_norm_tst=normalize_PLS(sperm_tst_l);
sperm_mean_tst=mean(sperm_norm_tst,1);
sperm_std_tst=std(sperm_norm_tst,1);
h1=errorbar(0:10,sperm_mean_tst,sperm_std_tst,'k.-','LineWidth',2,'Markersize',20);

hold on;
oocyte_norm_tst=normalize_PLS(oocyte_tst_l);
oocyte_mean_tst=mean(oocyte_norm_tst,1);
oocyte_std_tst=std(oocyte_norm_tst,1);
h2=errorbar(0:10,oocyte_mean_tst,oocyte_std_tst,'k.-','LineWidth',2,'Markersize',20);
set(h2,'Color',[0.5 0.5 0.5]);
axis([0 11 0.6 1.6]);

%h_legend=legend([h1 h2],'Sperm gene set','Oocyte gene set');
h_legend=legend('Sperm gene set','Oocyte gene set');
h_xlabel=xlabel('Number of factors');
h_ylabel=ylabel('Mean squared prediction error');
h_title=title('Normalized mean squared prediction error');

set(gca,'YTick',0.6:0.2:1.6);
set(gca,'XTick',0:2:10);
set(gca,'FontSize',18);
set(h_legend,'FontSize',24);
set(h_title,'FontSize',24);
set(h_xlabel,'FontSize',24);
set(h_ylabel,'FontSize',24);

exportfig(gcf,'figs1/sperm_oocyte.eps','color','rgb');
