PLS.eps:plot_PLS.m 
sperm_gene.eps, oocyte_gene.eps:plot_filter_fuse2_sperm_oocyte.m
sperm_oocyte.eps: plot_filter_fuse2_sperm_oocyte1.m
ACGTG.eps: output_mot_dist.m
PCA_PLS1.eps,PCA_PLS2.eps: PCA_PLS.m
sperm_MCS.eps, oocyte_MCS.eps: orthologo/analyze_rate.m


PLS_ind.eps, PLS_ind_time.eps: study_reduce/study_PLS_ind.m
corr_kmer_factor1-4.eps: GO/kmer_to_corr.m
cluster_E.eps, cluster_F.eps: clustering_PCA/cluster_EF.m
cluster_1,2,3.eps, cluster1,2,3_mot.eps: clustering_PCA/run_cluster3.m process_alignace.m
sperm/oocyte_kmer.eps, sperm/oocyte_corr.eps: GO/study_GSEA_sperm_oocyte.m
count_edge.eps:cluster_coeff/compare_cyto_clustercoeff.m
oocyte_pval.eps:ana_factor_oocyte.m
sperm_pval.eps:ana_factor_sperm.m
omni_hill: GO/GO_hill.m
