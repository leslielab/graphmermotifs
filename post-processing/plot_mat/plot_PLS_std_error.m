%export figs1
addpath('GO/');

load ../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_rand.mat tst_l;
tst_l_rand=squeeze(tst_l(4,2,1:6,:));
ign=mean(tst_l_rand,1);
tst_l_rand1=ign(1);

load ../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67.mat tst_l;
tst_l1=tst_l;
load ../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat tst_l result;

tst_l1(4,2,1,:)=tst_l(4,2,1,:);
tst_l=tst_l1;
tst_l=squeeze(tst_l(4,2,:,:));

%nonzero
fold_idx=find(tst_l(:,1)~=0);

load ../../../experiments/PLS_filter_mot_count_bkseq_zscore_qt50_kmer67.mat tst_s tst_s_ind tst_s_rand;

tst_s=tst_s(fold_idx,:);
tst_l=tst_l(fold_idx,:);
tst_s_ind=tst_s_ind(fold_idx,:);
tst_s_rand=tst_s_rand(fold_idx,:);


tst1=mean(tst_s,1);
tst1=tst1(1);



figure(1);
clf;
%plot(0:10,mean(tst_s,1)/tst1,'b.-','LineWidth',2,'Markersize',20);
%hold on;
%plot(0:10,mean(tst_s_rand,1)/tst1,'bx--','LineWidth',2,'Markersize',8);
%hold on;
%plot(0:10,mean(tst_l,1)/tst1,'k.-','Linewidth',2,'Markersize',20);
%hold on;
%plot(0:10,mean(tst_l_rand,1)/tst_l_rand1,'kx--','Linewidth',2,'Markersize',8);
%axis([0 10 0.8 1.6]);


norm_tst=normalize_PLS(tst_s);
mean_tst=mean(norm_tst,1);
std_tst=std(norm_tst,1);
h1=errorbar(0:10,mean_tst,std_tst,'b.-','LineWidth',2,'Markersize',20);
hold on;

norm_tst=normalize_PLS(tst_s_rand);
mean_tst=mean(norm_tst,1);
std_tst=std(norm_tst,1);
h2=errorbar(0:10,mean_tst,std_tst,'bx--','LineWidth',2,'Markersize',8);
hold on;

norm_tst=normalize_PLS(tst_l);
mean_tst=mean(norm_tst,1);
std_tst=std(norm_tst,1);
h3=errorbar(0:10,mean_tst,std_tst,'k.-','LineWidth',2,'Markersize',20);
hold on;

norm_tst=normalize_PLS(tst_l_rand);
mean_tst=mean(norm_tst,1);
std_tst=std(norm_tst,1);
h4=errorbar(0:10,mean_tst,std_tst,'kx--','LineWidth',2,'Markersize',8);
axis([0 11 0.8 1.6]);
%h_legend=legend('Standard PLS','Standard PLS on randomized data','Graph-regularized PLS','Graph-regularized PLS on randomized data','location','NorthWest');

%h_legend=legend('Standard PLS','Standard PLS on randomized data','Graph-regularized PLS','Graph-regularized PLS on randomized data');
h_legend=legend([h1 h2 h3 h4],'Standard PLS','Standard PLS on randomized data','Graph-regularized PLS','Graph-regularized PLS on randomized data');

h_xlabel=xlabel('Number of factors');
h_ylabel=ylabel('Mean squared prediction error');
%h_title=title('Normalized mean squared prediction error on test data versus number of latent factors')
h_title=title('Normalized mean squared prediction error');

set(gca,'YTick',0.8:0.2:1.6);
set(gca,'XTick',0:2:10);
set(gca,'FontSize',20);
set(h_legend,'FontSize',22);
set(h_title,'FontSize',24);
set(h_xlabel,'FontSize',24);
set(h_ylabel,'FontSize',24);

%exportfig(gcf,'figs1/PLS.eps','color','rgb');
%f=getframe(gcf);
imwrite(gcf,'figs1/PLS.pdf','color','rgb','Resolution',600);
