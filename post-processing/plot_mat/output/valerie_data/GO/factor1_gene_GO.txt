embryonic development ending in birth or egg hatching	2.567342e-07
body morphogenesis	4.176562e-07
cell division	5.682791e-07
embryonic development	8.365347e-07
behavior	9.162807e-07
reproductive behavior in a multicellular organism	1.039820e-06
oviposition	1.039820e-06
reproductive behavior	1.039820e-06
cytokinesis	1.104489e-06
reproductive process in a multicellular organism	1.220993e-06
multicellular organism reproduction	1.432031e-06
organ development	1.874586e-06
gonad development	2.500989e-06
reproductive structure development	2.500989e-06
locomotion	2.512409e-06
organelle localization	2.796760e-06
establishment of organelle localization	2.796760e-06
development of primary sexual characteristics	2.939218e-06
growth	3.037845e-06
regulation of growth	3.345366e-06
anatomical structure morphogenesis	3.622368e-06
hermaphrodite genitalia development	4.166970e-06
morphogenesis of an epithelium	4.267374e-06
anatomical structure development	4.622006e-06
pronuclear migration	4.851739e-06
fertilization	4.851739e-06
single fertilization	4.851739e-06
genitalia development	5.226450e-06
cuticle development	5.600644e-06
protein-based cuticle development	5.600644e-06
collagen and cuticulin-based cuticle development	5.600644e-06
molting cycle	5.600644e-06
molting cycle, protein-based cuticle	5.600644e-06
molting cycle, collagen and cuticulin-based cuticle	5.600644e-06
establishment of nucleus localization	5.653560e-06
nuclear migration	5.653560e-06
nucleus localization	5.653560e-06
aging	7.244729e-06
multicellular organismal aging	7.244729e-06
determination of adult life span	7.244729e-06
positive regulation of growth	7.244925e-06
vulval development	8.508919e-06
regulation of multicellular organism growth	8.661211e-06
multicellular organism growth	8.661211e-06
positive regulation of growth rate	9.179270e-06
regulation of growth rate	9.179270e-06
system development	1.022520e-05
post-embryonic development	1.172801e-05
larval development	1.172801e-05
nematode larval development	1.172801e-05
positive regulation of multicellular organism growth	1.303068e-05
positive regulation of developmental process	1.321284e-05
sex determination	1.613875e-05
sexual reproduction	1.651991e-05
positive regulation of vulval development	1.677417e-05
regulation of vulval development	1.787281e-05
reproductive process	1.832059e-05
mitotic spindle organization and biogenesis	1.998656e-05
negative regulation of vulval development	2.002619e-05
sex differentiation	2.177935e-05
spindle organization and biogenesis	2.310554e-05
cell motility	2.430183e-05
localization of cell	2.430183e-05
germ-line sex determination	2.433202e-05
hermaphrodite germ-line sex determination	2.433202e-05
reproductive developmental process	2.480988e-05
dauer larval development	2.786270e-05
regulation of developmental process	3.092627e-05
meiotic cell cycle	3.105093e-05
regulation of locomotion	3.463027e-05
reproduction	3.565118e-05
positive regulation of biological process	4.056897e-05
M phase of meiotic cell cycle	4.234070e-05
meiosis	4.234070e-05
cell migration	4.687212e-05
negative regulation of growth	4.873482e-05
negative regulation of multicellular organism growth	4.873482e-05
negative regulation of developmental process	5.389409e-05
cell cycle	5.623870e-05
multicellular organismal development	5.731394e-05
positive regulation of locomotion	5.807384e-05
developmental process	5.841320e-05
negative regulation of biological process	6.304549e-05
cell cycle process	6.696123e-05
cell cycle phase	6.730367e-05
M phase	6.730367e-05
mitotic cell cycle	6.741465e-05
establishment of cellular localization	6.864216e-05
chromosome segregation	7.049792e-05
regulation of cell cycle	7.137091e-05
cellular localization	7.299289e-05
dormancy process	7.520583e-05
response to stimulus	7.879875e-05
regulation of cell cycle process	7.942535e-05
meiotic chromosome segregation	8.377485e-05
gamete generation	8.477401e-05
multicellular organismal process	8.921794e-05
gastrulation	9.123600e-05
regulation of cell proliferation	1.009976e-04
dauer entry	1.065842e-04
microtubule cytoskeleton organization and biogenesis	1.088658e-04
cellular developmental process	1.263507e-04
cell differentiation	1.263507e-04
cell death	1.388079e-04
death	1.388079e-04
cell development	1.406477e-04
female gamete generation	1.408074e-04
oogenesis	1.408074e-04
masculinization of hermaphroditic germ-line	1.489474e-04
nuclear organization and biogenesis	1.681665e-04
cytoskeleton organization and biogenesis	1.914665e-04
positive regulation of embryonic development	2.022144e-04
microtubule-based process	2.049096e-04
regulation of meiosis	2.160705e-04
cell cycle switching	2.160705e-04
cell cycle switching, mitotic to meiotic cell cycle	2.160705e-04
germline cell cycle switching, mitotic to meiotic cell cycle	2.160705e-04
cell proliferation	2.162102e-04
gastrulation with mouth forming first	3.036049e-04
cellular component organization and biogenesis	3.187283e-04
response to stress	3.319535e-04
regulation of embryonic development	4.505663e-04
biological regulation	5.166071e-04
cytoplasm organization and biogenesis	5.256731e-04
defense response	5.602131e-04
negative regulation of transferase activity	6.172459e-04
negative regulation of kinase activity	6.172459e-04
negative regulation of protein kinase activity	6.172459e-04
regulation of biological process	6.315858e-04
centrosomal and pronuclear rotation	6.574197e-04
regulation of transferase activity	6.582258e-04
regulation of kinase activity	6.582258e-04
regulation of protein kinase activity	6.582258e-04
sensory perception of mechanical stimulus	6.789999e-04
system process	7.149054e-04
response to external stimulus	7.245191e-04
organelle organization and biogenesis	7.272739e-04
di-, tri-valent inorganic cation homeostasis	7.296654e-04
response to abiotic stimulus	7.646709e-04
mitotic metaphase/anaphase transition	8.209793e-04
protein import	8.269485e-04
mechanosensory behavior	8.719603e-04
response to mechanical stimulus	8.719603e-04
gene silencing by miRNA, production of miRNAs	9.416418e-04
primary microRNA processing	9.416418e-04
secretion	9.896748e-04
