function pval=gsea(rvals,svals,pexp,numperms,GO_term)
% Performs a variant of GSEA as described in Subramanian et.al. PNAS (early
% ed -- DOI 10.1073.  rvals are the 'correlation' values for each gene, and
% they must be in sort order.  svals are +1 or -1 depending on whether the
% gene is in the reference set S.  pexp is the exponent for the rvals for
% computation of the ES value.  Gene permutation only is used here, so
% there is no need for the actual data (there are not enough conditions in
% the data I am using to make condition permutation workable).  No multiple
% hypothesis testing is supported.
%

 [rvals,idx]=sort(rvals,'descend');
 svals=svals(idx);
% get distribution of es scores based on randomization
for i=1:numperms
    es_perm=compute_ES(rvals,svals(randperm(length(svals))),pexp);
    es_dist(i)=es_perm;
end;

[es_actual,leadedge_actual,es_scores_actual]=compute_ES(rvals,svals,pexp);
if es_actual>max(es_dist(:))
    pval=1/numperms;
else
    pval=sum(es_dist(:)>=es_actual)/numperms;
end;

% Chebyschev bound
es_dist_std=std(es_dist(:));
es_dist_mean=mean(es_dist(:));
es_actual_Z = abs(es_actual-es_dist_mean)/es_dist_std;
pval_cheby=1/(es_actual_Z.^2);

% print output file
%outfid=fopen(outfile,'w');
%fprintf(outfid,'PARAMETERS FOR THIS EXECUTION OF gsea2.m:\n%s\nEND OF %PARAMETERS\n\n',parmstr);
%fprintf(outfid,'Max ES score:\t%g\n',es_actual);
%fprintf(outfid,'P value:\t%g\n',pval);
%fprintf(outfid,'Null ES mean:\t%g\n',es_dist_mean);
%fprintf(outfid,'Null ES std:\t%g\n',es_dist_std);
%fprintf(outfid,'Max ES score Z score:\t%g\n',es_actual_Z);
%fprintf(outfid,'P value Chebyshev:\t%g\n',pval_cheby);
%fprintf(outfid,'Leading Edge Size:\t%d',leadedge_actual);
%fprintf(outfid,'\n\nGene\tES_score\tInLeadingEdge?\tInSet?%\tOverallRank\tOrthologRank\tNonOrthologRank\n');

figure(1);
clf;
plot(1:length(es_scores_actual),es_scores_actual(:));
yy=ylim;
xx=xlim;
hold on;
plot([leadedge_actual leadedge_actual],[yy(1) yy(2)],'r:');
%title(GO_term);
xlabel('gene number');
ylabel('running enrichment score');

function [es,leadedge_ix,es_scores_all]=compute_ES(rvals,svals,pexp);
  nargout_num=nargout;
  N_hit=0;
  norm_hit_score=zeros(length(rvals),1);
  norm_hit=zeros(length(rvals),1);
%  for i=1:length(rvals);
%      if svals(i)==1
%          N_hit=N_hit+1;
%          norm_hit(i)=1;
%          if pexp~=0
%             if rvals(i)~=0
%                  norm_hit_score(i)=abs(rvals(i))^pexp;
%              else
%                  norm_hit_score(i)=0;
%              end;
%          else
%              norm_hit_score(i)=1;
%          end;
%      end;
%  end;

  N_hit=sum(svals==1);
  norm_hit(find(svals==1))=1;
  norm_hit_score=norm_hit;

 norm_hit_score1=norm_hit_score;
  norm_hit_score=cumsum(norm_hit_score);
  norm_hit_score=norm_hit_score/norm_hit_score(end);
  
  N_miss=length(rvals)-N_hit;
  norm_miss_score=cumsum(1-norm_hit)/N_miss;
 
  es_score=norm_hit_score-norm_miss_score;
  max_es_score_ix=find(es_score==max(es_score)); 
%  es_score=zeros(length(rvals),1);
%  for i=1:length(rvals)
%      es_score(i)=norm_hit_score(i)-norm_miss_score(i);      
%      if i==1
%          max_es_score_ix=i;
%      elseif es_score(i)>es_score(max_es_score_ix)
%          max_es_score_ix=i;
%      end;
%  end;
  if nargout_num>=1
      es=es_score(max_es_score_ix(1));
  end;
  if nargout_num>=2
      leadedge_ix=max_es_score_ix;
  end;
  if nargout_num>=3
      es_scores_all=es_score;
  end;
  
  return;
      
