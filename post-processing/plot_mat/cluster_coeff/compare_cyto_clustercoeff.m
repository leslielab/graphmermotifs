%compare total number of edges/average clustering coefficients among PLS, lasso ,fuse, and fuse lasso

addpath('../');
order='descend';
output='../cytoscape_files/test.txt'
N=50;
sum_w=0.3;
%fix number of N if choose_w=0
%normalize w to determine N
choose_w=0;


file1='../../../../experiments/PLS_filter_mot_count_bkseq_zscore_qt50_kmer67.mat';
for i=1:5
%c_avg: average clustering coefficient
%edge: total number of edges
[c_avg(1,i),edge(1,i),Ns(1,i)]=output_cyto(file1,output,i,N,sum_w,order,choose_w);
end
file2='../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_onlylasso.mat';
for i=1:5
[c_avg(2,i),edge(2,i),Ns(2,i)]=output_cyto(file2,output,i,N,sum_w,order,choose_w);
end
file3='../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_onlyfuse.mat';
for i=1:5
[c_avg(3,i),edge(3,i),Ns(3,i)]=output_cyto(file3,output,i,N,sum_w,order,choose_w);
end
file4='../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat';
for i=1:5
[c_avg(4,i),edge(4,i),Ns(4,i)]=output_cyto(file4,output,i,N,sum_w,order,choose_w);
end

figure(1);
clf;
bar(c_avg','group');
legend('PLS','lasso','fuse','lasso+fuse');

if(choose_w==1)
title(char(['average clustering coefficients given N defined w(1:N)=sum(w)*',num2str(sum_w)]));
%title(char(['average clustering coefficients given N defined']));
figure(2);
bar(N','group');
legend('PLS','lasso','fuse','lasso+fuse');
title('N');

else
title(char(['average clustering coefficients given N=',num2str(N)]));
end


%plot total number of edges
figure(3);
clf; 
bar(edge','group');
h_ylabel=ylabel('Total number of edges');
h_xlabel=xlabel('Latent factor');
h_legend=legend('Standard PLS','Lasso constrained PLS','Graph constrained PLS','Both Lasso and graph constrained PLS'); 

h_title=title('Number of graph-mer edges under different PLS constraints');

set(h_title,'FontSize',20);
set(h_legend,'FontSize',20);
set(h_xlabel,'FontSize',20);
set(h_ylabel,'FontSize',20);
exportfig(gcf,'../figs1/count_edge.eps','color','rgb');
