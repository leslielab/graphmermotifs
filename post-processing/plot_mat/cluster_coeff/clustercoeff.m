function [c_avg,edge] = clusteringcoef(g)
% clusteringcoef    - clustering coefficient of given adjacency matrix
%
%   coefs = clusteringcoef(g) cluster coefficient is ratio of the number of
%   edges Ei between the first neighbors of the vertex i, and the
%   respective number of edges, Ei(max) = ai(ai-1)/2, in the complete graph
%   that can be formed by the nearest neighbors of this vertex:
%
%   g is a graph or an alternatively adjacency matrix.
%
%          2 Ei
%   ci = -----------
%        ai (ai - 1)
%

if isa(g, 'graph')
    adj = adjacency(g);
else
    adj = g;
end

n = length(adj);
%ci = ones(1,n) .* nan;
ci = zeros(1,n);
for k = 1:n
    neighbours = find(adj(:,k));
    neighbours(find(neighbours==k)) = []; % self link deleted
    a = length(neighbours);
    if a < 2, continue; end
    E = a;
    for k1 = 1:a
        for k2 = k1+1:a
            if adj(neighbours(k1),neighbours(k2)) | adj(neighbours(k2),neighbours(k1))
                E = E + 1;
          end
        end
    end
    ci(k) = 2 * E / (a*(a+1));
end

        
c_avg=mean(ci);  
edge=sum(sum(g))/2;      
