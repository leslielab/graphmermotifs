function tst_norm=normalize(tst)

tst_norm=tst./repmat(tst(:,1),[1 size(tst,2)]);
