%expand a core in all sequences to obtain a kmer set

load ../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt25_fuse=23.mat;


core_mot='AATTA';
d1=2;
d2=2;
file_sperm=char(['kmer_set/',core_mot,'_sperm.txt']);
file_no=char(['kmer_set/',core_mot,'_no.txt']);

logo_sperm=char(['../../mot_map/core_logo/',core_mot,'_sperm.png']);
logo_no=char(['../../mot_map/core_logo/',core_mot,'_no.png']);

%get the length of each promoter sequence
addpath('../');
dir='../../../../data/';
load([dir,'seqs.mat'],'a');
seq=a';
len=500*ones(1,size(a,1));
a1=a;
a1(:,end)=1;
x=(a1==5);
x1=x(:,2:end)-x(:,1:end-1);
[m,n]=find(x1);

len(m)=n;

mot_num(find(core_mot=='A'))=1;
mot_num(find(core_mot=='C'))=2;
mot_num(find(core_mot=='G'))=3;
mot_num(find(core_mot=='T'))=4;

pbg=textread([dir,'pbg.txt']);
dimers_maxgap=10;
        
        pssm=[core_mot=='A';core_mot=='C';core_mot=='G';core_mot=='T'];
        pssm=pssm+eps*ones(size(pssm));
       pthr=sum(max(log(pssm./(pbg'*ones(1,size(pssm,2)))),[],1))-1;
       mot_groupidx=0;
      %Nhit is  count matrix (length of promoter*number of genes) 
      Nhit=scan_pssm(dir,mot_groupidx,pssm,pbg,pthr,dimers_maxgap);  
     
      no_idx=setdiff(1:size(Nhit,2),sperm_idx);
      Nhit_sperm=Nhit(:,sperm_idx);
      Nhit_no=Nhit(:,no_idx);
       seq_sperm=seq(:,sperm_idx);
       seq_no=seq(:,no_idx);

     
     scan_core(mot_num,Nhit_sperm,seq_sperm,file_sperm,d1,d2);
     scan_core(mot_num,Nhit_no,seq_no,file_no,d1,d2); 



cd ../HSA/weblogo;
%
eval(char(['!./seqlogo -F PNG -f ../../mot_map/',file_sperm,'>',logo_sperm,' -c']));
%
eval(char(['!./seqlogo -F PNG -f ../../mot_map/',file_no,'>',logo_no,' -c'])); 


cd ../../mot_map;

