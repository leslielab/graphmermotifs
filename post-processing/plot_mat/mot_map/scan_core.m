 function scan_core(mot_num,Nhit,seq,file,d1,d2)
%d1=d2=2   
 %extend the core kmer in the sequences, output the kmer set to file 
       %length of the kmer 
       N=length(mot_num);    
 
       pos=find(Nhit);
       pos1=pos;

       for i=1:d1  
          pos1=[pos-i pos1];
       end 

       for i=1:d2+N-1
         pos1=[pos1 pos+i];
       end
 
      pos=pos1; 
      spos=seq(pos);
     % reverse complement 
    %    inv_idx=find(spos(:,1+d1)==5-mot_num(end));
       
        [i,j]=ismember(spos(:,d1+1:d1+N),5-mot_num(end:-1:1),'rows');
        inv_idx=find(i);
        spos(inv_idx,:)=5-spos(inv_idx,end:-1:1);
     
       spos(find(spos==1))='A'; 
       spos(find(spos==2))='C'; 
       spos(find(spos==3))='G'; 
       spos(find(spos==4))='T'; 
     %  spos(find(spos==5))='N';
     %  spos(find(spos==0))='N';
       spos(find(spos==5))='G';
       spos(find(spos==0))='G';
       
       spos=char(spos);

       dlmwrite(file,spos,'delimiter','');
  

