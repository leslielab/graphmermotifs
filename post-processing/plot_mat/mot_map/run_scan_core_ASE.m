%expand a core in all sequences to obtain a kmer set

function run_scan_core_ASE(core_mot)
load ../../../../experiments/PLS_AFD_ASE_kmer67.mat;

%core_mot='AGCCAA';
d1=3;
d2=3;

file=char(['kmer_set/',core_mot,'.txt']);
logo=char(['../../mot_map/core_logo/',core_mot,'.png']);

%get the length of each promoter sequence
addpath('../');
dir='../../../../data/AFD_ASE/';
load([dir,'seqs.mat'],'a');
seq=a';
len=500*ones(1,size(a,1));
a1=a;
a1(:,end)=1;
x=(a1==5);
x1=x(:,2:end)-x(:,1:end-1);
[m,n]=find(x1);

len(m)=n;

mot_num(find(core_mot=='A'))=1;
mot_num(find(core_mot=='C'))=2;
mot_num(find(core_mot=='G'))=3;
mot_num(find(core_mot=='T'))=4;

pbg=textread([dir,'pbg.txt']);
dimers_maxgap=10;
        
        pssm=[core_mot=='A';core_mot=='C';core_mot=='G';core_mot=='T'];
        pssm=pssm+eps*ones(size(pssm));
       pthr=sum(max(log(pssm./(pbg'*ones(1,size(pssm,2)))),[],1))-1;
       mot_groupidx=0;
      %Nhit is  count matrix (length of promoter*number of genes) 
      Nhit=scan_pssm(dir,mot_groupidx,pssm,pbg,pthr,dimers_maxgap);  
     
     
     scan_core(mot_num,Nhit,seq,file,d1,d2); 



cd ../HSA/weblogo;
eval(char(['!./seqlogo -F PNG -f ../../mot_map/',file,'>',logo,' -c'])); 


cd ../../mot_map;


