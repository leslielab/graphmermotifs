load ../../matfiles/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt25_fuse=23.mat;
oocyte_genes=targets_names(oocyte_idx);
sperm_genes=targets_names(sperm_idx);

file='sperm_oocyte_operon.mat';

%cut strand_coord.txt and gene_operon.txt from operon.txt 
 %!cut -f1 operon.txt> strand.txt;
 %!cut -f2 operon.txt> coord.txt;
%gene 
%!cut -f7 operon.txt > gene.txt;
 %!sed -i 's/^/>/' gene.txt;
%which operon
 %!cut -f6 operon.txt > operon1.txt; 

%read (strand,coord,gene,operon) and (gene, promoter)
   strand=textread('strand.txt');
   coord=textread('coord.txt');
   strand_coord=strand.*coord;
   gene=textread('gene.txt','%s');
   operon=textread('operon1.txt','%s');
 %unique operons
   uni_operon=unique(operon);


 %  !rm strand.txt coord.txt gene.txt operon1.txt; 

sperm_operon=[];
sperm_operon_genes=[];
sperm_operon_allgenes=[];
sperm_operon_downstream_genes=[];
sperm_isoperon=[];

oocyte_operon=[];
oocyte_operon_genes=[];
oocyte_operon_allgenes=[];
oocyte_operon_downstream_genes=[];
oocyte_isoperon=[];

%for every unique operon
  for i=1:length(uni_operon)
      i
              

     idx=find(ismember(operon,uni_operon(i)));
%get strand*coords and genes for that operon 
    
% genes in that operon
      strand_coord_i=strand_coord(idx);
      gene_i=gene(idx);

      [s_k,j]=ismember(sperm_genes,gene_i);

      if(sum(s_k)>0)
%operon  
      sperm_operon{end+1}=uni_operon{i};  
%sperm genes in the  operon
         sperm_operon_genes{end+1}=find(s_k);
%all genes in the operon
         sperm_operon_allgenes{end+1}=gene_i;
% first gene in the operon
          first_gene_idx=find(strand_coord_i==min(strand_coord_i));            
% remove the first gene and keep the downstream genes    
%          sperm_operon_downstream_genes{end+1}=setdiff(gene_i,gene_i(first_gene_idx));
         sperm_operon_downstream_genes=[sperm_operon_downstream_genes  ;setdiff(gene_i,gene_i(first_gene_idx))];         
 
         if(min(strand_coord_i(j(find(s_k))))==min(strand_coord_i))
%the first gene in the operon is a sperm gene
                sperm_isoperon(end+1)=1;
          %downstream genes in the operon
          else
                sperm_isoperon(end+1)=0;
          end

        end
      
      [o_k,j]=ismember(oocyte_genes,gene_i);  
      
      if(sum(o_k)>0)
        oocyte_operon{end+1}=uni_operon{i};
        oocyte_operon_genes{end+1}=find(o_k);
        oocyte_operon_allgenes{end+1}=gene_i;

% first gene in the operon
          first_gene_idx=find(strand_coord_i==min(strand_coord_i));            
% remove the first gene and keep the downstream genes    
%         oocyte_operon_downstream_genes{end+1}=setdiff(gene_i,gene_i(first_gene_idx));
         oocyte_operon_downstream_genes= [oocyte_operon_downstream_genes  ;setdiff(gene_i,gene_i(first_gene_idx))];         
         if(min(strand_coord_i(j(find(o_k))))==min(strand_coord_i))
                oocyte_isoperon(end+1)=1;
          else
                oocyte_isoperon(end+1)=0;
          end
       end

%pro_idx is the index in promoter sequence
%      pro_idx=j(find(k));
%      strand_coord_i=strand_coord_i(find(k));
   
%      j=find(strand_coord_i==min(strand_coord_i));
%      if(length(j)>1)
%        j=j(1); 
%      end;
%      for k=1:length(pro_idx) 
%        gene_pro(2,pro_idx(k))=gene_pro(2,pro_idx(j)); 
%     end 
end

 save(file,'targets_names','sperm_idx','oocyte_idx','sperm_genes','oocyte_genes','sperm_operon','sperm_operon_genes','oocyte_operon','oocyte_operon_genes','sperm_operon_allgenes','oocyte_operon_allgenes','sperm_operon_downstream_genes','oocyte_operon_downstream_genes','sperm_isoperon','oocyte_isoperon'); 
