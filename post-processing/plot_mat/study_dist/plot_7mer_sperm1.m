 load matfiles/oocyte_7mer.mat;
% is_7mer=is_7mer_all(1:300);

 %7mers: motif weight, motif hit, p-val enrichment
 w_oocyte_7mer=w_oocyte(find(is_7mer));
 hit_per_oocyte_7mer=hit_per_oocyte(find(is_7mer));
 p_oocyte_7mer=p_oocyte(find(is_7mer)); 

 %5,6mers
 w_oocyte_56mer=w_oocyte(find(is_7mer==0));
 hit_per_oocyte_56mer=hit_per_oocyte(find(is_7mer==0));
 p_oocyte_56mer=p_oocyte(find(is_7mer==0)); 

 figure(1);
 subplot(2,1,1);
 plot(w_oocyte_7mer,log10(eps+p_oocyte_7mer),'r.');    
 axis([0 1e-3 -16 0]);
 xlabel('7mer weight');
 ylabel('P-val for 7mer enrichment'); i
 title('top 300 motifs for oocyte genes');
 
 subplot(2,1,2);
 plot(w_oocyte_56mer,log10(eps+p_oocyte_56mer),'r.');
 axis([0 1e-3 -16 0]);
 xlabel('56mer weight');
 ylabel('P-val for 56mer enrichment');
  
 figure(2);
 subplot(2,1,1);
 plot(w_oocyte_7mer,hit_per_oocyte_7mer,'r.');
 axis([0 1e-3 0 1]);
 xlabel('7mer weight');
 ylabel('Percentage of hits');
 title('top 300 motifs for oocyte genes');
 
 subplot(2,1,2);
 plot(w_oocyte_56mer,hit_per_oocyte_56mer,'r.');
 axis([0 1e-3 0 1]);
 xlabel('56mer weight');
 ylabel('Percentage of hits');
 
 figure(3);
 subplot(2,1,1);
 hist(hit_per_oocyte_7mer,20);
 xlabel('Percentage of oocyte gene hits for 7mers');
 
 subplot(2,1,2);
 hist(hit_per_oocyte_56mer,20);
 xlabel('Percentage of oocyte gene hits for 56mers');
  
