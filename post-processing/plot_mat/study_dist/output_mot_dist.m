%plot distribution of distance of ACGTG to coding region
load ../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat;

addpath('../../');

factor=2;
r=result.weights.r(:,factor);
nit=1;
tst=zeros(1,nit);
tst0=sum(sum(Y_tst.^2))/size(Y_tst,1);

%r=-r;
[ig,idx]=sort(r,'descend');

N=1471;
%sort motifs
%mot=motifs(idx(1:N));
mot=cell(1,1);
mot{1}='GATAAG';
%mot{2}='AATTAA';
%mot{3}='CAATTA';
%mot{4}='CTAATTA';

%sorted weights;
w=ig;

%get the length of each promoter sequence
dir='../../../../data/';
load([dir,'seqs.mat'],'a');
len=500*ones(1,size(a,1));
a1=a;
%a1(:,end)=1;
x=(a1==5);
x1=x(:,2:end)-x(:,1:end-1);
[m,n]=find(x1);

len(m)=n;



pbg=textread([dir,'pbg.txt']);
dimers_maxgap=10;


 

for i=1:length(mot)
 %      if(mod(i,100)==0)
 %        save mot_dist.mat; 
 %      end

        i
        motif=mot{i};
        pssm=[motif=='A';motif=='C';motif=='G';motif=='T'];
        pssm=pssm+eps*ones(size(pssm));
       pthr=sum(max(log(pssm./(pbg'*ones(1,size(pssm,2)))),[],1))-1;
       mot_groupidx=0;
       Nhit=scan_pssm(dir,mot_groupidx,pssm,pbg,pthr,dimers_maxgap);  

      len_sperm=len(sperm_idx);
      Nhit_sperm=Nhit(:,sperm_idx);
      [r,c]=find(Nhit_sperm);


      %only count the kmer closest to the coding region
%      [k1,k2]=ismember(unique(c),c);
%      r=r(k2);
%      c=c(k2);

      %distance to coding region of sperm genes
      dist_sperm=len_sperm(c)-r';      
 
      no_idx=setdiff(1:size(Nhit,2),sperm_idx);
      len_no=len(no_idx);
      Nhit_no=Nhit(:,no_idx);
      [r,c]=find(Nhit_no);
      
%      [k1,k2]=ismember(unique(c),c);
%      r=r(k2);
%      c=c(k2);
      %distance to coding region of non-sperm genes
      dist_no=len_no(c)-r';      


      [N_no,x]=hist(dist_no,10);
      N_no=N_no/sum(N_no);
      N_sperm=hist(dist_sperm,x,10);
      N_sperm=N_sperm/sum(N_sperm);

      range=0:10:500;
      figure(2);
      clf;
      subplot(2,1,1); 
%      bar(range,hist(dist_sperm,range)/length(dist_sperm));
      hist(dist_sperm,range); 
%    axis([0 500 0 0.1]);
      title(['Distribution of distance of ',motif,' to coding region of sperm genes']);
      subplot(2,1,2); 
%      bar(range,hist(dist_no,range)/length(dist_no));
     hist(dist_no,range);
%     axis([0 500 0 0.1]);
      title(['Distribution of distance of ',motif,' to coding region of non-sperm genes']);
      pause;
      

end


