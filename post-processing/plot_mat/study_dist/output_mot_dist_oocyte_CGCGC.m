%plot distribution of distance of CGCGCG motifs to coding region

%load ../matfiles/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt25_fuse=23.mat;

load ../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer7_l4f2.mat;
load ana_operon/sperm_oocyte_operon.mat; 

oocyte_operon_idx=[];
for j=1:length(oocyte_operon)
      N=length(oocyte_operon_genes{j});
      oocyte_operon_idx(end+1:end+N)=oocyte_operon_genes{j}; 
end

mots=textread('CGCGC.txt','%s');



addpath('../');


%get the length of each promoter sequence
dir='../../../../data/';
load([dir,'seqs.mat'],'a');
len=500*ones(1,size(a,1));
a1=a;
%a1(:,end)=1;
x=(a1==5);
x1=x(:,2:end)-x(:,1:end-1);
[m,n]=find(x1);

len(m)=n;




pbg=textread([dir,'pbg.txt']);
dimers_maxgap=10;


for i=1:length(mots)

        i
        motif=mots{i};
        pssm=[motif=='A';motif=='C';motif=='G';motif=='T'];
        pssm=pssm+eps*ones(size(pssm));
       pthr=sum(max(log(pssm./(pbg'*ones(1,size(pssm,2)))),[],1))-1;
       mot_groupidx=0;

%      
%       if(i==1)
           Nhit=scan_pssm(dir,mot_groupidx,pssm,pbg,pthr,dimers_maxgap);  
%       else
%           Nhit=Nhit+scan_pssm(dir,mot_groupidx,pssm,pbg,pthr,dimers_maxgap);  
%       end

%end

      
      %oocyte genes
      len_oocyte=len(oocyte_idx);
      Nhit_oocyte=Nhit(:,oocyte_idx);
      [dist_oocyte,count_hits_oocyte]=cal_dist(len_oocyte,Nhit_oocyte);
      
      %non-oocyte genes
      no_idx=setdiff(1:size(Nhit,2),oocyte_idx);
      len_no=len(no_idx);
      Nhit_no=Nhit(:,no_idx);
      [dist_no,count_hits_no]=cal_dist(len_no,Nhit_no);      

      %oocyte genes in a operon
       Nhit_oocyte_operon=Nhit_oocyte(:,oocyte_operon_idx);  
       len_oocyte_operon=len_oocyte(:,oocyte_operon_idx);
      [dist_oocyte_operon,count_hits_oocyte_operon]=cal_dist(len_oocyte_operon,Nhit_oocyte_operon);

     %oocyte genes as start of the operon 
       Nhit_oocyte_operon1=Nhit_oocyte_operon(:,find(oocyte_isoperon));
       len_oocyte_operon1=len_oocyte_operon(find(oocyte_isoperon));
      [dist_oocyte_operon1,count_hits_oocyte_operon1]=cal_dist(len_oocyte_operon1,Nhit_oocyte_operon1);
       
      
     %oocyte genes not in a operon
       oocyte_no_operon_idx=setdiff(1:size(Nhit_oocyte,2),oocyte_operon_idx);   
       Nhit_oocyte_no_operon=Nhit_oocyte(:,oocyte_no_operon_idx);  
       len_oocyte_no_operon=len_oocyte(oocyte_no_operon_idx);  
      [dist_oocyte_no_operon,count_hits_oocyte_no_operon]=cal_dist(len_oocyte_no_operon,Nhit_oocyte_no_operon);
 

     per300_oocyte_operon(i)=sum(dist_oocyte_operon<300)/length(dist_oocyte_operon);
     per300_oocyte_no_operon(i)=sum(dist_oocyte_no_operon<300)/length(dist_oocyte_no_operon);
     per300_oocyte(i)=sum(dist_oocyte<300)/length(dist_oocyte);
     per300_no(i)=sum(dist_no<300)/length(dist_no);  

%      [N_no,x]=hist(dist_no,10);
%      N_no=N_no/sum(N_no);
%      N_oocyte_operon=hist(dist_oocyte_operon,x,10);
%      N_oocyte_operon=N_oocyte_operon/sum(N_oocyte_operon);
%      N_oocyte_no_operon=hist(dist_oocyte_no_operon,x,10);
%      N_oocyte_no_operon=N_oocyte_no_operon/sum(N_oocyte_no_operon);

%      if(length(dist_oocyte_opero)>100)
%         JS_ent(i)=ent((N_no+N_oocyte)/2)-ent(N_no)/2-ent(N_oocyte)/2;
%      else
%         JS_ent(i)=0;
%      end


      range=0:10:500;
      figure(2);
      clf;
       subplot(3,1,1);
     hist(dist_oocyte_operon,range);
     title([motif,' in oocyte genes in an operon (less than 300bps ',num2str(per300_oocyte_operon(i)),')']); 
      subplot(3,1,2);   
      hist(dist_oocyte_no_operon,range);
     title([motif,' in oocyte genes not in an operon (less than 300bps  ',num2str(per300_oocyte(i)),')']); 
      
      subplot(3,1,3);
      hist(dist_no,range);
  %   title(num2str(quantile(dist_no,0.8)));
     title([motif,' in non-oocyte genes (less than 300bps  ',num2str(per300_no(i)),')']); 
      
     figure(1);
      clf;
       subplot(3,1,1);
   plot(dist_oocyte_operon,count_hits_oocyte_operon,'r.');     
     title(['Distribution of distance of ',motif,' to coding region of oocyte genes']);
      
      subplot(3,1,2);   
      plot(dist_oocyte,count_hits_oocyte,'r.');     
      
      subplot(3,1,3);
      plot(dist_no,count_hits_no,'r.'); 
     title(['Distribution of distance of ',motif,' to coding region of non-oocyte genes']);

% 
      pause;      
end

  figure(3);
  clf;
  plot(per300_oocyte,per300_no,'r.');
  hold on;
  plot(0:0.01:1,0:0.01:1,'g');
  axis([0 1 0 1]);
  xlabel('Percentage of motifs within 300 bps in oocyte genes');
  ylabel('Percentage of motifs within 300 bps in non-oocyte genes');
  
  figure(4);
  clf;
  plot(per300_oocyte_operon,per300_no,'r.');
  hold on;
  plot(0:0.01:1,0:0.01:1,'g');
  axis([0 1 0 1]);
  xlabel('Percentage of motifs within 300 bps in oocyte genes in an operon');
  ylabel('Percentage of motifs within 300 bps in non-oocyte genes');
