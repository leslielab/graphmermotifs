load matfiles/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt25_fuse=23.mat;

addpath('../../');

%get the length of each promoter sequence
dir='medusa/';
load([dir,'seqs.mat'],'a');
len=500*ones(1,size(a,1));
a1=a;
a1(:,end)=1;
x=(a1==5);
x1=x(:,2:end)-x(:,1:end-1);
[m,n]=find(x1);

len(m)=n;

Nhits=cell(1,6);
mot=cell(1,6);
mot{2}='GCGCGA';
mot{1}='AGCGCG';
mot{3}= 'CCGCGA';
mot{4}='CGCGAC';
mot{5}='CCGCGC';
mot{6}='ACGCGA';
imagemapflag=0;
seqlen=500;
ifname='CGCG.mat';
odirname='mot_map';
idirname='mot_map';
ofname='CGCG';

Nmot=length(mot);
selectmotifs=1:Nmot;
motcolormap=zeros(Nmot,1,3);
%motcolormap(:,1,:)=[1 0 0;0 0 1;0 1 0];
motcolormap(:,1,:)=rand(Nmot,3);

%mot{5}='CACGTA';
%mot{6}='AACGTA';
%mot{7}='ACGTAA';
pbg=textread([dir,'pbg.txt']);
dimers_maxgap=10;
for i=1:length(mot)

        i
        motif=mot{i};
        pssm=[motif=='A';motif=='C';motif=='G';motif=='T'];
        pssm=pssm+eps*ones(size(pssm));
       pthr=sum(max(log(pssm./(pbg'*ones(1,size(pssm,2)))),[],1))-1;
       mot_groupidx=0;
      %Nhit is  count matrix (length of promoter*number of genes) 
      Nhit=scan_pssm(dir,mot_groupidx,pssm,pbg,pthr,dimers_maxgap);  
      Nhits{i}=Nhit;
  
      len_sperm=len(oocyte_idx);
      Nhit_sperm=Nhit(:,oocyte_idx);
      [r,c]=find(Nhit_sperm);
      
      %distance to coding region of sperm genes
      dist_sperm=len_sperm(c)-r';      
      %N_sperm=zeros(size(Nhit_sperm));
      %N_sperm(sub2ind(size(N_sperm),dist_sperm,c'))=1;

     %sperm genes hit by motif i
       c_u=unique(c);
       hitgenenames{i}=c_u;
     % 
       for g=1:length(c_u)
     %motif i's position in sperm gene g in c_u
           hitstart{i}{g}=500-dist_sperm(find(c==c_u(g)))+2*(i-1);
       end 
     % length of motif i
      hitlength(i)=length(mot{i});

           
%     if(i==1)
%         Nhit_all=Nhit;
%     else
%         Nhit_all=Nhit_all+Nhit;
%     end

end

%number of motifs
%motcolormap(:,1,:)=[1 0 0;1 0 0;1 0 0;1 0 0;0 0 1;0 0 1;0 0 1];

isdimers(1:Nmot)=0;
isseq(1:Nmot)=1;
avggap(1:Nmot)=0;
pssm_words=mot;



%      Nhit_all=Nhit_all>0;
      %only count the kmer closest to the coding region
     % [k1,k2]=ismember(unique(c),c);
     % r=r(k2);
     % c=c(k2);
     % no_idx=setdiff(1:size(Nhit_all,2),oocyte_idx);
     % len_no=len(no_idx);
     % Nhit_no=Nhit_all(:,no_idx);
     % [r,c]=find(Nhit_no);
     % [k1,k2]=ismember(unique(c),c);
     % r=r(k2);
     % c=c(k2);
      %distance to coding region of non-sperm genes
      %dist_no=len_no(c)-r';      
      %N_no=zeros(size(Nhit_no));
      %N_no(sub2ind(size(N_no),dist_no,c'))=1;

 



%N=(Nhit_all>0);

%N_sperm=N(oocyte_idx,:);
%N_no=N(setdiff(1:size(N,1),oocyte_idx),:);




%idx=find(N_sperm==1);
%N_sperm([idx+1 idx+2 idx+3 idx+4])=1;
%idx=find(N_no==1);
%N_no([idx+1 idx+2 idx+3 idx+4])=1;

%N_sperm=-N_sperm+1;
%N_no=-N_no+1;

%figure(1);
%subplot(1,2,1);
%imagesc(N_sperm'*100);
%xlabel('distance in bps');
%ylabel('sperm genes');
%title('Distance of ACGTG to coding region');
%subplot(1,2,2);
%imagesc(N_no'*100);
%xlabel('distance in bps');
%ylabel('non-sperm genes');
%title('Distance of ACGTG to coding region');
%colormap('gray');

for k=11:25

G=50;
selectgnames=(k-1)*G+1:k*G;
selectgcomnames=selectgnames;
save([idirname,'/',ifname],'hitgenenames','hitstart','hitlength','isdimers','isseq','avggap','pssm_words');
motifmaps(selectgnames,selectgcomnames,selectmotifs,motcolormap,imagemapflag,seqlen,idirname,ifname,odirname,ofname);
pause;
end


