%investigate if sperm/oocyte genes are AT or CG enriched

load ../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat sperm_idx oocyte_idx;


dir='../../../../data/';

load([dir,'seqs.mat'],'a');

seq=a;
sperm_seq=a(sperm_idx,:);
oocyte_seq=a(oocyte_idx,:);

all_AT=(sum(sum(seq==1))+sum(sum(seq==4)))/sum(sum(seq~=5));
sperm_AT=(sum(sum(sperm_seq==1))+sum(sum(sperm_seq==4)))/sum(sum(sperm_seq~=5));
oocyte_CG=(sum(sum(oocyte_seq==2))+sum(sum(oocyte_seq==3)))/sum(sum(oocyte_seq~=5));
