%study hits of 7mers


load ../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat;


factor=1;
r=result.weights.r(:,factor);
%r=-r;
[w_oocyte,idx]=sort(r,'descend');

N=100;
%sort motifs
motifs=motifs(idx);
mots=motifs(1:N);




addpath('../');
dir='../../../../data/';
pbg=textread([dir,'pbg.txt']);
dimers_maxgap=10;

for i=1:length(mots)

        i
        motif=mots{i};
        pssm=[motif=='A';motif=='C';motif=='G';motif=='T'];
        pssm=pssm+eps*ones(size(pssm));
       pthr=sum(max(log(pssm./(pbg'*ones(1,size(pssm,2)))),[],1))-1;
       mot_groupidx=0;



           Nhit=scan_pssm(dir,mot_groupidx,pssm,pbg,pthr,dimers_maxgap);
     %      if(sum(motif=='A')+sum(motif=='T')
           if(sum(motif=='C')+sum(motif=='G')>size(pssm,2)*0.66)
               is_CGCG(i)=1;      
           end
           hit_per(i)=sum(sum(Nhit,1)>0)/size(Nhit,2);        
    
           Nhit_oocyte=Nhit(:,oocyte_idx);
           hit_per_oocyte(i)=sum(sum(Nhit_oocyte,1)>0)/size(Nhit_oocyte,2);        
      
     %number of genes
      M=size(Nhit,2);
     %number of genes having the motif
      K=sum(sum(Nhit,1)>0);
      %number of oocyte genes
      N_oocyte=size(Nhit_oocyte,2);
    %number of oocyte genes having the motif
      X_oocyte=sum(sum(Nhit_oocyte,1)>0); 
   %p-val   
   p_oocyte(i)=1-hygecdf(X_oocyte,M,K,N_oocyte);    

end 


 %7mers: motif weight, motif hit, p-val enrichment
 w_oocyte_CGCG=w_oocyte(find(is_CGCG));
 hit_per_oocyte_CGCG=hit_per_oocyte(find(is_CGCG));
 p_oocyte_CGCG=p_oocyte(find(is_CGCG)); 

 %5,6mers
 w_oocyte_nonCGCG=w_oocyte(find(is_CGCG==0));
 hit_per_oocyte_nonCGCG=hit_per_oocyte(find(is_CGCG==0));
 p_oocyte_nonCGCG=p_oocyte(find(is_CGCG==0)); 

 figure(3);
 subplot(2,1,1);
 hist(hit_per_oocyte_CGCG,30);
 title('distribution of percentage of oocyte gene hits for 7mers');
 subplot(2,1,2);
 hist(hit_per_oocyte_nonCGCG,30);
 title('distribution of percentage of oocyte gene hits for non-7mers');
 


 figure(1);
 subplot(2,1,1);
 plot(w_oocyte_CGCG,log10(eps+p_oocyte_CGCG),'r.');    
 axis([0 1e-3 -16 0]);
 xlabel('CGCG weight');
 ylabel('P-val for 7mer enrichment'); 
 
 subplot(2,1,2);
 plot(w_oocyte_nonCGCG,log10(eps+p_oocyte_nonCGCG),'r.');
 axis([0 1e-3 -16 0]);
 xlabel('nonCGCG weight');
 ylabel('P-val for 56mer enrichment');
  
 figure(2);
 subplot(2,1,1);
 plot(w_oocyte_CGCG,hit_per_oocyte_CGCG,'r.');
 axis([0 1e-3 0 1]);
 xlabel('CGCG weight');
 ylabel('Percentage of hits');
 
 subplot(2,1,2);
 plot(w_oocyte_nonCGCG,hit_per_oocyte_nonCGCG,'r.');
 axis([0 1e-3 0 1]);
 xlabel('nonCGCG weight');
 ylabel('Percentage of hits');
 
 
