%study hits of 7mers

%load ../matfiles/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt25_fuse=23.mat;

load ../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer7_l4f2_weightedA.mat;
file='matfiles/oocyte_7mer_weightedA.mat';


factor=1;
r=result.weights.r(:,factor);
%
%r=-r;
[w_oocyte,idx]=sort(r,'descend');

N=300;
%sort motifs
motifs=motifs(idx);
mots=motifs(1:N);


mot_ham=ones(length(motifs));
for i=1:N
  i
   for k=i+1:length(motifs)
       mot_ham(i,k)=hamming(motifs{i},motifs{k});
  end
end




addpath('../');
dir='../../../../data/';
pbg=textread([dir,'pbg.txt']);
dimers_maxgap=10;

for i=1:length(mots)

        i
        motif=mots{i};
        pssm=[motif=='A';motif=='C';motif=='G';motif=='T'];
        pssm=pssm+eps*ones(size(pssm));
       pthr=sum(max(log(pssm./(pbg'*ones(1,size(pssm,2)))),[],1))-1;
       mot_groupidx=0;



           Nhit=scan_pssm(dir,mot_groupidx,pssm,pbg,pthr,dimers_maxgap);
           if(size(pssm,2)==7)
             is_7mer(i)=1;
           end
           hit_per(i)=sum(sum(Nhit,1)>0)/size(Nhit,2);        
    
           Nhit_oocyte=Nhit(:,oocyte_idx);
           hit_per_oocyte(i)=sum(sum(Nhit_oocyte,1)>0)/size(Nhit_oocyte,2);        
      
     %number of genes
      M=size(Nhit,2);
     %number of genes having the motif
      K=sum(sum(Nhit,1)>0);
      %number of oocyte genes
      N_oocyte=size(Nhit_oocyte,2);
    %number of oocyte genes having the motif
      X_oocyte=sum(sum(Nhit_oocyte,1)>0); 
   %p-val   
   p_oocyte(i)=1-hygecdf(X_oocyte,M,K,N_oocyte);    

end 

save(file);

 %7mers: motif weight, motif hit, p-val enrichment
 w_oocyte_7mer=w_oocyte(find(is_7mer));
 hit_per_oocyte_7mer=hit_per_oocyte(find(is_7mer));
 p_oocyte_7mer=p_oocyte(find(is_7mer)); 

 %5,6mers
 w_oocyte_56mer=w_oocyte(find(is_7mer==0));
 hit_per_oocyte_56mer=hit_per_oocyte(find(is_7mer==0));
 p_oocyte_56mer=p_oocyte(find(is_7mer==0)); 

 figure(1);
 subplot(2,1,1);
 plot(w_oocyte_7mer,log10(eps+p_oocyte_7mer),'r.');    
 axis([0 1e-3 -16 0]);
 xlabel('7mer weight');
 ylabel('P-val for 7mer enrichment'); 
 
 subplot(2,1,2);
 plot(w_oocyte_56mer,log10(eps+p_oocyte_56mer),'r.');
 axis([0 1e-3 -16 0]);
 xlabel('56mer weight');
 ylabel('P-val for 56mer enrichment');
  
 figure(2);
 subplot(2,1,1);
 plot(w_oocyte_7mer,hit_per_oocyte_7mer,'r.');
 axis([0 1e-3 0 1]);
 xlabel('7mer weight');
 ylabel('Percentage of hits');
 
 subplot(2,1,2);
 plot(w_oocyte_56mer,hit_per_oocyte_56mer,'r.');
 axis([0 1e-3 0 1]);
 xlabel('56mer weight');
 ylabel('Percentage of hits');
 
 
