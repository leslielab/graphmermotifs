clear all;
%study motif enrichment in oocyte genes

load ../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat;
load ana_operon/sperm_oocyte_operon.mat; 


oocyte_operon_idx=[];
for j=1:length(oocyte_operon)
      N=length(oocyte_operon_genes{j});
      oocyte_operon_idx(end+1:end+N)=oocyte_operon_genes{j}; 
end


factor=1;
r=result.weights.r(:,factor);

    [ig,idx]=sort(r);

N=1;


%sort motifs
mots=motifs(idx(end:-1:end-N+1));
%mot=motifs(idx(1:N));


addpath('data/');

%get the length of each promoter sequence
dir='../../../../data/';
load([dir,'seqs.mat'],'a');
len=500*ones(1,size(a,1));
a1=a;
x=(a1==5);
x1=x(:,2:end)-x(:,1:end-1);
[m,n]=find(x1);
len(m)=n;

%read in sequences for all genes
gene_pro=textread('germline_promoter_500_inter.fasta','%s');
gene_pro=reshape(gene_pro,[2 length(gene_pro)/2]);

oocyte_operon_downstream_genes=targets_names(oocyte_idx(oocyte_operon_idx(find(oocyte_isoperon==0))));
idx=ismember(gene_pro(1,:),oocyte_operon_downstream_genes);
%downstream genes in the operon which contain a oocyte gene
genes_oocyte_operon_down=gene_pro(1,find(idx));
a_oocyte_operon_down=strvcat(gene_pro(2,find(idx)));
a_oocyte_operon_down=(a_oocyte_operon_down=='A')+2*(a_oocyte_operon_down=='C')+3*(a_oocyte_operon_down=='G')+4*(a_oocyte_operon_down=='T')+5*(a_oocyte_operon_down==' ');

len_oocyte_operon_down=500*ones(1,size(a_oocyte_operon_down,1));
a1=a_oocyte_operon_down;
x=(a1==5);
x1=x(:,2:end)-x(:,1:end-1);
[m,n]=find(x1);
len_oocyte_operon_down(m)=n;


pbg=textread([dir,'pbg.txt']);
dimers_maxgap=10;


for i=1:length(mots)

        i
        motif=mots{i};
        pssm=[motif=='A';motif=='C';motif=='G';motif=='T'];
        pssm=pssm+eps*ones(size(pssm));
       pthr=sum(max(log(pssm./(pbg'*ones(1,size(pssm,2)))),[],1))-1;
       mot_groupidx=0;

           Nhit=scan_pssm(dir,mot_groupidx,pssm,pbg,pthr,dimers_maxgap,a);  
       
     %number of genes
      M=size(Nhit,2);
     %number of genes having the motif
      K=sum(sum(Nhit,1)>0);
      

      %oocyte genes
     % len_oocyte=len(oocyte_idx);
      Nhit_oocyte=Nhit(:,oocyte_idx);
      a_oocyte=a(oocyte_idx,:);

      N_oocyte=size(Nhit_oocyte,2);
      X_oocyte=sum(sum(Nhit_oocyte,1)>0); 
      p_oocyte(i)=1-hygecdf(X_oocyte,M,K,N_oocyte);    

 
      %oocyte genes that are downstream ones in an operon. take their upstream sequences
       Nhit_oocyte_operon_down=scan_pssm(dir,mot_groupidx,pssm,pbg,pthr,dimers_maxgap,a_oocyte_operon_down); 
      idx=find(len_oocyte_operon_down>400);
%      Nhit_oocyte_operon_down=Nhit_oocyte_operon_down(:,idx);  
      N_oocyte_operon_down=size(Nhit_oocyte_operon_down,2);
      X_oocyte_operon_down=sum(sum(Nhit_oocyte_operon_down,1)>0); 
      p_oocyte_operon_down(i)=1-hygecdf(X_oocyte_operon_down,M,K,N_oocyte_operon_down);    
     
 
      %oocyte genes in a operon
      Nhit_oocyte_operon=Nhit_oocyte(:,oocyte_operon_idx);  
      a_oocyte_operon=a_oocyte(oocyte_operon_idx,:);
     %  len_oocyte_operon=len_oocyte(:,oocyte_operon_idx);

     %oocyte genes as start of the operon 
       Nhit_oocyte_operon1=Nhit_oocyte_operon(:,find(oocyte_isoperon));
       a_oocyte_operon1=a_oocyte_operon(find(oocyte_isoperon),:); 
     % len_oocyte_operon1=len_oocyte_operon(find(oocyte_isoperon));
     
      %oocyte genes not in a operon
       oocyte_no_operon_idx=setdiff(1:size(Nhit_oocyte,2),oocyte_operon_idx);   
       Nhit_oocyte_no_operon=Nhit_oocyte(:,oocyte_no_operon_idx);  
       a_oocyte_no_operon=a(oocyte_no_operon_idx,:);  
     %len_oocyte_no_operon=len_oocyte(oocyte_no_operon_idx);  

     %oocyte gene not in a operon or as start of a operon
      Nhit_oocyte_no_operon1=[Nhit_oocyte_no_operon Nhit_oocyte_operon1]; 
      a_oocyte_no_operon1=[a_oocyte_no_operon; a_oocyte_operon1];
      N_oocyte_no_operon1=size(Nhit_oocyte_no_operon1,2);
      X_oocyte_no_operon1=sum(sum(Nhit_oocyte_no_operon1,1)>0); 
      p_oocyte_no_operon1(i)=1-hygecdf(X_oocyte_no_operon1,M,K,N_oocyte_no_operon1);    
    
     %non-oocyte genes
      no_idx=setdiff(1:size(Nhit,2),oocyte_idx);
      len_no=len(no_idx);
      Nhit_no=Nhit(:,no_idx);


      len_oocyte_operon_down=sum(a_oocyte_operon_down~=5,2);
      len_oocyte_no_operon1=sum(a_oocyte_no_operon1~=5,2);

      figure(2);
      subplot(2,1,1); 
      hist(len_oocyte_operon_down);
      title('Length of promoter region of operon downstream oocyte genes');
      subplot(2,1,2);
      hist(len_oocyte_no_operon1);
      title('Length of promoter region of non-operon oocyte genes and operon downstream oocyte genes');
end

figure(1);
clf;
plot(log10(p_oocyte_no_operon1+eps),log10(p_oocyte_operon_down+eps),'r.');
title('Enrichment of top 50 kmers for oocyte genes');
ylabel('log10(p-val) operon downstream oocyte genes'); 
xlabel('log10(p-val) non-operon oocyte genes and operon upstream oocyte genes');
