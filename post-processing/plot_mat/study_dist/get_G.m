%output sperm/oocyte genes for experimental validation

load ../matfiles/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt25_fuse=23.mat;

load ana_operon/sperm_oocyte_operon.mat;
output_file='sperm_genes.txt';
!rm sperm_genes.txt;
%output 50 sperm genes
%pick N_first genes first, filter the genes by expression later
N_first=100;
N_gene=50;
%motif='CGCGC';
motif='ACGTG';

%get the length of each promoter sequence
dir='../../../../data/';
load([dir,'seqs.mat'],'a');
len=500*ones(1,size(a,1));
a1=a;
%a1(:,end)=1;
x=(a1==5);
x1=x(:,2:end)-x(:,1:end-1);
[m,n]=find(x1);

len(m)=n;



pbg=textread([dir,'pbg.txt']);
dimers_maxgap=10;


        pssm=[motif=='A';motif=='C';motif=='G';motif=='T'];
        pssm=pssm+eps*ones(size(pssm));
       pthr=sum(max(log(pssm./(pbg'*ones(1,size(pssm,2)))),[],1))-1;
       mot_groupidx=0;
       Nhit=scan_pssm(dir,mot_groupidx,pssm,pbg,pthr,dimers_maxgap);  


sperm_operon_idx=[];
for j=1:length(sperm_operon)
      N=length(sperm_operon_genes{j});
      sperm_operon_idx(end+1:end+N)=sperm_operon_genes{j}; 
end
      sperm_genes=targets_names(sperm_idx);
      %remove sperm downstream genes in an operon
      sperm_operon_down_genes=sperm_genes(sperm_operon_idx(find(sperm_isoperon==0)));

      sperm_seqs=a(sperm_idx,:);
      sperm_cexp=cexp_real(sperm_idx,:);

      len_sperm=len(sperm_idx);
      Nhit_sperm=Nhit(:,sperm_idx);
      [r,c]=find(Nhit_sperm);


      %only count the kmer closest to the coding region
      [k1,k2]=ismember(unique(c),c);
      r=r(k2);
      c=c(k2);

      %distance to coding region of sperm genes
      dist_sperm=len_sperm(c)-r';      

      %sperm genes that have ACGTG within 200 bps from coding region
      idx=c(find(dist_sperm<200));  

      sperm_genes=sperm_genes(idx);
      sperm_seqs=sperm_seqs(idx,:);
      sperm_cexp=sperm_cexp(idx,:);

      Nhit_sperm=Nhit_sperm(:,idx);

      %consider sperm genes that have multiple ACGTG
      count_hits=sum(Nhit_sperm,1);
      [ign,idx]=sort(count_hits,'descend');
      
      hits=ign(1:N_first);
      sperm_genes=sperm_genes(idx(1:N_first));
      sperm_seqs=sperm_seqs(idx(1:N_first),:);
      sperm_cexp=sperm_cexp(idx(1:N_first),:);
      

 %filter by expression
      remove_idx=[];
      for i=1:N_first
        [max_cexp,max_T]=max(sperm_cexp(i,:),[],2);
        %
        if(max_T>6|max_T<4) %sperm genes
%        if(max_T<8)  %sperm genes
               remove_idx(end+1)=i;
        else
             % 
            local_max=setdiff(localmax(sperm_cexp(i,:)),max_T);
       %if the local maximum is greater than half of the maximum
            %
            if(length(local_max)>0)  %sperm genes
            %
          if(max(sperm_cexp(i,local_max)/max_cexp)>0.5) %sperm genes
            % if(length(local_max)>=3|max(sperm_cexp(i,1:6))/max_cexp>0.5) %sperm genes
                     remove_idx(end+1)=i;
                  end 
            %
             end
        end
     end


        sperm_genes(remove_idx)=[];
        sperm_seqs(remove_idx,:)=[];
        sperm_cexp(remove_idx,:)=[];
        hits(remove_idx)=[];
 
     %remove genes in the operon
      [k,j]=ismember(sperm_genes,sperm_operon_down_genes);
      remove_idx=find(k==1);
        sperm_genes(remove_idx)=[];
        sperm_seqs(remove_idx,:)=[];
        sperm_cexp(remove_idx,:)=[];
        hits(remove_idx)=[];
    
   

      fid=fopen(output_file,'a');
      for i=1:length(sperm_genes)
          fprintf(fid,'\n>%s\n',sperm_genes{i});
            for j=1:size(sperm_seqs,2)
                switch sperm_seqs(i,j)
                  case 1
                    fprintf(fid,'A');
                  case 2
                    fprintf(fid,'C');
                  case 3
                    fprintf(fid,'G');
                  case 4
                    fprintf(fid,'T');
                   otherwise
                  end
            end  
      end
      fclose(fid);      



