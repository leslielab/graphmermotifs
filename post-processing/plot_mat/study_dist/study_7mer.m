%study hits of 7mers


%load ../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67_l4f2.mat;
load ../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer7.mat;
file='matfiles/sperm_67mer.mat';


factor=2;
r=result.weights.r(:,factor);
%r=-r;
[w_sperm,idx]=sort(r,'descend');

N=300
is_7mer=zeros(N,1);
%sort motifs
motifs=motifs(idx);
mots=motifs(1:N);






addpath('../');
dir='../../../../data/';
pbg=textread([dir,'pbg.txt']);
dimers_maxgap=10;

for i=1:length(mots)

        i
        motif=mots{i};
        pssm=[motif=='A';motif=='C';motif=='G';motif=='T'];
        pssm=pssm+eps*ones(size(pssm));
       pthr=sum(max(log(pssm./(pbg'*ones(1,size(pssm,2)))),[],1))-1;
       mot_groupidx=0;



           Nhit=scan_pssm(dir,mot_groupidx,pssm,pbg,pthr,dimers_maxgap);
           if(size(pssm,2)==7)
             is_7mer(i)=1;
           end
           hit_per(i)=sum(sum(Nhit,1)>0)/size(Nhit,2);        
    
           Nhit_sperm=Nhit(:,sperm_idx);
           hit_per_sperm(i)=sum(sum(Nhit_sperm,1)>0)/size(Nhit_sperm,2);        
      
     %number of genes
      M=size(Nhit,2);
     %number of genes having the motif
      K=sum(sum(Nhit,1)>0);
      %number of sperm genes
      N_sperm=size(Nhit_sperm,2);
    %number of sperm genes having the motif
      X_sperm=sum(sum(Nhit_sperm,1)>0); 
   %p-val   
   p_sperm(i)=1-hygecdf(X_sperm,M,K,N_sperm);    

end 

%save(file);

 %7mers: motif weight, motif hit, p-val enrichment
 w_sperm_7mer=w_sperm(find(is_7mer));
 hit_per_sperm_7mer=hit_per_sperm(find(is_7mer));
 p_sperm_7mer=p_sperm(find(is_7mer)); 

 %5,6mers
 w_sperm_56mer=w_sperm(find(is_7mer==0));
 hit_per_sperm_56mer=hit_per_sperm(find(is_7mer==0));
 p_sperm_56mer=p_sperm(find(is_7mer==0)); 

 figure(3);
 subplot(2,1,1);
 hist(hit_per_sperm_7mer,30);
 title('distribution of percentage of sperm gene hits for 7mers');
 subplot(2,1,2);
 hist(hit_per_sperm_56mer,30);
 title('distribution of percentage of sperm gene hits for non-7mers');
 


 figure(1);
 subplot(2,1,1);
 plot(w_sperm_7mer,log10(eps+p_sperm_7mer),'r.');    
 axis([0 1e-3 -16 0]);
 xlabel('7mer weight');
 ylabel('P-val for 7mer enrichment'); 
 
 subplot(2,1,2);
 plot(w_sperm_56mer,log10(eps+p_sperm_56mer),'r.');
 axis([0 1e-3 -16 0]);
 xlabel('56mer weight');
 ylabel('P-val for 56mer enrichment');
  
 figure(2);
 subplot(2,1,1);
 plot(w_sperm_7mer,hit_per_sperm_7mer,'r.');
 axis([0 1e-3 0 1]);
 xlabel('7mer weight');
 ylabel('Percentage of hits');
 
 subplot(2,1,2);
 plot(w_sperm_56mer,hit_per_sperm_56mer,'r.');
 axis([0 1e-3 0 1]);
 xlabel('56mer weight');
 ylabel('Percentage of hits');
 
 
