function [dist,count_hits]=cal_dist(len,Nhit)
           
      [r,c]=find(Nhit);
      %only count the kmer closest to the coding region. consider the closest kmer
   %
   [k1,k2]=ismember(unique(c),c);
   %
   r=r(k2);
   %
   c=c(k2);
      %distance to coding region of oocyte genes
      dist=len(c)-r'; 
      count_hits=sum(Nhit(:,c),1);
