function remove_idx=rm_rever_com(motifs)
%remove motifs if the reverse complements are in previous motifs

Nm=length(motifs);
remove_idx=[];

for i=2:Nm
  motif_rc=rever_com(motifs{i});
%if the reverse complement of current ith motif is in the first (i-1)th motif  
 if(i==4589)
  disp('h');
 end 

   if(sum(ismember(motifs(1:i-1),motif_rc)))
 i  
        remove_idx(end+1)=i;
    end
end
