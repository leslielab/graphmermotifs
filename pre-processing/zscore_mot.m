function rand_count=zscore_mot(motifs,seq,N,file)

%rand_count is a matrix keeping track of motif counts. size   #motifs by #shuffle
rand_count=zeros(length(motifs),N);
Nseq=length(seq);

for i=1:N
   i
    if(i>1)
%shuffle the sequence 
       idx=randperm(Nseq);
       rand_seq=seq(idx); 
    else
      rand_seq=seq;
    end
%reverse complement
     rand_seq_rc=rand_seq(end:-1:1);
     idx1=find(rand_seq_rc==1);
     idx2=find(rand_seq_rc==2);
     idx3=find(rand_seq_rc==3);
     idx4=find(rand_seq_rc==4);
     rand_seq_rc(idx1)=4; 
     rand_seq_rc(idx4)=1; 
     rand_seq_rc(idx2)=3; 
     rand_seq_rc(idx3)=2; 
     rand_seq=[rand_seq;rand_seq_rc];
   
  for m=1:length(motifs)
         m
           if(mod(m,100)==0) 
            save(file,'rand_count');
           end
          mot=zeros(1,length(motifs{m}));
          mot(find(motifs{m}=='A'))=1;             
          mot(find(motifs{m}=='C'))=2;             
          mot(find(motifs{m}=='G'))=3;             
          mot(find(motifs{m}=='T'))=4;             

% rand_kmer is matrix length(seq)*length(kmer)
         if(length(mot)==5)
               rand_kmer=[rand_seq(1:end-4) rand_seq(2:end-3) rand_seq(3:end-2) rand_seq(4:end-1) rand_seq(5:end)];
                
         elseif(length(mot)==6)
           rand_kmer=[rand_seq(1:end-5) rand_seq(2:end-4) rand_seq(3:end-3) rand_seq(4:end-2) rand_seq(5:end-1) rand_seq(6:end)];
         else
          rand_kmer=[rand_seq(1:end-6) rand_seq(2:end-5) rand_seq(3:end-4) rand_seq(4:end-3) rand_seq(5:end-2) rand_seq(6:end-1) rand_seq(7:end)];
         end  

% count how many times motif m occur in the randomize sequence
               rand_count(m,i)=sum(sum(rand_kmer-repmat(mot,[size(rand_kmer,1) 1])~=0,2)==0);
   end
   clear rand_kmer;

end

