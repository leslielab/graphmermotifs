#!/usr/bin/perl
# reads promoter sequences from ../../data/utr5_sc_500.fasta; also calculates background probabilities
# usage: perl init_sequence.pl gnamesfile path
# where gnamesfile is the file containing the names of the target genes
#
# (c) Manuel Middendorf, 2005 


#$utrfile = "../../data/utr5_sc_500.fasta";
print "$ARGV[0]\n$ARGV[1]\n$ARGV[2]";

$seqfile1=$ARGV[2];
$utrfile = "$seqfile1";
#print "utrfile is $utrfile\n";
$procpath = $ARGV[1];
#print "path is $procpath\n";
$pbgfile = "$procpath/pbg.txt";

$seqfile = "$procpath/seq.txt";

open(GN,"<$ARGV[0]");
$line = <GN>;
chomp($line);
@gn = split(/\s+/,$line);

# initialize dumpfile for sequences
&initialize_seq_dump(@gn);
	
sub initialize_seq_dump {
	open(IN,"<$utrfile") || die "could not open $utrfile";
#	open(OUT,">$seqfile") || die "could not open $dumpfile";
	my $orflabel=0;
        my $name;
        my @allnames;
	my %allseqs;
	while($line = <IN>){
		if ($line =~ m/^>(\S*?)\s/){
	                $orflabel++;
#         	        if($orflabel>1){
#                		print OUT "$allseqs{$name}\n";
#                        }
                        $name = $1;
		        push @allnames,$name;
                    	$allseqs{$name} = "";
		} else {
			chomp($line);
			$allseqs{$name} .= $line;
		}
	}
	close(IN);
	my @seqs;
 	for ($i=0;$i<$orflabel;$i++){
		$seqs[$i] = $allseqs{$allnames[$i]};
                
	}

	open(OUT,">$seqfile") || die "could not open $dumpfile";

	for ($i=0;$i<$orflabel;$i++){
		print OUT  "$seqs[$i]\n";
	}
	close(OUT);

	&count_pbg(\@seqs);
}

sub count_pbg {
	$seqref = $_[0];
	my %counts = {'A'=>0,'T'=>0,'C'=>0,'G'=>0};
	my @str;
	my $i; my $j;
	for ($i=0;$i<@{$seqref};$i++){
		@str = split(//,${$seqref}[$i]);
		for ($j=0;$j<@str;$j++){
			$counts{$str[$j]}++;
		}
	}
	my $sum = 0;
	foreach $key (keys %counts){
        #print "\n$counts{$key}";
        #print "\n$sum";
		$sum += $counts{$key};
	}
	foreach $key (keys %counts){
		$counts{$key} /= $sum;
	}

	open(OUT,">$pbgfile") || die "could not open $pbgfile";
	print OUT "$counts{'A'}\t$counts{'C'}\t$counts{'G'}\t$counts{'T'}\n";
	close(OUT);
}


