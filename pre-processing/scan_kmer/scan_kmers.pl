#!/usr/bin/perl
# scans shuffled promoter sequences for all k-mers for k=1,...,$ARGV[1]
# usage: perl scan_kmers.pl filename kmin kmax seqfile 
#output to filename: motif (#of counts in all sequences)


$filename = $ARGV[0];
#minimum length of k-mer
$Nmin=$ARGV[1];
#maximum length of k-mer
$Nmax=$ARGV[2];
#sequence file
$seqfile=$ARGV[3];
#number of times of shuffling the sequence
$rand=$ARGV[4];

open(IN,"<$seqfile") || die "could not open utr-file";


$orflabel=0;
$analyze=0;
$seq = "";

#readin the sequence file
while(<IN>){
	chomp;
	if (m/^>/){
		m/^>(.*?)\ /;
#extract the sequence of a gene
#		m/^>(.*)range=.*$/;
#                m/^>(.*)chromosome.*$/;   
                $orflabel++;
                if($orflabel>1) {$analyze=1;} 

	} else {
		$seq = $seq.$_;
	}

       if ($analyze==1) {
#scan the sequence for k-mers
           &getmers($orflabel-1); 
        }
}
           &getmers($orflabel); 

close(IN);

print "$filename";

#output k-mers to file MOT
open(MOT,">$filename.mot") || die "could not open utr-file";
#output counts of k-mers to file COUNT 
open(COUNT,">$filename.count") || die "could not open utr-file";

foreach $motif (sort keys %mers){
       print MOT "$motif\n";
       print COUNT "$mers{$motif}\n";
}
close(MOT);
close(COUNT);

sub getmers {
	my $idx = $_[0];
       #         print STDOUT "$idx\n";
          print STDOUT "scanning gene $idx\r";

      #shuffle sequence 
      if($rand==1) 
      { my @seq1=split('',$seq);
#shuffle the sequence
        my @sh_seq1=shuffle(\@seq1);
        my $sh_seq=join('',@sh_seq1);
        $seq=$sh_seq;
      }
	$bp = length($seq);
        $N1=$Nmin;
 	$N2 = $Nmax;
#scan the sequence
	for ($i=0;$i<$bp-$N1+1;$i++){
		$j=$i+1;
		if ($bp-$i+1<$N){ $N2 = $bp-$i+1;}
		for ($ii=$N1;$ii<=$N2;$ii++){
			$motif = substr($seq,$i,$ii);
#			$motif = substr($seq,$i,$ii);
			if(!$mers{$motif}){
				$mers{$motif} = 0;
			}
#keep track of counts of k-mers
                        $mers{$motif}=$mers{$motif}+1;
           }
	}

      $analyze=0;
      $seq="";
}


sub shuffle{
    my($input)=@_;
    my @new;
 
     while(@$input){
     push (@new,splice(@$input,rand(@$input),1));
}

    return @new;
}
