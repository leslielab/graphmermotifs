function AT_per=AT_content(motif)
AT_per=(sum(motif=='A')+sum(motif=='T'))/length(motif);
