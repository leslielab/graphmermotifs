function motif_rev=revcomp(motif)
%reverse complement of motif

  motif_rev=motif;

  C_idx=find(motif=='C');
  G_idx=find(motif=='G');
  A_idx=find(motif=='A');
  T_idx=find(motif=='T');

  motif_rev(C_idx)='G';
  motif_rev(G_idx)='C';
  motif_rev(A_idx)='T';
  motif_rev(T_idx)='A';


  motif_rev(end:-1:1)=motif_rev;
