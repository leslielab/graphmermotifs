%clear all;

load ../../../../../experiments/PLS_fuse2_lasso_filter_mot_count_bkseq_zscore_qt50_kmer67.mat result motifs;
%
load ../exon_promoter.mat;


%find exon motifs in promoter motifs
pe_motifs=intersect(p_motifs,e_motifs);
[ign,idx]=ismember(pe_motifs,e_motifs);
e_motifs=pe_motifs;
e_count=e_count(idx);
e_rand_count=e_rand_count(idx,:);



%find reverse complements of p_motifs
%for i=1:length(pe_motifs)
%i    
%[ign,idx1]=ismember(pe_motifs{i},p_motifs);
%    [ign,idx2]=ismember(revcomp(pe_motifs{i}),p_motifs);
%add up counts from motifs and motif reverse complements
%    p_count1(i)=p_count(idx1)+p_count(idx2);
%    p_rand_count1(i,:)=sum(p_rand_count([idx1;idx2],:));
%

%    e_count(i)
%    p_count1(i)
%    e_rand_count(i,:)
%    p_rand_count1(i,:)   
 
%end

load test.mat;
p_motifs=pe_motifs;
p_count=p_count1';
p_rand_count=p_rand_count1;


%PLS motifs
factor=2;
N=100;
qt=0.25;
%
%qt=1-length(motifs)/length(p_motifs)


r=result.weights.r(:,factor);
[ig,idx]=sort(r);
motifs0=motifs(idx(end-49:end));

e_zscore=(e_count-mean(e_rand_count,2))./sqrt(mean(e_rand_count,2));
p_zscore=(p_count-mean(p_rand_count,2))./sqrt(mean(p_rand_count,2));

e_mot_idx=find(e_zscore > quantile(e_zscore,qt));
e_motifs_filter=e_motifs(e_mot_idx);
e_motifs0=intersect(motifs0,e_motifs_filter);
p_mot_idx=find(p_zscore > quantile(p_zscore,qt));
e_motifs_removed=setdiff(e_motifs,motifs);
p_motifs_filter=p_motifs(p_mot_idx);
p_motifs_removed=p_motifs(p_zscore < quantile(p_zscore,qt));
p_motifs0=intersect(motifs0,p_motifs_filter);

for i=1:length(e_motifs_removed)
AT_e_removed(i)=AT_content(e_motifs_removed{i});
end

for i=1:length(e_motifs_filter)
AT_e_filter(i)=AT_content(e_motifs_filter{i});
end

for i=1:length(p_motifs_removed)
 AT_p_removed(i)=AT_content(p_motifs_removed{i});
end

for i=1:length(p_motifs_filter)
 AT_p_filter(i)=AT_content(p_motifs_filter{i});
end
%[i1,j1]=ismember(motifs0,motifs1);
%[i,j]=ismember(motifs0,motifs);
%count10=count1(j1);
%count00=count(j);
%rand_count00=rand_count(j,:);
%rand_count10=rand_count1(j1,:);
%regular PLS with filtering motifs,6 and 7mers
%shuffle the Sequence and estimate a empirical distribution of count; estimate a zscore for each motif 

