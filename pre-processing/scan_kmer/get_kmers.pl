#!/usr/bin/perl
# scans promoter sequences for all k-mers for k=1,...,$ARGV[1]
# usage: perl get_kmers.pl filename kmin kmax seqfile 
# where filename is the stem of the files which will contain the scanning results, and kmax is the maximum k.

$filename = $ARGV[0];
$N0=$ARGV[1];
$N=$ARGV[2];
$seqfile=$ARGV[3];
print STDOUT "scanning k-mers...\n";
open(IN,"<$seqfile") || die "could not open utr-file";
open(MAT,">$filename.matrix");

$orflabel=0;
$analyze=0;
$seq = "";
$motiflabel = 1;
@motifs=();

while(<IN>){
	chomp;
	if (m/^>/){
#		m/^>(.*?)\ /;
#		m/^>(.*)range=.*$/;
#modify pattern identifier for seqfile
                m/^>(.*)chromosome.*$/;
		$orflabel++;
		$orfname[$orflabel]=$1;
		if ($orflabel>1){
			$analyze=1; }
	} else {
		$seq = $seq.$_;
	}
	if ($analyze==1){
# scan current sequence
		&getmers($orflabel-1);
	}
}
# scan current sequence
&getmers($orflabel);

for ($i=0;$i<@motifs;$i++){
  print MAT "$motifs[$i]\n";
}

close(MAT);
close(IN);
open(MOT,">$filename.motifs");
foreach $motif (sort keys %mers){
	print MOT "$mers{$motif}\t$motif\n";
}
close(MOT);
open(ORF,">$filename.orfs");
for ($i=1;$i<@orfname;$i++){
	print ORF "$i\t$orfname[$i]\n";
}
close(ORF);
		
			
sub getmers {
	my $idx = $_[0];
	print STDOUT "scanning gene #$idx\r";
	$bp = length($seq);
        $N1=$N0;
 	$N2 = $N;
	for ($i=0;$i<$bp-$N1+1;$i++){
		$j=$i+1;
		if ($bp-$i+1<$N){ $N2 = $bp-$i+1;}
		for ($ii=$N1;$ii<=$N2;$ii++){
			$motif = substr($seq,$i,$ii);
			if(!$mers{$motif}){
				$mers{$motif} = $motiflabel;
				$motiflabel++;
			}
#push $motif into $motifs if $motif or the reverse complement is not in @motifs                     
#                      $yes=1;
#                       foreach $m (@motifs){       
#                         if($m eq $motif || $m eq &revcom($motif))
#                          {$yes=0;}
#                       } 	
#                       if($yes==1){   
#                            push @motifs,$motif;}
               print MAT "$idx\t$mers{$motif}\t$j\n"; 
           }
	}
	$analyze=0;
	$seq="";
}

			
