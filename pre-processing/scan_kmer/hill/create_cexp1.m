load hill_processed;
targets_names1=comnames;

gene_qt=0.75;

N_rand=3;

load seqs.mat a;
input_file='input/germline_motifs.mat';
kmer_file=['_gene=qt',num2str(100*gene_qt)];

load(input_file, 'motifs', 'mot', 'targets_names');

%get mot matrix
[gene_idx,ign]=ismember(targets_names1,targets_names);
targets_names=targets_names1(find(gene_idx));
cexp_real=cexp_real(find(gene_idx),:);
mot=mot(ign(find(gene_idx)),:);
a=a(ign(find(gene_idx)),:);

%filter genes
cexp_var=var(cexp_real');
var_cutoff=quantile(cexp_var,gene_qt);

gene_idx=find(cexp_var>var_cutoff);
cexp_real=cexp_real(gene_idx,:);
mot=mot(gene_idx,:);
a=a(gene_idx,:);
targets_names=targets_names(gene_idx);

%output sequence to txt file, to be read by scan_kmer.m
!rm seq1.fasta
for i=1:size(a,1)
     s=a(i,:);
     s(find(s==5))=[];
     s(find(s==1))='A';
     s(find(s==2))='C';
     s(find(s==3))='G';
     s(find(s==4))='T';
     dlmwrite('seq1.fasta',char(s),'delimiter','','-append');
end


%scan kmers in sequences and shuffled sequences; save motifs, count, rand_counts in kmer_file 

cd ..;
scan_kmers_hill('hill/output',kmer_file,6,7,'hill/seq1.fasta',N_rand);
cd hill;

%filter kmers
motifs_germline=motifs;
load(['output/kmer',kmer_file,'.mat'], 'motifs', 'count', 'rand_counts');
[k,j]=ismember(motifs_germline,motifs);
j=j(find(k));
k=find(k);


all_motifs=motifs_germline(k);
all_mot=mot(:,k);
count=count(j);
rand_counts=rand_counts(j);

zscore=(count-mean(rand_counts,2))./sqrt(mean(rand_counts,2));


%filter motifs
for mot_qt=0.25:0.25:0.75

  mot_idx=find(zscore>quantile(zscore,mot_qt));
  mot=all_mot(:,mot_idx);
  motifs=all_motifs(mot_idx);

  output_file=['output/cexp_mot=qt',num2str(100*mot_qt),'gene=qt',num2str(100*gene_qt),'.mat'];
  save(output_file,'motifs', 'mot', 'cexp_real', 'targets_names');
end
