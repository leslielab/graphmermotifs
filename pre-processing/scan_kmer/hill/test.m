load hill_processed;
targets_names1=comnames;

gene_qt=0.01;

N_rand=3;

load seqs.mat a;
input_file='input/germline_motifs.mat';
kmer_file='all';

load(input_file, 'motifs', 'mot', 'targets_names');

%get mot matrix
[gene_idx,ign]=ismember(targets_names1,targets_names);
targets_names=targets_names1(find(gene_idx));
cexp_real=cexp_real(find(gene_idx),:);
mot=mot(ign(find(gene_idx)),:);
a=a(ign(find(gene_idx)),:);

%filter genes
cexp_var=var(cexp_real');
var_cutoff=quantile(cexp_var,gene_qt);

gene_idx=find(cexp_var>var_cutoff);
cexp_real=cexp_real(gene_idx,:);
mot=mot(gene_idx,:);
a=a(gene_idx,:);
targets_names=targets_names(gene_idx);

%output sequence to txt file, to be read by scan_kmer.m
!rm seq1.fasta
for i=1:size(a,1)
     s=a(i,:);
     s(find(s==5))=[];
     s(find(s==1))='A';
     s(find(s==2))='C';
     s(find(s==3))='G';
     s(find(s==4))='T';
     dlmwrite('seq1.fasta',char(s),'delimiter','','-append');
end


%scan kmers in sequences and shuffled sequences; save motifs, count, rand_counts in kmer_file 

cd ..;
scan_kmers_hill('hill/output',kmer_file,6,7,'hill/seq1.fasta',N_rand);
cd hill;

%filter kmers
motifs_germline=motifs;
load(['output/kmer',kmer_file,'.mat'], 'motifs', 'count', 'rand_counts');

