function init_sequence(path,gnames,seqfile);
% function init_motif_disc(path,gnames);
%
% runs init_sequence.pl, gets the promoter sequences for the wanted target genes and saves them in a .mat file for later fast loading
% INPUT:
%	path = path to the directory of the run
%	gnames = list of target gene names
%
% (c) Manuel Middendorf, 2005 


perlcmd = '!perl ';

a = char(gnames); 
a(:,end+1) = ' ';
a = a';
a = a(:)';
%path(end)=[];

gnamesfile = [path,'/gnames.txt'];
fid = fopen(gnamesfile,'w');
fprintf(fid,'%s\n',a);
fclose(fid);

eval([perlcmd,' init_sequence.pl ',gnamesfile,' ', path, ' ', seqfile]);

%a = strvcat(textread([path,'/seq.txt'],'%s\n','bufsize',25000));
astr1 = textread([path,'/seq.txt'],'%s\n','bufsize',25000);
%bound the length of sequence by 1000.
for i=1:length(astr1)
   str1=astr1{i};
   len=min(length(str1),1000); 
   astr{i}=str1(end-len+1:end); 
end

a=strvcat(astr);
a = (a=='A') + 2*(a=='C') + 3*(a=='G') + 4*(a=='T')+5*(a==' ');

save([path,'/seqs.mat'],'a');



