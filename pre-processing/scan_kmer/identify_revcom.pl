#!/usr/bin/perl
# takes the output of get_kmers.pl and identifies motifs with their reverse-complements
# usage: perl identify_revcom.pl filename
# where filename has to be the same as used in get_kmers.pl
#
# (c) Manuel Middendorf, 2005 

$filename = $ARGV[0];
open(IN,"<$filename.motifs") || die "could not open input";
open(OUT,">$filename.motifs.identify");

while(<IN>){
	chomp;
	@fields = split(/\s/,$_);
	$label = $fields[0];
	$motif = $fields[1];
	$rev = &revcom($motif);
	if (!$allmotifs{$rev}){
		$allmotifs{$motif}=$label;
	} else {
		print OUT "$label\t$allmotifs{$rev}\n";
	}
}	
	

sub revcom {
	my $text = $_[0];
#	$text =~ tr/ATCG/TAGC/;
	$text =~ tr/AUCG/UAGC/;
	$text = scalar reverse $text;
}
