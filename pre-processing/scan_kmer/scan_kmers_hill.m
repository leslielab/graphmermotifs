function  scan_kmers_hill(path,kmer_file,Nmin,Nmax,seqfile,N_rand)
%
% calls perl scripts to scan promoter sequences for k-mers and shuffled promoter
% sequences for k-mers
% INPUT:
%	path = path to run-directory (string)
%	Nmin = minimum k for the k-mers to scan
%	Nmax = maximum k for the k-mers to scan
%       seqfile = seqs to be scanned
%       N_rand = number of times of shuffling the seqs
% OUTPUT:
%	mot: kmers scanned from promoter sequences
%       count: kmer counts in promoter sequences
%       rand_counts: kmers counts in shuffled seqs


file = [path,'/kmer',kmer_file];
rand_file=[path,'/kmer_rand',kmer_file];
%real sequence
eval(['!perl scan_kmers.pl ',file,' ',num2str(Nmin),' ',num2str(Nmax),' ', seqfile,' ',num2str(0)]);

%all motifs
mot = textread([file,'.mot'],'%s');
%counts of motifs in all sequences
count = load([file,'.count']);

%

%shuffled sequence
for i=1:N_rand

i

eval(['!perl scan_kmers.pl ',rand_file,' ',num2str(Nmin),' ',num2str(Nmax),' ', seqfile,' ',num2str(1)]);

rand_mot = textread([rand_file,'.mot'],'%s');
rand_count = load([rand_file,'.count']);

[k,j]=ismember(mot,rand_mot);
j=j(find(k));
k=find(k);
rand_counts(k,i)=rand_count(j);

end


%some mot never appears in rand_count
x=sum(rand_counts,2);
idx=find(x==0);
mot(idx)=[];
count(idx)=[];
rand_counts(idx,:)=[];

motifs=mot;
save([file,'.mat'],'motifs','count','rand_counts');
%save([file,'.mat'],'motifs','count','rand_mot','rand_counts','rand_count');

