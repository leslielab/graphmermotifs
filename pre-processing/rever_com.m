function motif_rc=rever_com(motif)
motif_rc=motif;

idx_A=find(motif=='A');
idx_C=find(motif=='C');
idx_G=find(motif=='G');
idx_T=find(motif=='T');

motif_rc(idx_A)='T';
motif_rc(idx_C)='G';
motif_rc(idx_G)='C';
motif_rc(idx_T)='A';
motif_rc=motif_rc(end:-1:1);
